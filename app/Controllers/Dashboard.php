<?php

namespace App\Controllers;

use \App\Models\M_View;

class Dashboard extends BaseController
{
	public function __construct() {
		$this->M_View = new M_View();
		helper('trial');
		date_default_timezone_set("Asia/Jakarta");
	}

	public function index()
	{
		if(!session()->get('login')){
			return redirect()->to('/auth');
		}

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

		// dd($session->get('emailGoogle'));

		if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

		loggedIn();

		// used, borrowed, and available code
		$type = $this->M_View->getAllAssetType();

		$bulan = [];
		$jumlah = [];

		$data = [
			'menu' => $menu,
			'post' => $this->M_View->getPreviewPost(),
			'overview' => $this->M_View->getAllOverview(),
			'validation' => \Config\Services::validation(),
			'type' => $type,
			'status' => $this->M_View->getAllStatus(),
			'lastUser' => $lastUser,
            'position' => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender' => $this->M_View->getAllGender(),
            'use' => $this->M_View->getAllAccessUse(),
            'deskera' => $this->M_View->getAllAccessDeskera(),
            'emailUse' => $this->M_View->getAllEmailUse(),
			'operatingSystem' => $this->M_View->getAllOperatingSystem(),
			'it' => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept' => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level' => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
			'support' => $this->M_View->getAllUser(),
			'uniqueSupport' => $this->M_View->getAllSupportUnique(),
			"lastDaily" => $this->M_View->getLastCountDailyReport(),
			"test" => $this->M_View->getReportPerMonth(),
			'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
		];

		return view('dashboard', $data);
	}

	public function insertData() {

		$session = session();

		// ambil data dari web
		$id = esc($this->request->getVar('asset_id'));
		$type = esc($this->request->getVar('asset_name'));
		$available = esc($this->request->getVar('available'));
		$borrowed = esc($this->request->getVar('borrowed'));
		$info = esc($this->request->getVar('information'));
		// masukan ke database
		$this->M_View->insertDataOverview($id, $type, $available, $borrowed, $info);
		// pesan suksses
		$session->setFlashdata('success_message', 'Success insert new overview data! ');
		return redirect()->to('/dashboard');
	}

	public function deleteData() {
		$deleteId = $this->request->getVar('deleteId');
		$session = session();

		// jika ada delete
		if(isset($deleteId)) {
			$this->M_View->deleteDataOverview($deleteId);
			$session->setFlashdata('success_message', 'Success delete overview data! ');
			return redirect()->to('/dashboard');
		} else {
			$session->setFlashdata('error_message', 'Failed delete overview data! ');
			return redirect()->to('/dashboard');
		}
	}

	public function editData($id) {
		$dataLama = $this->M_View->getDataOverviewById($id);

		$data = [
			'dataLama' => $dataLama,
		];

		return redirect()->to('/Dashboard', $data);
	}

	public function supportSite() {
		$session = session();
		$support = esc($this->request->getVar('support'));
		$user_id = $session->get('user_id');
		$support_date = date('F j, Y, G:i:s');
		
		$this->M_View->supportSite($user_id, $support, $support_date);
		$session->setFlashdata('success_login', 'Thanks for your support 😊');
		return redirect()->to('/Dashboard');
	}

}
