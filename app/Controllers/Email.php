<?php

namespace App\Controllers;

use \App\Models\M_View;

class Email extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
        helper('trial');
    }

    public function index() {

        loggedIn();

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu'      => $menu,
            'email'     => $this->M_View->getAllEmail(),
            'validation' => \Config\Services::validation(),
			'type'      => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'       => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'        => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'      => $this->M_View->getAllDept(),
            'deptName'  => $this->M_View->getDeptById($session->get('dept')),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level'     => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
            "test"      => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('email', $data);
    }

    public function insertData() {

        $lastUser = $this->M_View->getAllUserDesc();
        $session = session();

        // ambil data dari web
        $user_id    = $lastUser['user_asset_id'];
        $email_pass = esc($this->request->getVar('email_pass'));
        $email_use  = $this->request->getVar('email_use');
        $email_user = esc($this->request->getVar('email_user'));

        // validate
        if(!$this->validate([
            'email_pass'    => 'required',
            'email_use'     => 'required',
            'email_user'    => 'required|is_unique[email.email_user]',
        ], [
            'email_pass'    => [
                'required'  => 'Harus di isi...'
            ],
            'email_use'     => [
                'required'  => 'Harus di isi...',
            ],
            'email_user'    => [
                'required'  => "Harus di isi...",
                'is_unique' => 'Sudah ada cuy...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert new email !');
            return redirect()->to('/email')->withInput()->with('validation', $validation);
        }

        // masukan ke database
        if($this->M_View->getEditEmailByUserId($user_id) != null) {
            // pesan error
            $session->setFlashdata('error_message', 'Email already exists');
            return redirect()->to('/email');
        } else {
            $this->M_View->insertDataEmail($user_id, $email_pass, $email_use, $email_user);

            // pesan sukses
            $session->setFlashdata('success_message', 'Success insert new email! ');
            return redirect()->to('/email');
        }

    }

    public function deleteData() {
        $deleteId = $this->request->getVar('deleteId');
		$session = session();

		// jika ada delete
		if(isset($deleteId)) {
			$this->M_View->deleteDataEmail($deleteId);
			$session->setFlashdata('success_message', 'Success delete asset data! ');
			return redirect()->to('/email');
		} else {
			$session->setFlashdata('error_message', 'Failed delete asset data! ');
			return redirect()->to('/email');
		}
    }
    public function editData($id) {
        $session = session();

        $getOldData = $this->M_View->getEditEmail($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
			'status' => $this->M_View->getAllStatus(),
            'using' => $this->M_View->getAllEmailUse(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_email', $data);
    }

    public function doEditData($id) {
        $session = session();


        // ambil data
        $emailUser  = esc($this->request->getVar('email_user'));
        $emailPass  = esc($this->request->getVar('email_pass'));
        $using      = $this->request->getVar('email_use');
        $email_id   = $id;
        $getOldData = $this->M_View->getEditEmail($id);
		
				
		//dd($emailUser, $emailPass, $using);

        // validasi
        if($getOldData['email_user'] == $emailUser) {
            $rule = 'required';
        } else {
            $rule = 'required|is_unique[email.email_user]';
        }
				
        if(!$this->validate([
            'email_user'    => $rule,
            'email_pass'    => 'required',
            'email_use'     => 'required',
        ], [
            'email_user'    => [
                'is_unique' => 'Sudah ada boss !',
                'required'  => 'Harus di isi...'
            ],
            'email_pass'    => [
                'required'  => 'Harus di isi...'
            ],
            'email_use'     => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data ! ');
            return redirect()->to('/Email/editData' . '/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editDataEmail($email_id, $emailPass, $using, $emailUser);

        // pesan sukses
        $session->setFlashdata('success_message', 'Successfully update existing email data ! ');
        return redirect()->to('/Email');  
    }

    public function importData() {
        $session = session();
        $file = $this->request->getFile('importData');
        $filename = $file->getName();
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('/Email')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }
				
                // validasi duplicate
                $check = $this->M_View->checkEmail($kolom['0']);
                $checkUserID = $this->M_View->checkUserId($kolom['0']);

                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null) {
                    continue;
                }

                if($check != null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } elseif($kolom['0'] == null || $kolom['1'] == null || $kolom['2'] == null || $kolom['3'] == null) {
                    $log['failed'] += 1;
                    continue;
                } elseif($checkUserID == null) {
                    $log['failed'] += 1;
                    continue;
                } else {

                    $userAssetId = $kolom['0'];
                    $username    = $kolom['1'];
                    $password 	 = $kolom['2'];
                    $use         = $kolom['3'];
                            
                    // masukan ke database
                    $this->M_View->importEmail($userAssetId, $username, $password, $use);
                    $log['success'] += 1;
                }
            }  

            $session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('/Email');
        }
    }

    public function templateImportData() {
        // dd('masuk download template preventive');
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

		$dataUse = $this->M_View->getAllEmailUse();

        $sheet->setCellValue('A1', 'User Asset ID*');
        $sheet->setCellValue('B1', 'Username*');
        $sheet->setCellValue('C1', 'Password*');
        $sheet->setCellValue('D1', 'Use*');

        $sheet->setCellValue('A2', '2175');
        $sheet->setCellValue('B2', 'nuryadi');
        $sheet->setCellValue('C2', 'password');
        $sheet->setCellValue('D2', '2');

		// informasi untuk template 
        $sheet->setCellValue('F2', "# Information");
        $sheet->setCellValue('F3', "# Email Use");

        $rowUse = 4;

        foreach($dataUse as $du) {
            $sheet->setCellValue('F'.$rowUse, $du['id'] . ". " . $du['name']);
            $rowUse++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import email';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

	public function exportData() {
		$session = session();
        $date = date('j M Y');

		$dataTemplate = $this->M_View->getAllEmail();
		$fileName = 'All data email - ' . $date;

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();


        $sheet->setCellValue('A1', 'Asset ID');
        $sheet->setCellValue('B1', 'Fullname');
        $sheet->setCellValue('C1', 'Username');
        $sheet->setCellValue('D1', 'Email');
        $sheet->setCellValue('E1', 'Password');
        $sheet->setCellValue('F1', 'Use');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['user_asset_id']);
            $sheet->setCellValue('B' . $rows, $dt['user_fullname']);
            $sheet->setCellValue('C' . $rows, $dt['email_user']);
            $sheet->setCellValue('D' . $rows, $dt['user_email']);
            $sheet->setCellValue('E' . $rows, $dt['email_pass']);
            $sheet->setCellValue('F' . $rows, $dt['email_use']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
	}
}
