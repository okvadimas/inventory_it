<?php

namespace App\Controllers;

use \App\Models\M_View;

class Asset extends BaseController
{ 
	public function __construct() {
		$this->M_View = new M_View();
		helper('trial');
	}

	public function index()
	{
        $session = session();

		loggedIn();

		// dd($session->get('login'));

		$lastUser = $this->M_View->getAllUserDesc();

		if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
		
		$data = [
			'menu' => $menu,
			'asset' => $this->M_View->getAllAsset(),
			'validation' => \Config\Services::validation(),
			'type' => $this->M_View->getAllAssetType(),
			'status' => $this->M_View->getAllStatus(),
			'lastUser' => $lastUser,
			'position' => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender' => $this->M_View->getAllGender(),
			'use' => $this->M_View->getAllAccessUse(),
            'deskera' => $this->M_View->getAllAccessDeskera(),
			'emailUse' => $this->M_View->getAllEmailUse(),
			'operatingSystem' => $this->M_View->getAllOperatingSystem(),
			'it' => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept' => $this->M_View->getAllDept(),
			'deptName' => $this->M_View->getDeptById($session->get('dept')),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level' => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
			"test" => $this->M_View->getReportPerMonth(),
			'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
		];

		return view('asset', $data);
	}

	public function insertData() {

		$session = session();

		$lastUser = $this->M_View->getAllUserDesc();

		// ambil dari web
		$asset_type 	= $this->request->getVar('asset_type');
		$information 	= esc($this->request->getVar('information'));
		$processor 		= esc($this->request->getVar('processor'));
		$motherboard 	= esc($this->request->getVar('motherboard'));
		$storage 		= esc($this->request->getVar('storage'));
		$memory 		= esc($this->request->getVar('memory'));
		$graphics 		= esc($this->request->getVar('graphics'));
		$monitor 		= esc($this->request->getVar('monitor'));
		$jumlah 		= esc($this->request->getVar('jumlah'));
		$user_id 		= $lastUser["user_asset_id"];
		$operatingSystem = $this->request->getVar('operatingSystem');
		$serialNumber 	= $this->request->getVar('serialNumber');

		// validasi
		if(!$this->validate([
			'asset_type' 	=> 'required',
			'information'	=> 'required',
			'processor' 	=> 'required',
			'motherboard'	=> 'required',
			'storage' 		=> 'required',
			'memory' 		=> 'required',
			'graphics' 		=> 'required',
			'monitor' 		=> 'required',
			'jumlah' 		=> 'required',
			'operatingSystem' => 'required',
			'serialNumber' 	=> 'required'
		], [
			'asset_type' 	=> [
				'required' 	=> 'Harus di isi... !'
			],
			'information' 	=> [
				'required' 	=> 'Harus di isi... !'
			],
			'processor' 	=> [
				'required' 	=> 'Harus di isi... !'
			],
			'motherboard' 	=> [
				'required' 	=> 'Harus di isi... !'
			],
			'storage' 		=> [
				'required' 	=> 'Harus di isi... !'
			],
			'memory' 		=> [
				'required' 	=> 'Harus di isi... !'
			],
			'graphics' 		=> [
				'required' 	=> 'Harus di isi... !'
			],
			'monitor' 		=> [
				'required' 	=> 'Harus di isi... !'
			],
			'jumlah' 		=> [
				'required' 	=> 'Harus di isi... !'
			],
			'operatingSystem' => [
				'required' 	=> 'Harus di isi... !'
			],
			'serialNumber' 	=> [
				'required' 	=> 'Harus di isi... !'
			]
		])) {
			$validation = \Config\Services::validation();
			$session->setFlashdata('error_message', '<strong>Failed insert new data ! </strong>');
			return redirect()->to('/asset')->withInput()->with('validation', $validation);
		}		

		// masukan ke database
		if($this->M_View->getEditAssetByUserId($user_id) != null) {
			// pesan sukses 

			$session->setFlashdata('error_message', '<strong>Already exists ! </strong>');
			return redirect()->to('/asset');
		} else {
			$this->M_View->insertDataAsset($asset_type, $information, $processor, $motherboard, $storage, $memory, $graphics, $monitor, $jumlah, $user_id, $operatingSystem, $serialNumber);

			// pesan sukses 
			$session->setFlashdata('success_message', '<strong>Success insert new data ! </strong>');
			return redirect()->to('/asset');
		}

	}

	public function deleteData() {

		$deleteId = $this->request->getVar('deleteId');
		$session = session();

		// jika ada delete
		if(isset($deleteId)) {
			$this->M_View->deleteDataAsset($deleteId);
			$session->setFlashdata('success_message', '<strong>Success delete asset data ! </strong>');
			return redirect()->to('/asset');
		} else {
			$session->setFlashdata('error_message', '<strong>Failed delete asset data ! </strong>');
			return redirect()->to('/asset');
		}

	}

	public function editData($id) {
		$session = session();

        $getOldData = $this->M_View->getEditAsset($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' 	=> $menu,
            'data' 	=> $getOldData,
            'validation' => \Config\Services::validation(),
			'type' 	=> $this->M_View->getAllAssetType(),
			'status' => $this->M_View->getAllStatus(),
			'operatingSystem' => $this->M_View->getAllOperatingSystem(),
			"test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_asset', $data);
	}

	public function doEditData($id) {
		$session = session();

		$id 		= $id;
		$asset_type = $this->request->getVar('asset_type');
		$processor 	= esc($this->request->getVar('processor'));
		$storage 	= esc($this->request->getVar('storage'));
		$graphics 	= esc($this->request->getVar('graphics'));
		$motherboard = esc($this->request->getVar('motherboard'));
		$memory 	= esc($this->request->getVar('memory'));
		$monitor 	= esc($this->request->getVar('monitor'));
		$jumlah 	= esc($this->request->getVar('jumlah'));
		$information = esc($this->request->getVar('information'));
		$operating_system = $this->request->getVar('operatingSystem');
		$serial_number = $this->request->getVar('serialNumber');
		$getOldData = $this->M_View->getEditAsset($id);

		// validasi
		if(!$this->validate([
			'asset_type' 	=> 'required',
			'information' 	=> 'required',
			'processor' 	=> 'required',
			'motherboard' 	=> 'required',
			'storage' 		=> 'required',
			'memory' 		=> 'required',
			'graphics' 		=> 'required',
			'monitor' 		=> 'required',
			'jumlah' 		=> 'required',
			'operatingSystem' => 'required',
			'serialNumber' 	=> 'required'
		], [
			'asset_type' 	=> [
				'required' 	=> 'Harus di isi... !'
			],
			'information' 	=> [
				'required' 	=> 'Harus di isi... !'
			],
			'processor' 	=> [
				'required' 	=> 'Harus di isi... !'
			],
			'motherboard' 	=> [
				'required' 	=> 'Harus di isi... !'
			],
			'storage' 		=> [
				'required' 	=> 'Harus di isi... !'
			],
			'memory' 		=> [
				'required' 	=> 'Harus di isi... !'
			],
			'graphics' 		=> [
				'required' 	=> 'Harus di isi... !'
			],
			'monitor' 		=> [
				'required' 	=> 'Harus di isi... !'
			],
			'jumlah' 		=> [
				'required' 	=> 'Harus di isi... !'
			],
			'operatingSystem' => [
				'required' 	=> 'Harus di isi... !'
			],
			'serialNumber' 	=> [
				'required' 	=> 'Harus di isi... !'
			]
		])) {
			$validation = \Config\Services::validation();
			$this->setFlashdata('error_message', '<strong>Failed insert new data ! </strong>');
			return redirect()->to('/Asset/editData/' . $id)->withInput()->with('validation', $validation);
		}

		// masukan ke database
		$this->M_View->editAssetData($id, $asset_type, $processor, $storage, $graphics, $motherboard, $memory, $monitor, $jumlah, $information, $operating_system, $serial_number);

		// pesan sukses
		$session->setFlashdata('success_message', "<strong>Successfully update existing data ! </strong>");
		return redirect()->to('/Asset');

	}

	public function importData() {
        $session = session();
        $file = $this->request->getFile('importData');
        $filename = $file->getName();
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('/Asset')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }
				
                // validasi duplicate
                $check = $this->M_View->checkAsset($kolom['0'], $kolom['1']);
                $checkUserID = $this->M_View->checkUserId($kolom['0']);

                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null && $kolom['4'] == null && $kolom['5'] == null && $kolom['6'] == null && $kolom['7'] == null && $kolom['8'] == null && $kolom['9'] == null && $kolom['10'] == null && $kolom['11'] == null) {
                    continue;
                }

                if($check != null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } elseif($kolom['0'] == null || $kolom['1'] == null || $kolom['2'] == null || $kolom['3'] == null || $kolom['4'] == null || $kolom['5'] == null || $kolom['6'] == null || $kolom['7'] == null || $kolom['8'] == null || $kolom['9'] == null || $kolom['10'] == null || $kolom['11'] == null) {
                    $log['failed'] += 1;
                    continue;
                } elseif($checkUserID == null) {
                    $log['failed'] += 1;
                    continue;
                } else {

                    $userAssetId = $kolom['0'];
                    $assetType  = $kolom['1'];
                    $processor 	= $kolom['2'];
                    $storage  	= $kolom['3'];
                    $graphics   = $kolom['4'];
                    $motherboard = $kolom['5'];
                    $memory    	= $kolom['6'];
                    $monitor    = $kolom['7'];
                    $serialNumber = $kolom['8'];
                    $operatingSystem = $kolom['9'];
                    $jumlah    	= $kolom['10'];
					$information = $kolom['11'];
                            
                    // masukan ke database
                    $this->M_View->importAsset($userAssetId, $assetType, $processor, $storage, $graphics, $motherboard, $memory, $monitor, $serialNumber, $operatingSystem, $jumlah, $information);
                    $log['success'] += 1;
                }
            }  

            $session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('/Asset');
        }
    }

    public function templateImportData() {
        // dd('masuk download template preventive');
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

		$dataOperatingSystem = $this->M_View->getAllOperatingSystem();
		$dataAssetType = $this->M_View->getAllAssetType();

        $sheet->setCellValue('A1', 'User Asset ID*');
        $sheet->setCellValue('B1', 'Asset Type*');
        $sheet->setCellValue('C1', 'Processor*');
        $sheet->setCellValue('D1', 'Storage*');
        $sheet->setCellValue('E1', 'Grapchis*');
        $sheet->setCellValue('F1', 'Motherboards');
        $sheet->setCellValue('G1', 'Memory');
        $sheet->setCellValue('H1', 'Monitor');
        $sheet->setCellValue('I1', 'Serial Number');
        $sheet->setCellValue('J1', 'Operating System');
        $sheet->setCellValue('K1', 'Jumlah');
        $sheet->setCellValue('L1', 'Information');

        $sheet->setCellValue('A2', '2175');
        $sheet->setCellValue('B2', '3');
        $sheet->setCellValue('C2', 'Intel Core i5 - 9400');
        $sheet->setCellValue('D2', '500 GB SSD & 1 TB HDD');
        $sheet->setCellValue('E2', 'Asus GTX 1650 4 GB');
        $sheet->setCellValue('F2', 'Asus H510M');
		$sheet->setCellValue('G2', 'Corsair 8 GB DDR4');
        $sheet->setCellValue('H2', 'Samsung 24 Inc FHD');
        $sheet->setCellValue('I2', 'MBT6M-YJHY2-RCPFK-T3RPR-F24YW');
        $sheet->setCellValue('J2', '3');
        $sheet->setCellValue('K2', '1');
        $sheet->setCellValue('L2', 'information');

		// informasi untuk template 
        $sheet->setCellValue('N2', "# Information");
        $sheet->setCellValue('N3', "# User ID");
        $sheet->setCellValue('N4', "1. Input/Register the User First");
        $sheet->setCellValue('N5', "2. Download All/Specific Data User");
        $sheet->setCellValue('N6', "3. Find & Use the User ID from that Data for Input Data 'User ID' in Import Asset");

        $sheet->setCellValue('N8', "# Operating System");
        $rowOs = 9;

        foreach($dataOperatingSystem as $dos) {
            $sheet->setCellValue('N'.$rowOs, $dos['id'] . ". " . $dos['name']);
            $rowOs++;
        }

        $sheet->setCellValue('P3', "# Asset Type");
        $rowAt = 4;

        foreach($dataAssetType as $dat) {
            $sheet->setCellValue('P'.$rowAt, $dat['id'] . ". " . $dat['type']);
            $rowAt++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import asset';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

	public function exportData() {
		$session = session();
        $date = date('j M Y');

		$dataTemplate = $this->M_View->getAllAsset();
		$fileName = 'All data asset - ' . $date;

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();


        $sheet->setCellValue('A1', 'Asset ID');
        $sheet->setCellValue('B1', 'Username');
        $sheet->setCellValue('C1', 'Department');
        $sheet->setCellValue('D1', 'Type');
        $sheet->setCellValue('E1', 'Processor');
        $sheet->setCellValue('F1', 'Motherboard');
        $sheet->setCellValue('G1', 'Storage');
        $sheet->setCellValue('H1', 'Graphics');
        $sheet->setCellValue('I1', 'Memory');
        $sheet->setCellValue('J1', 'Monitor');
        $sheet->setCellValue('K1', 'Operating System');
        $sheet->setCellValue('L1', 'Serial Number');
        $sheet->setCellValue('M1', 'Information');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['user_asset_id']);
            $sheet->setCellValue('B' . $rows, $dt['user_fullname']);
            $sheet->setCellValue('C' . $rows, $dt['dept']);
            $sheet->setCellValue('D' . $rows, $dt['type']);
            $sheet->setCellValue('E' . $rows, $dt['asset_cpu']);
            $sheet->setCellValue('F' . $rows, $dt['asset_mb']);
            $sheet->setCellValue('G' . $rows, $dt['asset_hdd']);
            $sheet->setCellValue('H' . $rows, $dt['asset_vga']);
            $sheet->setCellValue('I' . $rows, $dt['asset_ram']);
            $sheet->setCellValue('J' . $rows, $dt['asset_monitor']);
            $sheet->setCellValue('K' . $rows, $dt['os']);
            $sheet->setCellValue('L' . $rows, $dt['asset_serial']);
            $sheet->setCellValue('M' . $rows, $dt['asset_info']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
	}
}
