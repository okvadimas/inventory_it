<?php

namespace App\Controllers;

use \App\Models\M_View;

class MonitoringSpeed extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {
        
        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu'  => $menu,
            'monitoring'    => $this->M_View->getAllMonitoring(),
            'backup'    => $this->M_View->getAllBackup(),
            'validation'    => \Config\Services::validation(),
			'type'  => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'   => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'    => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'  => $this->M_View->getAllDept(),
            'accessUse'     => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level' => $this->M_View->getAllUserLevel(),
            'userAccess'    => $this->M_View->getAllUserAccess(),
            "test"  => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('monitoringSpeed', $data);
    }

    public function insertData() {

        $session = session();

        // ambil data dari web
        $monitoringYear     = esc($this->request->getVar('monitoring_year'));
        $monitoringMonth    = esc($this->request->getVar('monitoring_month'));
        $monitoringAverage  = esc($this->request->getVar('monitoring_average'));
        $monitoringDowntime = esc($this->request->getVar('monitoring_downtime'));
        $monitoringComment  = esc($this->request->getVar('monitoring_comment'));

        // validate
        if(!$this->validate([
            'monitoring_year'   => 'required',
            'monitoring_month'  => 'required',
            'monitoring_average'    => 'required',
            'monitoring_downtime'   => 'required',
        ], [
            'monitoring_year' => [
                'required' => 'Harus di isi...'
            ],
            'monitoring_month' => [
                'required' => "Harus di isi..."
            ],
            'monitoring_average' => [
                'required' => 'Harus di isi...'
            ],
            'monitoring_downtime' => [
                'required' => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert new data !');
            return redirect()->to('/monitoringSpeed')->withInput()->with('validation', $validation);
        }

        // getOldData kalau sudah ada bulan dan tahun yang sama validate error
        $check = $this->M_View->checkMonitoring($monitoringYear, $monitoringMonth);

        if($check != null) {
            // pesan error
            $session->setFlashdata('error_message', '<strong>Already exists !</strong>');
            return redirect()->to('/monitoringSpeed');
        } else {
            // masukan ke database
            $this->M_View->insertDataMonitoring($monitoringYear, $monitoringMonth, $monitoringAverage, $monitoringDowntime, $monitoringComment);

            // pesan sukses
            $session->setFlashdata('success_message', '<strong>Success insert new monitoring data !</strong>');
            return redirect()->to('/monitoringSpeed');
        }


    }

    public function deleteData() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataMonitoring($deleteId);
            $session->setFlashdata('success_message', '<strong>Success delete monitoring data ! </strong>');
            return redirect()->to('/monitoringSpeed');
        } else {
            $session->setFlashdata('error_message', '<strong>Failed delete monitoring data ! </strong>');
            return redirect()->to('/monitoringSpeed');
        }
    }

    public function editData($id) {
        $session = session();

        $getOldData = $this->M_View->getEditMonitoring($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            'dept' => $this->M_View->getAllDept(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_monitoringSpeed', $data);
    }

    public function doEditData($id) {
        $session = session();

        // ambil data
        $monitoringYear = esc($this->request->getVar('monitoring_year'));
        $monitoringMonth    = esc($this->request->getVar('monitoring_month'));
        $monitoringAverage  = esc($this->request->getVar('monitoring_average'));
        $monitoringDowntime = esc($this->request->getVar('monitoring_downtime'));
        $monitoringComment  = esc($this->request->getVar('monitoring_comment'));
        $id     = $id;
        $getOldData = $this->M_View->getEditMonitoring($id);

        // validate
        if(!$this->validate([
            'monitoring_year'   => 'required',
            'monitoring_month'  => 'required',
            'monitoring_average'    => 'required',
            'monitoring_downtime'   => 'required',
            'monitoring_comment'    => 'required',
        ], [
            'monitoring_year'   => [
                'required'  => 'Harus di isi...'
            ],
            'monitoring_month'  => [
                'required'  => 'Harus di isi...'
            ],
            'monitoring_average'    => [
                'required'  => 'Harus di isi...'
            ],
            'monitoring_downtime'   => [
                'required'  => 'Harus di isi...'
            ],
            'monitoring_comment'    => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed edit data monitoring !');
            return redirect()->to('/monitoringSpeed/editData/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        // getOldData kalau sudah ada bulan dan tahun yang sama validate error
        // cek dari seluruh tabel 
        $check = $this->M_View->checkEditMonitoring($monitoringYear, $monitoringMonth, $monitoringAverage, $monitoringDowntime, $monitoringComment);

        if($check != null) {

            // ini untuk menghindari jika data mau di ubah tapi kalau ada data lain yg sama biar muncul error tetapi jika data lain gaada yg sama maka its oke jika data input sekarang sama dengan data old
            // jika data tidak berubah tidak apa2 (cek dari dalam atau cek id untuk yg di edit sekarang)
            if(
                $getOldData['monitoring_year'] == $monitoringYear AND
                $getOldData['monitoring_month'] == $monitoringMonth AND
                $getOldData['monitoring_average'] == $monitoringAverage AND
                $getOldData['monitoring_downtime'] == $monitoringDowntime AND
                $getOldData['monitoring_comment'] == $monitoringComment
            ) {
                // masukan ke database
                $this->M_View->editMonitoring($monitoringYear, $monitoringMonth, $monitoringAverage, $monitoringDowntime, $monitoringComment, $id);

                // pesan sukses
                $session->setFlashdata('success_message', '<strong>Success update existing monitoring data !</strong>');
                return redirect()->to('/monitoringSpeed');
            } else {
                // pesan error
                $validation = \Config\Services::validation();
                $session->setFlashdata('error_message', '<strong>Already exists !</strong>');
                return redirect()->to('/monitoringSpeed/editData/' . $id)->withInput()->with('validation', $validation);
            }

        } else {
            // masukan ke database
            $this->M_View->editMonitoring($monitoringYear, $monitoringMonth, $monitoringAverage, $monitoringDowntime, $monitoringComment, $id);

            // pesan sukses
            $session->setFlashdata('success_message', '<strong>Success update existing monitoring data !</strong>');
            return redirect()->to('/monitoringSpeed');
        }
    }

    public function importData() {
        $session = session();
        $file = $this->request->getFile('importData');
        $filename = $file->getName();
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('/MonitoringSpeed')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }
				
                // validasi duplicate
                $check = $this->M_View->checkMonitoring($kolom['0'], $kolom['1']);

                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] === null && $kolom['3'] == null) {
                    continue;
                }

                if($check != null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } elseif($kolom['0'] == null || $kolom['1'] == null || $kolom['2'] === null || $kolom['3'] == null) {
                    $log['failed'] += 1;
                    continue;
                } else {

                    $year       = $kolom['0'];
                    $sequence   = $kolom['1'];
                    $average    = $kolom['2'];
                    $downtime   = $kolom['3'];
                    $comment    = $kolom['4'];
                            
                    // masukan ke database
                    $this->M_View->importMonitoringSpeed($year, $sequence, $average, $downtime, $comment);
                    $log['success'] += 1;
                }
            }  

            $session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('/MonitoringSpeed');
        }
    }

    public function templateImportData() {
        // dd('masuk download template preventive');
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Year*');
        $sheet->setCellValue('B1', 'Sequence*');
        $sheet->setCellValue('C1', 'Average*');
        $sheet->setCellValue('D1', 'Downtime*');
        $sheet->setCellValue('E1', 'Comment');

        $sheet->setCellValue('A2', '2022-03-15');
        $sheet->setCellValue('B2', '2');
        $sheet->setCellValue('C2', '55.5');
        $sheet->setCellValue('D2', '10');
        $sheet->setCellValue('E2', 'comment');

        // informasi untuk template 
        $sheet->setCellValue('G2', "# Information");
        $sheet->setCellValue('G3', "# Downtime in minutes");

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import monitoring speed';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function exportData() {
        $session = session();

        // get data
        $fromYear   = esc($this->request->getVar('exportData3'));

        $date = date('j M Y');

        // rule
        if($fromYear == null) {
            $dataTemplate = $this->M_View->getAllDataMonitoring();
            $fileName = 'All data monitoring - ' . $date;
        } elseif($fromYear != null) {
            $dataTemplate = $this->M_View->getAllDataMonitoringByYear($fromYear);
            $fileName = 'All data monitoring by year ' . $fromYear . ' - ' . $date;
        } else {
            $session->setFlashdata('error_message', '<strong>Failed !!</strong> wrong rule ! Please read the rule and try again...');
            return redirect()->to('monitoring');
        }

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Year');
        $sheet->setCellValue('B1', 'Month');
        $sheet->setCellValue('C1', 'Average');
        $sheet->setCellValue('D1', 'Downtime');
        $sheet->setCellValue('E1', 'Comment');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['monitoring_year']);
            $sheet->setCellValue('B' . $rows, $dt['monitoring_month']);
            $sheet->setCellValue('C' . $rows, $dt['monitoring_average']);
            $sheet->setCellValue('D' . $rows, $dt['monitoring_downtime']);
            $sheet->setCellValue('E' . $rows, $dt['monitoring_comment']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
