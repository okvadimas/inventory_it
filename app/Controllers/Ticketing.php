<?php

namespace App\Controllers;

use \App\Models\M_View;
use \App\Models\ModelsTicket;
use Config\Services;

class Ticketing extends BaseController
{ 
    protected $table = 'ticket';
    // protected $database = 'inventory1.5';
    protected $column_order = ['ticket_id', null, 'ticket_number', null, null, 'ticket_status', null];
    protected $column_search = ['ticket_number', 'created_at', 'ticket_message'];
    protected $order = ['ticket_id' => 'DESC'];

    public function __construct() {
        $this->request = Services::request();
        $this->session = session();
        $this->models = new ModelsTicket($this->request, $this->table, $this->column_order, $this->column_search, $this->order);
        $this->M_View = new M_View();
        helper('trial');
        date_default_timezone_set("Asia/Jakarta");
        $this->session = session();

        // try auto refresh (works) - refresh setiap 60 detik
        $url = $_SERVER['REQUEST_URI'];
        header("Refresh: 60; URL=$url");
    }

    public function index() {

        loggedIn();

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu' => $menu,
            'daily' => $this->M_View->getAllDaily(),
            'validation' => \Config\Services::validation(),
			'type' => $this->M_View->getAllAssetType(),
			'status' => $this->M_View->getAllStatus(),
			'lastUser' => $lastUser,
            'position' => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender' => $this->M_View->getAllGender(),
            'use' => $this->M_View->getAllAccessUse(),
            'deskera' => $this->M_View->getAllAccessDeskera(),
            'emailUse' => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it' => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept' => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level' => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
            'supportSystem' => $this->M_View->getAllSupport(),
            "test" => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('ticketing', $data);
    }

    public function ajaxList()
    {           
        if ($this->request->getMethod(true) === 'POST') { 
            $fromDate = $this->request->getVar('fromDate');
            $toDate = $this->request->getVar('toDate');
            $lists = $this->models->getDatatables($fromDate, $toDate);
            $data = [];
            $no = $this->request->getPost('start');

            foreach ($lists as $list) {
                $no++; 
                $status = $list->ticket_status == 7 ? 'text-success' : ($list->ticket_status == 9 ? 'text-danger' : 'text-warning');
                $row = [];
                $row[] = $no;
                if($list->ticket_type == 1 || $list->ticket_type == 3) {
                    $row[] = "
                    <a href='/Ticketing/finishTicket/$list->ticket_number' title='Done Status' id='doneTikect' data-info='Done' class='far fa-check-square text-decoration-none text-success mx-2 ". ($list->ticket_status == 7 ? 'text-muted' : '') ."" . ($this->session->get('level') != 1 ? ' text-muted' : '') . "' style='" . ($list->ticket_status == 7 ? 'pointer-events: none;' : '') . "" . ($this->session->get('level') != 1 ? ' pointer-events: none;' : '') . "'></a>
                    <a href='/Ticketing/clearTicket/$list->ticket_number' title='Clear Status' id='clearTicket' data-info='Clear' class='fab fa-cuttlefish text-decoration-none text-warning ". ($list->ticket_status != 7 ? 'text-muted' : '') ."" . ($this->session->get('level') != 1 ? ' text-muted' : '') . "' style='" . ($list->ticket_status != 7 ? 'pointer-events: none;' : '') . "" . ($this->session->get('level') != 1 ? ' pointer-events: none;' : '') . "'></a>";
                } else {
                    $row[] = "
                    <a href='/Ticketing/finishTicket/$list->ticket_number' title='Approved Status' id='doneTikect' data-info='Done' class='far fa-check-square text-decoration-none text-success ". ($list->ticket_status == 7 || $list->ticket_status == 9 ? 'text-muted' : '') ."" . ($this->session->get('level') != 1 ? ' text-muted' : '') . "' style='" . ($list->ticket_status == 7 ? 'pointer-events: none;' : '') . "" . ($this->session->get('level') != 1 ? ' pointer-events: none;' : '') . "'></a>
                    <a href='/Ticketing/rejectTicket/$list->ticket_number' title='Reject Status' id='rejectTicket' data-info='Reject' class='fas fa-times text-decoration-none text-danger mx-2 ". ($list->ticket_status == 7 || $list->ticket_status == 9 ? 'text-muted' : '') ."" . ($this->session->get('level') != 1 ? ' text-muted' : '') . "' style='" . ($list->ticket_status == 7 ? 'pointer-events: none;' : '') . "" . ($this->session->get('level') != 1 ? ' pointer-events: none;' : '') . "'></a>
                    <a href='/Ticketing/clearTicket/$list->ticket_number' title='Clear Status' id='clearTicket' data-info='Clear' class='fab fa-cuttlefish text-decoration-none text-warning ". ($list->ticket_status == 7 || $list->ticket_status == 9 ? '' : 'text-muted') ."" . ($this->session->get('level') != 1 ? ' text-muted' : '') . "' style='" . ($list->ticket_status == 7 || $list->ticket_status == 9 ? '' : 'pointer-events: none;') . "" . ($this->session->get('level') != 1 ? ' pointer-events: none;' : '') . "'></a>";
                }
                $row[] = "<h6 class='fw-bold text-dark'>$list->ticket_number</h6><small>$list->created_at</small>";
                $row[] = "<h6 class='fw-bold text-primary'>$list->createdBy</h6><small>$list->dept Dept</small>";
                $row[] = $list->ticket_message . ($list->ticket_type == 2 ? ($list->ticket_level == 1 ? '<br>Urgent - ' . $list->ticket_due : '<br>Standart - ' . $list->ticket_due) : '');
                $row[] = "<h6 class='fw-bold $status'>". ($list->ticket_type == 1 || $list->ticket_type == 3 ? $list->status1 : $list->status2) ."</h6>";
                $row[] = "<h6 class='fw-bold text-info'>". ($list->updated_by != null ? $list->updatedBy : 'Not yet') ."</h6><small>". ($list->updated_at != null ? $list->updated_at : 'Not yet') ."</small>";
                $data[] = $row;
            }

            $output = [
                'draw' => $this->request->getPost('draw'),
                'recordsTotal' => $this->models->countAll(),
                'recordsFiltered' => $this->models->countFiltered($fromDate, $toDate),
                'data' => $data
            ];

            echo json_encode($output);
        }
    }

    public function insertMessage() {
        $session = session();

        // ambil data dari web
        $message    = esc($this->request->getVar('ticketMessage'));
        $userId     = $this->session->get('asset_id');
        $createdAt  = date("Y-m-d H:i:s");
        $status     = 8;
        $opsi1      = $this->request->getVar('opsi1');
        $opsi2      = $this->request->getVar('opsi2');
        $opsi3      = $this->request->getVar('opsi3');

        if($opsi3 == null) {
            $opsi3  = date('Y-m-d', strtotime('now + 1day'));
        }

        // dd($this->models->getLastIdDataTicket());   

        // auto ws number
        if($this->models->getLastIdDataTicket() != null) {
            $lastTicket = $this->models->getLastIdDataTicket();
            $number = explode('-', $lastTicket['ticket_number']);
            if(substr($number[1],0,4) == date('Y')) {                
                $ticketNumber = 'IT-' . intval($number[1] + 1);
            } else {
                $ticketNumber = 'IT-' . date('Y') . '00001';
            }
        } else {
            $ticketNumber = 'IT-' . date('Y') . '00001';
        }

        // validate
        if(!$this->validate([
            'ticketMessage' => 'required',
            'opsi1'         => 'required',
        ], [
            'ticketMessage' => [
                'required'  => 'Harus di isi...',
            ],
            'opsi1'         => [
                'required'  => 'Harus di isi...',
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Masukan pesan dulu cuy !');
            return redirect()->to('/')->withInput()->with('validation', $validation);
        }

        // dd($ticketNumber, $message, $status, $opsi1, $opsi2, $opsi3);
        // to get tele id use this url: api.telegram.org/bot(token)/getupdates, then copy the id from that result
        $telegram_id1 = '2117554169';
        $telegram_id2 = '-1001758660794';
        $secret_token = '5164356080:AAEJMB6O-tXmDt6DpWASApeoXAxBdEt2tqY';
        $username = $this->models->getUsernameByUserId($userId);
        $message_bot = $message . ' - by ' . $username['user_username'];

        if($username['user_level'] != 1) {
            $url1 = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=" . $telegram_id1;
            $url2 = "https://api.telegram.org/bot" . $secret_token . "/sendMessage?parse_mode=markdown&chat_id=" . $telegram_id2;
            $url1 = $url1 . "&text=" . urlencode($message_bot);
            $url2 = $url2 . "&text=" . urlencode($message_bot);
            $ch1 = curl_init();
            $ch2 = curl_init();

            $optArray1 = array(
                    CURLOPT_URL => $url1,
                    CURLOPT_RETURNTRANSFER => true
            );
            $optArray2 = array(
                    CURLOPT_URL => $url2,
                    CURLOPT_RETURNTRANSFER => true
            );

            curl_setopt_array($ch1, $optArray1);
            $result = curl_exec($ch1);
            $err = curl_error($ch1);

            curl_setopt_array($ch2, $optArray2);
            $result = curl_exec($ch2);
            $err = curl_error($ch2);
            curl_close($ch1);
            curl_close($ch2);
        }

        // masukan ke database
        $this->models->insertDataTicket($userId, $message, $createdAt, $status, $ticketNumber, $opsi1, $opsi2, $opsi3);
        
        // pesan sukses
        $session->setFlashdata('success_message', 'Success mengirim pesan ke IT ! ');
        return redirect()->to('/');
    }

    public function exportData() {
		$session = session();
        $date = date('j M Y');
        $fromDate = $this->request->getVar('fromDate');
        $toDate = $this->request->getVar('toDate');

        if($fromDate == null || $toDate == null) {
            $dataTemplate = $this->models->getAllDataTicket();
            $fileName = 'All data ticket - ' . $date;
        } else {
            $dataTemplate = $this->models->getSpecificDataTicket($fromDate, $toDate);
            $fileName = 'All data ticket '. $fromDate . ' - ' . $toDate;
        }


        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Ticket Number');
        $sheet->setCellValue('B1', 'User');
        $sheet->setCellValue('C1', 'Department');
        $sheet->setCellValue('D1', 'Created At');
        $sheet->setCellValue('E1', 'Message');
        $sheet->setCellValue('F1', 'Status');
        $sheet->setCellValue('G1', 'Updated By');
        $sheet->setCellValue('H1', 'Updated At');
        $rows = 2;

        foreach ($dataTemplate as $dt) {
            $sheet->setCellValue('A' . $rows, $dt['ticket_number']);
            $sheet->setCellValue('B' . $rows, $dt['createdBy']);
            $sheet->setCellValue('C' . $rows, $dt['dept']);
            $sheet->setCellValue('D' . $rows, $dt['created_at']);
            $sheet->setCellValue('E' . $rows, $dt['ticket_message']);
            $sheet->setCellValue('F' . $rows, $dt['status']);
            $sheet->setCellValue('G' . $rows, $dt['updatedBy']);
            $sheet->setCellValue('H' . $rows, $dt['updated_at']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
	}

    public function finishTicket($id) {
        $updatedBy  = $this->session->get('asset_id');
        $updatedAt  = date("Y-m-d H:i:s");
        $status     = 7; 

        $this->models->updateTicket($updatedBy, $updatedAt, $status, $id);

        // pesan sukses
        $this->session->setFlashdata('success_message', '<strong>Success update ticket ! </strong>');
        return redirect()->to('/Ticketing');
    }

    public function rejectTicket($id) {
        $updatedBy  = $this->session->get('asset_id');
        $updatedAt  = date("Y-m-d H:i:s");
        $status     = 9; 

        $this->models->updateTicket($updatedBy, $updatedAt, $status, $id);

        // pesan sukses
        $this->session->setFlashdata('success_message', '<strong>Success update ticket ! </strong>');
        return redirect()->to('/Ticketing');
    }

    public function clearTicket($id) {
        $updatedBy  = $this->session->get('asset_id');
        $updatedAt  = date("Y-m-d H:i:s");
        $status     = 8; 

        $this->models->updateTicket($updatedBy, $updatedAt, $status, $id);

        // pesan sukses
        $this->session->setFlashdata('success_message', '<strong>Success clear ticket ! </strong>');
        return redirect()->to('/Ticketing');
    }
} 
