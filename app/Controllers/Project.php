<?php

namespace App\Controllers;

use \App\Models\M_View;

class Project extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {
        
        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu'      => $menu,
            'project'   => $this->M_View->getAllProject(),
            'validation' => \Config\Services::validation(),
			'type'      => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'       => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'        => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'      => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level'     => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
            "test"      => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('project', $data);
    }

    public function insertData() {

        $session = session();

        // ambil data dari web
        $project_name       = esc($this->request->getVar('project_name'));
        $planning_date      = $this->request->getVar('planning_date_project');
        $actual_date        = $this->request->getVar('actual_date_project');
        $project_status     = $this->request->getVar('project_status');
        $project_comment    = esc($this->request->getVar('project_comment'));

        // validate
        if(!$this->validate([
            'project_name'  => 'required',
            'planning_date_project'  => 'required',
            'actual_date_project'   => 'required',
            'project_status'    => 'required',
        ], [
            'project_name'  => [
                'required'  => 'Harus di isi...'
            ],
            'planning_date_project'  => [
                'required'  => 'Harus di isi...'
            ],
            'actual_date_project'   => [
                'required'  => 'Harus di isi...'
            ],
            'project_status'    => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert new project !');
            return redirect()->to('/project')->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->insertDataProject($project_name, $planning_date, $actual_date, $project_status, $project_comment);

        // pesan sukses
        $session->setFlashdata('success_message', "Success insert new project! ");
        return redirect()->to('/project');
    }

    public function deleteData() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataProject($deleteId);
            $session->setFlashdata('success_message', 'Success delete project! ');
            return redirect()->to('/project');
        } else {
            $session->setFlashdata('error_message', 'Failed delete project! ');
            return redirect()->to('/project');
        }
    }
    
    public function editData($id) {
        $session = session();

        $getOldData = $this->M_View->getEditProject($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_project', $data);
    }

    public function doEditData($id) {
        $session = session();

        // ambil data
        $projectName        = esc($this->request->getVar('project_name'));
        $projectPlanning    = esc($this->request->getVar('project_planning'));
        $projectActual      = esc($this->request->getVar('project_actual'));
        $projectStatus      = $this->request->getVar('project_status');
        $projectComment     = esc($this->request->getVar('project_comment'));
        $id = $id;
        $getOldData = $this->M_View->getEditProject($id);

        // validate
        if($projectName == $getOldData['project_name']) {
            $rule = 'required';
        } else {
            $rule = 'required|is_unique[project.project_name]';
        }

        if(!$this->validate([
            'project_name'      => $rule,
            'project_planning'  => 'required',
            'project_actual'    => 'required',
            'project_status'    => 'required',
        ], [
            'project_name'  => [
                'required'  => 'Harus di isi...',
                'is_unique' => 'Sudah ada cuy...'
            ],
            'project_planning'  => [
                'required'  => 'Harus di isi...'
            ],
            'project_actual'    => [
                'required'  => 'Harus di isi...'
            ],
            'project_status'    => [
                'required'  => 'Harus di isi...'  
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed edit project data !');
            return redirect()->to('/Project/editData/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editProject($id, $projectName, $projectPlanning, $projectActual, $projectStatus, $projectComment);

        // pesan sukses
        $session->setFlashdata('success_message', "Successfully update existing project ! ");
        return redirect()->to('/Project');

    }
}
