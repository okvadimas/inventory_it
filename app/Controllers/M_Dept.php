<?php

namespace App\Controllers;

use \App\Models\M_View;

class M_Dept extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {

        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu'  => $menu,
            'user'  => $this->M_View->getAllUserManagement(),
            'validation' => \Config\Services::validation(),
			'type'  => $this->M_View->getAllAssetType(),
			'status' => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'       => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'    => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'  => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level'     => $this->M_View->getAllUserLevel(),
            "test"      => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('m_dept', $data);
    }

    public function insertData() {

        $session = session();

        // ambil data dari web
        $id     = esc($this->request->getVar('identity'));
        $name   = esc($this->request->getVar('name'));
        $slug   = esc(strtoupper($this->request->getVar('slug')));

        // validasi
        if(!$this->validate([
            'identity'  => 'required|is_unique[m_dept.id]',
            'name'      => 'required|is_unique[m_dept.name]',
            'slug'      => 'required|is_unique[m_dept.slug]',
        ], [
            'identity' => [
                'is_unique' => 'Sudah ada cuy... !',
                'required'  => 'Harus di isi...'
            ],
            'name' => [
                'is_unique' => 'Sudah ada cuy... !',
                'required'  => 'Harus di isi...'
            ],
            'slug' => [
                'is_unique' => 'Sudah ada cuy... !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert new dept ! ');
            return redirect()->to('/m_dept')->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->insertDataDept($id, $name, $slug);

        // pesan sukses
        $session->setFlashdata('success_message', 'Success insert new dept ! ');
        return redirect()->to('/m_dept');
    }

    public function deleteData() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataDept($deleteId);
            $session->setFlashdata('success_message', 'Success delete data dept ! ');
            return redirect()->to('/m_dept');
        } else {
            $session->setFlashdata('error_message', 'Failed delete data dept ! ');
            return redirect()->to('/m_dept');
        }
    }

    public function editDept($id) {
        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        $getOldData = $this->M_View->getEditDept($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'lastUser' => $lastUser,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_m_dept', $data);
    }

    public function doEditDept($id) {
        $session = session();

        $identity = esc($this->request->getVar('identity'));
        $dept = esc($this->request->getVar('name'));
        $slug = esc($this->request->getVar('slug'));
        $id = $id;
        $getOldData = $this->M_View->getEditDept($id);

        if($getOldData['id'] == $identity) {
            $rule1 = 'required';
        } else {
            $rule1 = "required|is_unique[m_dept.id]";
        }

        if($getOldData['name'] == $dept) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[m_dept.name]';
        }

        if($getOldData['slug'] == $slug) {
            $rule3 = 'required';
        } else {
            $rule3 = 'required|is_unique[m_dept.slug]';
        }

        // validasi
        if(!$this->validate([
            'identity' => $rule1,
            'name' => $rule2,
            'slug' => $rule3
        ],[
            'identity' => [
                'is_unique' => 'Sudah ada cuy... !',
                'required'  => 'Harus di isi...'
            ],
            'name' => [
                'is_unique' => 'Sudah ada cuy... !',
                'required'  => 'Harus di isi...'
            ], 
            'slug' => [
                'is_unique' => 'Sudah ada cuy... !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data! ');
            return redirect()->to('/M_Dept/editDept' . '/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editDept($id, $identity, $dept, $slug);

        $validation = \Config\Services::validation();
        $session->setFlashdata('success_message', 'Successfully update existing dept! ');
        return redirect()->to('/M_Dept')->withInput()->with('validation', $validation);
    }
}
