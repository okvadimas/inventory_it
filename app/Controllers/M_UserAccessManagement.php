<?php

namespace App\Controllers;

use \App\Models\M_View;

class M_UserAccessManagement extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {
        
        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu'      => $menu,
            'allPost'   => $this->M_View->getAllPostMessage(),
            'user'      => $this->M_View->getAllUserManagement(),
            'validation' => \Config\Services::validation(),
			'type'      => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'       => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'    => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'  => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level' => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
            "test"  => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('m_userAccessManagement', $data);
    }

    public function insertDataUserAccess() {

        $session = session();

        // ambil data dari web
        $level  = esc($this->request->getVar('level'));
        $menu   = esc($this->request->getVar('menu'));

        // validasi 
        $cek = $this->M_View->getUserAccessByLevel($level, $menu);
        if(!$this->validate([
            'level' => 'required',
            'menu'  => 'required'
        ], [
            'level' => [
                'required'  => 'Harus di isi...'
            ],  
            'menu'  => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert, please input your data first ! ');
            return redirect()->to('/m_userAccessManagement')->withInput()->with('validation', $validation);
        }

        if($cek == null) {
            $this->M_View->insertDataUserAccess($level, $menu);
            $session->setFlashdata('success_message', 'Success insert new user access ! ');
            return redirect()->to('/m_userAccessManagement');
        } else {
            $session->setFlashdata('error_message', 'Failed insert new data ! duplicate detected, please check and try again... ');
            return redirect()->to('/m_userAccessManagement')->withInput();
        }
    }

    public function deleteDataUserAccess() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataUserAccess($deleteId);
            $session->setFlashdata('success_message', 'Success delete user access ! ');
            return redirect()->to('/m_userAccessManagement');
        } else {
            $session->setFlashdata('error_message', 'Failed delete user access ! ');
            return redirect()->to('/m_userAccessManagement');
        }
    }
    
    public function editUserAccess($id) {
        $session    = session();
        $lastUser   = $this->M_View->getAllUserDesc();
        
        $getOldData = $this->M_View->getEditUserAccess($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'lastUser' => $lastUser,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_m_userAccessManagement', $data);
    }

    public function doEditUserAccess($id) {
        $session = session();

        $level  = esc($this->request->getVar('level'));
        $menu   = $this->request->getVar('menu');
        $getOldData     = $this->M_View->getEditUserAccess($id);
        $getDataLevel   = $this->M_View->getAllUserLevel();
        $id     = $id;

        // validasi
        if(!$this->validate([
            'level' => 'required',
            'menu'  => 'required'
        ], [
            'level' => [
                'required'  => 'Harus di isi...'
            ],
            'menu'  => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed! unknown level inserted ');
            return redirect()->to('/M_UserAccessManagement/editUserAccess' . '/' . $id)->withInput()->with('validation', $validation);
        }

        foreach($getDataLevel as $l) {
            if($level == $l['id']) {
                $hasil = True;
                break;
            } else {
                $hasil = False;
            }
        }

        if($hasil == False) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed! unknown level inserted ');
            return redirect()->to('/M_UserAccessManagement/editUserAccess' . '/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editUserAccess($id, $level, $menu);

        $validation = \Config\Services::validation();
        $session->setFlashdata('success_message', "Successfully update existing user access menu! ");
        return redirect()->to('/M_UserAccessManagement')->withInput()->with('validation', $validation);
    }
}
