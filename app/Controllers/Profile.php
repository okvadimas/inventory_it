<?php

namespace App\Controllers;

use App\Models\M_View;

class Profile extends BaseController
{
	public function __construct() {
		$this->M_View = new M_View();
	}
	public function index()
	{
		$session = session();

        // if(!session()->get('login')) {
        //     return redirect()->to('/auth');
        // }

		$user = $this->M_View->getUserProfile($session->get('user_id'));

		$session = session();
		$lastUser = $this->M_View->getAllUserDesc();

		if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

		$data = [
			'menu' => $menu,
			'user' => $user,
			'dept' => $this->M_View->getAllDept(),
			'validation' => \Config\Services::validation(),
			'type' => $this->M_View->getAllAssetType(),
			'status' => $this->M_View->getAllStatus(),
			'lastUser' => $lastUser,
            'position' => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender' => $this->M_View->getAllGender(),
            'use' => $this->M_View->getAllAccessUse(),
            'deskera' => $this->M_View->getAllAccessDeskera(),
            'emailUse' => $this->M_View->getAllEmailUse(),
			'operatingSystem' => $this->M_View->getAllOperatingSystem(),
			'deptName' => $this->M_View->getDeptById($session->get('dept')),
			'it' => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept' => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level' => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
			"test" => $this->M_View->getReportPerMonth(),
			'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
		];

		return view('profile', $data);
	}

	public function updateProfile($id) {

		$session = session();

		// ambil data dari web
		$fullname = esc($this->request->getVar('name'));
		$email = esc($this->request->getVar('email'));
		$phone = esc($this->request->getVar('phone'));
		$dept = $this->request->getVar('dept');
		$birth = $this->request->getVar('birth');
		$asset_id = $this->request->getVar('identity');
		$id = $session->get('user_id');

		$pic = $this->request->getFile('picture');

		$getOldData = $this->M_View->getUserProfile($id);
		$oldPicture = $getOldData['user_picture'];

		// validasi
		if($pic->getError() == 4) {
			$pictureName = $oldPicture;
		} else {
			$pictureName = $pic->getRandomName();
		}

		// dd($pictureName);

		if($getOldData['user_fullname'] == $fullname) {
			$rule1 = 'required';
		} else {
			$rule1 = 'required|is_unique[user.user_fullname]';
		}

		if($getOldData['user_email'] == $email) {
			$rule2 = 'required';
		} else {
			$rule2 = 'required|is_unique[user.user_email]';
		}

		$rule3 = 'required';

		if($getOldData['user_asset_id'] == $asset_id) {
			$rule6 = 'required';
		} else {
			$rule6 = 'required|is_unique[user.user_asset_id]';
		}

		if(!$this->validate([
			'name' => $rule1,
			'email' => $rule2,
			'phone' => $rule3,
			'identity' => $rule6,
			'picture' => 'max_size[picture,1024]|is_image[picture]|mime_in[picture,image/jpg,image/jpeg,image/png]',
		],[
			'name' => [
				'is_unique' => 'Sudah ada ! punya kembaran kah ? '
			],
			'email' => [
				'is_unique' => 'Sudah ada ! tolong cek lagi namanya '
			], 
			'phone' => [
				'is_unique' => 'Sudah ada ! '
			],
			'identity' => [
				'is_unique' => 'Sudah ada ! mungkin kamu typo '
			],
			'picture' => [
				'max_size' => 'Itu foto apa besar banget size-nya, maks 1 Mb ya.. ',
				'is_image' => 'Itu beneran file image ? khusus .jpg .jpeg dan .png ya.. ',
			]
		])) {
			$session->setFlashdata('error_message', 'Failed change profile ! ');
			return redirect()->to('/profile')->withInput();
		}

		if($pic->getError() == 4) {
			$pictureName = $oldPicture;
		} else {
			$pictureName = $pic->getRandomName();

			$pic->move('images/users', $pictureName);
			// resizing image
			\Config\Services::image()->withFile('images/users/' . $pictureName)->resize(600,600)->save('images/users/' . $pictureName);

			// delete old picture
			if($oldPicture != 'default.jpg') {				
				unlink('images/users/' . $oldPicture);
			}
		}

		// kirim data update an dept pake session
		$session->set('dept', $dept);

		// insert to database
		$this->M_View->updateProfile($fullname, $email, $phone, $dept, $birth, $asset_id, $id, $pictureName);

		// pesan sukses
		$session->setFlashdata('success_message', 'Success update profile! ');
		return redirect()->to('/profile');
	}

	public function resetPassword() {

		$session = session();

		// validasi
		if(!$this->validate([
			'currentPass' => 'required',
			'newPass' => 'required|min_length[6]',
			'confirmPass' => 'required|matches[newPass]'
		],
		[   // Errors
			'newPass' => [
				'min_length' => 'Terlalu pendek boss! minimal 6 character..'
			],
			'confirmPass' => [
				'matches' => 'Password tidak sama, ulangi boss! '
			]
		]
		)) { 
			$validation = \Config\Services::validation();
			$session->setFlashdata('error_message', 'Failed change password! ');
			return redirect()->to('/profile')->withInput()->with('validation', $validation);
		}

		// ambil data dari web
		$currentPass = $this->request->getVar('currentPass');
		$newPass = esc($this->request->getVar('newPass'));
		$confirmPass = $this->request->getVar('confrimPass');
		// jika sandi sama dengan database lanjutkan
		$user = $this->M_View->getUser($session->get('username'));
		if(password_verify($currentPass, $user['user_pass'])) {
			// hash new password
			$newPass = password_hash($newPass, PASSWORD_DEFAULT);
			// update to database
			$this->M_View->resetPassword($newPass, $session->get('user_id'));
			
			// pesan sukses
			$session->setFlashdata('success_message', 'Success change password! The new password will take effect on the next login...');
			return redirect()->to('/profile');

		} else {
			$session->setFlashdata('error_message', 'Failed change password! ');
			return redirect()->to('/profile');
		}
	}
}
