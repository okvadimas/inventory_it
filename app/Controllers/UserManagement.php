<?php

namespace App\Controllers;

use \App\Models\M_View;

class UserManagement extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {
        
        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu' => $menu,
            'user' => $this->M_View->getAllUserManagement(),
            'validation' => \Config\Services::validation(),
			'type' => $this->M_View->getAllAssetType(),
			'status' => $this->M_View->getAllStatus(),
			'lastUser' => $lastUser,
            'position' => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender' => $this->M_View->getAllGender(),
            'use' => $this->M_View->getAllAccessUse(),
            'deskera' => $this->M_View->getAllAccessDeskera(),
            'emailUse' => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it' => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept' => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level' => $this->M_View->getAllUserLevel(),
            "test" => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('userManagement', $data);
    }

    public function insertData() {

        $session = session();

        // ambil data dari web
        $username = $this->request->getVar('username');
        $password = $this->request->getVar('password');
        $fullname = $this->request->getVar('fullname');
        $identity = $this->request->getVar('identity');
        $department = $this->request->getVar('department');
        $level = $this->request->getVar('level');
        $status = $this->request->getVar('status');
        $create = date('Y-m-d H:i:s');
        $picture = 'default.jpg';
        
        // validasi
        if(!$this->validate([
            'username' => 'required|is_unique[user.user_username]',
            'password' => 'required',
            'fullname' => 'required',
            'identity' => 'required|is_unique[user.user_asset_id]',
            'department'    => 'required',
            'level'    => 'required',
            'status'   => 'required'
        ], [
            'username' => [
                'is_unique' => 'Sudah ada boss !',
                'required'  => 'Harus di isi...'
            ],
            'identity' => [
                'is_unique' => 'Sudah ada boss !',
                'required'  => 'Harus di isi...'
            ],
            'password'  => [
                'required'  => 'Harus di isi...'
            ],
            'fullname'  => [
                'required'  => 'Harus di isi...'
            ],
            'department'    => [
                'required'  => 'Harus di isi...'
            ],
            'level' => [
                'required'  => 'Harus di isi...'
            ],
            'status'    => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert new data !');
            return redirect()->to("/UserManagement")->withInput()->with('validation', $validation);
        }

        // encrypt password
        $password = password_hash($password, PASSWORD_DEFAULT);

        // masukan ke database
        $this->M_View->insertDataUserManagement($username, $password, $fullname, $identity, $department, $level, $status, $create, $picture);
        // pesan sukses
        $session->setFlashdata('success_message', "Success insert new user! ");
        return redirect()->to('/userManagement');
    }

    public function deleteData() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataUserManagement($deleteId);
            $session->setFlashdata('success_message', 'Success delete user! ');
            return redirect()->to('/userManagement');
        } else {
            $session->setFlashdata('error_message', 'Failed delete user! ');
            return redirect()->to('/userManagement');
        }
    }

    public function editData($id) {
        $session = session();
        $getOldData = $this->M_View->getEditUserManagement($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            'dept' => $this->M_View->getAllDept(),
            'status' => $this->M_View->getAllStatus(),
            'level' => $this->M_View->getAllUserLevel(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_userManagement', $data);
    }

    public function doEditData($id) {
        $session = session();
        // ambil data
        $id = $id;
        $userUsername = esc($this->request->getVar('user_username'));
        $userPass = esc($this->request->getVar("user_pass"));
        $userFullname = esc($this->request->getVar('user_fullname'));
        $userEmail = esc($this->request->getVar('user_email'));
        $userBirth = esc($this->request->getVar('user_birth'));
        $userPhone = esc($this->request->getVar('user_phone'));
        $userAssetId = esc($this->request->getVar('user_asset_id'));
        $userDept = $this->request->getVar('user_dept');
        $userLevel = $this->request->getVar('user_level');
        $userStatus = $this->request->getVar('user_status');
        $getOldData = $this->M_View->getEditUserManagement($id);
        
        $picture = $this->request->getFile('user_picture');
		$getOldData = $this->M_View->getUserProfile($id);
        $oldPicture = $getOldData['user_picture'];

		// validasi
		if($picture->getError() == 4) {
			$pictureName = $oldPicture;
		} else {
			$pictureName = $picture->getRandomName();
		}

        // validasi
        if($getOldData['user_username'] == $userUsername) {
            $rule1 = 'required';
        } else {
            $rule1 = 'required|is_unique[user.user_username]';
        }

        if($getOldData['user_email'] == $userEmail) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[user.user_email]'; 
        }

        if($getOldData['user_asset_id'] == $userAssetId) {
            $rule3 = 'required';
        } else {
            $rule3 = 'required|is_unique[user.user_asset_id]';
        }

        if($getOldData['user_pass'] == $userPass) {
            $rule4 = 'required';
        } else {
            $rule4 = 'required|is_unique[user.user_pass]|min_length[6]';
        }

        if(!$this->validate([
            'user_username' => $rule1,
            'user_email' => $rule2,
            'user_asset_id' => $rule3,
            'user_pass' => $rule4,
            'user_picture' => 'max_size[user_picture,1024]|is_image[user_picture]|mime_in[user_picture,image/jpg,image/jpeg,image/png]',
        ], [
            'user_username' => [
                'is_unique' => "Sudah ada boss !",
                'required'  => 'Harus di isi...'
            ],
            'user_email' => [
                'is_unique' => "Sudah ada boss !",
                'required'  => 'Harus di isi...'
            ],
            'user_asset_id' => [
                'is_unique' => "Sudah ada boss !",
                'required'  => 'Harus di isi...'
            ],
            'user_pass' => [
                'is_unique' => "Sudah ada boss !",
                'min_length' => "Kurang complex, min 6 character !",
                'required'  => 'Harus di isi...'
            ],
            'user_picture' => [
				'max_size' => 'Kegeden cuy, maks 1 mb ya.. ',
				'is_image' => 'Khusus .jpg .jpeg dan .png cuy.. ',
                'required'  => 'Harus di isi...'
			]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data !');
            return redirect()->to("/UserManagement/editData" . "/" . $id)->withInput()->with('validation', $validation);
        }

        // image management
        if($picture->getError() == 4) {
			$pictureName = $oldPicture;
		} else {
			$pictureName = $picture->getRandomName();

			$picture->move('images/users', $pictureName);
			// resizing image
			\Config\Services::image()->withFile('images/users/' . $pictureName)->resize(600,600)->save('images/users/' . $pictureName);

			// delete old picture
			if($oldPicture != 'default.jpg') {				
				unlink('images/users/' . $oldPicture);
			}
		}

        // masukan ke database
        // konversi password
        if($getOldData['user_pass'] != $userPass) {
            $userPass = password_hash($userPass, PASSWORD_DEFAULT);
        } 
        
        $this->M_View->editUserManagement($id, $userUsername, $userPass, $userFullname, $userEmail, $userBirth, $userPhone, $pictureName,
            $userAssetId, $userDept, $userLevel, $userStatus);

        // pesan sukses
        $session->setFlashdata('success_message', 'Successfully update existing data ! ');
        return redirect()->to('/UserManagement');
    }

    public function exportData() {
        $session = session();
        $date = date('j M Y');

        $dataTemplate = $this->M_View->getAllDataUser();
        $fileName = 'All data user - ' . $date;

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Asset ID');
        $sheet->setCellValue('B1', 'Level');
        $sheet->setCellValue('C1', 'Username');
        $sheet->setCellValue('D1', 'Fullname');
        $sheet->setCellValue('E1', 'Email');
        $sheet->setCellValue('F1', 'Birth Date');
        $sheet->setCellValue('G1', 'Phone Number');
        $sheet->setCellValue('H1', 'Status');
        $sheet->setCellValue('I1', 'Create');
        $rows = 2;

        foreach ($dataTemplate as $dt) {
            $sheet->setCellValue('A' . $rows, $dt['user_asset_id']);
            $sheet->setCellValue('B' . $rows, $dt['user_level'] . " - " . $dt['level']);
            $sheet->setCellValue('C' . $rows, $dt['user_username']);
            $sheet->setCellValue('D' . $rows, $dt['user_fullname']);
            $sheet->setCellValue('E' . $rows, $dt['user_email']);
            $sheet->setCellValue('F' . $rows, $dt['user_birth']);
            $sheet->setCellValue('G' . $rows, $dt['user_phone']);
            $sheet->setCellValue('H' . $rows, $dt['status']);
            $sheet->setCellValue('I' . $rows, $dt['user_create']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
