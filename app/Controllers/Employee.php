<?php

namespace App\Controllers;

use \App\Models\M_View;

class Employee extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {

        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session    = session();
        $lastUser   = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu   = $this->M_View->getMenu();
		} else {
			$menu   = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu'      => $menu,
            'emp'       => $this->M_View->getAllEmp(),
			'validation' => \Config\Services::validation(),
			'type'      => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'       => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'        => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'      => $this->M_View->getAllDept(),
            'deptName'  => $this->M_View->getDeptById($session->get('dept')),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level'     => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
            "test"      => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('employee', $data);
    }

    public function insertData() {

        $lastUser = $this->M_View->getAllUserDesc();

        $session = session();

        // ambil data dari web
        $position   = $this->request->getVar('position');
        $gender     = $this->request->getVar('gender');
        $condition  = $this->request->getVar('condition');
        $address    = $this->request->getVar('address');
        $user_id    = $lastUser['user_asset_id'];

        // validate
        if(!$this->validate([
            'position'  => 'required',
            'gender'    => 'required',
            'condition' => 'required',
            'address'   => 'required',
        ], [
            'position'      => [
                'required'  => 'Harus di isi...'
            ],
            'gender'        => [
                'required'  => 'Harus di isi...'
            ],
            'condition'     => [
                'required'  => 'Harus di isi...'
            ],
            'address'       => [
                'required'  => 'Hasil di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed insert new data employee !</strong>');
            return redirect()->to('/employee')->withInput()->with('validation', $validation);
        }

        // masukan ke database
        if($this->M_View->getEditEmpByUserId($user_id) != null) {
            // pesan error
            $session->setFlashdata('error_message', '<strong>Aleardy exists ! </strong>');
            return redirect()->to('/employee');
        } else {
            $this->M_View->insertDataEmp($position, $gender, $condition, $address, $user_id);

            // pesan sukses
            $session->setFlashdata('success_message', '<strong>Success insert new employee ! </strong>');
            return redirect()->to('/employee');
        }

    }

    public function deleteData() {

        $deleteId = $this->request->getVar('deleteId');
		$session = session();

		// jika ada delete
		if(isset($deleteId)) {
			$this->M_View->deleteDataEmp($deleteId);
			$session->setFlashdata('success_message', '<strong>Success delete employee data ! </strong>');
			return redirect()->to('/employee');
		} else {
			$session->setFlashdata('error_message', '<strong>Failed delete employee data ! </strong>');
			return redirect()->to('/employee');
		}

    }

    public function editData($id) {
        $session = session();
        $getOldData = $this->M_View->getEditEmp($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu'      => $menu,
            'data'      => $getOldData,
            'validation' => \Config\Services::validation(),
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'status'    => $this->M_View->getAllStatus(),
            "test"      => $this->M_View->getReportPerMonth(),
        ];

        return view('/E_employee', $data);
    }

    public function doEditData($id) {
        $session = session();

        // ambil data
        $id         = $id;
        $position   = $this->request->getVar('position');
        $condition  = $this->request->getVar('condition');
        $gender     = $this->request->getVar('gender');
        $address    = esc($this->request->getVar('address'));
        $getOldData = $this->M_View->getEditEmp($id);

        // validate
        if(!$this->validate([
            'position'  => 'required',
            'condition' => 'required',
            'gender'    => 'required',
            'address'   => 'required'
        ], [
            'position'  => [
                'required' => 'Harus di isi...'
            ], 
            'condition' => [
                'required' => 'Harus di isi...'
            ],
            'gender'    => [
                'required' => 'Harus di isi...'
            ],
            'address'   => [
                'required' => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed edit employee data !</strong>');
            return redirect()->to('/employee/editData/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editDataEmp($id, $position, $condition, $gender, $address);

        // pesan sukses
        $session->setFlashdata('success_message', '<strong>Successfully update existing employee data ! </strong>');
        return redirect()->to('/Employee');
    }
    
    public function importData() {
        $session = session();
        $file = $this->request->getFile('importData');
        $filename = $file->getName();
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('/Employee')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }
				
                // validasi duplicate
                $check = $this->M_View->checkEmployee($kolom['0']);
                $checkUserID = $this->M_View->checkUserId($kolom['0']);

                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null && $kolom['4'] == null) {
                    continue;
                }

                if($check != null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } elseif($kolom['0'] == null || $kolom['1'] == null || $kolom['2'] == null || $kolom['3'] == null || $kolom['4'] == null) {
                    $log['failed'] += 1;
                    continue;
                } elseif($checkUserID == null) {
                    $log['failed'] += 1;
                    continue;
                } else {

                    $userAssetId = $kolom['0'];
                    $position    = $kolom['1'];
                    $gender 	 = $kolom['2'];
                    $lifeStatus  = $kolom['3'];
                    $address     = $kolom['4'];
                            
                    // masukan ke database
                    $this->M_View->importEmployee($userAssetId, $position, $gender, $lifeStatus, $address);
                    $log['success'] += 1;
                }
            }  

            $session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('/Employee');
        }
    }

    public function templateImportData() {
        // dd('masuk download template preventive');
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

		$dataPosition = $this->M_View->getAllPosition();
		$dataGender = $this->M_View->getAllGender();
        $dataLife = $this->M_View->getAllCondition();

        $sheet->setCellValue('A1', 'User Asset ID*');
        $sheet->setCellValue('B1', 'Position*');
        $sheet->setCellValue('C1', 'Gender*');
        $sheet->setCellValue('D1', 'Life Status*');
        $sheet->setCellValue('E1', 'Address');

        $sheet->setCellValue('A2', '2175');
        $sheet->setCellValue('B2', '3');
        $sheet->setCellValue('C2', '1');
        $sheet->setCellValue('D2', '2');
        $sheet->setCellValue('E2', 'Perumahan Klipang Permai Blok H.230, Semarang');

		// informasi untuk template 
        $sheet->setCellValue('H2', "# Information");
        $sheet->setCellValue('H3', "# Position");

        $rowPos = 4;

        foreach($dataPosition as $dp) {
            $sheet->setCellValue('H'.$rowPos, $dp['id'] . ". " . $dp['name']);
            $rowPos++;
        }

        $sheet->setCellValue('H'.$rowPos + 2, "# Gender");
        $rowGender = $rowPos + 3;

        foreach($dataGender as $dg) {
            $sheet->setCellValue('H'.$rowGender, $dg['id'] . ". " . $dg['name']);
            $rowGender++;
        }

        $sheet->setCellValue('J3', "# Life Status");
        $rowLife = 4;

        foreach($dataLife as $dl) {
            $sheet->setCellValue('J'.$rowLife, $dl['id'] . ". " . $dl['name']);
            $rowLife++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import employee';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

	public function exportData() {
		$session = session();
        $date = date('j M Y');

		$dataTemplate = $this->M_View->getAllEmp();
		$fileName = 'All data employee - ' . $date;

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();


        $sheet->setCellValue('A1', 'Asset ID');
        $sheet->setCellValue('B1', 'Fullname');
        $sheet->setCellValue('C1', 'Birth Date');
        $sheet->setCellValue('D1', 'Phone');
        $sheet->setCellValue('E1', 'Department');
        $sheet->setCellValue('F1', 'Position');
        $sheet->setCellValue('G1', 'Gender');
        $sheet->setCellValue('H1', 'Work Status');
        $sheet->setCellValue('I1', 'Life Status');
        $sheet->setCellValue('J1', 'Address');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['user_asset_id']);
            $sheet->setCellValue('B' . $rows, $dt['user_fullname']);
            $sheet->setCellValue('C' . $rows, $dt['user_birth']);
            $sheet->setCellValue('D' . $rows, $dt['user_phone']);
            $sheet->setCellValue('E' . $rows, $dt['dept']);
            $sheet->setCellValue('F' . $rows, $dt['position']);
            $sheet->setCellValue('G' . $rows, $dt['gender']);
            $sheet->setCellValue('H' . $rows, $dt['status1'] .  ' - ' . $dt['status2']);
            $sheet->setCellValue('I' . $rows, $dt['kondisi']);
            $sheet->setCellValue('J' . $rows, $dt['emp_address']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
	}
}
