<?php

namespace App\Controllers;

use \App\Models\M_View;

class Internet extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {
        
        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu'      => $menu,
            'internet'    => $this->M_View->getAllInternet(),
            'validation' => \Config\Services::validation(),
			'type'      => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'       => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'        => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'      => $this->M_View->getAllDept(),
            'deptName'  => $this->M_View->getDeptById($session->get('dept')),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level'     => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
            "test"      => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];


        return view('internet', $data);
    }

    public function insertData() {

        $lastUser = $this->M_View->getAllUserDesc();

        $session = session();

        // ambil data dari web
        $userId     = esc($this->request->getVar('userIdInternet'));
        $profile    = esc($this->request->getVar('profileInternet'));
        $username   = esc($this->request->getVar('usernameInternet'));
        $password   = esc($this->request->getVar('passwordInternet'));

        // validate
        if(!$this->validate([
            'userIdInternet'   => 'required|is_unique[internet.user_id]',
            'profileInternet'  => 'required',
            'usernameInternet' => 'required',
            'passwordInternet' => 'required',
        ], [
            'userIdInternet' => [
                'required'  => 'Harus di isi...',
                'is_unique' => "Sudah ada cuy..."
            ],
            'profileInternet'   => [
                'required'  => 'Harus di isi...'
            ],
            'usernameInternet'    => [
                'required'  => 'Harus di isi...'
            ],
            'passwordInternet'    => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert internet data !');
            return redirect()->to('/Internet')->withInput()->with('validation', $validation);
        }

        // masukan ke database
        if($this->M_View->getEditInternet($userId) != null ) {
            // pesan error
            $session->setFlashdata('error_message', 'Already exists ! ');
            return redirect()->to('/internet');
        } else {
            $this->M_View->insertDataInternet($userId, $profile, $username, $password);
            
            // pesan sukses
            $session->setFlashdata('success_message', 'Success insert new access! ');
            return redirect()->to('/Internet');
        }
    }

    public function deleteData() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataInternet($deleteId);
            $session->setFlashdata('success_message', 'Success delete data! ');
            return redirect()->to('/internet');
        } else {
            $session->setFlashdata('error_message', 'Failed delete data! ');
            return redirect()->to('/internet');
        }
    }

    public function editData($id) {
        $session = session();
        $getOldData = $this->M_View->getEditInternet($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu'      => $menu,
            'data'      => $getOldData,
            'validation' => \Config\Services::validation(),
            "test"      => $this->M_View->getReportPerMonth(),
        ];

        return view('/E_internet', $data);
    }

    public function doEditData($id) {
        $session = session();

        // ambil data
        $inetUser       = esc($this->request->getVar('userInternet'));
        $inetPass       = esc($this->request->getVar('passInternet'));
        $inetProfile    = esc($this->request->getVar('profileInternet'));
        $id             = $id;
        $getOldData     = $this->M_View->getEditInternet($id);

        // validasi 
        if($getOldData['inet_user'] == $inetUser) {
            $rule1 = 'required';
        } else {
            $rule1 = "required|is_unique[internet.inet_user]";
        } 

        if(!$this->validate([
            'userInternet'      => $rule1,
            'passInternet'      => 'required',
            'profileInternet'   => 'required',
        ], [
            'userInternet'  => [
                'is_unique' => 'Sudah ada boss ! ',
                'required'  => 'Harus di isi...'
            ], 
            'passInternet'    => [
                'required'  => 'Harus di isi...'
            ],
            'profileInternet'     => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data !');
            return redirect()->to('/Internet/editData' . '/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editInternet($id, $inetUser, $inetPass, $inetProfile);

        // pesan sukses
        $session->setFlashdata('success_message', 'Successfully update existing data ! ');
        return redirect()->to('/Internet');
    }

    public function importData() {
        $session = session();
        $file = $this->request->getFile('importData');
        $filename = $file->getName();
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('/Internet')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }
				
                // validasi duplicate
                $check = $this->M_View->checkInternet($kolom['0']);

                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null) {
                    continue;
                }

                if($check != null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } elseif($kolom['0'] == null || $kolom['1'] == null || $kolom['2'] == null || $kolom['3'] == null) {
                    $log['failed'] += 1; 
                    continue;
                } else {

                    $userAssetId = $kolom['0'];
                    $username   = $kolom['1'];
                    $password 	= $kolom['2'];
                    $profile    = $kolom['3'];
                            
                    // masukan ke database
                    $this->M_View->importInternet($userAssetId, $username, $password, $profile);
                    $log['success'] += 1;
                }
            }  

            $session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('/Internet');
        }
    }

    public function templateImportData() {
        // dd('masuk download template preventive');
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

		$dataUse = $this->M_View->getAllAccessUse();
		$dataDeskera = $this->M_View->getAllAccessDeskera();

        $sheet->setCellValue('A1', 'User Asset ID*');
        $sheet->setCellValue('B1', 'Username*');
        $sheet->setCellValue('C1', 'Password*');
        $sheet->setCellValue('D1', 'Profile*');

        $sheet->setCellValue('A2', '2175');
        $sheet->setCellValue('B2', 'nuryadi');
        $sheet->setCellValue('C2', 'password');
        $sheet->setCellValue('D2', 'Staff 2');

		// informasi untuk template 

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import internet';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

	public function exportData() {
		$session = session();
        $date = date('j M Y');

		$dataTemplate = $this->M_View->getAllInternet();
		$fileName = 'All data internet - ' . $date;

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Asset ID');
        $sheet->setCellValue('B1', 'Username');
        $sheet->setCellValue('C1', 'Password');
        $sheet->setCellValue('D1', 'Profile');
        $rows = 2;

        // dd($accUse);

        foreach ($dataTemplate as $dt) {
            $sheet->setCellValue('A' . $rows, $dt['user_id']);
            $sheet->setCellValue('B' . $rows, $dt['inet_user']);
            $sheet->setCellValue('C' . $rows, $dt['inet_password']);
            $sheet->setCellValue('D' . $rows, $dt['inet_profile']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
	}
}
