<?php

namespace App\Controllers;

use \App\Models\M_View;

class Backup extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {
        
        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu' => $menu,
            'backup' => $this->M_View->getAllBackup(),
            'validation' => \Config\Services::validation(),
			'type' => $this->M_View->getAllAssetType(),
			'status' => $this->M_View->getAllStatus(),
			'lastUser' => $lastUser,
            'position' => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender' => $this->M_View->getAllGender(),
            'use' => $this->M_View->getAllAccessUse(),
            'deskera' => $this->M_View->getAllAccessDeskera(),
            'emailUse' => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it' => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept' => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level' => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
            "test" => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('backup', $data);
    }

    public function insertData() {

        $session = session();

        // ambil data dari web
        $backupDate = $this->request->getVar('backup_date');
        $backupStart = esc($this->request->getVar('backup_start'));
        $backupEnd = esc($this->request->getVar('backup_end'));
        $backupMonth = esc($this->request->getVar('backup_month'));
        $backupType = esc($this->request->getVar('backup_type'));
        $backupComment = esc($this->request->getVar('backup_comment'));

        // validate
        if(!$this->validate([
            'backup_date'   => 'required',
            'backup_start'  => 'required',
            'backup_end'    => 'required',
            'backup_type'   => 'required',
            'backup_month'   => 'required'
        ], [    
            'backup_date'   => [
                'required'  => 'Harus di isi...'
            ],
            'backup_start'  => [
                'required'   => 'Harus di isi...'
            ],
            'backup_end'    => [
                'required'  => 'Harus di isi...'
            ],
            'backup_type'   => [
                'required'  => 'Harus di isi...'
            ],
            'backup_month'   => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed insert data backup ! </strong>');
            return redirect()->to('/backup')->withInput()->with('validation', $validation);
        }

        // cek duls
        $check = $this->M_View->checkBackup($backupMonth, $backupType);

        if($check != null) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Already exist ! </strong>');
            return redirect()->to('/backup')->withInput()->with('validation', $validation);
        } else {
            // masukan ke database
            $this->M_View->insertDataBackup($backupDate, $backupStart, $backupEnd, $backupMonth, $backupType, $backupComment);

            // pesan sukses
            $session->setFlashdata('success_message', '<strong>Success insert new backup data ! </strong>');
            return redirect()->to('/backup');
        }
    }

    public function deleteData() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataBackup($deleteId);
            $session->setFlashdata('success_message', '<strong>Success delete backup data ! </strong>');
            return redirect()->to('/backup');
        } else {
            $session->setFlashdata('error_message', '<strong>Failed delete backupId data ! </strong>');
            return redirect()->to('/backup');
        }
    }

    public function editData($id) {
        $session = session();

        $getOldData = $this->M_View->getEditBackup($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            'dept' => $this->M_View->getAllDept(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_backup', $data);
    }

    public function doEditData($id) {
        $session = session();

        // ambil data
        $backupDate = $this->request->getVar('backup_date');
        $backupStart = esc($this->request->getVar('backup_start'));
        $backupEnd = esc($this->request->getVar('backup_end'));
        $backupMonth = esc($this->request->getVar('backup_month'));
        $backupType = esc($this->request->getVar('backup_type'));
        $backupComment = esc($this->request->getVar('backup_comment'));
        $id = $id;
        $getOldData = $this->M_View->getEditBackup($id);

        // validate
        if(!$this->validate([
            'backup_date'   => 'required',
            'backup_start'  => 'required',
            'backup_end'    => 'required',
            'backup_month'   => 'required',
            'backup_type'   => 'required',
        ], [
            'backup_date'   => [
                'required'  => 'Harus di isi...'
            ],
            'backup_start'  => [
                'required'  => 'Harus di isi...'
            ],
            'backup_end'    => [
                'required'  => 'Harus di isi...'
            ],
            'backup_type'   => [
                'required'  => 'Harus di isi...'
            ],
            'backup_month'   => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed edit data backup !</strong>');
            return redirect()->to('/backup/editData/' . $id)->withInput()->with('validation', $validation);
        }

        // checkk duls
        $check = $this->M_View->checkBackup($backupMonth, $backupType);

        if($check != null) {
            // jika data sama persis sm old data its oke
            if(
                $getOldData['backup_date'] == $backupDate AND
                $getOldData['backup_start'] == $backupStart AND
                $getOldData['backup_end'] == $backupEnd AND
                $getOldData['backup_month'] == $backupMonth AND
                $getOldData['backup_type'] == $backupType AND
                $getOldData['backup_comment'] == $backupComment
            ) {
                // masukan ke database
                $this->M_View->editBackup($id, $backupDate, $backupStart, $backupEnd, $backupMonth, $backupType, $backupComment);

                // pesan sukses
                $session->setFlashdata('success_message', '<strong>Successfully update existing backup data ! </strong>');
                return redirect()->to("/backup");
            } else {
                $validation = \Config\Services::validation();
                $session->setFlashdata('error_message', '<strong>Already exists !</strong>');
                return redirect()->to('/backup/editData/' . $id)->withInput()->with('validation', $validation);
            }
        } else {
            // masukan ke database
            $this->M_View->editBackup($id, $backupDate, $backupStart, $backupEnd, $backupMonth, $backupType, $backupComment);

            // pesan sukses
            $session->setFlashdata('success_message', '<strong>Successfully update existing backup data ! </strong>');
            return redirect()->to("/backup");
        }
    }

    public function importData() {
        $session = session();
        $file = $this->request->getFile('importData');
        $filename = $file->getName();
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('/Backup')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }
				
                // validasi duplicate
                $check = $this->M_View->checkBackup($kolom['1'], $kolom['0']);

                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null && $kolom['4'] == null) {
                    continue;
                }

                // format tanggal harus bener YYYY-MM-DD
                if(!preg_match("/^((((19|[2-9]\d)\d{2})\-(0[13578]|1[02])\-(0[1-9]|[12]\d|3[01]))|(((19|[2-9]\d)\d{2})\-(0[13456789]|1[012])\-(0[1-9]|[12]\d|30))|(((19|[2-9]\d)\d{2})\-02\-(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))\-02\-29))$/", $kolom['0'])) {
                    $log['failed'] += 1;
                    continue;
                }

                if($check != null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } elseif($kolom['0'] == null || $kolom['1'] == null || $kolom['2'] == null || $kolom['3'] == null || $kolom['4'] == null) {
                    $log['failed'] += 1;
                    continue;
                } else {

                    $date       = $kolom['0'];
                    $sequence   = $kolom['1'];
                    $sizeBefore = $kolom['2'];
                    $sizeAfter  = $kolom['3'];
                    $type       = $kolom['4'];
                    $comment    = $kolom['5'];
                            
                    // masukan ke database
                    $this->M_View->importBackup($date, $sequence, $sizeBefore, $sizeAfter, $type, $comment);
                    $log['success'] += 1;
                }
            }  

            $session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('/Backup');
        }
    }

    public function templateImportData() {
        // dd('masuk download template preventive');
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Date*');
        $sheet->setCellValue('B1', 'Sequence*');
        $sheet->setCellValue('C1', 'Size Before*');
        $sheet->setCellValue('D1', 'Size After*');
        $sheet->setCellValue('E1', 'Type*');
        $sheet->setCellValue('F1', 'Comment');

        $sheet->setCellValue('A2', '2022-03-15');
        $sheet->setCellValue('B2', '2');
        $sheet->setCellValue('C2', '3.5 TB');
        $sheet->setCellValue('D2', '4 TB');
        $sheet->setCellValue('E2', '1');
        $sheet->setCellValue('F2', 'comment');

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import backup';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function exportData() {
        $session = session();

        // get data
        $fromYear   = esc($this->request->getVar('exportData3'));

        $date = date('j M Y');

        // rule
        if($fromYear == null) {
            $dataTemplate = $this->M_View->getAllDataBackup();
            $fileName = 'All data backup - ' . $date;
        } elseif($fromYear != null) {
            $dataTemplate = $this->M_View->getAllDataBackupByYear($fromYear);
            $fileName = 'All data backup by year ' . $fromYear . ' - ' . $date;
        } else {
            $session->setFlashdata('error_message', '<strong>Failed !!</strong> wrong rule ! Please read the rule and try again...');
            return redirect()->to('backup');
        }

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();


        $sheet->setCellValue('A1', 'Date');
        $sheet->setCellValue('B1', 'Sequence');
        $sheet->setCellValue('C1', 'Size Before');
        $sheet->setCellValue('D1', 'Size After');
        $sheet->setCellValue('E1', 'Type');
        $sheet->setCellValue('F1', 'Comment');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['backup_date']);
            $sheet->setCellValue('B' . $rows, $dt['backup_month']);
            $sheet->setCellValue('C' . $rows, $dt['backup_start']);
            $sheet->setCellValue('D' . $rows, $dt['backup_end']);
            $sheet->setCellValue('E' . $rows, $dt['backup_type']);
            $sheet->setCellValue('F' . $rows, $dt['backup_comment']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
