<?php

namespace App\Controllers;

use \App\Models\M_View;

class SupportSystem extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
        helper('trial');
        $this->email = \Config\Services::email();
        date_default_timezone_set("Asia/Jakarta");
    }

    public function index() {

        loggedIn();

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu' => $menu,
            'daily' => $this->M_View->getAllDaily(),
            'validation' => \Config\Services::validation(),
			'type' => $this->M_View->getAllAssetType(),
			'status' => $this->M_View->getAllStatus(),
			'lastUser' => $lastUser,
            'position' => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender' => $this->M_View->getAllGender(),
            'use' => $this->M_View->getAllAccessUse(),
            'deskera' => $this->M_View->getAllAccessDeskera(),
            'emailUse' => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it' => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept' => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level' => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
            'supportSystem' => $this->M_View->getAllSupport(),
            "test" => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('supportSystem', $data);
    }

    public function deleteData() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteSupportSystem($deleteId);
            $session->setFlashdata('success_message', 'Success delete support system ! ');
            return redirect()->to('/supportSystem');
        } else {
            $session->setFlashdata('error_message', 'Failed delete support system ! ');
            return redirect()->to('/supportSystem');
        }
    }

    public function sendEmail() {
        
        $session = session();

        $this->email->setFrom('dimasokva@gmail.com', 'Dimas Okva');
        $this->email->setTo('dosolichin@ptpacificfurniture.id');
        // $this->email->setCC('another@another-example.com');
        // $this->email->setBCC('them@their-example.com');

        $this->email->setSubject('Email Test');
        $this->email->setMessage('http://192.168.0.202/asset');

        if (!$this->email->send())
        {
            // Generate error
            $session->setFlashdata('error_message', 'Failed send email !');
            return redirect()->to('/dashboard');
            $this->email->printDebugger(['headers']);
        } else {
            return redirect()->to('/SupportSystem');
        }
        
        
    }

} 
