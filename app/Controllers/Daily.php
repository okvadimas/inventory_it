<?php

namespace App\Controllers;

use \App\Models\M_View;

class Daily extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
        helper('trial');
    }

    public function index() {

        loggedIn();

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu'      => $menu,
            'daily'     => $this->M_View->getAllDaily(),
            'validation' => \Config\Services::validation(),
			'type'      => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'       => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'        => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'      => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level'     => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
            "test"      => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('daily', $data);
    }

    public function insertData() {

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        // ambil data dari web
        $user_id = $session->get('user_id');
        $date = $this->request->getVar('date_daily');
        $task = esc($this->request->getVar('task_daily'));

        // validate
        if(!$this->validate([
            "date_daily"    => 'required',
            'task_daily'    => 'required'
        ], [
            'date_daily' => [
                'required'  => 'Harus di isi...'
            ],
            'task_daily'    => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert new daily data !');
            return redirect()->to('/daily')->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->insertDataDaily($user_id, $date, $task);

        // pesan sukses
        $session->setFlashdata('success_message', 'Success insert new daily! ');
        return redirect()->to('/daily');
    }

    public function deleteData() {
        $delete_id = $this->request->getVar('deleteId');
        $session = session();

        if(isset($delete_id)) {
            $this->M_View->deleteDataDaily($delete_id);
            $session->setFlashdata('success_message', 'Success delete data daily! ');
            return redirect()->to('/daily');
        } else {
            $session->setFlashdata('error_message', 'Failed delete data daily! ');
            return redirect()->to('/daily');
        }
    }

    public function editData($id) {
        $session = session();

        $getOldData = $this->M_View->getEditDaily($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_daily', $data);
    }

    public function doEditData($id) {
        $session = session();

        // ambil data
        $dailyDate  = esc($this->request->getVar('daily_date'));
        $dailyTask  = esc($this->request->getVar('daily_task'));
        $id         = $id;
        $getOldData = $this->M_View->getEditDaily($id);
        
        // validasi
        if($getOldData['daily_date'] === $dailyDate) {
            $rule = 'required';
        } else {
            $rule = 'required|is_unique[daily.daily_date]';
        }

        if(!$this->validate([
            'daily_date'    => $rule,
            'daily_task'    => 'required'
        ], [
            'daily_date' => [
                'is_unique' => 'Sudah ada boss !',
                'required'  => 'Harus di isi...'
            ],
            'daily_task'    => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data !');
            return redirect()->to("/Daily/editData" . '/' . $id)->withInput()->with('validation', $validation);
        }
		
        // masukan ke database
		$this->M_View->editDataDaily($id, $dailyDate, $dailyTask);

        // pesan sukses
        $session->setFlashdata('success_message', "Successfully update existing data ! ");
        return redirect()->to("/Daily");

    }
} 
