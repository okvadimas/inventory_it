<?php

namespace App\Controllers;

use \App\Models\M_View;

class MenuManagement extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {
        
        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu'  => $menu,
            'user'  => $this->M_View->getAllUserManagement(),
            'validation' => \Config\Services::validation(),
			'type'      => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'   => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'    => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'  => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level' => $this->M_View->getAllUserLevel(),
            "test"  => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('menuManagement', $data);
    }

    public function insertData() {

        $session = session();

        // ambil data dari web
        $name   = esc($this->request->getVar('name'));
        $icon   = esc($this->request->getVar('icon'));
        $url    = esc($this->request->getVar('url'));
        $sort   = esc($this->request->getVar('sort'));

        // validasi data dari web bener atau ngga
        if(!$this->validate([
            'name' => 'required|is_unique[menu.menu_name]',
            'icon' => 'required',
            'url'  => 'required',
            'sort' => 'required'
        ],
        [   // Errors
            'name' => [
                'is_unique' => 'Sudah ada menu itu boss..',
                'required'  => 'Harus di isi...'
            ],
            'icon'  => [
                'required'  => 'Harus di isi...'
            ],
            'url'   => [
                'required'  => 'Harus di isi...'
            ],
            'sort'  => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            session()->setFlashdata('error_message', 'Failed add new menu! ');
            return redirect()->to('/menuManagement')->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->insertDataMenuManagement($name, $icon, $url, $sort);

        // pesan sukses
        $session->setFlashdata('success_message', 'Success insert menu! ');
        return redirect()->to('/menuManagement');
    }

    public function deleteData() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataMenuManagement($deleteId);
            $session->setFlashdata('success_message', 'Success delete menu! ');
            return redirect()->to('/menuManagement');
        } else {
            $session->setFlashdata('error_message', 'Failed delete menu! ');
            return redirect()->to('/menuManagement');
        }
    }

    public function editData($id) {
        $session = session();
        $getOldData = $this->M_View->getEditMenuManagement($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_menuManagement', $data);
    }

    public function doEditData($id) {
        $session = session();

        // ambil data
        $menuName = esc($this->request->getVar('menu_name'));
        $menuIcon = esc($this->request->getVar('menu_icon'));
        $menuUrl = esc($this->request->getVar('menu_url'));
        $menuSort = esc($this->request->getVar('sort'));
        $id = $id;
        $getOldData = $this->M_View->getEditMenuManagement($id);

        // validasi
        if($getOldData["menu_name"] == $menuName) {
            $rule1 = 'required';
        } else {
            $rule1 = 'required|is_unique[menu.menu_name]';
        }

        if($getOldData['menu_url'] == $menuUrl) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[menu.menu_url]';
        }

        if($getOldData['menu_icon'] == $menuIcon) {
            $rule3 = 'required';
        } else {
            $rule3 = 'required|is_unique[menu.menu_icon]';
        }

        if(!$this->validate([
            'menu_name' => $rule1,
            'menu_url'  => $rule2,
            'menu_icon' => $rule3,
        ], [
            'menu_name' => [
                'is_unique' => "Sudah ada boss !",
                'required'  => 'Harus di isi...'
            ],
            'menu_url' => [
                'is_unique' => "Sudah ada boss !",
                'required'  => 'Harus di isi...'
            ],
            'menu_icon' => [
                'is_unique' => 'Sudah ada cuy...',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data !');
            return redirect()->to("/MenuManagement/editData" . '/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editMenuManagement($menuName, $menuIcon, $menuUrl, $menuSort, $id);

        // pesan sukses
        $session->setFlashdata('success_message', 'Successfully update existing data ! ');
        return redirect()->to('/MenuManagement');
    }
}
