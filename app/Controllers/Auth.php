<?php

namespace App\Controllers;

use \App\Models\M_View;

class Auth extends BaseController
{ 
	public function __construct() {
		$this->M_View = new M_View();
        date_default_timezone_set("Asia/Jakarta");
        $this->googleClient = new \Google_Client();
		$this->googleClient->setClientId("1057440331285-78lucu14dkop9f1tl373j194c3em7le8.apps.googleusercontent.com");
		$this->googleClient->setClientSecret("xbcwQ6moGF-vKvzGpYRFme3D");
		$this->googleClient->setRedirectUri("http://localhost/auth/googleauth");
		$this->googleClient->addScope("email");
		$this->googleClient->addScope("profile");
	}

	public function index()
	{
        $googleButton = '<a href="'.$this->googleClient->createAuthUrl().'">Sign In with Google</a>';
        $data = [
            'title' => 'Login Page',
            'google' => $googleButton,
        ];

        return view('login', $data);
	}

    public function googleauth() {
        $session = session();

        $token = $this->googleClient->fetchAccessTokenWithAuthCode($this->request->getVar("code"));
        if(!isset($token['error'])) {
            $this->googleClient->setAccessToken($token['access_token']);
            session()->set('access_token', $token['access_token']);

            $googleService = new \Google_Service_Oauth2($this->googleClient);
            $dataGoogle = $googleService->userinfo->get();
            // dd($dataGoogle);

            // cek data id sudah ada apa belum di database
            // jika sudah ada dan sudah login atau mau login ulang, pakai function update data yg baru 
            if($this->M_View->checkIdGoogle($dataGoogle['id']) !== null) {
                $userData = [
                    'login' => True,
                    'level' => 1,
                    'emailGoogle' => $dataGoogle['email'],
                    'usernameGoogle' => $dataGoogle['name'],
                    'familyNameGoogle' => $dataGoogle['familyName'],
                    'idGoogle' => $dataGoogle['id'],
                    'pictureGoogle' => $dataGoogle['picture'],
                    'genderGoogle' => $dataGoogle['gender'],
                    'verifiedEmailGoogle' => $dataGoogle['verifiedEmail'],
                ];

                // dd('update');
                $this->M_View->updateDataGoogle($dataGoogle['email'], $dataGoogle['name'], $dataGoogle['familyName'], $dataGoogle['id'], $dataGoogle['picture'], $dataGoogle['gender'], $dataGoogle['verifiedEmail']);
                $session->set($userData);

            } else {
                // jika belum ada atau pengguna baru maka pakai function insert dataGoogle baru ke database
                $userData = [
                    'login'  => True,
                    'level' => 1,
                    'emailGoogle' => $dataGoogle['email'],
                    'usernameGoogle' => $dataGoogle['name'],
                    'familyNameGoogle' => $dataGoogle['familyName'],
                    'idGoogle' => $dataGoogle['id'],
                    'pictureGoogle' => $dataGoogle['picture'],
                    'genderGoogle' => $dataGoogle['gender'],
                    'verifiedEmailGoogle' => $dataGoogle['verifiedEmail'],
                ];
                // dd('insert');
                $this->M_View->insertDataGoogle($dataGoogle['email'], $dataGoogle['name'], $dataGoogle['familyName'], $dataGoogle['id'], $dataGoogle['picture'], $dataGoogle['gender'], $dataGoogle['verifiedEmail']);
                $session->set($userData);

            }

            $jam = intval(date('H'));
            if($jam <= 10) {
                $pesan = ["Ohayogozaimasu ", "Good Morning ", "Irasshaimase ", "Selamat Pagi "];
                $urutan = array_rand($pesan, 1);
            } elseif($jam > 10 && $jam <= 1) {
                $pesan = ["Irasshaimase ", "Kon'nichiwa ", "Good Afternoon ", "Selamat Siang "];
                $urutan = array_rand($pesan, 1);
            } elseif($jam >= 2 && $jam < 6) {
                $pesan = ["Konbanwa ", "Good Evening ", "Selamat Sore ", "Irasshaimase "];
                $urutan = array_rand($pesan, 1);
            } else {
                $pesan = ['Irasshaimase '];
                $urutan = array_rand($pesan, 1);
            }

            // dd($token['error']);

            // set pesan
            session()->setFlashdata('success_login', $pesan[$urutan] . " " . $dataGoogle['name'] . '-san. Otsukaresama desu 😊');
            return redirect()->to('/');

        } else {
            session()->setFlashdata('error_message', 'Something Went Wrong! ');
            return redirect()->to('/auth');
        }
    }

    public function Auth() {

        $session = session();

        // terima inputan dari web
        $userAuth = $this->request->getVar('userAuth');
        $passAuth = $this->request->getVar('passAuth');
        // cek di database ada apa ngga
        $user = $this->M_View->getUser($userAuth);
        if($user != null) {
        // jika ada cek password apakah benar
            if(password_verify($passAuth, $user['user_pass'])) {
                // set session 
                $user = $this->M_View->getUser($userAuth);
                $sessionUser = [
                    'login'     => true,
                    'user_id'   => $user['user_id'],
                    'username'  => $user['user_username'],
                    'email'     => $user['user_email'],
                    'fullname'  => $user['user_fullname'],
                    'birth'     => $user['user_birth'],
                    'dept'      => $user['user_dept'],
                    'phone'     => $user['user_phone'],
                    'level'     => $user['user_level'],
                    'create'    => $user['user_create'],
                    'picture'   => $user['user_picture'],
                    'status'    => $user['user_status'],
                    'asset_id'  => $user['user_asset_id']
                ]; 

                // session tidak pakai timer
                //\Config\App:sessExpiration = 0;
                $session->set($sessionUser);
                // report daily j hari f bulan Y tahun
                $dateDaily = intval(date('j'));
                $monthDaily = intval(date('m'));
                $yearDaily = intval(date('Y'));

                // test
                // $dateDaily = 10;
                // $monthDaily = 8;
                // $yearDaily = 2022;

                if($this->M_View->getLastDateDailyReport() != null) {
                    $LastDateDailyReport = $this->M_View->getLastDateDailyReport();
                    if($dateDaily == $LastDateDailyReport['report_date'] && $monthDaily == $LastDateDailyReport['report_month'] && $yearDaily == $LastDateDailyReport['report_year']) {
                        $LastCountDailyReport = $this->M_View->getLastCountDailyReport();
                        $currentCountDaily = $LastCountDailyReport['report_total'];
                        $newCountDaily = $currentCountDaily + 1;
                        $this->M_View->updateReportDaily($dateDaily, $monthDaily, $yearDaily, $newCountDaily);
                    } else {
                        $countDaily = 1;
                        $this->M_View->insertReportDaily($dateDaily, $monthDaily, $yearDaily, $countDaily);
                    }
                } else {
                    $countDaily = 1;
                    $this->M_View->insertReportDaily($dateDaily, $monthDaily, $yearDaily, $countDaily);
                }
				
				$jam = intval(date('H'));
                if($jam <= 10) {
                    $pesan = ["Ohayogozaimasu ", "Good Morning ", "Irasshaimase ", "Selamat Pagi "];
                    $urutan = array_rand($pesan, 1);
                } elseif($jam > 10 && $jam <= 1) {
                    $pesan = ["Irasshaimase ", "Kon'nichiwa ", "Good Afternoon ", "Selamat Siang "];
                    $urutan = array_rand($pesan, 1);
                } elseif($jam >= 2 && $jam < 6) {
                    $pesan = ["Konbanwa ", "Good Evening ", "Selamat Sore ", "Irasshaimase "];
                    $urutan = array_rand($pesan, 1);
                } else {
                    $pesan = ['Irasshaimase '];
                    $urutan = array_rand($pesan, 1);
                }

                // set pesan
                $session->setFlashdata('success_login', $pesan[$urutan] . " " . $userAuth . '-san. Otsukaresama desu 😊');
                return redirect()->to('/');
            } else {
                // set pesan
                $session->setFlashdata('error_message', 'Wrong Password!');
                return redirect()->to('/auth');
            }
        } else {
            // set pesan
            $session->setFlashdata('error_message', 'User not found!');
            return redirect()->to('/auth');
        }
         
    }

    public function signUp() {

        $data = [
            'title' => 'Sign Up Page',
            'dept' => $this->M_View->getAllDept(),
            'level' => $this->M_View->getAllUserLevel(),
        ];

        return view('register', $data);
    }

    public function doSignUp() {

        $session = session();

        // validasi data dari web bener atau ngga
        if(!$this->validate([
            'userReg' => 'required|is_unique[user.user_username]',
            'emailReg' => 'required|valid_email'
        ])) {
            $validation = \Config\Services::validation();
            session()->setFlashdata('error_message', 'User already exist!');
            return redirect()->to('/auth/signup')->withInput()->with('validation', $validation);
        }

        if(!$this->validate([
            'pass1Reg' => 'required',
            'pass2Reg' => 'required|matches[pass1Reg]'
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', "Password doesn't match!");
            return redirect()->to('/auth/signup')->withInput()->with('validation', $validation);
        }
        // ambil data dari web
        $userReg = $this->request->getVar('userReg');
        $emailReg = $this->request->getVar('emailReg');
        $passReg = $this->request->getVar('pass2Reg');
        $dept = $this->request->getVar('dept');
        $asset_id = $this->request->getVar('identity');

        $level = $this->request->getVar('level');
        $create = date('Y-m-d H:i:s');
        $status = 2;

        // enkripsi password
        $passReg = password_hash($passReg, PASSWORD_DEFAULT);

        // import to database
        $this->M_View->addUser($userReg, $passReg, $emailReg, $level, $create, $status, $dept, $asset_id);

        // set pesan
        $session->setFlashdata('success_message', 'Success created new account!');
        return redirect()->to('/auth');
    }

    public function signOut() {
        
        $session = session();

        $session->destroy();

        $session->setFlashdata('success_message', 'Success Sign Out! ');
        return redirect()->to('/auth');
    }
}

