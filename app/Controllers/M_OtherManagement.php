<?php

namespace App\Controllers;

use \App\Models\M_View;

class M_OtherManagement extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {
        
        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu'  => $menu,
            'user'  => $this->M_View->getAllUserManagement(),
            'validation' => \Config\Services::validation(),
			'type'  => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'   => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'    => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'  => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level'     => $this->M_View->getAllUserLevel(),
            "test"      => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('m_otherManagement', $data);
    }

    public function insertDataDeskera() {
        
        $session = session();

        // ambil data dari web
        $id     = esc($this->request->getVar('identityDeskera'));
        $name   = esc($this->request->getVar('nameDeskera'));

        // validasi
        if(!$this->validate([
            'identityDeskera'   => 'required|is_unique[m_access_deskera.id]',
            'nameDeskera'       => 'required|is_unique[m_access_deskera.name]',
        ], [
            'identityDeskera'   => [
                'is_unique'     => 'Sudah ada cuy... ! ',
                'required'      => 'Harus di isi...'
            ],
            'nameDeskera'   => [
                'is_unique' => 'Sudah ada cuy... !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert new Deskera !');
            return redirect()->to('/m_otherManagement')->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->insertDataDeskera($id, $name);

        // pesan sukses
        $session->setFlashdata('success_message', "Success insert new deskera !");
        return redirect()->to('/m_otherManagement');
    }

    public function editDataDeskera($id) {
        $session = session();
        $getOldData = $this->M_View->getEditDataDeskera($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_deskera', $data);
    }

    public function doEditDataDeskera($id) {
        $session = session();
        
        // ambil data
        $identity = esc($this->request->getVar('identityDeskera'));
        $name = esc($this->request->getVar('nameDeskera'));
        $id = $id;
        $getOldData = $this->M_View->getEditDataDeskera($id);

        // validasi
        if($getOldData['id'] == $identity) {
            $rule1 = 'required';
        } else {
            $rule1 = 'required|is_unique[m_access_deskera.id]';
        }

        if($getOldData['name'] == $name) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[m_access_deskera.name]';
        }

        if(!$this->validate([
            'identityDeskera' => $rule1,
            'nameDeskera' => $rule2
        ], [
            'identityDeskera' => [
                'is_unique' => "Sudah ada cuy !",
                'required'  => 'Harus di isi...'
            ],
            'nameDeskera' => [
                'is_unique' => "Sudah ada cuy !",
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata("error_message", 'Failed update existing data !');
            return redirect()->to("/M_OtherManagement/editDataDeskera" . '/' . $id)->withInput()->with("validation", $validation);
        }

        // masukan ke database
        $this->M_View->editDeskera($id, $identity, $name);

        // pesan sukses
        $session->setFlashdata('success_message', 'Successfully update existing data ! ');
        return redirect()->to("/M_OtherManagement");
    }

    public function deleteDataDeskera() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataDeskera($deleteId);
            $session->setFlashdata('success_message', 'Success delete data deskera ! ');
            return redirect()->to('/m_otherManagement');
        } else {
            $session->setFlashdata('error_mesage', 'Failed delete data deskera ! ');
            return redirect()->to('/m_otherManagement');
        }
    }

    public function insertDataAccessUse() {
                
        $session = session();

        // ambil data dari web
        $id     = esc($this->request->getVar('identityAccessUse'));
        $name   = esc($this->request->getVar('nameAccessUse'));

        // validasi
        if(!$this->validate([
            'identityAccessUse' => 'required|is_unique[m_access_use.id]',
            'nameAccessUse'     => 'required|is_unique[m_access_use.name]',
        ], [
            'identityAccessUse' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ],
            'nameAccessUse' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert new access use ! ');
            return redirect()->to('/m_otherManagement')->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->insertDataAccessUse($id, $name);

        // pesan sukses
        $session->setFlashdata('success_message', "Success insert new access use ! ");
        return redirect()->to('/m_otherManagement');
    }

    public function editAccessUse($id) {
        $session = session();
        $getOldData = $this->M_View->getEditAccessUse($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_accessUse', $data);
    }

    public function doEditAccessUse($id) {
        $session = session();

        // ambil data
        $id = $id;
        $identity   = esc($this->request->getVar("identityAccessUse"));
        $name       = esc($this->request->getVar("nameAccessUse"));
        $getOldData = $this->M_View->getEditAccessUse($id);

        // validasi
        if($getOldData['id'] == $identity) {
            $rule1 = 'required';
        } else {
            $rule1 = 'required|is_unique[m_access_use.id]';
        }

        if($getOldData['name'] == $name) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[m_access_use.name]';
        }

        if(!$this->validate([
            'identityAccessUse' => $rule1,
            'nameAccessUse'     => $rule2
        ], [
            'identityAccessUse' => [
                'is_unique' => "Sudah ada cuy !",
                'required'  => 'Harus di isi...'
            ],
            'nameAccessUse' => [
                'is_unique' => "Sudah ada cuy !",
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', "Failed update existing data ! ");
            return redirect()->to('/M_OtherManagement/editAccessUse' . '/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editAccessUse($id, $identity, $name);

        // pesan sukses
        $session->setFlashdata("success_message", "Successfully update exiting data ! ");
        return redirect()->to("M_OtherManagement");
    }

    public function deleteDataAccessUse() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataAccessUse($deleteId);
            $session->setFlashdata('success_message', 'Success delete data access use ! ');
            return redirect()->to('/m_otherManagement');
        } else {
            $session->setFlashdata('error_mesage', 'Failed delete data access use ! ');
            return redirect()->to('/m_otherManagement'); 
        }           
    }

    public function insertDataCondition() {

        $session = session();

        // ambil data dari web
        $id     = esc($this->request->getVar('identityCondition'));
        $name   = esc($this->request->getVar('nameCondition'));

        // validasi
        if(!$this->validate([
            'identityCondition' => 'required|is_unique[m_condition.id]',
            'nameCondition'     => 'required|is_unique[m_condition.name]'
        ], [
            'identityCondition' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ],
            'nameCondition' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert new condition ! ');
            return redirect()->to('/m_otherManagement')->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->insertDataCondition($id, $name);

        // pesan sukses
        $session->setFlashdata('success_message', 'Success insert new condition ! ');
        return redirect()->to('/m_otherManagement');
    }

    public function editCondition($id) {
        $session = session();
        $getOldData = $this->M_View->getEditCondition($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_condition', $data);
    }

    public function doEditCondition($id) {
        $session = session();

        // ambil data
        $id = $id;
        $identity   = esc($this->request->getVar("identityCondition"));
        $name       = esc($this->request->getVar("nameCondition"));
        $getOldData = $this->M_View->getEditCondition($id);

        // validasi
        if($getOldData['id'] == $identity) {
            $rule1 = "required";
        } else {
            $rule1 = 'required|is_unique[m_condition.id]';
        }

        if($getOldData['name'] == $name) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[m_condition.name]';
        }

        if(!$this->validate([
            'identityCondition' => $rule1,
            'nameCondition'     => $rule2
        ], [
            'identityCondition' => [
                'is_unique' => "Sudah ada cuy !",
                'required'  => 'Harus di isi...'
            ],
            'nameCondition' => [
                'is_unique' => "Sudah ada cuy !",
                'required'  => 'Harus di isi... '
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', "Failed update existing data ! ");
            return redirect()->to("/M_OtherManagement/editCondition" . '/' . $id)->withInput()->with("validation", $validation);
        }

        // masukan ke database
        $this->M_View->editCondition($id, $identity, $name);

        // pesan sukses
        $session->setFlashdata("success_message", 'Successfully update existing data ! ');
        return redirect()->to("/M_OtherManagement");

    }

    public function deleteDataCondition() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        // masukan ke database
        if(isset($deleteId)) {
            $this->M_View->deleteDataCondition($deleteId);
            $session->setFlashdata('success_message', 'Success delete data condition ! ');
            return redirect()->to('/m_otherManagement');
        } else {
            $session->setFlashdata('error_message', 'Failed delete data condition ! ');
            return redirect()->to('/m_otherManagement');
        }
    }

    public function insertDataEmailUse() {

        $session = session();

        // ambil data dari web
        $id     = esc($this->request->getVar('identityEmailUse'));
        $name   = esc($this->request->getVar('nameEmailUse'));

        // validasi
        if(!$this->validate([
            'identityEmailUse'  => 'required|is_unique[m_email_use.id]',
            'nameEmailUse'      => 'required|is_unique[m_email_use.name]'
        ], [
            'identityEmailUse' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ], 
            'nameEmailUse' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert new email use ! ');
            return redirect()->to('/m_otherManagement')->withInput()->with('validation', $validation);
        } 

        // masukan ke database
        $this->M_View->insertDataEmailUse($id, $name);

        // pesan sukses
        $session->setFlashdata('success_message', 'Success insert new email use ! ');
        return redirect()->to('/m_otherManagement');
    }

    public function editEmailUse($id) {
        $session = session();
        $getOldData = $this->M_View->getEditEmailUse($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_emailUse', $data);
    }

    public function doEditEmailUse($id) {
        $session = session();

        // ambil data
        $id = $id;
        $identity = esc($this->request->getVar("identityEmailUse"));
        $name = esc($this->request->getVar("nameEmailUse"));
        $getOldData = $this->M_View->getEditEmailUse($id);

        // validasi
        if($getOldData['id'] == $identity) {
            $rule1 = "required";
        } else {
            $rule1 = "required|is_unique[m_email_use.id]";
        }

        if($getOldData['name'] == $name) {
            $rule2 = 'required';
        } else {
            $rule2 = "required|is_unique[m_email_use.name]";
        }

        if(!$this->validate([
            'identityEmailUse' => $rule1,
            'nameEmailUse' => $rule2
        ], [
            'identityEmailUse' => [
                'is_unique' => "Sudah ada cuy !",
                'required'  => 'Harus di isi...'
            ],
            'nameEmailUse' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data ! ');
            return redirect()->to("/M_OtherManagement/editEmailUse" . '/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editEmailUse($id, $identity, $name);

        // pesan sukses
        $session->setFlashdata('success_message', 'Successfully update existing data ! ');
        return redirect()->to("/M_OtherManagement");

    }

    public function deleteDataEmailUse() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataEmailUse($deleteId);
            $session->setFlashdata('success_message', 'Success delete data email use ! ');
            return redirect()->to('/m_otherManagement');
        } else {
            $session->setFlashdata('error_message', 'Failed delete data email use ! ');
            return redirect()->to('/m_otherManagement');
        }
    }

    public function insertDataOperatingSystem() {

        $session = session();

        // ambil data dari web
        $id     = esc($this->request->getVar('identityOperatingSystem'));
        $name   = esc($this->request->getVar('nameOperatingSystem'));

        // validasi
        if(!$this->validate([
            'identityOperatingSystem' => 'required|is_unique[m_operating_system.id]',
            'nameOperatingSystem' => 'required|is_unique[m_operating_system.name]'
        ], [
            'identityOperatingSystem' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ], 
            'nameOperatingSystem' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert new operating system ! ');
            return redirect()->to('/m_otherManagement')->withInput()->with('validation', $validation);
        } 

        // masukan ke database
        $this->M_View->insertDataOperatingSystem($id, $name);

        // pesan sukses
        $session->setFlashdata('success_message', 'Success insert new operating system ! ');
        return redirect()->to('/m_otherManagement');
    }

    public function editOperatingSystem($id) {
        $session = session();
        $getOldData = $this->M_View->getEditOperatingSystem($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_operatingSystem', $data);
    }

    public function doEditOperatingSystem($id) {
        $session = session();

        // ambil data
        $id = $id;
        $identity   = esc($this->request->getVar("identityOperatingSystem"));
        $name       = esc($this->request->getVar("nameOperatingSystem"));
        $getOldData = $this->M_View->getEditOperatingSystem($id);

        // validasi
        if($getOldData['id'] == $identity) {
            $rule1 = "required";
        } else {
            $rule1 = "required|is_unique[m_operating_system.id]";
        }

        if($getOldData['name'] == $name) {
            $rule2 = 'required';
        } else {
            $rule2 = "required|is_unique[m_operating_system.name]";
        }

        if(!$this->validate([
            'identityOperatingSystem' => $rule1,
            'nameOperatingSystem' => $rule2
        ], [
            'identityOperatingSystem' => [
                'is_unique' => "Sudah ada cuy !",
                'required'  => 'Harus di isi...'
            ],
            'nameOperatingSystem' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data ! ');
            return redirect()->to("/M_OtherManagement/editOperatingSystem" . '/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editOperatingSystem($id, $identity, $name);

        // pesan sukses
        $session->setFlashdata('success_message', 'Successfully update existing data ! ');
        return redirect()->to("/M_OtherManagement");

    }

    public function deleteDataOperatingSystem() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataOperatingSystem($deleteId);
            $session->setFlashdata('success_message', 'Success delete data operating system ! ');
            return redirect()->to('/m_otherManagement');
        } else {
            $session->setFlashdata('error_message', 'Failed delete data operating system ! ');
            return redirect()->to('/m_otherManagement');
        }
    }

    public function insertDataGender() {

        $session = session();

        // ambil data dari web
        $id     = esc($this->request->getVar('identityGender'));
        $name   = esc($this->request->getVar('nameGender'));

        // validate
        if(!$this->validate([
            'identityGender' => 'required|is_unique[m_gender.id]',
            'nameGender'     => 'required|is_unique[m_gender.name]'
        ], [
            'identityGender' => [
                'is_unique'  => 'Sudah ada cuy !',
                'required'   => 'Harus di isi...'
            ], 
            'nameGender' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert new gender ! ');
            return redirect()->to('/m_otherManagement')->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->insertDataGender($id, $name);

        // pesan sukses
        $session->setFlashdata('success_message', 'Success insert data gender ! ');
        return redirect()->to('/m_otherManagement');
    }

    public function editDataGender($id) {
        $session = session();
        $getOldData = $this->M_View->getEditGender($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_gender', $data);
    }

    public function doEditDataGender($id) {
        $session = session();

        // ambil data
        $id = $id;
        $identity   = esc($this->request->getVar("identityGender"));
        $name       = esc($this->request->getVar("nameGender"));
        $getOldData = $this->M_View->getEditGender($id);

        // validasi
        if($getOldData['id'] == $identity) {
            $rule1 = 'required';
        } else {
            $rule1 = 'required|is_unique[m_gender.id]';
        }

        if($getOldData['name'] == $name) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[m_gender.name]';
        }

        if(!$this->validate([
            'identityGender' => $rule1,
            'nameGender'     => $rule2
        ], [
            'identityGender' => [
                'is_unique'  => "Sudah ada cuy !",
                'required'   => 'Harus di isi...'
            ],
            'nameGender' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data ! ');
            return redirect()->to("/M_OtherManagement/editDataGender" . '/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editDataGender($id, $identity, $name);

        // pesan sukses
        $session->setFlashdata('success_message', 'Successfully update existing data ! ');
        return redirect()->to("/M_OtherManagement");
    }

    public function deleteDataGender() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataGender($deleteId);
            $session->setFlashdata("success_message", 'Success delete data gender ! ');
            return redirect()->to('/m_otherManagement');
        } else {
            $session->setFlashdata("error_message", 'Failed delete data gender ! ');
            return redirect()->to('/m_otherManagement');
        }
    }

    public function insertDataPosition() {

        $session = session();

        // ambil data dari web
        $id     = esc($this->request->getVar('identityPosition'));
        $name   = esc($this->request->getVar('namePosition'));

        // validasi
        if(!$this->validate([
            'identityPosition' => 'required|is_unique[m_position.id]',
            'namePosition' => 'required|is_unique[m_position.name]'
        ], [
            'identityPosition' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ], 
            'namePosition' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert new position ! ');
            return redirect()->to('/m_otherManagement')->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->insertDataPosition($id, $name);

        // pesan sukses
        $session->setFlashdata('success_message', 'Success insert new position ! ');
        return redirect()->to('/m_otherManagement');
    }

    public function deleteDataPosition() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataPosition($deleteId);
            $session->setFlashdata('success_message', 'Success delete data position ! ');
            return redirect()->to('/m_otherManagement');
        } else {
            $session->setFlashdata('error_message', 'Failed delete data position ! ');
            return redirect()->to('/m_otherManagement');
        }
    }

    public function editDataPosition($id) {
        $session = session();
        $getOldData = $this->M_View->getEditPosition($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_position', $data);
    }

    public function doEditPosition($id) {
        $session = session();

        // ambil data
        $id = $id;
        $identity   = esc($this->request->getVar('identityPosition'));
        $name       = esc($this->request->getVar('namePosition'));
        $getOldData = $this->M_View->getEditPosition($id);

        // validasi
        if($getOldData['id'] == $identity) {
            $rule1 = 'required';
        } else {
            $rule1 = 'required|is_unique[m_position.id]';
        }

        if($getOldData['name'] == $name) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[m_position.name]';
        }

        if(!$this->validate([
            'identityPosition' => $rule1,
            'namePosition' => $rule2
        ], [
            'identityPosition' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ],
            'namePosition' => [
                'is_unique' => "Sudah ada cuy !",
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data ! ');
            return redirect()->to("/M_OtherManagement/editDataPosition" . '/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editPosition($id, $identity, $name);

        // pesan sukses
        $session->setFlashdata('success_message', "Successfully update existing data ! ");
        return redirect()->to("/M_OtherManagement");

    }

    public function insertDataStatus() {
        $session = session();

        // ambil data dari web
        $id     = esc($this->request->getVar('identityStatus'));
        $name1  = esc($this->request->getVar('name1Status'));
        $name2  = esc($this->request->getVar('name2Status'));

        // validasi 
        if(!$this->validate([
            'identityStatus' => 'required|is_unique[m_status.id]',
            'name1Status'   => 'required|is_unique[m_status.name1]',
            'name2Status'   => 'required|is_unique[m_status.name2]'
        ], [
            'identityStatus' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ], 
            'name1Status'   => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ],
            'name2Status'   => [
                'is_unique' => 'Sudah ada cuy...',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert data status ! ');
            return redirect()->to('/m_otherManagement')->withInput()->with('validation', $validation);
        } 

        // masukan ke database
        $this->M_View->insertDataStatus($id, $name1, $name2);

        // pesan sukses
        $session->setFlashdata('success_message', 'Success insert new status ! ');
        return redirect()->to('/m_otherManagement');
    }

    public function editStatus($id) {
        $session = session();
        $getOldData = $this->M_View->getEditStatus($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_status', $data);
    }

    public function doEditStatus($id) {
        $session = session();

        // ambil data
        $id = $id;
        $identity   = $this->request->getVar("identityStatus");
        $name1      = $this->request->getVar("nameStatus1");
        $name2      = $this->request->getVar("nameStatus2");
        $getOldData = $this->M_View->getEditStatus($id);

        // validasi
        if($getOldData['id'] == $identity) {
            $rule1 = 'required';
        } else {
            $rule1 = 'required|is_unique[m_status.id]';
        }

        if($getOldData['name1'] == $name1 && $getOldData['name2'] == $name2) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[m_status.name1]|is_unique[m_status.name2]';
        }

        if(!$this->validate([
            'identityStatus' => $rule1,
            'nameStatus1'   => $rule2,
            'nameStatus2'   => $rule2
        ],[
            'identityStatus' => [
                'is_unique'  => 'Sudah ada cuy !',
                'required'   => 'Harus di isi...'
            ],
            'nameStatus1' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ],
            'nameStatus2' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update exisiting data ! ');
            return redirect()->to('/M_OtherManagement/editStatus' . '/' . $id)->withInput()->with("validation", $validation);
        }

        // masukan ke database
        $this->M_View->editStatus($id, $identity, $name1, $name2);

        // pesan sukses
        $session->setFlashdata('success_message', "Successfully update existing data ! ");
        return redirect()->to("/M_OtherManagement");
        
    }
 
    public function deleteDataStatus() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataStatus($deleteId);
            $session->setFlashdata('success_message', 'Success delete new status ! ');
            return redirect()->to('/m_otherManagement');
        } else {
            $session->setFlashdata('error_message', 'Failed delete new status ! ');
            return redirect()->to('/m_otherManagement');
        }
    }

    public function insertDataProjectStatus() {

        $session = session();

        // ambil data dari web
        $id     = esc($this->request->getVar('identityProjectStatus'));
        $name   = esc($this->request->getVar('nameProjectStatus'));
        
        // validasi 
        if(!$this->validate([
            'identityProjectStatus' => 'required|is_unique[m_project_status.id]',
            'nameProjectStatus'     => 'required|is_unique[m_project_status.name]'
        ], [
            'identityProjectStatus' => [
                'is_unique' => 'Sudah ada cuty !',
                'required'  => 'Harus di isi...'
            ], 
            'nameProjectStatus' => [
                'is_unique' => 'Sudah ada cuty !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert data project status ! ');
            return redirect()->to('/m_otherManagement')->withInput()->with('validation', $validation);
        } 

        // masukan ke database
        $this->M_View->insertDataProjectStatus($id, $name);

        // pesan sukses
        $session->setFlashdata('success_message', 'Success insert new project status');
        return redirect()->to('/m_otherManagement');
    }

    public function editProjectStatus($id) {
        $session = session();
        $getOldData = $this->M_View->getEditProjectStatus($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_projectStatus', $data);
    }

    public function doEditProjectStatus($id) {
        $session = session();

        // ambil data
        $id = $id;
        $identity   = $this->request->getVar("identityProjectStatus");
        $name       =  $this->request->getVar("nameProjectStatus");
        $getOldData = $this->M_View->getEditProjectStatus($id);

        // validasi
        if($getOldData['id'] == $identity) {
            $rule1 = 'required';
        } else {
            $rule1 = 'required|is_unique[m_project_status.id]';
        }

        if($getOldData['name'] == $name) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[m_project_status.name]';
        }

        if(!$this->validate([
            'identityProjectStatus' => $rule1, 
            'nameProjectStatus'     => $rule2
        ],[
            'identityProjectStatus' => [
                'is_unique' => "Sudah ada cuy !",
                'required'  => 'Harus di isi...'
            ],
            'nameProjectStatus' => [
                'is_unique' => "Sudah ada cuy !",
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data !');
            return redirect()->to('/M_OtherManagement/editProjectStatus' . '/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editProjectStatus($id, $identity, $name);

        // pesan sukses
        $session->setFlashdata('success_message', 'Successfully update existing data ! ');
        return redirect()->to('/M_OtherManagement');

    }

    public function deleteDataProjectStatus() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataProjectStatus($deleteId);
            $session->setFlashdata('success_message', 'Success delete new project status ! ');
            return redirect()->to('/m_otherManagement');
        } else {
            $session->setFlashdata('error_message', 'Failed delete new project status ! ');
            return redirect()->to('/m_otherManagement');
        }
    }
}
