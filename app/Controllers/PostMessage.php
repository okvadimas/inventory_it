<?php

namespace App\Controllers;

use \App\Models\M_View;

class PostMessage extends BaseController
{
	public function __construct() {
		$this->M_View = new M_View();
        date_default_timezone_set("Asia/Jakarta");
	}
	public function index()
	{
		if(!session()->get('login')){
			return redirect()->to('auth');
		}

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

		$data = [
			'menu'      => $menu,
            'allPost'   => $this->M_View->getAllPostMessage(),
            'user'      => $this->M_View->getAllUserManagement(),
            'validation' => \Config\Services::validation(),
			'type'      => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'       => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'        => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'      => $this->M_View->getAllDept(),
            'hrd'       => $this->M_View->getUserDeptHrd(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level'     => $this->M_View->getAllUserLevel(),
            "test"      => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
		];

		return view('postMessage', $data);
	}

	public function postMessage() {

        $session = session();

		// dapet data dari web tntng messange-nya (form post message - akses khusus hrd)
        $title      = esc($this->request->getVar('title'));
        $message    = esc($this->request->getVar('message'));
        $createAt   = date('F j, Y, G:i:s');
        $by         = esc($this->request->getVar('by'));
        $id         = $session->get('user_id');

        // validasi 1
        if(!$this->validate([
            'title'     => 'required|is_unique[post.post_title]',
            'message'   => 'required',
            'by'        => 'required'
        ], [
            'title' => [
                'is_unique' => 'Sudah ada cuy !',
                'required'  => 'Harus di isi...'
            ],
            'message'   => [
                'required'  => 'Harus di isi...'
            ],
            'by'    => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', "Failed post, please try again !");
            return redirect()->to('/postMessage')->withInput()->with('validation', $validation);
        }

        // validasi 2
		// check kebenaran user (selain hrd kick)
        if(session()->get('level') == 2) {
            // post ke database
            $this->M_View->postMessage($title, $message, $createAt, $by, $id);

            // beri pesan sukses 
            $validation = \Config\Services::validation();
            $session->setFlashdata('success_message', "Successfully post new message! ");
            return redirect()->to('/postMessage')->withInput()->with('validation', $validation);
        } else {
            // beri pesan gagal
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', "You're not authorized! ");
            return redirect()->to('/postMessage')->withInput()->with('validation', $validation);
        }
	}

    public function deletePostMessage() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deletePostMessage($deleteId);
            $session->setFlashdata('success_message', 'Success delete post message ! ');
            return redirect()->to('/PostMessage');
        } else {
            $session->setFlashdata('error_message', 'Failed delete post message ! ');
            return redirect()->to('/PostMessage');
        }
    }

    public function editPostMessage($id) {
        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        $getOldData = $this->M_View->getEditPostMessage($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'lastUser' => $lastUser,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_postMessage', $data);
    }

    public function doEditPostMessage($id) {
        $session = session();

        $id = $id;
        $title = esc($this->request->getVar('title'));
        // $by = esc($this->request->getVar('by'));
        $create = date('F j, Y, G:i:s');
        $message = esc($this->request->getVar('message'));
        $getOldData = $this->M_View->getEditPostMessage($id);

        // validasi 
        if($getOldData['post_title'] == $title) {
            $rule = 'required';
        } else {
            $rule = 'required|is_unique[post.post_title]';
        }

        if(!$this->validate([
            'title'     => $rule,
            'message'   => 'required'
        ], [
            'title'     => [
                'required'  => 'Harus di isi...',
                'is_unique' => 'Sudah ada cuy !'
            ],
            'message'   => [
                'required'  => "Harus di isi...",
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', "Failed update existing post message !");
            return redirect()->to('/postMessage/editPostMessage/' . $id)->withInput()->with('validation', $validation);
        }

        // validasi 2
		// check kebenaran user (selain hrd kick)
        if(session()->get('level') == 2) {
            // post ke database
            $this->M_View->editPostMessage($id, $title, $create, $message);

            // beri pesan sukses 
            $validation = \Config\Services::validation();
            $session->setFlashdata('success_message', "Successfully update existing post message !");
            return redirect()->to('/postMessage')->withInput()->with('validation', $validation);
        } else {
            // beri pesan gagal
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', "You're not authorized !");
            return redirect()->to('/postMessage/editPostMessage/' . $id)->withInput()->with('validation', $validation);
        }
    }
}