<?php

namespace App\Controllers;

use \App\Models\M_View;

class ResetPassword extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {
        
        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu'  => $menu,
            'reset' => $this->M_View->getAllResetPassword(),
            'validation' => \Config\Services::validation(),
			'type'  => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'   => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'    => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'  => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level' => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
            "test" => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('resetPassword', $data);
    }

    public function insertData() {

        $session = session();

        // ambil data dari web
        $resetDate  = $this->request->getVar('reset_date');
        $resetUser  = esc($this->request->getVar('reset_user'));
        $resetSeq   = esc($this->request->getVar('reset_seq'));
        $resetDomain = esc($this->request->getVar('reset_domain'));
        $resetOld   = esc($this->request->getVar('reset_old'));
        $resetNew   = esc($this->request->getVar('reset_new'));
        $resetComment = esc($this->request->getVar('reset_comment'));

        // validate
        if(!$this->validate([
            'reset_date'    => 'required',
            'reset_user'    => 'required',
            'reset_seq'     => 'required',
            'reset_domain'  => 'required',
            'reset_old'     => 'required',
            'reset_new'     => 'required',
        ], [
            'reset_date'    => [
                'required'  => 'Harus di isi...'
            ],
            'reset_user'    => [
                'required'  => 'Harus di isi...'
            ],
            'reset_seq'     => [
                'required'  => 'Harus di isi...'
            ],
            'reset_domain'  => [
                'required'  => 'Harus di isi...'
            ],
            'reset_old'     => [
                'required'  => 'Harus di isi...'
            ],
            'reset_new'     => [
                'required'   => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed insert new data ! </strong>');
            return redirect()->to('/resetPassword')->withInput()->with('validation', $validation);
        }

        // validasi jika data sudah ada domain dan sequence yang sama validate error
        $check = $this->M_View->checkResetPassword($resetDomain, $resetSeq);

        if($check != null) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', "<strong>Already exists ! <?/strong>");
            return redirect()->to('/resetPassword')->withInput()->with('validation', $validation);
        } else {
            // masukan ke database
            $this->M_View->insertDataResetPassword($resetUser, $resetDate, $resetSeq, $resetDomain, $resetOld, $resetNew, $resetComment);

            // edit data di access (new domain, new password - ini overwrite sesuai user asset id nya) 
            $this->M_View->editAccessFromResetPassword($resetUser, $resetDomain, $resetNew);
            
            // pesan sukses
            $session->setFlashdata('success_message', '<strong>Success insert new reset password data ! </strong>');
            return redirect()->to('/resetPassword');
        }

    }

    public function deleteData() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataResetPassword($deleteId);
            $session->setFlashdata('success_message', '<strong>Success reset password data ! </strong>');
            return redirect()->to('/resetPassword');
        } else {
            $session->setFlashdata('error_message', '<strong>Failed delete reset password data ! </strong>');
            return redirect()->to('/resetPassword');
        }
    }

    public function editData($id) {
        $session = session();

        $getOldData = $this->M_View->getEditResetPassword($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_ResetPassword', $data);
    }

    public function doEditData($id) {
        $session = session();

        // ambil data
        $resetDate = $this->request->getVar('reset_date');
        $resetUser = esc($this->request->getVar('reset_user'));
        $resetDomain = esc($this->request->getVar('reset_domain'));
        $resetSeq = esc($this->request->getVar('reset_seq'));
        $resetOld = esc($this->request->getVar('reset_old'));
        $resetNew = esc($this->request->getVar('reset_new'));
        $resetComment = esc($this->request->getVar('reset_comment'));
        $id = $id;
        $getOldData = $this->M_View->getEditResetPassword($id);

        // validate
        if(!$this->validate([
            'reset_date'    => 'required',
            'reset_user'    => 'required',
            'reset_domain'  => 'required',
            'reset_seq'     => 'required',
            'reset_old'     => 'required',
            'reset_new'     => 'required',
        ], [
            'reset_date'    => [
                'required'  => 'Harus di isi...'
            ],
            'reset_user'    => [
                'required'  => 'Harus di isi...'
            ],
            'reset_domain'  => [
                'required'  => 'Harus di isi...'
            ],
            'reset_seq'     => [
                'required'  => 'Harus di isi...'
            ],
            'reset_old'     => [
                'required'  => 'Harus di isi...'
            ],
            'reset_new'     => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed edit data !');
            return redirect()->to('/resetPassword/editData/' . $id)->withInput()->with('validation', $validation);
        }

        // validasi jika data sudah ada domain dan sequence yang sama validate error
        $check = $this->M_View->checkResetPassword($resetDomain, $resetSeq);

        if($check != null) {

            // cek dari dalam (lewat id yg sedang di edit)
            // jika data sama persis maka its oke
            if(
                $getOldData['reset_date']   == $resetDate AND
                $getOldData['reset_user']   == $resetUser AND
                $getOldData['reset_domain'] == $resetDomain AND
                $getOldData['reset_old']    == $resetOld AND
                $getOldData['reset_new']    == $resetNew AND
                $getOldData['reset_seq']    == $resetSeq AND
                $getOldData['reset_comment'] == $resetComment
            ) {
                // masukan ke database
                $this->M_View->editResetPassword($id, $resetDate, $resetUser, $resetDomain, $resetSeq, $resetOld, $resetNew, $resetComment);

                // edit data di access (new domain, new password - ini overwrite sesuai user asset id nya) 
                $this->M_View->editAccessFromResetPassword($resetUser, $resetDomain, $resetNew);

                // pesan sukses
                $session->setFlashdata('success_message', '<strong>Successfully update existing password ! </strong>');
                return redirect()->to("/ResetPassword");
            } else {
                $validation = \Config\Services::validation();
                $session->setFlashdata('error_message', '<strong>Aleady exists ! </strong>');
                return redirect()->to('/resetPassword/editData/' . $id)->withInput()->with('validation', $validation);
            }
 
        } else {
            // masukan ke database
            $this->M_View->editResetPassword($id, $resetDate, $resetUser, $resetDomain, $resetSeq, $resetOld, $resetNew, $resetComment);

            // edit data di access (new domain, new password - ini overwrite sesuai user asset id nya) 
            $this->M_View->editAccessFromResetPassword($resetUser, $resetDomain, $resetNew);
    
            // pesan sukses
            $session->setFlashdata('success_message', '<strong>Successfully update existing password ! </strong>');
            return redirect()->to("/ResetPassword");
        }
    }

    public function importData() {
        $session = session();
        $file = $this->request->getFile('importData');
        $filename = $file->getName();
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('/ResetPassword')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }
				
                // validasi duplicate
                $check = $this->M_View->checkResetPassword($kolom['3'], $kolom['1']);

                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null && $kolom['4'] == null && $kolom['5'] == null && $kolom['6'] == null) {
                    continue;
                }

                // format tanggal harus bener YYYY-MM-DD
                if(!preg_match("/^((((19|[2-9]\d)\d{2})\-(0[13578]|1[02])\-(0[1-9]|[12]\d|3[01]))|(((19|[2-9]\d)\d{2})\-(0[13456789]|1[012])\-(0[1-9]|[12]\d|30))|(((19|[2-9]\d)\d{2})\-02\-(0[1-9]|1\d|2[0-8]))|(((1[6-9]|[2-9]\d)(0[48]|[2468][048]|[13579][26])|((16|[2468][048]|[3579][26])00))\-02\-29))$/", $kolom['0'])) {
                    $log['failed'] += 1;
                    continue;
                }

                if($check != null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } elseif($kolom['0'] == null || $kolom['1'] == null || $kolom['2'] == null || $kolom['3'] == null || $kolom['4'] == null || $kolom['5'] == null || $kolom['6'] == null) {
                    $log['failed'] += 1;
                    continue;
                } else {

                    $date       = $kolom['0'];
                    $sequence   = $kolom['1'];
                    $username   = $kolom['2'];
                    $domain     = $kolom['3'];
                    $oldPassword    = $kolom['4'];
                    $newPassword    = $kolom['5'];
                    $comment    = $kolom['6'];
                            
                    // masukan ke database
                    $this->M_View->importResetPassword($date, $sequence, $username, $domain, $oldPassword, $newPassword, $comment);

                    // edit data di access (new domain, new password - ini overwrite sesuai user asset id nya) 
                    $this->M_View->editAccessFromResetPassword($username, $domain, $newPassword);
                    $log['success'] += 1;
                }
            }  

            $session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('/ResetPassword');
        }
    }

    public function templateImportData() {
        // dd('masuk download template preventive');
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $dataDepartment = $this->M_View->getAllDept();

        $sheet->setCellValue('A1', 'Date*');
        $sheet->setCellValue('B1', 'Sequence*');
        $sheet->setCellValue('C1', 'User ID*');
        $sheet->setCellValue('D1', 'Domain*');
        $sheet->setCellValue('E1', 'Old Password*');
        $sheet->setCellValue('F1', 'New Password*');
        $sheet->setCellValue('G1', 'Comment');

        $sheet->setCellValue('A2', '2022-03-15');
        $sheet->setCellValue('B2', '2');
        $sheet->setCellValue('C2', '2175');
        $sheet->setCellValue('D2', 'managerhrd');
        $sheet->setCellValue('E2', 'password lama');
        $sheet->setCellValue('F2', 'password baru');
        $sheet->setCellValue('G2', 'comment');

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import reset password';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function exportData() {
        $session = session();

        // get data
        $fromSeq    = esc($this->request->getVar('exportData1'));
        $toSeq      = esc($this->request->getVar('exportData2'));
        $fromYear   = esc($this->request->getVar('exportData3'));

        $date = date('j M Y');

        // rule
        if($fromSeq == null && $toSeq == null && $fromYear == null) {
            $dataTemplate = $this->M_View->getAllDataResetPassword();
            $fileName = 'All data reset password - ' . $date;
        } elseif($fromSeq == null && $toSeq == null && $fromYear != null) {
            $dataTemplate = $this->M_View->getAllDataResetPasswordByYear($fromYear);
            $fileName = 'All data reset password by year ' . $fromYear . ' - ' . $date;
        } elseif($fromSeq != null && $toSeq != null && $fromYear != null) {
            $dataTemplate = $this->M_View->getAllDataResetPasswordSpecific($fromSeq, $toSeq, $fromYear);
            $fileName = 'All data reset password sequence ' . $fromSeq . ' to ' . $toSeq . ' by year ' . $fromYear . ' - ' . $date;
        } elseif($fromSeq != null && $toSeq != null && $fromYear == null) {
            $dataTemplate = $this->M_View->getAllDataResetPasswordBySeq($fromSeq, $toSeq);
            $fileName = 'All data reset password sequence ' . $fromSeq . ' to ' . $toSeq . ' - ' . $date;
        } else {
            $session->setFlashdata('error_message', '<strong>Failed !!</strong> wrong rule ! Please read the rule and try again...');
            return redirect()->to('resetPassword');
        }

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();


        $sheet->setCellValue('A1', 'Date');
        $sheet->setCellValue('B1', 'Sequence');
        $sheet->setCellValue('C1', 'Username');
        $sheet->setCellValue('D1', 'Department');
        $sheet->setCellValue('E1', 'Domain');
        $sheet->setCellValue('F1', 'Old Password');
        $sheet->setCellValue('G1', 'New Password');
        $sheet->setCellValue('H1', 'Comment');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['reset_date']);
            $sheet->setCellValue('B' . $rows, $dt['reset_seq']);
            $sheet->setCellValue('C' . $rows, $dt['user_fullname']);
            $sheet->setCellValue('D' . $rows, $dt['dept_name']);
            $sheet->setCellValue('E' . $rows, $dt['reset_domain']);
            $sheet->setCellValue('F' . $rows, $dt['reset_old']);
            $sheet->setCellValue('G' . $rows, $dt['reset_new']);
            $sheet->setCellValue('H' . $rows, $dt['reset_comment']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
