<?php

namespace App\Controllers;

use \App\Models\M_View;

class M_AssetType extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {

        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu' => $menu,
            'user' => $this->M_View->getAllUserManagement(),
            'validation' => \Config\Services::validation(),
			'type'  => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'   => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem'   => $this->M_View->getAllOperatingSystem(),
            'it'    => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus'     => $this->M_View->getAllProjectStatus(),
            'dept'  => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level'     => $this->M_View->getAllUserLevel(),
            "test"      => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('m_assetType', $data);
    }

    public function insertData() {

        $session = session();

        // ambil data dari web
        $identity   = esc($this->request->getVar('identity'));
        $type       = esc($this->request->getVar('type'));
        $borrowed   = $this->request->getVar('borrowed');
        $available  = $this->request->getVar('available');
        $information = esc($this->request->getVar('information'));

        // validasi data dari web bener atau ngga
        if(!$this->validate([
            'identity'  => 'required|is_unique[m_asset_type.id]',
            'type'      => 'required',
            'borrowed'  => 'required',
            'available' => 'required',
        ],
        [   // Errors
            'identity' => [
                'is_unique' => 'Sudah ada cuy...',
                'required'  => 'Harus di isi...'
            ],
            'type'  => [
                'required'  => 'Harus di isi...'
            ],
            'borrowed'  => [
                'required'  => 'Harus di isi...'
            ],
            'available' => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            session()->setFlashdata('error_message', 'Failed add new asset type! ');
            return redirect()->to('/M_AssetType')->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->insertDataAssetType($identity, $type, $borrowed, $available, $information);

        // pesan sukses
        $session->setFlashdata('success_message', 'Success insert new asset type! ');
        return redirect()->to('/m_assetType');
    }

    public function deleteData() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataAssetType($deleteId);
            $session->setFlashdata('success_message', 'Success delete data asset type! ');
            return redirect()->to('/m_assetType');
        } else {
            $session->setFlashdata('error_message', 'Failed delete data asset type! ');
            return redirect()->to('/m_assetType');
        }
    }

    public function editAssetType($id) {
        $session = session();

        $getOldData = $this->M_View->getEditAssetType($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_m_assetType', $data);
    }

    public function doEditAssetType($id) {
        $session = session();

        // get data
        $id = $id;
        $asset_id = $this->request->getVar('identity');
        $type = $this->request->getVar('type');
        $borrowed = $this->request->getVar('borrowed');
        $available = $this->request->getVar('available');
        $information = $this->request->getVar('information');
        $getOldData = $this->M_View->getEditAssetType($id);

        // validasi
        if($getOldData['asset_id'] == $asset_id) {
            $rule = 'required';
        } else {
            $rule = 'required|is_unique[m_asset_type.asset_id]';
        }
        
        if(!$this->validate([
            'identity'  => $rule,
            'type'      => 'required',
            'borrowed'  => 'required',
            'available' => 'required'
        ],[
            'identity' => [
                'is_unique' => 'Sudah ada boss !',
                'required'  => 'Harus di isi...'
            ],
            'type'  => [
                'required'  => 'Harus di isi...'
            ],
            'borrowed'  => [
                'required'  => 'Harus di isi...'
            ],
            'available' => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data! ');
            return redirect()->to('/M_AssetType/editAssetType' . '/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editAssetType($id, $asset_id, $type, $borrowed, $available, $information);

        // pesan sukses 
        $validation = \Config\Services::validation();
        $session->setFlashdata('success_message', "Successfully update existing data ! ");
        return redirect()->to('/M_AssetType')->withInput()->with('validation', $validation);
    }
}
