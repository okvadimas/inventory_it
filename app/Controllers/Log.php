<?php

namespace App\Controllers;

use \App\Models\M_View;

class Log extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
        helper('trial');
    }

    public function index() {

        loggedIn();

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu' => $menu,
            'email' => $this->M_View->getAllEmail(),
            'validation' => \Config\Services::validation(),
			'type' => $this->M_View->getAllAssetType(),
			'status' => $this->M_View->getAllStatus(),
			'lastUser' => $lastUser,
            'position' => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender' => $this->M_View->getAllGender(),
            'use' => $this->M_View->getAllAccessUse(),
            'deskera' => $this->M_View->getAllAccessDeskera(),
            'emailUse' => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it' => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept' => $this->M_View->getAllDept(),
            'deptName' => $this->M_View->getDeptById($session->get('dept')),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level' => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
            "test" => $this->M_View->getReportPerMonth(),
            "log" => $this->M_View->getAllLog(),
            'statusLog' => $this->M_View->getAllStatusLog(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('log', $data);
    }

    public function insertData() {

        $lastUser = $this->M_View->getAllUserDesc();
        $session = session();

        // ambil data dari web
        $asset  = $this->request->getVar('log_asset');
        $name   = esc($this->request->getVar('log_name'));
        $dept   = $this->request->getVar('log_dept');
        $status = $this->request->getVar('log_status');
        $stock  = esc($this->request->getVar('log_stock'));
        $desc   = esc($this->request->getVar('log_desc'));

        // validate all required but not desc
        if(!$this->validate([
            'log_asset' => 'required',
            'log_name'  => 'required',
            'log_dept'  => 'required',
            'log_status' => 'required',
            'log_stock' => 'required',
        ], [
            'log_asset' => [
                'required' => 'Harus di-isi...'
            ],
            'log_name' => [
                'required' => 'Harus di-isi...'
            ],
            'log_dept' => [
                'required' => 'Harus di-isi...'
            ],
            'log_status' => [
                'required' => 'Harus di-isi...'
            ],
            'log_stock' => [
                'required' => 'Harus di-isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert new data !');
            return redirect()->to('/log')->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->insertDataLog($asset, $name, $dept, $status, $stock, $desc);

        // update data m_asset_type
        // 5 = in, 6 = out
        if($status == 5) {
            $getOldData = $this->M_View->getEditAssetTypeByAssetId($asset);
            $newStock = $getOldData['available'] + $stock;
            $this->M_View->updateDataAssetTypeLog($newStock, $asset);
        } else if($status == 6) {
            $getOldData = $this->M_View->getEditAssetTypeByAssetId($asset);
            $newStock = $getOldData['available'] - $stock;
            $this->M_View->updateDataAssetTypeLog($newStock, $asset);
        }

        // pesan sukses
        $session->setFlashdata('success_message', 'Success insert new data ! ');
        return redirect()->to('/log');
    }

    public function deleteData() {
        $deleteId = $this->request->getVar('deleteId');
		$session = session();

		// jika ada delete
		if(isset($deleteId)) {

            // edit total m_asset_type available-nya
            $getOldDataLog = $this->M_View->getEditLog($deleteId);
            $getOldDataAssetType = $this->M_View->getEditAssetTypeByAssetId($getOldDataLog['log_asset']);

            // jika log status = 5 (in) maka dikurangi dan jika 6 (out) maka di tambah
            if($getOldDataLog['log_status'] == 5) {
                $newStock = $getOldDataAssetType['available'] - $getOldDataLog['log_stock'];
                $this->M_View->updateDataAssetTypeLog($newStock, $getOldDataLog['log_asset']);
            } else if($getOldDataLog['log_status'] == 6) {
                $newStock = $getOldDataAssetType['available'] + $getOldDataLog['log_stock'];
                $this->M_View->updateDataAssetTypeLog($newStock, $getOldDataLog['log_asset']);
            }

            // setelah edit baru delete lognya wqwq
            $this->M_View->deleteDataLog($deleteId);
			$session->setFlashdata('success_message', 'Success delete log status ! ');

			return redirect()->to('/log');
		} else {
			$session->setFlashdata('error_message', 'Failed delete log status ! ');
			return redirect()->to('/log');
		}
    }
    public function editData($id) {
        $session = session();

        $getOldData = $this->M_View->getEditLog($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
			'type' => $this->M_View->getAllAssetType(),
            'dept' => $this->M_View->getAllDept(),
            'statusLog' => $this->M_View->getAllStatusLog(),
            "test" => $this->M_View->getReportPerMonth(),
        ];

        return view('/E_log', $data);
    }

    public function doEditData($id) {
        $session = session();

        // ambil data
        $log_asset = $this->request->getVar('log_asset');
        $log_name = esc($this->request->getVar('log_name'));
        $log_dept = $this->request->getVar('log_dept');
        $log_status = $this->request->getVar('log_status');
        $log_stock = esc($this->request->getVar('log_stock'));
        $log_desc = esc($this->request->getVar('log_desc'));
        $log_id = $id;
        $getOldDataLog = $this->M_View->getEditLog($id);

        // validasi
        if(!$this->validate([
            'log_asset' => 'required',
            'log_name'  => 'required',
            'log_dept'  => 'required',
            'log_status' => 'required',
            'log_stock' => 'required',
            'log_desc'  => 'required',
        ], [
            'log_asset' => [
                'required' => 'Harus di isi... !'
            ],
            'log_name' => [
                'required' => 'Harus di isi... !'
            ],
            'log_dept' => [
                'required' => 'Harus di isi... !'
            ],
            'log_status' => [
                'required' => 'Harus di isi... !'
            ],
            'log_stock' => [
                'required' => 'Harus di isi... !'
            ],
            'log_desc' => [
                'required' => 'Harus di isi... !'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data ! ');
            return redirect()->to('/Log/editData' . '/' . $id)->withInput()->with('validation', $validation);
        }

        // update data m_asset_type
        
        // jika status tetap sama maka gaperlu delete data dulu
        if($getOldDataLog['log_status'] == $log_status) {
            // 5 = in, 6 = out
            if($log_status == 5) {
                $getOldDataAsset = $this->M_View->getEditAssetTypeByAssetId($log_asset);
                $newStock = $getOldDataAsset['available'] + ($log_stock - $getOldDataLog['log_stock']);
                $this->M_View->updateDataAssetTypeLog($newStock, $log_asset);
            } else if($log_status == 6) {
                $getOldDataAsset = $this->M_View->getEditAssetTypeByAssetId($log_asset);
                $newStock = $getOldDataAsset['available'] - ($log_stock - $getOldDataLog['log_stock']);
                $this->M_View->updateDataAssetTypeLog($newStock, $log_asset);
            }
        } else {
            if($log_status == 5) {
                $getOldDataAsset = $this->M_View->getEditAssetTypeByAssetId($log_asset);

                // delete data available dulu dengan data log dulu biar data balik awal seperti ketika delete log (sm algonya)
                $resetData = $getOldDataAsset['available'] + $getOldDataLog['log_stock'];
                $this->M_View->updateDataAssetTypeLog($resetData, $log_asset);

                // kyk yg di insert biasa updatenya (tinggal tambah kalo in dan kurang kalo out)
                $newStock = $resetData + $log_stock;
                $this->M_View->updateDataAssetTypeLog($newStock, $log_asset);
            } else if($log_status == 6) {
                $getOldDataAsset = $this->M_View->getEditAssetTypeByAssetId($log_asset);

                // delete data available dulu dengan data log dulu biar data balik awal seperti ketika delete log (sm algonya)
                $resetData = $getOldDataAsset['available'] - $getOldDataLog['log_stock'];
                $this->M_View->updateDataAssetTypeLog($resetData, $log_asset);

                // kyk yg di insert biasa updatenya (tinggal tambah kalo in dan kurang kalo out)
                $newStock = $resetData - $log_stock;
                $this->M_View->updateDataAssetTypeLog($newStock, $log_asset);
            }
        }

        // masukan ke database
        $this->M_View->editDataLog($log_asset, $log_name, $log_dept, $log_status, $log_stock, $log_desc, $log_id);

        // pesan sukses
        $session->setFlashdata('success_message', 'Successfully update existing log data ! ');
        return redirect()->to('/Log');

    }
}
