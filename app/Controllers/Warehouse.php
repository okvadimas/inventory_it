<?php

namespace App\Controllers;

use \App\Models\M_View;

class Warehouse extends BaseController
{ 
	public function __construct() {
		$this->M_View = new M_View();
		helper('trial');
	}

	public function index()
	{

		loggedIn();

        $session = session();
		$lastUser = $this->M_View->getAllUserDesc();

		if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
		
		$data = [
			'menu' => $menu,
			'warehouse' => $this->M_View->getAllWarehouse(),
			'validation' => \Config\Services::validation(),
			'type' => $this->M_View->getAllAssetType(),
			'status' => $this->M_View->getAllStatus(),
			'lastUser' => $lastUser,
			'position' => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender' => $this->M_View->getAllGender(),
			'use' => $this->M_View->getAllAccessUse(),
            'deskera' => $this->M_View->getAllAccessDeskera(),
			'emailUse' => $this->M_View->getAllEmailUse(),
			'operatingSystem' => $this->M_View->getAllOperatingSystem(),
			'it' => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept' => $this->M_View->getAllDept(),
			'deptName' => $this->M_View->getDeptById($session->get('dept')),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level' => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
			"test" => $this->M_View->getReportPerMonth(),
			'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
		];

		return view('warehouse', $data);
	}

	public function print() {

		$range1 = $this->request->getVar('range1_filter');
		$range2 = $this->request->getVar('range2_filter');
		$multi = $this->request->getVar('multiple');

		if(isset($_POST['range'])) {
			$getData = $this->M_View->getDataWarehouseByRange($range1, $range2);
			$typeInput = 'range';
		} elseif(isset($_POST['multipleSubmit'])) {
			$getData = $this->M_View->getDataWarehouseByMultiple($multi);
			$typeInput = 'multiple';
		} else {
			$getData = "Coba Lagi";
		}

		$data = [
			'typeInput' => $typeInput,
			'data' => $getData,
		];

		$mpdf = new \Mpdf\Mpdf();
		$htmld = view('/layout/print/print', $data);
		// $html = '
		// <!DOCTYPE html>
		// <html dir="ltr" lang="en">
		
		// <head>
		// 	<meta charset="utf-8">
		// 	<meta http-equiv="X-UA-Compatible" content="IE=edge">
		// 	<!-- Tell the browser to be responsive to screen width -->
		// 	<meta name="viewport" content="width=device-width, initial-scale=1">
		// 	<title>PT Pacific Furniture - Print Template</title>
		// 	<!-- Custom CSS -->
		// 	<link rel="stylesheet" href="print.css">
		// </head>
		
		// <body>
		// 	<div class="container"> 
		// 		<div class="card">
		// 			<div class="head">
		// 				<h5>INVENTORY COUNT TAG</h5>
		// 				<div class="child">
		// 					<p>Site : </p>
		// 					<p>Date : </p>
		// 				</div>
		// 			</div>
		// 		</div>
		// 	</div>
		// </body>
		
		// </html>
		
		
		// ';
		$mpdf->WriteHTML($htmld);
		$this->response->setHeader('Content-Type', 'application/pdf');
		$mpdf->Output('test.pdf','I'); // opens in browser
	}

    function htmlToPDF(){

		$range1 = $this->request->getVar('range1_filter');
		$range2 = $this->request->getVar('range2_filter');
		$multi = $this->request->getVar('multiple');

		if(isset($_POST['range'])) {
			$getData = $this->M_View->getDataWarehouseByRange($range1, $range2);
			$typeInput = 'range';
		} elseif(isset($_POST['multipleSubmit'])) {
			$getData = $this->M_View->getDataWarehouseByMultiple($multi);
			$typeInput = 'multiple';
		} else {
			$getData = "Coba Lagi";
			$typeInput = "Coba Lagi";
		}

		$data = [
			'typeInput' => $typeInput,
			'data' => $getData,
		];

		$mpdf = new \Mpdf\Mpdf();
		$html = '
			<div class="row px-2 py-3">
			<?php foreach($data as $d) : ?>
				<?php if($typeInput == "range") : ?>

						<div class="col-5 m-2">
							<div class="card">
								<div class="row text-center pt-3">
									<div class="col-12">
										<h3>INVENTORY COUNT TAG</h3>
										<h5>Site : Stock Room 1</h5>
									</div>
								</div>
								<div class="row px-5 pt-2">
									<div class="col-6">
										<h5>Date : </h5>
									</div>
									<div class="col-6">
										<h5>Tag No :</h5>
									</div>
								</div>
								<div class="row pt-4 px-5">
									<div class="col-12">
										<h5>Item No : <?= $d["product_id"] ?></h5>
										<h5>Description : <?= $d["description"] ?></h5>
										<h5>Location : </h5>
										<h5>Stock on Hand : </h5>
										<h5>JO Number : </h5>
										<h5>Model Code : </h5>
										<h5>Based Unit of Measurement : </h5>
										<h5 style="margin-left: 125px">Physical Count : </h5>
										<h5>Actual Unit of Measurement : </h5>
										<h5>Remarks : </h5>
									</div>
								</div>
								<div class="row px-5 my-4 pb-5">
									<div class="col-4">
										<h5>Counted By : </h5>
									</div>
									<div class="col-4">
										<h5>Checked By : </h5>
									</div>
									<div class="col-4">
										<h5>Audited By : </h5>
									</div>
								</div>
							</div>
						</div>
				<?php elseif($typeInput == "multiple") : ?>
						<div class="col-5 m-2">
							<div class="card">
								<div class="row text-center">
									<div class="col-12">
										<h3>INVENTORY COUNT TAG</h3>
										<h5>Site : Stock Room 1</h5>
									</div>
								</div>
								<div class="row px-5 pt-2">
									<div class="col-6">
										<h5>Date : </h5>
									</div>
									<div class="col-6">
										<h5>Tag No :</h5>
									</div>
								</div>
								<div class="row pt-4 px-5">
									<div class="col-12">
										<h5>Item No : <?= $d[0]["product_id"] ?></h5>
										<h5>Description : <?= $d[0]["description"] ?></h5>
										<h5>Location : </h5>
										<h5>Stock on Hand : </h5>
										<h5>JO Number : </h5>
										<h5>Model Code : </h5>
										<h5>Based Unit of Measurement : </h5>
										<h5 style="margin-left: 125px">Physical Count : </h5>
										<h5>Actual Unit of Measurement : </h5>
										<h5>Remarks : </h5>
									</div>
								</div>
								<div class="row px-5 my-4 pb-5">
									<div class="col-4">
										<h5>Counted By : </h5>
									</div>
									<div class="col-4">
										<h5>Checked By : </h5>
									</div>
									<div class="col-4">
										<h5>Audited By : </h5>
									</div>
								</div>
							</div>
						</div>
				<?php else : ?>
					<?= "Sorry i dont understand.." ?>
				<?php endif; ?>
			<?php endforeach; ?>
			
		</div>
		';
		$mpdf->WriteHTML($html, $data);
		$this->response->setHeader('Content-Type', 'application/pdf');
		$mpdf->Output('test.pdf','I'); // opens in browser
    }
}

// 	public function insertData() {

// 		$session = session();

// 		$lastUser = $this->M_View->getAllUserDesc();

// 		// ambil dari web
// 		$asset_type = $this->request->getVar('asset_type');
// 		$information = esc($this->request->getVar('information'));
// 		$processor = esc($this->request->getVar('processor'));
// 		$motherboard = esc($this->request->getVar('motherboard'));
// 		$storage = esc($this->request->getVar('storage'));
// 		$memory = esc($this->request->getVar('memory'));
// 		$graphics = esc($this->request->getVar('graphics'));
// 		$monitor = esc($this->request->getVar('monitor'));
// 		$jumlah = esc($this->request->getVar('jumlah'));
// 		$status = $this->request->getVar('status');
// 		$user_id = $lastUser["user_id"];
// 		$operatingSystem = $this->request->getVar('operatingSystem');
// 		$serialNumber = $this->request->getVar('serialNumber');
// 		// masukan ke database
// 		$this->M_View->insertDataAsset($asset_type, $information, $processor, $motherboard, $storage, $memory, $graphics, $monitor, $jumlah, $status, $user_id, $operatingSystem, $serialNumber);
// 		// pesan sukses
// 		$session->setFlashdata('success_message', 'Success insert new data! ');
// 		return redirect()->to('/asset');
// 	}

// 	public function deleteData() {

// 		$deleteId = $this->request->getVar('deleteId');
// 		$session = session();

// 		// jika ada delete
// 		if(isset($deleteId)) {
// 			$this->M_View->deleteDataAsset($deleteId);
// 			$session->setFlashdata('success_message', 'Success delete asset data! ');
// 			return redirect()->to('/asset');
// 		} else {
// 			$session->setFlashdata('error_message', 'Failed delete asset data! ');
// 			return redirect()->to('/asset');
// 		}

// 	}

// 	public function editData($id) {
// 		$session = session();

//         $getOldData = $this->M_View->getEditAsset($id);

//         if($session->get('level') == 1) {
// 			$menu = $this->M_View->getMenu();
// 		} else {
// 			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
// 		}

//         $data = [
//             'menu' => $menu,
//             'data' => $getOldData,
//             'validation' => \Config\Services::validation(),
// 			'type' => $this->M_View->getAllAssetType(),
// 			'status' => $this->M_View->getAllStatus(),
// 			'operatingSystem' => $this->M_View->getAllOperatingSystem(),
//         ];

//         return view('/E_asset', $data);
// 	}

// 	public function doEditData($id) {
// 		$session = session();

// 		$id = $id;
// 		$asset_type = $this->request->getVar('asset_type');
// 		$processor = esc($this->request->getVar('processor'));
// 		$storage = esc($this->request->getVar('storage'));
// 		$graphics = esc($this->request->getVar('graphics'));
// 		$motherboard = esc($this->request->getVar('motherboard'));
// 		$memory = esc($this->request->getVar('memory'));
// 		$monitor = esc($this->request->getVar('monitor'));
// 		$jumlah = esc($this->request->getVar('jumlah'));
// 		$status = $this->request->getVar('status');
// 		$information = esc($this->request->getVar('information'));
// 		$operating_system = $this->request->getVar('operatingSystem');
// 		$serial_number = $this->request->getVar('serialNumber');

// 		// masukan ke database
// 		$this->M_View->editAssetData($id, $asset_type, $processor, $storage, $graphics, $motherboard, $memory, $monitor, $jumlah, $status, $information, $operating_system, $serial_number);

// 		// pesan sukses
// 		$session->setFlashdata('success_message', "Successfully update existing data ! ");
// 		return redirect()->to('/Asset');
// 	}

// 	public function print() {
// 		return view('/layout/print');
// 	}
// }
