<?php

namespace App\Controllers;

use \App\Models\M_View;

class Access extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {
        
        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu'      => $menu,
            'access'    => $this->M_View->getAllAccess(),
            'validation' => \Config\Services::validation(),
			'type'      => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'       => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'        => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'      => $this->M_View->getAllDept(),
            'deptName'  => $this->M_View->getDeptById($session->get('dept')),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level'     => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
            "test"      => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];


        return view('access', $data);
    }

    public function insertData() {

        $lastUser = $this->M_View->getAllUserDesc();

        $session = session();

        // ambil data dari web
        $access_domain  = esc($this->request->getVar('domain_access'));
        $access_pass    = esc($this->request->getVar('domain_pass'));
        $access_use     = $this->request->getVar('use_access');
        $access_deskera = $this->request->getVar('deskera_access');
        $access_anydesk = esc($this->request->getVar('anydesk'));
        $user_id        = $lastUser['user_asset_id'];

        $access_use = implode(',', $access_use);

        // validate
        if(!$this->validate([
            'domain_access' => 'required',
            'domain_pass'   => 'required',
            'use_access'    => 'required',
            'deskera_access' => 'required',
            'anydesk'       => 'required',
        ], [
            'domain_access' => [
                'required'  => 'Harus di isi...',
            ],
            'domain_pass'   => [
                'required'  => 'Harus di isi...'
            ],
            'use_access'    => [
                'required'  => 'Harus di isi...'
            ],
            'deskera_access'    => [
                'required'  => 'Harus di isi...'
            ],
            'anydesk'       => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed insert access data !');
            return redirect()->to('/access')->withInput()->with('validation', $validation);
        }

        // masukan ke database
       if($this->M_View->getEditAccessByUserId($user_id) != null ) {
            // pesan error
            $session->setFlashdata('error_message', 'Already exists ! ');
            return redirect()->to('/access');
       } else {
            $this->M_View->insertDataAccess($access_domain, $access_pass, $access_use, $access_deskera, $access_anydesk, $user_id);
            
            // pesan sukses
            $session->setFlashdata('success_message', 'Success insert new access! ');
            return redirect()->to('/access');
        }
       
    }

    public function deleteData() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deletaDataAccess($deleteId);
            $session->setFlashdata('success_message', 'Success delete access data! ');
            return redirect()->to('/access');
        } else {
            $session->setFlashdata('error_message', 'Failed delete access data! ');
            return redirect()->to('/access');
        }
    }

    public function editData($id) {
        $session = session();
        $getOldData = $this->M_View->getEditAccess($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $accUse = explode(",", $getOldData['access_use']);

        $data = [
            'menu'      => $menu,
            'data'      => $getOldData,
            'validation' => \Config\Services::validation(),
			'type'      => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'accUse'    => $accUse,
            "test"      => $this->M_View->getReportPerMonth(),
        ];

        return view('/E_access', $data);
    }

    public function doEditData($id) {
        $session = session();

        // ambil data
        $domainAccess   = esc($this->request->getVar('domainAccess'));
        $domainPass     = esc($this->request->getVar('domainPass'));
        $accessUse      = $this->request->getVar('accessUse');
        $deskera        = $this->request->getVar('deskera');
        $anydesk        = esc($this->request->getVar('anydesk'));
        $id             = $id;
        $getOldData     = $this->M_View->getEditAccess($id);

        $accessUse = implode(',', $accessUse);

        // validasi 
        if($getOldData['access_domain'] == $domainAccess) {
            $rule1 = 'required';
        } else {
            $rule1 = "required";
        } 

        if(!$this->validate([
            'domainAccess'  => $rule1,
            'domainPass'    => 'required',
            'accessUse'     => 'required',
            'deskera'       => 'required',
            'anydesk'       => 'required'
        ], [
            'domainAccess'  => [
                'required'  => 'Harus di isi...'
            ], 
            'domainPass'    => [
                'required'  => 'Harus di isi...'
            ],
            'accessUse'     => [
                'required'  => 'Harus di isi...'
            ],
            'deskera'       => [
                'required'  => 'Harus di isi...'
            ],
            'anydesk'       => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data !');
            return redirect()->to('/Access/editData' . '/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->editAccess($id, $domainAccess, $domainPass, $accessUse, $deskera, $anydesk);
        
        // pesan sukses
        $session->setFlashdata('success_message', 'Successfully update existing data ! ');
        return redirect()->to('/Access');
    }

    public function importData() {
        $session = session();
        $file = $this->request->getFile('importData');
        $filename = $file->getName();
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('/Access')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }
				
                // validasi duplicate
                $check = $this->M_View->checkAccess($kolom['0']);
                $checkUserID = $this->M_View->checkUserId($kolom['0']);

                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null && $kolom['4'] == null && $kolom['5'] == null) {
                    continue;
                }

                if($check != null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } elseif($kolom['0'] == null || $kolom['1'] == null || $kolom['2'] == null || $kolom['3'] == null || $kolom['4'] == null || $kolom['5'] == null) {
                    $log['failed'] += 1; 
                    continue;
                } elseif($checkUserID == null) {
                    $log['failed'] += 1;
                    continue;
                } else {

                    $userAssetId = $kolom['0'];
                    $domain     = $kolom['1'];
                    $password 	= $kolom['2'];
                    $use        = $kolom['3'];
                    $deskera    = $kolom['4'];
                    $anydesk    = $kolom['5'];
                            
                    // masukan ke database
                    $this->M_View->importAccess($userAssetId, $domain, $password, $use, $deskera, $anydesk);
                    $log['success'] += 1;
                }
            }  

            $session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('/Access');
        }
    }

    public function templateImportData() {
        // dd('masuk download template preventive');
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

		$dataUse = $this->M_View->getAllAccessUse();
		$dataDeskera = $this->M_View->getAllAccessDeskera();

        $sheet->setCellValue('A1', 'User Asset ID*');
        $sheet->setCellValue('B1', 'Domain*');
        $sheet->setCellValue('C1', 'Password*');
        $sheet->setCellValue('D1', 'Use*');
        $sheet->setCellValue('E1', 'Deskera*');
        $sheet->setCellValue('F1', 'Anydesk*');

        $sheet->setCellValue('A2', '2175');
        $sheet->setCellValue('B2', 'nuryadi');
        $sheet->setCellValue('C2', 'password');
        $sheet->setCellValue('D2', '2');
        $sheet->setCellValue('E2', '1');
        $sheet->setCellValue('F2', '235332345');

		// informasi untuk template 
        $sheet->setCellValue('H2', "# Information");
        $sheet->setCellValue('H3', "# Access Use");

        $rowAu = 4;

        foreach($dataUse as $du) {
            $sheet->setCellValue('H'.$rowAu, $du['id'] . ". " . $du['name']);
            $rowAu++;
        }

        $sheet->setCellValue('H'.$rowAu + 2, "# Deskera");
        $rowDeskera = $rowAu + 3;

        foreach($dataDeskera as $dd) {
            $sheet->setCellValue('H'.$rowDeskera, $dd['id'] . ". " . $dd['name']);
            $rowDeskera++;
        }


        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import access';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

	public function exportData() {
		$session = session();
        $date = date('j M Y');

		$dataTemplate = $this->M_View->getAllAccess();
		$fileName = 'All data access - ' . $date;
        $accessUse = $this->M_View->getAllAccessUse();

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'Asset ID');
        $sheet->setCellValue('B1', 'Domain');
        $sheet->setCellValue('C1', 'Domain Pass');
        $sheet->setCellValue('D1', 'Use');
        $sheet->setCellValue('E1', 'Deskera');
        $sheet->setCellValue('F1', 'Anydesk');
        $rows = 2;

        // dd($accUse);

        foreach ($dataTemplate as $dt) {
            $accUse = explode(",",$dt['access_use']);
            $result = '';
            $arrayAccessUse = [];
            if(count($accUse) > 1) {
                foreach($accessUse as $acc) {
                    $arrayAccessUse[] = $acc['id'];
                }
                $matchArray = array_intersect($accUse, $arrayAccessUse);
                foreach($matchArray as $match) {
                    $temp = $this->M_View->getNameAccessUse($match);
                    $result .= $temp['name'] . ',';
                }

                $result = substr($result, 0, -1);
            } else {
                $temp = $this->M_View->getNameAccessUse($accUse[0]);
                $result .= $temp['name'] . ',';
                $result = substr($result, 0, -1);
            }

            $sheet->setCellValue('A' . $rows, $dt['user_asset_id']);
            $sheet->setCellValue('B' . $rows, $dt['access_domain']);
            $sheet->setCellValue('C' . $rows, $dt['access_pass']);
            $sheet->setCellValue('D' . $rows, $result);
            $sheet->setCellValue('E' . $rows, $dt['access_deskera'] == 1 ? 'Yes' : 'No');
            $sheet->setCellValue('F' . $rows, $dt['access_anydesk']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
	}
}
