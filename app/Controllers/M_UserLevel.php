<?php

namespace App\Controllers;

use \App\Models\M_View;

class M_userLevel extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {
        
        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $data = [
            'menu' => $menu,
            'user' => $this->M_View->getAllUserManagement(),
            'validation' => \Config\Services::validation(),
			'type'      => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'       => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'    => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'  => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            'level'     => $this->M_View->getAllUserLevel(),
            "test"      => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('m_userLevel', $data);
    }

    public function insertData() {

        $session = session();

        // validasi
        if(!$this->validate([
            'identity'  => 'required|is_unique[m_user_level.id]',
            'name'      => 'required|is_unique[m_user_level.name]'
        ],[
            'identity' => [
                'is_unique' => 'Level sudah ada boss !',
                'required'  => 'Harus di isi...'
            ],
            'name' => [
                'is_unique' => 'Sudah ada boss !',
                'required'  => 'Harus di isi...'
            ]
            
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed add new user! ');
            return redirect()->to('/M_UserLevel')->withInput()->with('validation', $validation);
        }

        // ambil data dari web
        $id     = esc($this->request->getVar('identity'));
        $name   = esc($this->request->getVar('name'));

        // masukan ke database
        $this->M_View->insertDataUserLevel($id, $name);

        // pesan sukses
        $session->setFlashdata('success_message', 'Success insert new user ! ');
        return redirect()->to('/m_userLevel');
    }

    public function deleteData() {
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataUserLevel($deleteId);
            $session->setFlashdata('success_message', 'Success delete user ! ');
            return redirect()->to('/m_userLevel');
        } else {
            $session->setFlashdata('error_message', 'Failed delete user ! ');
            return redirect()->to('/m_userLevel');
        }
    }

    public function editUserlevel($id) {
        $session = session();

        $getOldData = $this->M_View->getEditUserLevel($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
        ];

        return view('/E_m_userLevel', $data);
    }

    public function doEditUserLevel($id) {
        $session = session();

        $getOldData = $this->M_View->getEditUserLevel($id);
        $identity = esc($this->request->getVar('identity'));
        $name = esc($this->request->getVar('name'));
        $id = $id;

        if($getOldData['id'] == $identity) {
            $rule1 = 'required';
        } else {
            $rule1 = "required|is_unique[m_user_level.id]";
        }

        if($getOldData['name'] == $name) {
            $rule2 = 'required';
        } else {
            $rule2 = 'required|is_unique[m_user_level.name]';
        }

        // validasi
        if(!$this->validate([
            'identity' => $rule1,
            'name' => $rule2,
        ],[
            'identity' => [
                'is_unique' => 'Level sudah ada cuy... !',
                'required'  => 'Harus di isi...'
            ],
            'name' => [
                'is_unique' => 'Sudah ada cuy... !',
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', 'Failed update existing data! ');
            return redirect()->to('/M_UserLevel/editUserLevel' . '/' . $id)->withInput()->with('validation', $validation);
        }

        // masukan ke database
        $this->M_View->EditUserLevel($identity, $name, $id);
    
        $session->setFlashdata('success_message', 'Successfully update existing data ! ');
        $validation = \Config\Services::validation();
        return redirect()->to('/M_UserLevel')->withInput()->with('validation', $validation);
    }
}
