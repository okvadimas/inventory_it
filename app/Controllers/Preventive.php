<?php

namespace App\Controllers;

use \App\Models\M_View;

class Preventive extends BaseController
{ 
    public function __construct() {
        $this->M_View = new M_View();
    }

    public function index() {
        
        if(!session()->get('login')) {
            return redirect()->to('/auth');
        }

        $session = session();
        $lastUser = $this->M_View->getAllUserDesc();

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}
        
        $preventiveStatus = $this->M_View->getAllPreventiveStatus();
        
        $data = [
            'menu'  => $menu,
            'preventive' => $this->M_View->getAllPreventive(),
            'validation' => \Config\Services::validation(),
			'type'       => $this->M_View->getAllAssetType(),
			'status'    => $this->M_View->getAllStatus(),
			'lastUser'  => $lastUser,
            'position'  => $this->M_View->getAllPosition(),
            'condition' => $this->M_View->getAllCondition(),
            'gender'    => $this->M_View->getAllGender(),
            'use'       => $this->M_View->getAllAccessUse(),
            'deskera'   => $this->M_View->getAllAccessDeskera(),
            'emailUse'  => $this->M_View->getAllEmailUse(),
            'operatingSystem' => $this->M_View->getAllOperatingSystem(),
            'it'        => $this->M_View->getUserProfile($session->get('user_id')),
            'projectStatus' => $this->M_View->getAllProjectStatus(),
            'dept'      => $this->M_View->getAllDept(),
            'accessUse' => $this->M_View->getAllAccessUse(),
            'preventiveStatus' => $preventiveStatus,
            'level'     => $this->M_View->getAllUserLevel(),
            'userAccess' => $this->M_View->getAllUserAccess(),
            "test"      => $this->M_View->getReportPerMonth(),
            'overviewPreventive' => $this->M_View->getDataOverviewPreventive(),
			'overviewReset'	=> $this->M_View->getDataOverviewReset(),
			'overviewBackup' => $this->M_View->getDataOverviewBackup(),
			'overviewMonitoring' => $this->M_View->getDataOverviewMonitoring(),
        ];

        return view('preventive', $data);
    }

    public function insertData() {

        $session = session();

        // ambil data dari web
        $name_preventive = $this->request->getVar('name_preventive');
        $date_preventive = $this->request->getVar('date_preventive');
        $sequence_preventive = $this->request->getVar('sequence_preventive');
        $antivirus_preventive = $this->request->getVar('antivirus_preventive');
        $hardware_preventive = $this->request->getVar('hardware_preventive');
        $connection_preventive = $this->request->getVar('connection_preventive');
        $security_preventive = $this->request->getVar('security_preventive');
        $backup_preventive = $this->request->getVar('backup_preventive');
        $clean_preventive = $this->request->getVar('clean_preventive');
        $comment_preventive = $this->request->getVar('comment_preventive');
        $getOldData = $this->M_View->getAllPreventive();


        // validate
        if(!$this->validate([
            'name_preventive'   => 'required',
            'date_preventive'   => 'required',
            'sequence_preventive'   => 'required',
            'antivirus_preventive'  => 'required',
            'hardware_preventive'   => 'required',
            'connection_preventive' => 'required',
            'security_preventive'   => 'required',
            'backup_preventive'     => 'required',
            'clean_preventive'      => 'required',
        ], [
            'name_preventive'   => [
                'required'  => 'Harus di isi...'
            ],
            'date_preventive'   => [
                'required'  => 'Harus di isi...'
            ],  
            'sequence_preventive'   => [
                'required'  => 'Harus di isi...'
            ],
            'antivirus_preventive'  => [
                'required'  => 'Harus di isi...'
            ],
            'hardware_preventive'   => [
                'required'  => 'Harus di isi...'
            ],
            'connection_preventive' => [
                'required'  => 'Harus di isi...'
            ],
            'security_preventive'   => [
                'required'  => 'Harus di isi...'
            ],
            'backup_preventive' => [
                'required'  => 'Harus di isi'
            ],
            'clean_preventive'  => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::Lvalidation();
            $session->setFlashdata('error_message', '<strong>Failed insert new data preventive ! </strong>');
            return redirect()->to('/preventive')->withInput()->with('validation', $validation);
        }

        // validasi jika data sudah ada nama dan sequence yang sama validate error
        $check = $this->M_View->checkPreventive($name_preventive, $sequence_preventive);

        if($check != null) {
            $validation = \Config\Services::Lvalidation();
            $session->setFlashdata('error_message', '<strong>Already exists ! </strong>');
            return redirect()->to('/preventive')->withInput()->with('validation', $validation);
        } else {
            // masukan ke database
            $this->M_View->insertDataPreventive($name_preventive, $date_preventive, $sequence_preventive,
                $antivirus_preventive, $hardware_preventive, $connection_preventive, $security_preventive, $backup_preventive, $clean_preventive, $comment_preventive);
            
                // pesan sukses
            $session->setFlashdata('success_message', '<strong>Success insert new preventive ! </strong>');
            return redirect()->to('/preventive');
        }

    }

    public function deleteData() {
        dd('masuk');
        $deleteId = $this->request->getVar('deleteId');
        $session = session();

        if(isset($deleteId)) {
            $this->M_View->deleteDataPreventive($deleteId);
            $session->setFlashdata('success_message', '<strong>Success delete preventive data ! </strong>');
            return redirect()->to('/preventive');
        } else {
            $session->setFlashdata('error_message', '<strong>Failed delete preventive data ! </strong>');
            return redirect()->to('/preventive');
        }
    }

    public function editData($id) {
        $session = session();

        $getOldData = $this->M_View->getEditPreventive($id);

        if($session->get('level') == 1) {
			$menu = $this->M_View->getMenu();
		} else {
			$menu = $this->M_View->getAllMenuByLevel($session->get('level'));
		}

        $data = [
            'menu' => $menu,
            'data' => $getOldData,
            'validation' => \Config\Services::validation(),
            'preventiveStatus' => $this->M_View->getAllPreventiveStatus(),
            "test" 	=> $this->M_View->getReportPerMonth(),
        ];

        return view('/E_preventive', $data);
    }

    public function doEditData($id) {
        $session = session();

        // ambil data
        $userId      = esc($this->request->getVar('preventive_user'));
        $preventiveDate = esc($this->request->getVar('preventive_date'));
        $preventiveSeq = esc($this->request->getVar('preventive_seq'));
        $preventiveAntivirus = $this->request->getVar('preventive_antivirus');
        $preventiveHardware = $this->request->getVar('preventive_hardware');
        $preventiveConnection = $this->request->getVar('preventive_conn');
        $preventiveSecurity = $this->request->getVar('preventive_sec');
        $preventiveBackup = $this->request->getVar('preventive_backup');
        $preventiveClean = $this->request->getVar('preventive_clean');
        $preventiveComment = esc($this->request->getVar('preventive_comment'));
        $id = $id;
        $getOldData = $this->M_View->getEditPreventive($id);

        // // validate
        // if($preventiveUser == $getOldData['preventive_user'] && $preventiveSeq == $getOldData['preventive_seq']) {
        //     $validation = \Config\Services::validation();
        //     $session->setFlashdata('error_message', '<strong>User or Sequence already exists ! </strong>');
        //     return redirect()->to('/preventive/editData/' . $id)->withInput()->with('validation', $validation);
        // } else {
        //     $rule = 'required';
        // }

        if(!$this->validate([
            'preventive_user'   => 'required',
            'preventive_date'   => 'required',
            'preventive_seq'    => 'required',
            'preventive_antivirus'  => 'required',
            'preventive_hardware'   => 'required',
            'preventive_conn'   => 'required',
            'preventive_sec'    => 'required',
            'preventive_backup'    => 'required',
            'preventive_clean'  => 'required',
        ], [
            'preventive_user' => [
                'required'  => 'Harus di isi...',
                'is_unique' => 'Sudah ada cuy...'
            ],
            'preventive_date'   => [
                'required'  => 'Harus di isi...'
            ],
            'preventive_seq'    => [
                'required'  => 'Harus di isi...',
                'is_unique' => 'Sudah ada cuy...'
            ],
            'preventive_antivirus'  => [
                'required'  => 'Harus di isi...'
            ],
            'preventive_hardware'   => [
                'required'  => 'Harus di isi...'
            ],
            'preventive_conn'   => [
                'required'  => 'Harus di isi...'
            ],
            'preventive_sec'    => [
                'required'  => 'Harus di isi...'
            ],
            'preventive_backup' => [
                'required'  => 'Harus di isi...'
            ],
            'preventive_clean'  => [
                'required'  => 'Harus di isi...'
            ]
        ])) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed edit preventive data ! </strong>');
            return redirect()->to('/preventive/editData/' . $id)->withInput()->with('validation', $validation);
        }

        // validasi jika data sudah ada bulan dan sequence yang sama validate error
        $check = $this->M_View->checkPreventive($userId, $preventiveSeq);

        if($check != null) {

            // cekk dari dalam (lewat id yg sedang di edit)
            // jika data nya sama semua dengan yg di old data maka its oke
            if(
                $getOldData['preventive_date']      == $preventiveDate AND
                $getOldData['user_id']      == $userId AND
                $getOldData['preventive_seq']       == $preventiveSeq AND 
                $getOldData['preventive_antivirus'] == $preventiveAntivirus AND
                $getOldData['preventive_hardware']  == $preventiveHardware AND
                $getOldData['preventive_conn']      == $preventiveConnection AND
                $getOldData['preventive_sec']       == $preventiveSecurity AND
                $getOldData['preventive_backup']    == $preventiveBackup AND
                $getOldData['preventive_clean']     == $preventiveClean AND
                $getOldData['preventive_comment']   == $preventiveComment
            ) {
                // masukan ke database

                $this->M_View->editPreventive($id, $userId, $preventiveDate, $preventiveSeq, $preventiveAntivirus,
                $preventiveHardware, $preventiveConnection, $preventiveSecurity, $preventiveBackup, $preventiveClean, $preventiveComment);

            // pesan sukses
            $session->setFlashdata('success_message', '<strong>Successfully update existing preventive ! </strong>');
            return redirect()->to("/Preventive");
            } else {
                $validation = \Config\Services::validation();
                $session->setFlashdata('error_message', '<strong>Already exists ! </strong>');
                return redirect()->to('/preventive/editData/' . $id)->withInput()->with('validation', $validation);
            }

        } else {
            // masukan ke database
            $this->M_View->editPreventive($id, $userId, $preventiveDate, $preventiveSeq, $preventiveAntivirus,
                $preventiveHardware, $preventiveConnection, $preventiveSecurity, $preventiveBackup, $preventiveClean, $preventiveComment);
    
            // pesan sukses
            $session->setFlashdata('success_message', '<strong>Successfully update existing preventive ! </strong>');
            return redirect()->to("/Preventive");
        }
    }

    public function importData() {
        $session = session();
        // dd('masuk import preventive');
        $file = $this->request->getFile('importData');
        $filename = $file->getName();
        $ext = $file->getClientExtension();
        
        if($ext == 'xls') {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xls();
        } else {
            $render = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
        }

        // jika error = 0 maka file ada, jika error = 4 maka file null
        if($file->getError() == 4) {
            $validation = \Config\Services::validation();
            $session->setFlashdata('error_message', '<strong>Failed !!</strong> file not found ! Please try again...');
            return redirect()->to('/Preventive')->withInput()->with('validation', $validation);
        }

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);

        $sheet = $spreadsheet->getActiveSheet()->toArray();
        $log = array('success' => 0, 'failed' => 0);

        if($spreadsheet != null) {
            foreach($sheet as $baris => $kolom) {
                if($baris == 0) {
                    continue;
                }
				
                // validasi duplicate
                $check = $this->M_View->checkPreventive($kolom['0'], $kolom['4']);

                if($kolom['0'] == null && $kolom['1'] == null && $kolom['2'] == null && $kolom['3'] == null && $kolom['4'] == null && $kolom['5'] == null && $kolom['6'] == null && $kolom['7'] == null && $kolom['8'] == null) {
                    continue;
                }

                if($check != null) {
                    $log['failed'] += 1;
                    // dd($log);
                    continue;
                } elseif($kolom['0'] == null || $kolom['1'] == null || $kolom['2'] == null || $kolom['3'] == null || $kolom['4'] == null || $kolom['5'] == null || $kolom['6'] == null || $kolom['7'] == null || $kolom['8'] == null) {
                    $log['failed'] += 1;
                    continue;
                } else {

                    $userId     = $kolom['0'];
                    $date       = $kolom['1'];
                    $sequence   = $kolom['2'];
                    $antiVirus  = $kolom['3'];
                    $hardware   = $kolom['4'];
                    $connection = $kolom['5'];
                    $security   = $kolom['6'];
                    $backup     = $kolom['7'];
                    $clean      = $kolom['8'];
                    $comment    = $kolom['9'];
                            
                    // masukan ke database
                    $this->M_View->importPreventive($userId, $date, $sequence, $antiVirus, $hardware, $connection, $security, $backup, $clean, $comment);
                    $log['success'] += 1;
                }
            }  

            $session->setFlashdata('success_message', '<strong>Successfully !!</strong> import ' . $log['success'] . ' and ' . $log['failed'] . ' <span class="text-danger">failure</span>');
            return redirect()->to('/Preventive');
        }
    }

    public function templateImportData() {
        // dd('masuk download template preventive');
        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $sheet->setCellValue('A1', 'User ID*');
        $sheet->setCellValue('B1', 'Date*');
        $sheet->setCellValue('C1', 'Sequence*');
        $sheet->setCellValue('D1', 'Antivirus*');
        $sheet->setCellValue('E1', 'Hardware*');
        $sheet->setCellValue('F1', 'Connection*');
        $sheet->setCellValue('G1', 'Security*');
        $sheet->setCellValue('H1', 'Backup*');
        $sheet->setCellValue('I1', 'Clean Up*');
        $sheet->setCellValue('J1', 'Comment');

        $sheet->setCellValue('A2', '2175');
        $sheet->setCellValue('B2', '2022-03-15');
        $sheet->setCellValue('C2', '3');
        $sheet->setCellValue('D2', '1');
        $sheet->setCellValue('E2', '1');
        $sheet->setCellValue('F2', '1');
        $sheet->setCellValue('G2', '1');
        $sheet->setCellValue('H2', '1');
        $sheet->setCellValue('I2', '1');
        $sheet->setCellValue('J2', 'All Good');

        // informasi untuk template 

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
        $fileName = 'Template import preventive';
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }

    public function exportData() {
        $session = session();

        // get data
        $fromSeq    = esc($this->request->getVar('exportData1'));
        $toSeq      = esc($this->request->getVar('exportData2'));
        $fromYear   = esc($this->request->getVar('exportData3'));

        $date = date('j M Y');

        // rule
        if($fromSeq == null && $toSeq == null && $fromYear == null) {
            $dataTemplate = $this->M_View->getAllDataPreventive();
            $fileName = 'All data preventive - ' . $date;
        } elseif($fromSeq == null && $toSeq == null && $fromYear != null) {
            $dataTemplate = $this->M_View->getAllDataPreventiveByYear($fromYear);
            $fileName = 'All data preventive by year ' . $fromYear . ' - ' . $date;
        } elseif($fromSeq != null && $toSeq != null && $fromYear != null) {
            $dataTemplate = $this->M_View->getAllDataPreventiveSpecific($fromSeq, $toSeq, $fromYear);
            $fileName = 'All data preventive sequence ' . $fromSeq . ' to ' . $toSeq . ' by year ' . $fromYear . ' - ' . $date;
        } elseif($fromSeq != null && $toSeq != null && $fromYear == null) {
            $dataTemplate = $this->M_View->getAllDataPreventiveBySeq($fromSeq, $toSeq);
            $fileName = 'All data preventive sequence ' . $fromSeq . ' to ' . $toSeq . ' - ' . $date;
        } else {
            $session->setFlashdata('error_message', '<strong>Failed !!</strong> wrong rule ! Please read the rule and try again...');
            return redirect()->to('preventive');
        }

        $spreadsheet = new \PhpOffice\PhpSpreadsheet\Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();


        $sheet->setCellValue('A1', 'Date');
        $sheet->setCellValue('B1', 'Sequence');
        $sheet->setCellValue('C1', 'Username');
        $sheet->setCellValue('D1', 'Department');
        $sheet->setCellValue('E1', 'Use');
        $sheet->setCellValue('F1', 'Antivirus');
        $sheet->setCellValue('G1', 'Hardware');
        $sheet->setCellValue('H1', 'Connection');
        $sheet->setCellValue('I1', 'Security');
        $sheet->setCellValue('J1', 'Backup');
        $sheet->setCellValue('K1', 'Clean');
        $sheet->setCellValue('L1', 'Comment');
        $rows = 2;

        foreach ($dataTemplate as $dt){
            $sheet->setCellValue('A' . $rows, $dt['preventive_date']);
            $sheet->setCellValue('B' . $rows, $dt['preventive_seq']);
            $sheet->setCellValue('C' . $rows, $dt['preventive_user']);
            $sheet->setCellValue('D' . $rows, $dt['dept_name']);
            $sheet->setCellValue('E' . $rows, $dt['au_name']);
            $sheet->setCellValue('F' . $rows, $dt['preventive_antivirus'] == 1 ? 'Done' : 'Pending');
            $sheet->setCellValue('G' . $rows, $dt['preventive_hardware'] == 1 ? 'Done' : 'Pending');
            $sheet->setCellValue('H' . $rows, $dt['preventive_conn'] == 1 ? 'Done' : 'Pending');
            $sheet->setCellValue('I' . $rows, $dt['preventive_sec'] == 1 ? 'Done' : 'Pending');
            $sheet->setCellValue('J' . $rows, $dt['preventive_backup'] == 1 ? 'Done' : 'Pending');
            $sheet->setCellValue('K' . $rows, $dt['preventive_clean'] == 1 ? 'Done' : 'Pending');
            $sheet->setCellValue('L' . $rows, $dt['preventive_comment']);
            $rows++;
        }

        // buat excelnya, fyi inisialisasi spreadsheet itu buat file excel kosong baru dan writer itu mengisi file kosong itu dengan data diatas
        $writer = new \PhpOffice\PhpSpreadsheet\Writer\Xlsx($spreadsheet);
    
        // Redirect hasil generate xlsx ke web client
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName.'.xlsx');
        header('Cache-Control: max-age=0');
    
        // auto download disini, gaperlu dibalikin ke redirect lagi
        $writer->save('php://output');

        // return redirect()->to('/warehouse/products');
    }
}
