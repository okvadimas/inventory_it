<?php 

use \App\Models\M_View;

function loggedIn() {

    $M_View = new M_View();
    $session = session();
    $uri = service('uri');

    if($session->get('login') != True) {
        // return redirect()->route('auth');
        return redirect()->to('/auth');
    } elseif($uri->getSegment(1) != null && $session->get('login') != null) {
        $level = $session->get('level');
        $menus = $uri->getSegment(1);

        $menuData = $M_View->getMenuIdByName($menus);
        $menuId = $menuData['menu_id'];

        if($level != 1) {
            $userAccess = $M_View->getUserAccess($level, $menuId);
                if($userAccess < 1 ) {
                return redirect()->to('/404');
            }
        } 
    } else {
        return redirect()->to('/auth');
    }
}