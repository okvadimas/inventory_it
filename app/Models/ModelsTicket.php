<?php namespace App\Models;

use CodeIgniter\Model;
use CodeIgniter\HTTP\RequestInterface;

class ModelsTicket extends Model
{
    public function __construct(RequestInterface $request, $table, $column_order, $column_search, $order) {
        $this->db = db_connect();
        $this->session = session();

        // dynamic server side
        $this->request = $request;
        $this->column_order = $column_order;
        $this->column_search = $column_search;
        $this->order = $order;
        $this->table = $table;

        $tabel = $this->table . ' ticket';
        $this->dt = $this->db->table($tabel);   
        $this->dt->select("ticket.*, md.name as dept, u1.user_fullname as createdBy, u2.user_fullname as updatedBy, ms.name1 as status1, ms.name2 as status2");
        $this->dt->join('user u1', 'ticket.created_by = u1.user_asset_id', 'left');
        $this->dt->join('user u2', 'ticket.updated_by = u2.user_asset_id', 'left');
        $this->dt->join('m_dept md', 'u1.user_dept = md.id', 'left');
        $this->dt->join('m_status ms', 'ticket.ticket_status = ms.id', 'left');
        $this->dt->orderBy('ticket.ticket_status DESC, ticket.created_at DESC');   
        // $query = $this->dt->get();
        // dd($query->getResultArray());     
    }
   
    public function getDatatables($fromDate = null, $toDate = null)
    {
        $this->getDatatablesQuery($fromDate, $toDate);

        if ($this->request->getPost('length') != -1)
            $this->dt->limit($this->request->getPost('length'), $this->request->getPost('start'));                       
        $query = $this->dt->get();

        return $query->getResult();
    }

    private function getDatatablesQuery($fromDate = null, $toDate = null)
    {        

        if($fromDate != null AND $toDate != null) {
            $this->dt->where("created_at BETWEEN '$fromDate' AND '$toDate'");
        }

        $i = 0;
        foreach ($this->column_search as $item) {
            if ($this->request->getPost('search')['value']) {
                if ($i === 0) {
                    $this->dt->groupStart();
                    $this->dt->like($item, $this->request->getPost('search')['value']);
                } else {
                    $this->dt->orLike($item, $this->request->getPost('search')['value']);
                }
                if (count($this->column_search) - 1 == $i)
                    $this->dt->groupEnd();
            }
            $i++;
        }

        // urutan
        if ($this->request->getPost('order')) {
         
            $this->dt->orderBy($this->column_order[$this->request->getPost('order')['0']['column']], $this->request->getPost('order')['0']['dir']);
        } else if (!isset($this->order)) {
            $order = $this->order;
            $this->dt->orderBy(key($order), $order[key($order)]);
        }
    }

    public function countFiltered($fromDate = null, $toDate = null)
    {
        $this->getDatatablesQuery($fromDate, $toDate);
        return $this->dt->countAllResults();
    }

    public function countAll()
    {
        $tbl_storage = $this->db->table($this->table);
        return $tbl_storage->countAllResults();
    }

    public function getAllDataTicket() {
        $query = "SELECT t.*, u1.user_fullname as createdBy, u2.user_fullname as updatedBy, d.name as dept, s.name1 as status FROM ticket 
            LEFT JOIN user u1 ON u1.user_asset_id = t.created_by
            LEFT JOIN user u2 ON u2.user_asset_id = t.updated_by
            LEFT JOIN m_dept d ON u1.user_dept = d.id
            LEFT JOIN m_status s ON t.ticket_status = s.id";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getSpecificDataTicket($fromDate, $toDate) {
        $query = "SELECT t.*, u1.user_fullname as createdBy, u2.user_fullname as updatedBy, d.name as dept, s.name1 as status FROM ticket 
            LEFT JOIN user u1 ON u1.user_asset_id = t.created_by
            LEFT JOIN user u2 ON u2.user_asset_id = t.updated_by
            LEFT JOIN m_dept d ON u1.user_dept = d.id
            LEFT JOIN m_status s ON t.ticket_status = s.id
            WHERE t.created_at BETWEEN '$fromDate' AND '$toDate'";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getLastIdDataTicket() {
        $query = "SELECT * FROM ticket ORDER BY ticket_id DESC";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function insertDataTicket($userId, $message, $createdAt, $status, $ticketNumber, $opsi1, $opsi2, $opsi3) {
        $query = "INSERT INTO ticket (ticket_number, ticket_message, ticket_status, created_by, created_at, ticket_type, ticket_level, ticket_due)
            VALUES ('$ticketNumber', '$message', '$status', '$userId', '$createdAt', '$opsi1', '$opsi2', '$opsi3')";
        $this->db->query($query);
    }

    public function updateTicket($updatedBy, $updatedAt, $status, $id) {
        $query = "UPDATE ticket SET ticket_status = '$status', updated_by = '$updatedBy', updated_at = '$updatedAt' WHERE ticket_number = '$id'";
        $this->db->query($query);
    }

    public function getUsernameByUserId($userId) {
        $query = "SELECT * FROM user WHERE user_asset_id = '$userId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }
}
