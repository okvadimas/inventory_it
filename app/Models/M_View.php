<?php namespace App\Models;

use CodeIgniter\Model;

class M_View extends Model
{
    public function checkIdGoogle($id) {
        $query = "SELECT * FROM google WHERE google_id = '$id'";
        $hasil = $this->db->query($query);
        // dd($hasil->getRowArray());
        return $hasil->getRowArray();
    }

    public function updateDataGoogle($email, $username, $familyName, $id, $picture, $gender, $verifiedEmail) {
        $query = "UPDATE google SET
            google_username = '$username',
            google_family_name = '$familyName',
            google_picture = '$picture',
            google_gender = '$gender',
            google_verified_email = '$verifiedEmail' WHERE google_id = '$id'";
        $this->db->query($query);
    }

    public function insertDataGoogle($email, $username, $familyName, $id, $picture, $gender, $verifiedEmail) {
        // dd($email, $username, $familyName, $id, $picture, $gender, $verifiedEmail);
        $query = "INSERT INTO google (google_email, google_username, google_family_name, google_id, google_picture, google_gender, google_verified_email) 
            VALUES ('$email', '$username', '$familyName', '$id', '$picture', '$gender', '$verifiedEmail')";
        $this->db->query($query);
    }

    public function supportSite($user_id, $support, $support_date) {
        $query = "INSERT INTO support (user_id, support_text, support_date) VALUES ('$user_id', '$support', '$support_date')";
        $this->db->query($query);
    }

    public function getAllSupportUnique() {
        $query = "SELECT DISTINCT user_id FROM support";
        $hasil = $this->db->query($query);
        return count($hasil->getResultArray());
    }

    public function getAllSupport() {
        $query = "SELECT s.*, u.user_fullname, u.user_level FROM support s INNER JOIN user u ON s.user_id = u.user_id";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function deleteSupportSystem($id) {
        $query = "DELETE FROM support WHERE support_id = '$id'";
        $this->db->query($query);
    }

    public function getReportPerMonth() {
        $query = "SELECT report_month, SUM(report_total) as report_total FROM report GROUP BY report_month";
        $hasil = $this->db->query($query);
        return $hasil->getResult();
    }

    public function getLastDateDailyReport() {
        $query = "SELECT report_date, report_month, report_year FROM report ORDER BY report_id DESC";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function insertReportDaily($dateDaily, $monthDaily, $yearDaily, $countDaily) {
        $query = "INSERT INTO report (report_date, report_month, report_year, report_total) VALUES ('$dateDaily', '$monthDaily', '$yearDaily', '$countDaily')";
        $this->db->query($query);
    }

    public function updateReportDaily($dateDaily, $monthDaily, $yearDaily, $currentCountDaily) {
        $query = "UPDATE report SET 
            report_total = '$currentCountDaily' WHERE report_date = '$dateDaily' AND report_month = '$monthDaily' AND report_year = '$yearDaily'";
        $this->db->query($query);
    }

    public function getLastCountDailyReport() {
        $query = "SELECT report_total, report_date, report_month, report_year FROM report ORDER BY report_id DESC";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getUser($user) {
       $query = "SELECT * FROM user WHERE user_username = '$user'";
       $hasil = $this->db->query($query);
       return $hasil->getRowArray();
    }

    public function getAllUser() {
        $query = "SELECT user_username FROM user";
        $hasil = $this->db->query($query);
        return count($hasil->getResultArray());
    }

    public function getUserDeptHrd() {
        $query = "SELECT u.user_id, u.user_fullname, d.name as dept FROM user u 
            INNER JOIN m_dept d ON u.user_dept = d.id 
            WHERE d.name = 'HRD'";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllUserDesc() {
        $query = "SELECT u.*, d.name as dept, d.slug as deptSlug, s.name1 as status, ul.name as level FROM user u 
            INNER JOIN m_dept d ON u.user_dept = d.id 
            INNER JOIN m_status s ON u.user_status = s.id 
            INNER JOIN m_user_level ul ON u.user_level = ul.id 
            ORDER BY user_id DESC LIMIT 1";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function addUser($userReg, $passReg, $emailReg, $level, $create, $status, $dept, $asset_id) {
        $query = "INSERT INTO user (user_username, user_pass, user_email, user_dept, user_level, user_create, user_status, user_asset_id)
            VALUES ('$userReg', '$passReg', '$emailReg', '$dept', '$level', '$create', '$status', '$asset_id')";
        $this->db->query($query);
    }

    public function getMenu()
    {
        $query = "SELECT * FROM menu ORDER BY sort ASC";
        $menu = $this->db->query($query);
        return $menu->getResultArray();
    }

    public function getMenuIdByName($name) {
        $query = "SELECT * FROM menu WHERE menu_url = '/$name'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getAllMenuByLevel($level) {
        $query = "SELECT m.* FROM menu m INNER JOIN user_access_menu uam ON m.menu_id = uam.menu_id WHERE uam.level = '$level' ORDER BY sort ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditMenuManagement($id) {
        $query = "SELECT * FROM menu WHERE menu_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editMenuManagement($menuName, $menuIcon, $menuUrl, $menuSort, $id) {
        $query = "UPDATE menu SET 
            menu_name = '$menuName',
            menu_icon = '$menuIcon',
            menu_url = '$menuUrl',
            sort = '$menuSort' WHERE menu_id = '$id'";
        $this->db->query($query);
    }

    public function insertDataMenuManagement($name, $icon, $url, $sort) {
        $query = "INSERT INTO menu (menu_name, menu_icon, menu_url, sort) VALUES ('$name', '$icon', '$url', '$sort')";
        $this->db->query($query);
    }

    public function deleteDataMenuManagement($id) {
        $query = "DELETE FROM menu WHERE menu_id = '$id'";
        $this->db->query($query);
    }

    public function getAllPostMessage() {
        $query = "SELECT p.*, u.user_fullname, d.name as dept FROM post p 
            INNER JOIN user u ON p.post_by = u.user_id
            INNER JOIN m_dept d ON u.user_dept = d.id";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditPostMessage($id) {
        $query = "SELECT p.*, u.user_fullname as name FROM post p INNER JOIN user u ON p.post_by = u.user_id WHERE p.post_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editPostMessage($id, $title, $by, $create, $message) {
        $query = "UPDATE post SET 
            post_title = '$title',
            post_message = '$message',
            post_create = '$create',
            post_by = '$by' WHERE post_id = '$id'";
        $this->db->query($query);
    }
    
    public function postMessage($title, $message, $createAt, $by, $id) {
        $query = "INSERT INTO post (post_title, post_message, post_create, post_by, user_id)
            VALUES ('$title', '$message', '$createAt', '$by', '$id')";
        $this->db->query($query);
    }

    public function deletePostMessage($id) {
        $query = "DELETE FROM post WHERE post_id = '$id'";
        $this->db->query($query);
    }

    public function getPreviewPost() {
        $query = "SELECT u.user_dept, u.user_fullname, m.name as dept, p.post_title, p.post_message, p.post_by, p.post_create 
            FROM post p INNER JOIN user u ON p.user_id = u.user_id 
            INNER JOIN m_dept m ON u.user_dept = m.id ORDER BY p.post_id desc LIMIT 1";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllOverview() {
        $query = "SELECT at.*, count(at.id = a.asset_type) as used FROM m_asset_type at INNER JOIN asset a ON at.id = a.asset_type GROUP BY a.asset_type ORDER BY at.id ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function insertDataOverview($id, $type, $available, $borrowed, $info) {
        $query = "INSERT INTO m_asset_type (asset_id, type, available, borrowed, information)
            VALUES ('$id', '$type', '$available', '$borrowed', '$info')";
        $this->db->query($query);
    }

    public function getDataOverviewById($id) {
        $query = 'SELECT * FROM m_asset_type';
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function deleteDataOverview($id) {
        $query = "DELETE FROM m_asset_type WHERE id = '$id'";
        $this->db->query($query);
    }

    // kalau email, domain, dan internet sudah di isi pakai ini
    // public function getUserProfile($user_id) {
    //     $query = "SELECT u.*, d.name as dept_name, d.slug as dept_slug, s.name1 as status_name1, s.name2 as status_name2,
    //         a.access_pass as domain, a.access_inet_pass as internet, e.email_pass as email FROM user u 
    //         INNER JOIN m_dept d ON u.user_dept = d.id 
    //         INNER JOIN m_status s ON u.user_status = s.id 
    //         INNER JOIN access a ON u.user_id = a.user_id
    //         INNER JOIN email e ON u.user_id = e.user_id WHERE u.user_id = '$user_id' LIMIT 1";
    //     $hasil = $this->db->query($query);
    //     return $hasil->getRowArray();
    // }

    public function getUserProfile($user_id) {
        $query = "SELECT u.*, d.name as dept_name, d.slug as dept_slug, s.name1 as status_name1, s.name2 as status_name2
            FROM user u 
            INNER JOIN m_dept d ON u.user_dept = d.id 
            INNER JOIN m_status s ON u.user_status = s.id WHERE u.user_id = '$user_id' LIMIT 1";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getAllDept() {
        $query = "SELECT * FROM m_dept ORDER BY id DESC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditDept($id) {
        $query = "SELECT * FROM m_dept WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editDept($id, $identity, $dept, $slug) {
        $query = "UPDATE m_dept SET id = '$identity', name = '$dept', slug = '$slug' WHERE id = '$id'";
        $this->db->query($query);
    }

    public function insertDataDept($id, $name, $slug) {
        $query = "INSERT INTO m_dept (id, name, slug) VALUES ('$id', '$name', '$slug')";
        $this->db->query($query);
    }

    public function deleteDataDept($id) {
        $query = "DELETE FROM m_dept WHERE id = '$id'";
        $this->db->query($query);
    }

    public function getDeptById($id) {
        $query = "SELECT * FROM m_dept WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getAllStatus() {
        $query = "SELECT * from m_status";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    } 

    public function getAllStatusLog() {
        $query = "SELECT * from m_status WHERE id BETWEEN 5 AND 6";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditStatus($id) {
        $query = "SELECT * FROM m_status WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editStatus($id, $identity, $name1, $name2) {
        $query = "UPDATE m_status SET id = '$identity', name1 = '$name1', name2 = '$name2' WHERE id = '$id'";
        $this->db->query($query);
    }

    public function insertDataStatus($id, $name1, $name2) {
        $query = "INSERT INTO m_status (id, name1, name2) VALUES ('$id', '$name1', '$name2')";
        $this->db->query($query);
    }

    public function deleteDataStatus($id) {
        $query = "DELETE FROM m_status WHERE id = '$id'";
        $this->db->query($query);
    }

    public function getAllAssetType() {
        $query = "SELECT * FROM m_asset_type";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getDataOverview() {
        $query = "SELECT at.asset_id FROM m_asset_type at INNER JOIN asset a ON at.id = a.asset_type ORDER BY id ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditAssetType($id) {
        $query = "SELECT * FROM m_asset_type WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getEditAssetTypeByAssetId($asset_id) {
        $query = "SELECT * FROM m_asset_type WHERE asset_id = '$asset_id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editAssetType($id, $asset_id, $type, $borrowed, $available, $information) {
        $query = "UPDATE m_asset_type SET type = '$type', asset_id = '$asset_id', available = '$available', borrowed = '$borrowed', information = '$information' WHERE id = '$id'";
        $this->db->query($query);
    }

    public function insertDataAssetType($identity, $type, $borrowed, $available, $information) {
        $query = "INSERT INTO m_asset_type (asset_id, type, borrowed, available, information)
            VALUES ('$identity', '$type', '$borrowed', '$available', '$information')";
        $this->db->query($query);
    }

    public function deleteDataAssetType($id) {
        $query = "DELETE FROM m_asset_type WHERE id = '$id'";
        $this->db->query($query);
    }

    public function getAllPosition() {
        $query = "SELECT * FROM m_position";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditPosition($id) {
        $query = "SELECT * FROM m_position WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editPosition($id, $identity, $name) {
        $query = "UPDATE m_position SET id = '$identity', name = '$name' WHERE id = '$id'";
        $this->db->query($query);
    }

    public function insertDataPosition($id, $name) {
        $query = "INSERT INTO m_position (id, name) VALUES ('$id', '$name')";
        $this->db->query($query);
    }

    public function deleteDataPosition($id) {
        $query = "DELETE FROM m_position WHERE id = '$id'";
        $this->db->query($query);
    }

    public function getAllCondition() {
        $query = "SELECT * FROM m_condition";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditCondition($id) {
        $query = "SELECT * FROM m_condition WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editCondition($id, $identity, $name) {
        $query = "UPDATE m_condition SET id = '$identity', name = '$name' WHERE id = '$id'";
        $this->db->query($query);
    }

    public function insertDataCondition($id, $name) {
        $query = "INSERT INTO m_condition (id, name) VALUES ('$id', '$name')";
        $this->db->query($query);
    }

    public function deleteDataCondition($id) {
        $query = "DELETE FROM m_condition WHERE id = '$id'";
        $this->db->query($query);
    }

    public function getAllGender() {
        $query = "SELECT * FROM m_gender";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditGender($id) {
        $query = "SELECT * FROM m_gender WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editDataGender($id, $identity, $name) {
        $query = "UPDATE m_gender SET id = '$identity', name = '$name' WHERE id = '$id'";
        $this->db->query($query);
    }

    public function insertDataGender($id, $name) {
        $query = "INSERT INTO m_gender (id, name) VALUES ('$id', '$name')";
        $this->db->query($query);
    }

    public function deleteDataGender($id) {
        $query = "DELETE FROM m_gender WHERE id = '$id'";
        $this->db->query($query);
    }

    public function getAllAccessUse() {
        $query = "SELECT * FROM m_access_use";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function insertDataAccessUse($id, $name) {
        $query = "INSERT INTO m_access_use (id, name) VALUES ('$id', '$name')";
        $this->db->query($query);
    }

    public function getEditAccessUse($id) {
        $query = "SELECT * FROM m_access_use WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editAccessUse($id, $identity, $name) {
        $query = "UPDATE m_access_use SET id = '$identity', name = '$name' WHERE id = '$id'";
        $this->db->query($query);
    }

    public function deleteDataAccessUse($id) {
        $query = "DELETE FROM m_access_use WHERE id = '$id'";
        $this->db->query($query);
    }

    public function getAllAccessDeskera() {
        $query = "SELECT * FROM m_access_deskera";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditDataDeskera($id) {
        $query = "SELECT * FROM m_access_deskera WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editDeskera($id, $identity, $name) {
        $query = "UPDATE m_access_deskera SET id = '$identity', name = '$name' WHERE id = '$id'";
        $this->db->query($query);
    }

    public function insertDataDeskera($id, $name) {
        $query = "INSERT INTO m_access_deskera (id, name) VALUES ('$id', '$name')";
        $this->db->query($query);
    }

    public function deleteDataDeskera($id) {
        $query = "DELETE FROM m_access_deskera WHERE id = '$id'";
        $this->db->query($query);
    }

    public function getAllEmailUse() {
        $query = "SELECT * FROM m_email_use";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditEmailUse($id) {
        $query = "SELECT * FROM m_email_use WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editEmailUse($id, $identity, $name) {
        $query = "UPDATE m_email_use SET id = '$identity', name = '$name' WHERE id = '$id'";
        $this->db->query($query);
    }

    public function insertDataEmailUse($id, $name) {
        $query = "INSERT INTO m_email_use (id, name) VALUES ('$id', '$name')";
        $this->db->query($query);
    }

    public function deleteDataEmailUse($id) {
        $query = "DELETE FROM m_email_use WHERE id = '$id'";
        $this->db->query($query);
    }

    public function getAllOperatingSystem() {
        $query = "SELECT * FROM m_operating_system";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditOperatingSystem($id) {
        $query = "SELECT * FROM m_operating_system WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editOperatingSystem($id, $identity, $name) {
        $query = "UPDATE m_operating_system SET id = '$identity', name = '$name' WHERE id = '$id'";
        $this->db->query($query);
    }

    public function insertDataOperatingSystem($id, $name) {
        $query = "INSERT INTO m_operating_system (id, name) VALUES ('$id', '$name')";
        $this->db->query($query);
    }

    public function deleteDataOperatingSystem($id) {
        $query = "DELETE FROM m_operating_system WHERE id = '$id'";
        $this->db->query($query);
    }

    public function getAllUserLevel() {
        $query = "SELECT * FROM m_user_level";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditUserLevel($id) {
        $query = "SELECT * FROM m_user_level WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function EditUserLevel($identity, $name, $id) {
        $query = "UPDATE m_user_level SET id = '$identity', name = '$name' WHERE id = '$id'";
        $this->db->query($query);
    }

    public function insertDataUserLevel($id, $name) {
        $query = "INSERT INTO m_user_level (id, name) VALUES ('$id', '$name')";
        $this->db->query($query);
    }

    public function deleteDataUserLevel($id) {
        $query = "DELETE FROM m_user_level WHERE id = '$id'";
        $this->db->query($query);
    }

    public function getAllProjectStatus() {
        $query = "SELECT * FROM m_project_status";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditProjectStatus($id) {
        $query = "SELECT * FROM m_project_status WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editProjectStatus($id, $identity, $name) {
        $query = "UPDATE m_project_status SET id = '$identity', name = '$name' WHERE id = '$id'";
        $this->db->query($query);
    }

    public function editProject($id, $projectName, $projectPlanning, $projectActual, $projectStatus, $projectComment) {
        $query = "UPDATE project SET
            project_name = '$projectName',
            project_planning = '$projectPlanning',
            project_actual = '$projectActual',
            project_status = '$projectStatus',
            project_comment = '$projectComment' WHERE project_id = '$id'";
        $this->db->query($query);
    }

    public function insertDataProjectStatus($id, $name) {
        $query = "INSERT INTO m_project_status (id, name) VALUES ('$id', '$name')";
        $this->db->query($query);
    }

    public function deleteDataProjectStatus($id) {
        $query = "DELETE FROM m_project_status WHERE id = '$id'";
        $this->db->query($query);
    }

    public function getAllUserAccess() {
        $query = "SELECT uam.*, ul.name as nameLevel, m.menu_name as menuName FROM user_access_menu uam 
            INNER JOIN m_user_level ul ON uam.level = ul.id 
            INNER JOIN menu m ON uam.menu_id = m.menu_id";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditUserAccess($id) {
        $query = "SELECT uam.*, m.menu_name FROM user_access_menu uam INNER JOIN menu m ON uam.menu_id = m.menu_id WHERE uam.uam_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editUserAccess($id, $level, $menu) {
        $query = "UPDATE user_access_menu SET level = '$level', menu_id = '$menu' WHERE uam_id = '$id'";
        $this->db->query($query);
    }

    public function getUserAccessByLevel($level, $menu) {
        $query = "SELECT * FROM user_access_menu WHERE level = '$level' AND menu_id = '$menu'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getUserAccess($level, $menu_id) {
        $query = "SELECT * FROM user_access_menu WHERE level = '$level' AND menu_id = '$menu_id'";
        $hasil = $this->db->query($query);
        return $hasil->getNumRows();
    } 

    public function insertDataUserAccess($level, $menu) {
        $query = "INSERT INTO user_access_menu (level, menu_id) VALUES ('$level', '$menu')";
        $this->db->query($query);
    }

    public function deleteDataUserAccess($id) {
        $query = "DELETE FROM user_access_menu WHERE uam_id = '$id'";
        $this->db->query($query);
    }

    public function getAllPreventiveStatus() {
        $query = "SELECT * FROM m_preventive_status";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }
    
    public function updateProfile($fullname, $email, $phone, $dept, $birth, $asset_id, $id, $pictureName) {
        $query = "UPDATE user SET
            user_fullname = '$fullname',
            user_email = '$email',
            user_phone = '$phone',
            user_dept = '$dept',
            user_birth = '$birth',
            user_picture = '$pictureName',
            user_asset_id = '$asset_id' WHERE user_id = '$id'
        ";
        $this->db->query($query);
    }

    public function resetPassword($newPass, $id) {
        $query = "UPDATE user SET user_pass = '$newPass' WHERE user_id = '$id'";
        $this->db->query($query);
    }

    public function getAllAsset() {
        $query = "SELECT a.*, u.user_fullname, u.user_picture, u.user_dept, u.user_asset_id, at.asset_id as asset_type_id, at.type, d.name as dept, s.name1 as status, os.name as os FROM asset a 
            INNER JOIN user u ON a.user_id = u.user_asset_id 
            INNER JOIN m_asset_type at ON a.asset_type = at.id 
            INNER JOIN m_dept d ON u.user_dept = d.id 
            INNER JOIN m_status s ON u.user_status = s.id
            INNER JOIN m_operating_system os ON a.asset_os = os.id ORDER BY RAND()";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditAsset($id) {
        $query = "SELECT a.*, u.user_fullname, u.user_asset_id, u.user_picture, u.user_dept, at.asset_id as asset_type_id, at.type, d.name as dept FROM asset a 
            INNER JOIN user u ON a.user_id = u.user_asset_id 
            INNER JOIN m_asset_type at ON a.asset_type = at.id 
            INNER JOIN m_dept d ON u.user_dept = d.id WHERE a.asset_id = '$id'";
            $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getEditAssetByUserId($userId) {
        $query = "SELECT a.*, u.user_fullname, u.user_asset_id, u.user_picture, u.user_dept, at.asset_id as asset_type_id, at.type, d.name as dept FROM asset a 
            INNER JOIN user u ON a.user_id = u.user_asset_id 
            INNER JOIN m_asset_type at ON a.asset_type = at.id 
            INNER JOIN m_dept d ON u.user_dept = d.id WHERE a.user_id = '$userId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editAssetData($id, $asset_type, $processor, $storage, $graphics, $motherboard, $memory, $monitor, $jumlah, $information, $operating_system, $serial_number) {
        $query = "UPDATE asset SET
            asset_type = '$asset_type',
            asset_cpu = '$processor',
            asset_hdd = '$storage',
            asset_vga = '$graphics',
            asset_mb = '$motherboard',
            asset_ram = '$memory',
            asset_monitor = '$monitor',
            asset_total = '$jumlah',
            asset_info = '$information',
            asset_os = '$operating_system',
            asset_serial = '$serial_number' WHERE asset_id = '$id'";
        $this->db->query($query);
    }

    public function insertDataAsset($asset_type, $information, $processor, $motherboard, $storage, $memory, $graphics, $monitor, $jumlah, $user_id, $operatingSystem, $serialNumber) {
        $query = "INSERT INTO asset (asset_type, asset_info, asset_total, asset_cpu, asset_mb, asset_hdd, asset_ram, asset_vga, asset_monitor, user_id, asset_os, asset_serial)
            VALUES ('$asset_type', '$information', '$jumlah', '$processor', '$motherboard', '$storage', '$memory', '$graphics', '$monitor', '$user_id', '$operatingSystem', '$serialNumber')";
        $this->db->query($query);
    }

    public function deleteDataAsset($id) {
        $query = "DELETE FROM asset WHERE asset_id = '$id'";
		$this->db->query($query);
    }

    public function getAllEmp() {
        $query = 'SELECT e.*, u.user_fullname, u.user_picture, u.user_birth, u.user_phone, u.user_asset_id, p.name as position, d.name as dept, 
            g.name as gender, c.name as kondisi, s.name1 as status1, s.name2 as status2 FROM employee e 
            INNER JOIN user u ON e.user_id = u.user_asset_id 
            INNER JOIN m_position p ON e.emp_position = p.id 
            INNER JOIN m_gender g ON e.emp_gender = g.id 
            INNER JOIN m_condition c ON e.emp_condition = c.id 
            INNER JOIN m_status s ON u.user_status = s.id 
            INNER JOIN m_dept d ON u.user_dept = d.id ORDER BY RAND()';
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditEmp($id) {
        $query = "SELECT e.*, u.user_fullname, u.user_picture, u.user_birth, u.user_phone, u.user_asset_id, u.user_dept, p.name as position, d.name as dept, 
            g.name as gender, c.name as kondisi FROM employee e 
            INNER JOIN user u ON e.user_id = u.user_asset_id 
            INNER JOIN m_position p ON e.emp_position = p.id 
            INNER JOIN m_gender g ON e.emp_gender = g.id 
            INNER JOIN m_condition c ON e.emp_condition = c.id 
            INNER JOIN m_dept d ON u.user_dept = d.id WHERE emp_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getEditEmpByUserId($userId) {
        $query = "SELECT e.*, u.user_fullname, u.user_picture, u.user_birth, u.user_phone, u.user_asset_id, u.user_dept, p.name as position, d.name as dept, 
            g.name as gender, c.name as kondisi FROM employee e 
            INNER JOIN user u ON e.user_id = u.user_asset_id 
            INNER JOIN m_position p ON e.emp_position = p.id 
            INNER JOIN m_gender g ON e.emp_gender = g.id 
            INNER JOIN m_condition c ON e.emp_condition = c.id 
            INNER JOIN m_dept d ON u.user_dept = d.id WHERE e.user_id = '$userId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }
    
    public function editDataEmp($id, $position, $condition, $gender, $address) {
        $query = "UPDATE employee SET 
            emp_position = '$position',
            emp_condition = '$condition',
            emp_gender = '$gender',
            emp_address = '$address' WHERE emp_id = '$id'";
        $this->db->query($query);
    }

    public function insertDataEmp($position, $gender, $condition, $address, $user_id) {
        $query = "INSERT INTO employee (emp_position, emp_gender, emp_condition, emp_address, user_id)
            VALUES ('$position', '$gender', '$condition', '$address', '$user_id')";
        $this->db->query($query);
    }

    public function deleteDataEmp($id) {
        $query = "DELETE FROM employee WHERE emp_id = '$id'";
		$this->db->query($query);
    }

    public function getAllAccess() {
        $query = "SELECT a.*, u.user_fullname, u.user_picture, u.user_dept, u.user_asset_id, d.name as dept, au.name as access, ad.name as deskera, 
            s.name1 as status FROM access a 
            INNER JOIN user u ON a.user_id = u.user_asset_id 
            INNER JOIN m_access_use au ON a.access_use = au.id 
            INNER JOIN m_access_deskera ad ON a.access_deskera = ad.id 
            INNER JOIN m_status s ON u.user_status = s.id 
            INNER JOIN m_dept d ON u.user_dept = d.id ORDER BY RAND()";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditAccess($id) {
        $query = "SELECT a.*, u.user_fullname, u.user_asset_id, u.user_picture, u.user_dept, d.name as dept, au.name as access, ad.name as deskera
            FROM access a 
            INNER JOIN user u ON a.user_id = u.user_asset_id 
            INNER JOIN m_access_use au ON a.access_use = au.id 
            INNER JOIN m_access_deskera ad ON a.access_deskera = ad.id 
            INNER JOIN m_dept d ON u.user_dept = d.id WHERE access_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getEditAccessByUserId($userId) {
        $query = "SELECT a.*, u.user_fullname, u.user_asset_id, u.user_picture, u.user_dept, d.name as dept, au.name as access, ad.name as deskera
            FROM access a 
            INNER JOIN user u ON a.user_id = u.user_id 
            INNER JOIN m_access_use au ON a.access_use = au.id 
            INNER JOIN m_access_deskera ad ON a.access_deskera = ad.id 
            INNER JOIN m_dept d ON u.user_dept = d.id WHERE a.user_id = '$userId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editAccess($id, $domainAccess, $domainPass, $accessUse, $deskera, $anydesk) {
        $query = "UPDATE access SET
            access_domain = '$domainAccess',
            access_pass = '$domainPass',
            access_use = '$accessUse',
            access_deskera = '$deskera',
            access_anydesk = '$anydesk' WHERE access_id = '$id'";
        $this->db->query($query);
    }

    public function editAccessFromResetPassword($username, $domain, $newPassword) {
        $query = "UPDATE access SET 
            access_domain = '$domain',
            access_pass = '$newPassword' WHERE user_id = '$username'";
        $this->db->query($query);
    }

    public function insertDataAccess($access_domain, $access_pass, $access_use, $access_deskera, $access_anydesk, $user_id) {
        $query = "INSERT INTO access (access_domain, access_pass, access_use, access_deskera, access_anydesk, user_id)
        VALUES ('$access_domain', '$access_pass', '$access_use', '$access_deskera', '$access_anydesk', '$user_id')";
        $this->db->query($query);
    }

    public function deletaDataAccess($id) {
        $query = "DELETE FROM access WHERE access_id = '$id'";
		$this->db->query($query);
    }

    public function getAllEmail() {
        $query = "SELECT e.*, u.user_email, u.user_picture, u.user_fullname, u.user_asset_id, eu.name as email_use, s.name1 as status FROM email e 
            INNER JOIN user u ON e.user_id = u.user_asset_id 
            INNER JOIN m_email_use eu ON e.email_use = eu.id 
            INNER JOIN m_status s ON u.user_status = s.id ORDER BY RAND()";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditEmail($id) {
        $query = "SELECT e.*, u.user_email, u.user_asset_id, u.user_fullname, d.name as dept, eu.name as email_use_name FROM email e 
            INNER JOIN user u ON e.user_id = u.user_asset_id 
            INNER JOIN m_email_use eu ON e.email_use = eu.id 
            INNER JOIN m_dept d ON u.user_dept = d.id WHERE email_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getEditEmailByUserId($userId) {
        $query = "SELECT e.*, u.user_email, u.user_asset_id, u.user_fullname, d.name as dept, eu.name as email_use_name FROM email e 
            INNER JOIN user u ON e.user_id = u.user_id 
            INNER JOIN m_email_use eu ON e.email_use = eu.id 
            INNER JOIN m_dept d ON u.user_dept = d.id WHERE e.user_id = '$userId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }
	
	public function editDataEmail($email_id, $emailPass, $using, $emailUser) {
		$query = "UPDATE email SET
			email_pass = '$emailPass',
			email_use = '$using',
			email_user = '$emailUser' WHERE email_id = '$email_id'";
		$this->db->query($query);
	}

    public function insertDataEmail($user_id, $email_pass, $email_use, $email_user) {
        $query = "INSERT INTO email (user_id, email_pass, email_use, email_user) 
            VALUES ('$user_id', '$email_pass', '$email_use', '$email_user')";
        $this->db->query($query);
    }

    public function deleteDataEmail($id) {
        $query = "DELETE FROM email WHERE email_id = '$id'";
        $this->db->query($query);
    }

    public function getAllDaily() {
        $query = "SELECT d.*, u.user_fullname as name, u.user_picture as picture FROM daily d INNER JOIN user u ON d.user_id = u.user_id";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditDaily($id) {
        $query = "SELECT dl.*, u.user_asset_id, u.user_fullname, d.name as dept FROM daily dl
            INNER JOIN user u  ON dl.user_id = u.user_id
            INNER JOIN m_dept d ON u.user_dept = d.id WHERE dl.daily_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }
	
	public function editDataDaily($daily_id, $date, $task) {
		$query = "UPDATE daily SET
			daily_date = '$date',
			daily_task = '$task' WHERE daily_id = '$daily_id'";
		$this->db->query($query);
	}

    public function insertDataDaily($user_id, $date, $task) {
        $query = "INSERT INTO daily (user_id, daily_date, daily_task)
            VALUES ('$user_id', '$date', '$task')";
        $this->db->query($query);
    }

    public function deleteDataDaily($id) {
        $query = "DELETE FROM daily WHERE daily_id = '$id'";
        $this->db->query($query);
    }

    public function getAllProject() {
        $query = "SELECT p.*, ps.name as projectStatus FROM project p INNER JOIN m_project_status ps ON p.project_status = ps.id";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditProject($id) {
        $query = "SELECT p.* FROM project p INNER JOIN m_project_status ps ON p.project_status = ps.id WHERE project_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function insertDataProject($project_name, $planning_date, $actual_date, $project_status, $project_comment) {
        $query = "INSERT INTO project (project_name, project_planning, project_actual, project_status, project_comment)
            VALUES ('$project_name', '$planning_date', '$actual_date', '$project_status', '$project_comment')";
        $this->db->query($query);
    }

    public function deleteDataProject($id) {
        $query = "DELETE FROM project WHERE project_id = '$id'";
        $this->db->query($query);
    }

    public function getAllPreventive() {
        $query = "SELECT p.*, u.user_fullname, d.name as dept FROM preventive p 
            INNER JOIN user u ON u.user_asset_id = p.user_id
            INNER JOIN m_dept d ON u.user_dept = d.id";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditPreventive($id) {
        $query = "SELECT p.* FROM preventive p WHERE preventive_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editPreventive($id, $userId, $preventiveDate, $preventiveSeq, $preventiveAntivirus,
    $preventiveHardware, $preventiveConnection, $preventiveSecurity, $preventiveBackup, $preventiveClean, $preventiveComment) {
        $query = "UPDATE preventive SET 
            user_id = '$userId',
            preventive_date = '$preventiveDate',
            preventive_seq = '$preventiveSeq',
            preventive_antivirus = '$preventiveAntivirus',
            preventive_hardware = '$preventiveHardware',
            preventive_conn = '$preventiveConnection',
            preventive_sec = '$preventiveSecurity',
            preventive_backup = '$preventiveBackup',
            preventive_clean = '$preventiveClean',
            preventive_comment = '$preventiveComment' WHERE preventive_id = '$id'";
        $this->db->query($query);   
    }

    public function insertDataPreventive($name_preventive, $date_preventive, $sequence_preventive,
    $antivirus_preventive, $hardware_preventive, $connection_preventive, $security_preventive, $backup_preventive, $clean_preventive, $comment_preventive) {
        $query = "INSERT INTO preventive (preventive_user, preventive_date, preventive_seq, preventive_antivirus, 
            preventive_hardware, preventive_conn, preventive_sec, preventive_backup, preventive_clean, preventive_comment) VALUES
            ('$name_preventive', '$date_preventive', '$sequence_preventive', '$antivirus_preventive',
            '$hardware_preventive', '$connection_preventive', '$security_preventive', '$backup_preventive', '$clean_preventive', '$comment_preventive')";
        $this->db->query($query);
    }

    public function checkPreventive($namePreventive, $seqPreventive) {
        $query = "SELECT * FROM preventive WHERE user_id = '$namePreventive' AND preventive_seq = '$seqPreventive'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function deleteDataPreventive($id) {
        $query = "DELETE FROM preventive WHERE preventive_id = '$id'";
        $this->db->query($query);
    }

    public function getAllResetPassword() {
        $query = "SELECT r.*, d.name as dept, u.user_fullname FROM reset_password r 
            INNER JOIN user u ON u.user_asset_id = r.reset_user
            INNER JOIN m_dept d ON u.user_dept = d.id";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditResetPassword($id) {
        $query = "SELECT r.*, d.name as dept FROM reset_password r 
            INNER JOIN user u ON u.user_asset_id = r.reset_user
            INNER JOIN m_dept d ON u.user_dept = d.id WHERE r.reset_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editResetPassword($id, $resetDate, $resetUser, $resetDomain, $resetSeq, $resetOld, $resetNew, $resetComment) {
        $query = "UPDATE reset_password SET 
            reset_date = '$resetDate',
            reset_user = '$resetUser',
            reset_domain = '$resetDomain',
            reset_seq = '$resetSeq',
            reset_old = '$resetOld',
            reset_new = '$resetNew',
            reset_comment = '$resetComment' WHERE reset_id = '$id'";
        $this->db->query($query);
    }

    public function checkResetPassword($resetDomain, $resetSeq) {
        $query = "SELECT * FROM reset_password WHERE reset_domain = '$resetDomain' AND reset_seq = '$resetSeq'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function insertDataResetPassword($resetUser, $resetDate, $resetSeq, $resetDomain, $resetOld, $resetNew, $resetComment) {
        $query = "INSERT INTO reset_password (reset_user, reset_date, reset_seq, reset_domain, reset_old, reset_new, reset_comment) 
            VALUES ('$resetUser', '$resetDate', '$resetSeq', '$resetDomain', '$resetOld', '$resetNew', '$resetComment')";
        $this->db->query($query);
    }

    public function deleteDataResetPassword($id) {
        $query = "DELETE FROM reset_password WHERE reset_id = '$id'";
        $this->db->query($query);
    }

    public function getAllBackup() {
        $query = "SELECT * FROM backup";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function checkBackup($month, $date) {
        $query = "SELECT * FROM backup WHERE backup_month = '$month' AND backup_date = '$date'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function insertDataBackup($backupDate, $backupStart, $backupEnd, $backupMonth, $backupType, $backupComment) {
        $query = "INSERT INTO backup (backup_date, backup_start, backup_end, backup_type, backup_comment)
            VALUES ('$backupDate', '$backupStart', '$backupEnd', '$backupType', '$backupComment')";
        $this->db->query($query);
    }

    public function getEditBackup($id) {
        $query = "SELECT * FROM backup WHERE backup_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editBackup($id, $backupDate, $backupStart, $backupEnd, $backupMonth, $backupType, $backupComment) {
        $query = "UPDATE backup SET 
            backup_date = '$backupDate',
            backup_start = '$backupStart',
            backup_end = '$backupEnd',
            backup_month = '$backupMonth',
            backup_type = '$backupType',
            backup_comment = '$backupComment' WHERE backup_id = '$id'";
        $this->db->query($query);
    }

    public function deleteDataBackup($id) {
        $query = "DELETE FROM backup WHERE backup_id = '$id'";
        $this->db->query($query);
    }

    public function getAllMonitoring() {
        $query = "SELECT * FROM monitoring_speed";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function checkMonitoring($year, $month) {
        $query = "SELECT * FROM monitoring_speed WHERE monitoring_year = '$year' AND monitoring_month = '$month'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function checkEditMonitoring($year, $month, $average, $downtime, $comment) {
        $query = "SELECT * FROM monitoring_speed WHERE monitoring_year = '$year' AND monitoring_month = '$month' AND monitoring_average = '$average' AND monitoring_downtime = '$downtime' AND monitoring_comment = '$comment'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function insertDataMonitoring($monitoringYear, $monitoringMonth, $monitoringAverage, $monitoringDowntime, $monitoringComment) {
        $query = "INSERT INTO monitoring_speed (monitoring_year, monitoring_month, monitoring_average, monitoring_downtime, monitoring_comment)
            VALUES ('$monitoringYear', '$monitoringMonth', '$monitoringAverage', '$monitoringDowntime', '$monitoringComment')";
        $this->db->query($query);
    }

    public function deleteDataMonitoring($id) {
        $query = "DELETE FROM monitoring_speed WHERE monitoring_id = '$id'";
        $this->db->query($query);
    }

    public function getEditMonitoring($id) {
        $query = "SELECT * FROM monitoring_speed WHERE monitoring_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }   

    public function editMonitoring($monitoringYear, $monitoringMonth, $monitoringAverage, $monitoringDowntime, $monitoringComment, $id) {
        $query = "UPDATE monitoring_speed SET 
            monitoring_year = '$monitoringYear',
            monitoring_month = '$monitoringMonth',
            monitoring_average = '$monitoringAverage',
            monitoring_downtime = '$monitoringDowntime',
            monitoring_comment = '$monitoringComment' WHERE monitoring_id = '$id'";
        $this->db->query($query);
    }
  
    public function getAllUserManagement() {
        $query = "SELECT u.*, d.name as dept, d.slug as slug, ul.name as level, s.name1 as status FROM user u 
            INNER JOIN m_dept d ON u.user_dept = d.id 
            INNER JOIN m_user_level ul ON u.user_level = ul.id 
            INNER JOIN m_status s ON u.user_status = s.id";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditUserManagement($id) {
        $query = "SELECT u.*, d.name as dept, d.slug as slug, ul.name as level, s.name1 as status FROM user u 
            INNER JOIN m_dept d ON u.user_dept = d.id 
            INNER JOIN m_user_level ul ON u.user_level = ul.id 
            INNER JOIN m_status s ON u.user_status = s.id WHERE u.user_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editUserManagement($id, $userUsername, $userPass, $userFullname, $userEmail, $userBirth, $userPhone, $pictureName,
    $userAssetId, $userDept, $userLevel, $userStatus) {
        $query = "UPDATE user SET
            user_username = '$userUsername',
            user_pass = '$userPass',
            user_fullname = '$userFullname',
            user_email = '$userEmail',
            user_birth = '$userBirth',
            user_phone = '$userPhone',
            user_picture = '$pictureName',
            user_asset_id = '$userAssetId',
            user_dept = '$userDept',
            user_level = '$userLevel',
            user_status = '$userStatus' WHERE user_id = '$id'";
        $this->db->query($query);
    }

    public function insertDataUserManagement($username, $password, $fullname, $identity, $department, $level, $status, $create, $picture) {
        $query = "INSERT INTO user (user_username, user_pass, user_fullname, user_asset_id, user_dept, user_level, user_status, user_create, user_picture) 
            VALUES ('$username', '$password', '$fullname', '$identity', '$department', '$level', '$status', '$create', '$picture')";
        $this->db->query($query);
    }

    public function deleteDataUserManagement($id) {
        $query = "DELETE FROM user WHERE user_id = '$id'";
        $this->db->query($query);
    }

    public function getAllLog() {
        $query = "SELECT l.*, at.type, d.name as dept, s.name1 as status1, s.name2 as status2, s.id as statusId FROM log l 
            INNER JOIN m_asset_type at ON l.log_asset = at.asset_id 
            INNER JOIN m_dept d ON l.log_dept = d.id 
            INNER JOIN m_status s ON l.log_status = s.id ";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function insertDataLog($asset, $name, $dept, $status, $stock, $desc) {
        $query = "INSERT INTO log (log_asset, log_name, log_dept, log_status, log_stock, log_desc)
            VALUES ('$asset', '$name', '$dept', '$status', '$stock', '$desc')";
        $this->db->query($query);
    }

    public function updateDataAssetTypeLog($new_stock, $asset_id) {
        $query = "UPDATE m_asset_type SET
            available = '$new_stock' WHERE asset_id = '$asset_id'";
        $this->db->query($query);
    }

    public function getEditLog($id) {
        $query = "SELECT * FROM log WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editDataLog($log_asset, $log_name, $log_dept, $log_status, $log_stock, $log_desc, $log_id) {
        $query = "UPDATE log SET
            log_asset = '$log_asset',
            log_name = '$log_name',
            log_dept = '$log_dept',
            log_status = '$log_status',
            log_stock = '$log_status',
            log_stock = '$log_stock',
            log_desc = '$log_desc' WHERE id = '$log_id'";
        $this->db->query($query);
    }

    public function deleteDataLog($id) {
        $query = "DELETE FROM log WHERE id = '$id'";
        $this->db->query($query);
    }

    public function getDataOverviewPreventive() {
        $query = "SELECT max(preventive_date) as date, preventive_seq FROM preventive GROUP BY preventive_seq";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getDataOverviewReset() {
        $year = "SELECT EXTRACT(YEAR FROM CURRENT_TIMESTAMP) as year";
        $year = $this->db->query($year)->getRowArray();
        $year = $year['year'];
        $query = "SELECT max(reset_date) as date, reset_seq from reset_password WHERE reset_date LIKE '$year%' GROUP BY reset_seq";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getDataOverviewBackup() {
        $year = "SELECT EXTRACT(YEAR FROM CURRENT_TIMESTAMP) as year";
        $year = $this->db->query($year)->getRowArray();
        $year = $year['year'];
        $query = "SELECT max(backup_date) as date, backup_month from backup WHERE backup_date LIKE '$year%' GROUP BY backup_month";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getDataOverviewMonitoring() {
        $query = "SELECT max(monitoring_year) as year, monitoring_month, monitoring_average, monitoring_downtime from monitoring_speed GROUP BY monitoring_month";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    // public function getReport() {
    //     $query = "SELECT report_date, report_daily FROM report";
    //     $hasil = $this->db->query($query);
    //     return $hasil->getResultArray();
    // }

    // try
    public function getPrev() {
        $query = "SELECT * FROM preventive WHERE preventive_seq = '2' ORDER BY preventive_id DESC LIMIT 1";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataPreventive() {
        $query = "SELECT p.*, u.user_fullname, d.name as dept_name FROM preventive p
            LEFT JOIN user u ON p.user_id = u.user_asset_id
            LEFT JOIN m_dept d ON d.id = u.user_dept ORDER BY preventive_seq ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataPreventiveByYear($year) {
        $query = "SELECT p.*, u.user_fullname, d.name as dept_name FROM preventive p
            LEFT JOIN user u ON p.user_id = u.user_asset_id
            LEFT JOIN m_dept d ON d.id = u.user_dept WHERE EXTRACT(YEAR FROM preventive_date) = $year ORDER BY preventive_seq ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataPreventiveSpecific($seq1, $seq2, $year) {
        $query = "SELECT p.*, u.user_fullname, d.name as dept_name FROM preventive p
            LEFT JOIN user u ON p.user_id = u.user_asset_id
            LEFT JOIN m_dept d ON d.id = u.user_dept WHERE (preventive_seq BETWEEN $seq1 AND $seq2) AND (EXTRACT(YEAR FROM preventive_date) = $year) ORDER BY preventive_seq ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataPreventiveBySeq($seq1, $seq2) {
        $query = "SELECT p.*, u.user_fullname, d.name as dept_name FROM preventive p
            LEFT JOIN user u ON p.user_id = u.user_asset_id
            LEFT JOIN m_dept d ON d.id = u.user_dept HERE preventive_seq BETWEEN $seq1 AND $seq2 ORDER BY preventive_seq ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataResetPassword() {
        $query = "SELECT rp.*, u.user_fullname, d.name as dept_name FROM reset_password rp
            LEFT JOIN user u ON u.user_asset_id = rp.reset_user
            LEFT JOIN m_dept d ON d.id = u.user_dept ORDER BY reset_seq ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataResetPasswordByYear($year) {
        $query = "SELECT rp.*, u.user_fullname, d.name as dept_name FROM reset_password rp
            LEFT JOIN user u ON u.user_asset_id = rp.reset_user
            LEFT JOIN m_dept d ON d.id = u.user_dept WHERE EXTRACT(YEAR FROM reset_date) = $year ORDER BY reset_seq ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataResetPasswordSpecific($seq1, $seq2, $year) {
        $query = "SELECT rp.*, u.user_fullname, d.name as dept_name FROM reset_password rp
            LEFT JOIN user u ON u.user_asset_id = rp.reset_user
            LEFT JOIN m_dept d ON d.id = u.user_dept WHERE (reset_seq BETWEEN $seq1 AND $seq2) AND (EXTRACT(YEAR FROM reset_date) = $year) ORDER BY reset_seq ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataResetPasswordBySeq($seq1, $seq2) {
        $query = "SELECT rp.*, u.user_fullname, d.name as dept_name FROM reset_password rp
            LEFT JOIN user u ON u.user_asset_id = rp.reset_user
            LEFT JOIN m_dept d ON d.id = u.user_dept WHERE reset_seq BETWEEN $seq1 AND $seq2 ORDER BY reset_seq ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function importPreventive($userId, $date, $sequence, $antiVirus, $hardware, $connection, $security, $backup, $clean, $comment) {
        $query = "INSERT INTO preventive (user_id, preventive_date, preventive_seq, preventive_antivirus, preventive_hardware, 
            preventive_conn, preventive_sec, preventive_backup, preventive_clean, preventive_comment) VALUES ('$userId', '$date', '$sequence', '$antiVirus',
            '$hardware', '$connection', '$security', '$backup', '$clean', '$comment')";
        $this->db->query($query);
    }

    public function importResetPassword($date, $sequence, $username, $domain, $oldPassword, $newPassword, $comment) {
        $query = "INSERT INTO reset_password (reset_date, reset_seq, reset_user, reset_domain, reset_old, reset_new, reset_comment) 
            VALUES ('$date', '$sequence', '$username', '$domain', '$oldPassword', '$newPassword', '$comment')";
        $this->db->query($query);
    }

    public function getAllDataBackup() {
        $query = "SELECT * FROM backup ORDER BY backup_date ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getAllDataBackupByYear($year) {
        $query = "SELECT * FROM backup WHERE EXTRACT(YEAR FROM backup_date) = $year ORDER BY backup_date ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function importBackup($date, $sequence, $sizeBefore, $sizeAfter, $type, $comment) {
        $query = "INSERT INTO backup (backup_date, backup_month, backup_start, backup_end, backup_type, backup_comment) 
            VALUES ('$date', '$sequence', '$sizeBefore', '$sizeAfter', '$type', '$comment')";
        $this->db->query($query);
    }

    public function getAllDataMonitoring() {
        $query = "SELECT * FROM monitoring_speed ORDER BY monitoring_month ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    } 

    public function getAllDataMonitoringByYear($year) {
        $query = "SELECT * FROM monitoring_speed WHERE monitoring_year = '$year' ORDER BY monitoring_month ASC";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function importMonitoringSpeed($year, $sequence, $average, $downtime, $comment) {
        $query = "INSERT INTO monitoring_speed (monitoring_year, monitoring_month, monitoring_average, monitoring_downtime, monitoring_comment)
            VALUES ('$year', '$sequence', '$average', '$downtime', '$comment')";
        $this->db->query($query);
    }

    public function checkAsset($userAssetId, $type) {
        $query = "SELECT * FROM asset WHERE user_id = '$userAssetId' AND asset_type = '$type'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function checkUserId($userId) {
        $query = "SELECT * FROM user WHERE user_asset_id = '$userId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function importAsset($userId, $assetType, $processor, $storage, $graphics, $motherboard, $memory, $monitor, $serialNumber, $operatingSystem, $jumlah, $information) {
        $query = "INSERT INTO asset (user_id, asset_type, asset_cpu, asset_hdd, asset_vga, asset_mb, asset_ram, asset_monitor, asset_serial, asset_os, asset_total, asset_info)
            VALUES ('$userId', '$assetType', '$processor', '$storage', '$graphics', '$motherboard', '$memory', '$monitor', '$serialNumber', '$operatingSystem', '$jumlah', '$information')";
        $this->db->query($query);
    }

    public function checkEmployee($userId) {
        $query = "SELECT * FROM employee WHERE user_id = '$userId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function importEmployee($userAssetId, $position, $gender, $lifeStatus, $address) {
        $query = "INSERT INTO employee (user_id, emp_position, emp_gender, emp_condition, emp_address)
            VALUES ('$userAssetId', '$position', '$gender', '$lifeStatus', '$address')";
        $this->db->query($query);
    }
    
    public function checkEmail($userId) {
        $query = "SELECT * FROM email WHERE user_id = '$userId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }
    
    public function importEmail($userAssetId, $username, $password, $use) {
        $query = "INSERT INTO email (user_id, email_user, email_pass, email_use) VALUES ('$userAssetId', '$username', '$password', '$use')";
        $this->db->query($query);
    }
    
    public function checkAccess($userId) {
        $query = "SELECT * FROM access WHERE user_id = '$userId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function importAccess($userAssetId, $domain, $password, $use, $deskera, $anydesk) {
        $query = "INSERT INTO access (user_id, access_domain, access_pass, access_use, access_deskera, access_anydesk)
            VALUES ('$userAssetId', '$domain', '$password', '$use', '$deskera', '$anydesk')";
        $this->db->query($query);
    }

    public function getNameAccessUse($id) {
        $query = "SELECT name FROM m_access_use WHERE id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function getAllInternet() {
        $query = "SELECT i.*, d.name as dept, p.name as position, u.user_fullname as fullname, u.user_picture FROM internet i
            LEFT JOIN user u ON i.user_id = u.user_asset_id
            LEFT JOIN m_dept d ON u.user_dept = d.id
            LEFT JOIN employee e ON e.user_id = i.user_id
            LEFT JOIN m_position p ON p.id = e.emp_position";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function getEditInternet($id) {
        $query = "SELECT i.*, d.name as dept, p.name as position, u.user_fullname as fullname, u.user_picture FROM internet i
            LEFT JOIN user u ON i.user_id = u.user_asset_id
            LEFT JOIN m_dept d ON u.user_dept = d.id
            LEFT JOIN employee e ON e.user_id = i.user_id
            LEFT JOIN m_position p ON p.id = e.emp_position WHERE inet_id = '$id'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function editInternet($id, $inetUser, $inetPass, $inetProfile) {
        $query = "UPDATE internet SET 
            inet_user = '$inetUser',
            inet_password = '$inetPass',
            inet_profile = '$inetProfile' WHERE inet_id = '$id'";
        $this->db->query($query);
    }

    public function deleteDataInternet($deleteId) {
        $query = "DELETE FROM internet WHERE inet_id = '$deleteId'";
        $this->db->query($query);
    }

    public function insertDataInternet($userId, $profile, $username, $password) {
        $query = "INSERT INTO internet (user_id, inet_profile, inet_user, inet_password)
            VALUES ('$userId', '$profile', '$username', '$password')";
        $this->db->query($query);
    }

    public function getAllDataUser() {
        $query = "SELECT u.*, d.name as deptName, s.name2 as status, l.name as level FROM user u
            LEFT JOIN m_dept d ON u.user_dept = d.id
            LEFT JOIN m_status s ON u.user_status = s.id
            LEFT JOIN m_user_level l ON u.user_level = l.id";
        $hasil = $this->db->query($query);
        return $hasil->getResultArray();
    }

    public function checkInternet($userId) {
        $query = "SELECT * FROM internet WHERE user_id = '$userId'";
        $hasil = $this->db->query($query);
        return $hasil->getRowArray();
    }

    public function importInternet($userAssetId, $username, $password, $profile) {
        $query = "INSERT INTO internet (user_id, inet_profile, inet_user, inet_password)
            VALUES ('$userAssetId', '$profile', '$username', '$password')";
        $this->db->query($query);
    }
}
