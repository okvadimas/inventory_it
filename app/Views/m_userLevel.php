<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">User Level Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">User Level</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('m_userLevel/insertData') ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Add new level ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Level</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="number" required name="identity" id="identity" class="form-control <?= ($validation->hasError('identity') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="1" value="<?= old('identity') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("identity"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Name</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="name" id="name" class="form-control <?= ($validation->hasError('name') ? 'is-invalid' : ""); ?>" style="border-radius: 15px;" placeholder="Administrator" <?= old('name') ?>>
                        <div class="invalid-feedback">
                            <?= $validation->getError("name"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="update" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Insert Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>

        <div class="col-sm-12">
            <div class="card" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Level</th>
                                <th scope="col">Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($level as $l) : ?>
                            <tr class="active">
                                <td class="align-middle" style="width: 10px;"><?= $no++; ?></td>
                                <td class="align-middle"><?= $l['id'] ?></td>
                                <td class="align-middle"><?= $l['name'] ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url('/M_UserLevel/editUserLevel') . '/' . $l['id'] ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteUserLevel" data-delete="<?= $l['id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" <?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Level</th>
                                <th scope="col">Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>