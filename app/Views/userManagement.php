<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">User Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Asset</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a class="btn btn-success <?= (session()->get('level') != 1 ? 'disabled' : '') ?> d-md-inline-block text-white mb-1 mb-sm-0" data-bs-toggle="modal" data-bs-target="#modalInsertUserManagement"> Add</a>
                <a href="<?= site_url('Usermanagement/exportData') ?>" class="btn btn-warning text-white me-0 <?= (session()->get('level') != 1 ? 'disabled' : '') ?>"><i class="icon-download"></i> Export</a>
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) :     ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card" data-aos-duration="500" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%" data-aos-duration="600" data-aos="fade-down">
                        <thead>
                            <tr data-aos="fade-down">
                                <th scope="col" class="text-center">No</th>
                                <th scope="col">Asset ID</th>
                                <th scope="col">Username</th>
                                <th scope="col">Full Name</th>
                                <th scope="col">Level</th>
                                <th scope="col">Created</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
						<?= $no = 1; ?>
                        <?php foreach($user as $u) : ?>
                            <tr class="active" data-aos="fade-down">
                                <td class="align-middle text-center"><?= $no++; ?></td>
                                <td class="align-middle" style="width: 130px;">PTPF-<?= $u['slug'] ?>-<?= $u['user_asset_id'] ?></td>
                                <td class="align-middle"><?= $u['user_username'] ?></td>
                                <td class="align-middle" style="width: 200px;">
                                    <h6><?= $u['user_fullname'] ?></h6><small class="text-muted"><?= $u['dept'] ?> Department</small>
                                </td>
                                <td class="align-middle" style="width: 20px;"><p class="ml-3"><?= $u['user_level'] ?></p></td>
                                <td class="align-middle"><?= $u['user_create'] ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url("UserManagement/editData" . "/" . $u['user_id']) ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteUserManagement" data-delete="<?= $u['user_id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col" class="text-center">No</th>
                                <th scope="col">Asset ID</th>
                                <th scope="col">Username</th>
                                <th scope="col">Full Name</th>
                                <th scope="col">Level</th>
                                <th scope="col">Created</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>