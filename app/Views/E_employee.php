<?php $this->extend("layout/edit"); ?>
<?php $this->section('edit'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Employee Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Asset</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/Employee/doEditData' . '/' . $data['emp_id']) ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Edit existing employee ?</h4>
                </div>
                <div class="row my-2">
                    <div class="col-md-8">
                        <input type="text" disabled class="form-control text-center" style="border-radius: 15px;" value="Update data for : <?= $data['user_asset_id'] ?> - <?= $data['user_fullname'] ?> - <?= $data['dept'] ?> Department">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Position *</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('position') ? 'is-invalid' : '') ?>" name="position" id="position" autocomplete="off">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($position as $p) : ?>
                                    <option value="<?= $p['id'] ?>" <?= ($data['emp_position'] == $p['id'] ? 'selected' : '') ?>><?= $p["id"] ?> - <?= $p["name"]; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("position"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Condition *</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('condition') ? 'is-invalid' : '') ?>" name="condition" id="condition" autocomplete="off">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($condition as $c) : ?>
                                    <option value="<?= $c['id'] ?>" <?= ($data['emp_condition'] == $c['id'] ? 'selected' : '') ?>><?= $c["id"] ?> - <?= $c["name"]; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("condition"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Gender *</h6>
                    </div>
                    <div class="col-md-6">
                    <div class="selectWrapper">
                            <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('gender') ? 'is-invalid' : '') ?>" name="gender" id="gender" autocomplete="off">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($gender as $g) : ?>
                                    <option value="<?= $g['id'] ?>" <?= ($data['emp_gender'] == $g['id'] ? 'selected' : '') ?>><?= $g["id"] ?> - <?= $g["name"]; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("gender"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Address *</h6>
                    </div>
                    <div class="col-md-6">
                        <textarea style="border-radius: 15px;" required name="address" id="address" class="form-control <?= ($validation->hasError('address') ? 'is-invalid' : '') ?>" cols="30" rows="10"><?= (old('address') ? old('address') : $data['emp_address']) ?></textarea>
                        <div class="invalid-feedback">
                            <?= $validation->getError("address"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="update" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Update Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection(); ?>