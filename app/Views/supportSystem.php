<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Support System</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Support</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <!-- <a href="<?= base_url('/SupportSystem/sendEmail') ?>" class="btn btn-success <?= (session()->get('level') != 1 ? 'disabled' : '') ?> fas fa-plus-square d-md-inline-block text-white mb-1 mb-sm-0"> Email</a> -->
                <a href="<?= base_url('/auth/signOut') ?>"
                class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card" data-aos-duration="500" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4">
                    <table id="example" class="table table-hover" style="width:100%" data-aos-duration="500" data-aos="fade-down">
                        <thead>
                            <tr data-aos="fade-down">
                                <th scope="col">No</th>
                                <th scope="col">From</th>
                                <th scope="col">Date</th>
                                <th scope="col">Message</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($supportSystem as $s) : ?>
                            <tr class="active" data-aos="fade-down">
                                <td class="align-middle"><?= $no++; ?></td>
                                <td class="align-middle"><?= $s['user_fullname'] ?></td>
                                <td class="align-middle"><?= $s['support_date'] ?></td>
                                <td class="align-middle text-left" style="width: 400px; max-width: 450px">
                                <pre style="font-family: 'Poppins', sans-serif; font-size: 16px; margin-top: 15px; white-space: pre-line;">
                                   <?= $s['support_text'] ?>
                                </pre></td>
                                <td class="align-middle">
                                    <a href="" class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteSupportSystem" data-delete="<?= $s['support_id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">From</th>
                                <th scope="col">Date</th>
                                <th scope="col">Message</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>