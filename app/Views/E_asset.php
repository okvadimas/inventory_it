<?php $this->extend("layout/edit"); ?>
<?php $this->section('edit'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Asset Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Asset</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/Asset/doEditData' . '/' . $data['asset_id']) ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Edit existing asset ?</h4>
                </div>
                <div class="row my-2">
                    <div class="col-md-8">
                        <input type="text" disabled class="form-control text-center" style="border-radius: 15px;" value="Update data for : <?= $data['user_asset_id'] ?> - <?= $data['user_fullname'] ?> - <?= $data['dept'] ?> Department">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Asset Type *</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" required <?= (session()->get('level') == 1 ? '' : 'readonly') ?> class="form-control selectNoArrow <?= ($validation->hasError('asset_type') ? 'is-invalid' : '') ?>" name="asset_type" id="asset_type" autocomplete="off">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($type as $t) : ?>
                                    <option <?= (session()->get('level') != 1 ? 'disabled' : '') ?> value="<?= $t['id'] ?>" <?= ($data['asset_type'] == $t['id'] ? 'selected' : '') ?>><?= $t["id"] ?> - <?= $t["type"]; ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("asset_type"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Processor *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required <?= (session()->get('level') == 1 ? '' : 'readonly') ?> name="processor" id="processor" class="form-control <?= ($validation->hasError('processor') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Intel i5 - 5200u 3.9Ghz" value="<?= (old('processor') ? old('processor') : $data['asset_cpu']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("processor"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Storage *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required <?= (session()->get('level') == 1 ? "" : 'readonly') ?> name="storage" id="storage" class="form-control <?= ($validation->hasError('storage') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Seagate HDD 1TB 5000 Rpm" value="<?= (old('storage') ? old('storage') : $data['asset_hdd']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("storage"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Graphics *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required <?= (session()->get('level') == 1 ? '' : 'readonly') ?> name="graphics" id="graphics" class="form-control <?= ($validation->hasError('graphics') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Asus GTX 1650 4Gb" value="<?= (old('graphics') ? old('graphics') : $data['asset_vga']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("graphics"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Motherboard *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required <?= (session()->get('level') == 1 ? '' : 'readonly') ?> name="motherboard" id="motherboard" class="form-control <?= ($validation->hasError('motherboard') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="AsRock B350 LGA 1151" value="<?= (old('motherboard') ? old('motherboard') : $data['asset_mb']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("motherboard"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Memory *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required <?= (session()->get('level') == 1 ? '' : 'readonly') ?> name="memory" id="memory" class="form-control <?= ($validation->hasError('memory') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Corsair Sodimm 4 Gb DDR3 1333Mhz" value="<?= (old('memory') ? old('memory') : $data['asset_ram']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("memory"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Monitor *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required <?= (session()->get('level') == 1 ? '' : 'readonly') ?> name="monitor" id="monitor" class="form-control <?= ($validation->hasError('monitor') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Samsung Flat 20 Inc FHD" value="<?= (old('monitor') ? old('monitor') : $data['asset_monitor']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("monitor"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Operating System *</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" required <?= (session()->get('level') == 1 ? '' : 'readonly') ?> class="form-control selectNoArrow" name="operatingSystem" id="operatingSystem">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($operatingSystem as $os) : ?>
                                    <option <?= (session()->get('level') != 1 ? 'disabled' : '') ?> value="<?= $os['id']; ?>" <?= ($data['asset_os'] == $os['id'] ? 'selected' : '') ?>><?= $os['id'] . ' - ' . $os['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("monitor"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Serial Number *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required <?= (session()->get('level') == 1 ? '' : 'readonly') ?> name="serialNumber" id="serialNumber" class="form-control <?= ($validation->hasError('serialNumber') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Serial Number..." value="<?= (old('serialNumber') ? old('serialNumber') : $data['asset_serial']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("serialNumber"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Jumlah *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required <?= (session()->get('level') == 1 ? '' : 'readonly') ?> name="jumlah" id="jumlah" class="form-control <?= ($validation->hasError('jumlah') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Jumlah Here..." value="<?= (old('jumlah') ? old('jumlah') : $data['asset_total']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("jumlah"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Information *</h6>
                    </div>
                    <div class="col-md-6">
                        <textarea style="border-radius: 15px;" required <?= (session()->get('level') == 1 ? '' : 'readonly') ?> name="information" id="information" class="form-control <?= ($validation->hasError('information') ? 'is-invalid' : '') ?>" cols="30" rows="10"><?= (old('information') ? old('information') : $data['asset_info']) ?></textarea>
                        <div class="invalid-feedback">
                            <?= $validation->getError("information"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="update" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>">Update Data</button>                
                    <button type="reset" class="btn btn-danger text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>">Reset</button>                
                </div>
            </div>
        </form>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection(); ?>