<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Dashboard</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end">
                <a href="<?= base_url('/auth/signOut'); ?>"
                    class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->

    <?php if (session()->getFlashData("success_login")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_login") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <?php if (session()->getFlashData("error_message")) : ?>
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong class="fw-bold"><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong class='fw-bold'><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <div class="row justify-content-center">
        <!-- Column -->
        <div class="col-lg-12 col-md-12">
            <div class="card border-gradient" style="border: 5px solid; border-image: linear-gradient(0, green, yellow) 1;" data-aos-duration="500" data-aos="fade-down">
                <!-- <img class="card-img-top img-responsive" src="images/big/img1.jpg" alt="Card"> -->
                <?php foreach($post as $p) : ?>
                <div class="card-body">
                    <ul class="list-inline d-flex align-items-center">
                        <li class="ps-0"><?= $p['post_create']; ?></li>
                        <li class="ms-auto"><?= $p['user_fullname']; ?> - <?= $p['dept'] ?> Department</li>
                    </ul>
                    <h4 class="font-normal"><?= $p['post_title']; ?></h4>
                    <p class="font-normal"><?= $p['post_message']; ?></p>
                </div>
                <?php endforeach; ?>
            </div>
        </div>
    </div>


    <!-- Recent blogss -->
    <!-- ============================================================== -->
    <div class="row justify-content-center">
        <!-- Column -->
        <div class="col-lg-12 col-md-12">
            <div class="card border-gradient" style="border: 5px solid; border-image: linear-gradient(0, red, yellow) 1;" data-aos-duration="500" data-aos="fade-down">
                <!-- <img class="card-img-top img-responsive" src="images/big/img1.jpg" alt="Card"> -->
                <div class="card-body">
                    <form action="<?= base_url('/ticketing/insertMessage'); ?>" method="POST">
                        <div class="head d-flex justify-content-between">
                            <label for="" class="fw-bold">Send Message to IT Department Here</label>
                            <button type="submit" class="btn btn-danger text-white">Send</button>
                        </div>
                        <textarea name="ticketMessage" id="ticketMessage" rows="3" style="width: 100%; border: 1px solid #000000;" class="form-control mt-1"></textarea>
                        <div class="row mt-2">
                            <div class="col-md-4">
                                <label for="opsi1" class="fw-bold">Type of message</label>
                                <select class="opsi1 w-100" style="width: 100%" name="opsi1" id="opsi1" onchange="opsi()">
                                    <option value="1" selected>Helpdesk (Permintaan Bantuan)</option>
                                    <option value="2">Request Item (Permintaan Barang)</option>
                                    <option value="3">Request Form (Permintaan Form)</option>
                                </select>
                            </div>
                            <div class="col-md-4 my-2 my-md-0">
                                <label for="opsi2" class="fw-bold">Type of level</label>
                                <select class="opsi2 w-100" style="width: 100%" disabled name="opsi2" id="opsi2">
                                    <option value="1">Urgent (Cepat)</option>
                                    <option value="2" selected>Standart (Normal)</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="opsi3" class="fw-bold">Due date</label>
                                <input type="date" class="form-control opsi3" name="opsi3" disabled style="width: 100%; border: 1px solid #AAAAAA; height: 27px;">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- ============================================================== -->
    <!-- Recent blogss -->
    <!-- Sales chart -->
    <!-- <div class="chart mt-3 mb-5">
        <div class="row">
            <div class="col-12">
            <canvas id="myChart" width="100%" height="50%"></canvas>
            </div>
        </div>
    </div> -->
    <!-- ============================================================== -->

    <div class="row">
        <div class="col-md-3">
            <div class="card"data-aos="fade-down">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 text-start" style="font-size: 12px">Preventive</div>
                        <div class="col-6 text-end"><a href="" data-bs-toggle="modal" data-bs-target="#modalDashboardPreventive" style="font-size: 10px; padding: 5px 8px; background-color: #7460EE; text-decoration: none; color: #fff; border-radius: 3px">Details</a></div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-start fw-bold" style="font-size: 16px"><?php $outerDate = array_column($overviewPreventive, 'date'); echo date('d F Y', strtotime(end($outerDate))); ?></div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-primary" role="progressbar"
                                    style="width: <?= count($overviewPreventive) * 100 / 6 ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0"
                                    aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card"data-aos="fade-down">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 text-start" style="font-size: 12px">Reset Password</div>
                        <div class="col-6 text-end"><a href="" data-bs-toggle="modal" data-bs-target="#modalDashboardResetPassword" style="font-size: 10px; padding: 5px 8px; background-color: #F62D51; text-decoration: none; color: #fff; border-radius: 3px">Details</a></div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-start fw-bold" style="font-size: 16px"><?php $outerDate = max($overviewReset); echo date('d F Y', strtotime($outerDate['date'])); ?></div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-danger" role="progressbar"
                                    style="width: <?= count($overviewReset) * 100 / 4 ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0"
                                    aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card"data-aos="fade-down">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 text-start" style="font-size: 12px">Backup</div>
                        <div class="col-6 text-end"><a href="" data-bs-toggle="modal" data-bs-target="#modalDashboardBackup" style="font-size: 10px; padding: 5px 8px; background-color: #55CE63; text-decoration: none; color: #fff; border-radius: 3px">Details</a></div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-start fw-bold" style="font-size: 16px"><?php $outerDate = max($overviewBackup); echo date('d F Y', strtotime($outerDate['date'])); ?></div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-success" role="progressbar"
                                    style="width: <?= count($overviewBackup) * 100 / 12 ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0"
                                    aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="card"data-aos="fade-down">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6 text-start" style="font-size: 12px">Monitoring</div>
                        <div class="col-6 text-end"><a href="" data-bs-toggle="modal" data-bs-target="#modalDashboardMonitoring" style="font-size: 10px; padding: 5px 8px; background-color: #FFBC34; text-decoration: none; color: #fff; border-radius: 3px">Details</a></div>
                    </div>
                    <div class="row">
                        <div class="col-12 text-start fw-bold" style="font-size: 16px"><?php $outerDate1 = array_column($overviewMonitoring, 'monitoring_month'); $outerDate2 = array_column($overviewMonitoring, 'year'); echo date("F ", mktime(0, 0, 0, end($outerDate1), 10)); echo end($outerDate2); ?></div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12">
                            <div class="progress">
                                <div class="progress-bar bg-warning" role="progressbar"
                                    style="width: <?= count($overviewMonitoring) * 100 / 12 ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0"
                                    aria-valuemax="100"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <div class="row">
        <!-- Column -->

        <div class="col-sm-6">
            <div class="card"data-aos="fade-down">
                <div class="card-body">
                    <h4 class="card-title fw-bold">Daily Report</h4>
                    <div class="text-end">
                        <h2 class="font-light mb-0"><i class="ti-arrow-up text-success"></i> <?= $lastDaily['report_total']; ?> </h2>
                        <span class="text-muted">Daily Login</span>
                    </div>
                    <span class="text-success"><?= $uniqueSupport ?> of <?= $support; ?> Support this site </span>
                    <div class="progress">
                        <div class="progress-bar bg-success" role="progressbar"
                            style="width: <?= $uniqueSupport * 100 / $support ?>%; height: 6px;" aria-valuenow="25" aria-valuemin="0"
                            aria-valuemax="100"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-sm-6">
            <div class="card" data-aos-duration="500" data-aos="fade-down">
                <div class="card-body">
                    <form action="<?= base_url('/Dashboard/supportSite') ?>" method="post">
                        <h4 class="card-title fw-bold">Support this site </h4>
                        <p>help improve this site for the better, thanks</p>
                        <div>
                            <input type="text" class="form-control" id="support" name="support" autocomplete="off" placeholder="tell me everything u want then just enter">
                        </div>
                        <div class="progress mt-2">
                            <div class="progress-bar bg-primary" role="progressbar"
                                style="width: 100%; height: 6px;" aria-valuenow="25" aria-valuemin="0"
                                aria-valuemax="100">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    <!-- ============================================================== -->

    
    <!-- Sales chart -->
    <!-- ============================================================== -->
    <div class="row">
        <!-- column -->
        <div class="col-sm-12" data-aos="fade-down">
            <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d350.0516188321119!2d110.33310223088195!3d-6.964186454924736!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e70f558f52ca729%3A0x1110871527641d51!2sPT.%20Pacific%20Furniture!5e0!3m2!1sen!2sid!4v1617008782584!5m2!1sen!2sid" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
        </div>
        <!-- column -->
    </div>
    <!-- ============================================================== -->

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row mt-4">
        <div class="col-sm-12">
            <div class="card" data-aos-duration="500" data-aos="fade-down">
                <div class="card-body">
                    <div class="d-md-flex mb-3">
                        <!-- <a href="" class="btn btn-success <?= (session()->get('level') != 1 ? 'disabled' : '') ?> d-none d-md-block text-white fa fa-plus-square" data-bs-toggle="modal" data-bs-target="#modalInsertOverview"> Add</a> -->
                        <h4 class="card-title col-md-10 mb-md-0 mb-3 align-self-center fw-bold">Overview Report</h4>
                        <!-- <div class="col-md-2 ms-auto">
                            <select class="form-select shadow-none col-md-2 ml-auto">
                                <option selected>January</option>
                                <option value="1">February</option>
                                <option value="2">March</option>
                                <option value="3">April</option>
                            </select>
                        </div> -->
                    </div>

                    <?php if (session()->getFlashData("error_message")) : ?>
                        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
                            <strong><?= session()->getFlashData("error_message") ?></strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            </button>
                        </div>
                    <?php elseif (session()->getFlashData("success_message")) : ?>
                        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
                            <strong><?= session()->getFlashData("success_message") ?></strong>
                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                            </button>
                        </div>
                    <?php endif; ?>

                    <div class="table-responsive mt-4">
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th class="border-top-0">No</th>
                                <th class="border-top-0">Identity</th>
                                <th class="border-top-0">Name</th>
                                <th class="border-top-0">Used</th>
                                <th class="border-top-0">Borrowed</th>
                                <th class="border-top-0">Available</th>
                                <th class="border-top-0">Information</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($overview as $o) : ?>
                            <tr class="active">
                                <td class="align-middle" style="width: 10px;"><?= $no++; ?></td>
                                <td class="align-middle"><?= $o["asset_id"] ?></td>
                                <td class="align-middle" style="width: 200px;"><?= $o['type'] ?></td>
                                <td class="align-middle"><?= $o['used'] ?></td>
                                <td class="align-middle"><?= $o['borrowed'] ?></td>
                                <td class="align-middle"><?= $o['available'] ?></td>
                                <td class="align-middle"><?= $o['information'] ?></td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th class="border-top-0">No</th>
                                <th class="border-top-0">Asset Identity</th>
                                <th class="border-top-0">Name</th>
                                <th class="border-top-0">Used</th>
                                <th class="border-top-0">Borrowed</th>
                                <th class="border-top-0">Available</th>
                                <th class="border-top-0">Information</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

    <!-- ============================================================== -->
    <!-- ============================================================== -->
    
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>