<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Reset Password Task</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Reset Password</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <!-- <a class="btn btn-success <?= (session()->get('level') != 1 ? 'disabled' : '') ?> d-md-inline-block text-white fa fa-plus-square mb-1 mb-sm-0" data-bs-toggle="modal" data-bs-target="#modalInsertResetPassword"> Add</a> -->
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-md-inline-block text-white" target="_blank">Sign Out</a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card" data-aos-duration="500" data-aos="fade-down">
                <div class="card-body">
                    <div class="d-sm-flex align-items-center justify-content-between border-bottom pb-2">
                        <div>
                            <h4>Reset Password</h4>
                        </div>
                        <div>
                            <div class="btn-wrapper">
                                <?php if(session()->get('level') <= 2) : ?>
                                <a href="#" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>" data-bs-toggle="modal" data-bs-target="#modalInsertResetPassword"><i class="icon-plus"></i> Add</a>
                                <a href="#" class="btn btn-info text-white" id="importDataResetPassword" data-bs-toggle="modal" data-bs-target="#modalImport"><i class="icon-upload"></i> Import</a>
                                <?php endif; ?>
                                <a href="<?= site_url('ResetPassword/exportData') ?>" class="btn btn-warning text-white me-0" id="exportDataResetPassword" data-bs-toggle="modal" data-bs-target="#modalExport"><i class="icon-download"></i> Export</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-4">
                    <table id="example" class="table table-hover" style="width:100%" data-aos-duration="500" data-aos="fade-down">
                        <thead>
                            <tr data-aos="fade-down">
                                <th scope="col">No</th>
                                <th scope="col">Date</th>
                                <th scope="col">User</th>
                                <th scope="col">Domain</th>
                                <th scope="col">Seq</th>
                                <th scope="col">Password</th>
                                <th scope="col">Comment</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($reset as $r) : ?>
                            <tr class="active" data-aos="fade-down">
                                <td class="align-middle" style="width: 10px;"><?= $no++; ?></td>
                                <td class="align-middle" style="width: 100px;"><?= $r['reset_date'] ?></td>
                                <td class="align-middle" style="width: 100px;">
                                    <h6><?= $r['user_fullname'] ?></h6><small class="text-muted"><?= $r['dept'] ?> Dept</small>
                                </td>
                                <td class="align-middle"><?= $r['reset_domain'] ?></td>
                                <td class="align-middle"><?= $r['reset_seq'] ?></td>
                                <td class="align-middle" style="width: 200px;">
                                    <h6><strong class="text-danger"><?= $r['reset_old'] ?> </strong></h6><h6><strong class="text-success"><?= $r['reset_new'] ?> </strong></h6>
                                </td>
                                <td class="align-middle"><?= $r['reset_comment'] ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url("/ResetPassword/editData" . '/' . $r['reset_id']) ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none;' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteResetPassword" data-delete="<?= $r['reset_id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Date</th>
                                <th scope="col">User</th>
                                <th scope="col">Domain</th>
                                <th scope="col">Seq</th>
                                <th scope="col">Password</th>
                                <th scope="col">Comment</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>