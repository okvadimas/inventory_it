<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Ticket List</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Ticket</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <!-- <a class="btn btn-success <?= (session()->get('level') != 1 ? 'disabled' : '') ?> d-md-inline-block text-white fa fa-plus-square mb-1 mb-sm-0" data-bs-toggle="modal" data-bs-target="#modalInsertAccess"> Add</a> -->
                <a class="btn btn-success d-md-inline-block text-white mb-1 mb-md-0" id="testToggle">Add</a>
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-md-inline-block text-white">Sign Out</a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <div class="row justify-content-center contentSend">
        <!-- Column -->
        <div class="col-lg-12 col-md-12">
            <div class="card border-gradient" style="border: 5px solid; border-image: linear-gradient(0, red, yellow) 1;" data-aos-duration="500" data-aos="fade-down">
                <!-- <img class="card-img-top img-responsive" src="images/big/img1.jpg" alt="Card"> -->
                <div class="card-body">
                    <form action="<?= base_url('/ticketing/insertMessage'); ?>" method="POST">
                        <div class="head d-flex justify-content-between">
                            <label for="" class="fw-bold">Send Message to IT Department Here</label>
                            <button type="submit" class="btn btn-danger text-white">Send</button>
                        </div>
                        <textarea name="ticketMessage" id="ticketMessage" rows="3" style="width: 100%; border: 1px solid #000000;" class="form-control mt-1"></textarea>
                        <div class="row mt-2">
                            <div class="col-md-4">
                                <label for="opsi1" class="fw-bold">Type of message</label>
                                <select class="opsi1 w-100" style="width: 100%" name="opsi1" id="opsi1" onchange="opsi()">
                                    <option value="1" selected>Helpdesk (Permintaan Bantuan)</option>
                                    <option value="2">Request Item (Permintaan Barang)</option>
                                    <option value="3">Request Form (Permintaan Form)</option>
                                </select>
                            </div>
                            <div class="col-md-4 my-2 my-md-0">
                                <label for="opsi2" class="fw-bold">Type of level</label>
                                <select class="opsi2 w-100" style="width: 100%" disabled name="opsi2" id="opsi2">
                                    <option value="1">Urgent (Cepat)</option>
                                    <option value="2" selected>Standart (Normal)</option>
                                </select>
                            </div>
                            <div class="col-md-4">
                                <label for="opsi3" class="fw-bold">Due date</label>
                                <input type="date" class="form-control opsi3" name="opsi3" disabled style="width: 100%; border: 1px solid #AAAAAA; height: 27px;">
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card" data-aos-duration="500" data-aos="fade-down">
                <div class="card-body">
                    <div class="d-sm-flex align-items-center justify-content-between border-bottom pb-2">
                        <div>
                            <h4>Ticket List</h4>
                        </div>
                        <div>
                            <div class="btn-wrapper">
                                <a data-bs-toggle="modal" style="<?= session()->get('level') != 1 ? 'display: none' : '' ?>" data-bs-target="#modalExportTicket" class="btn btn-success text-white me-0"><i class="icon-download"></i> Export</a>
                            </div>
                        </div>
                    </div>

                    <div class="row mt-4 mb-2">
                        <div class="col-6">
                            <label for="">From Date ?</label>
                            <input type="date" class="form-control mt-2" id="fromDateTicket">
                        </div>
                        <div class="col-6">
                            <label for="">To Date ?</label>
                            <input type="date" class="form-control mt-2" id='toDateTicket'>
                        </div>
                    </div>

                    <div class="table-responsive mt-4">
                    <table id="tabel_ticket" class="table table-hover" style="width:100%" data-aos-duration="600" data-aos="fade-down">
                        <thead>
                            <tr data-aos="fade-down">
                                <th scope="col">No</th> 
                                <th scope="col" class="text-center">Action</th> 
                                <th scope="col">Ticket</th>  
                                <th scope="col">Username</th>
                                <th scope="col">Message</th>
                                <th scope="col">Status</th>
                                <th scope="col">Update</th>
                            </tr>
                        </thead>
                        <tbody>
                        
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>