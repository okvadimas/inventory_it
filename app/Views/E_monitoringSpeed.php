<?php $this->extend("layout/edit"); ?>
<?php $this->section('edit'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Monitoring Speed</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Monitoring Speed Management</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/monitoringSpeed/doEditData' . '/' . $data['monitoring_id']) ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Edit existing data ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Year</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="number" required name="monitoring_year" id="monitoring_year" class="form-control <?= ($validation->hasError('monitoring_year') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Year here..." value="<?= (old('monitoring_year') ? old('monitoring_year') : $data['monitoring_year']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("monitoring_year"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Month</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="number" required name="monitoring_month" id="monitoring_month" class="form-control <?= ($validation->hasError('monitoring_month') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Month here..." value="<?= (old('monitoring_month') ? old('monitoring_month') : $data['monitoring_month']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("monitoring_month"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Average</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="number" step="0.01" required name="monitoring_average" id="monitoring_average" class="form-control <?= ($validation->hasError('monitoring_average') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Average here..." value="<?= (old('monitoring_average') ? old('monitoring_average') : $data['monitoring_average']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("monitoring_average"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Downtime</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="number" required name="monitoring_downtime" id="monitoring_downtime" class="form-control <?= ($validation->hasError('monitoring_downtime') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Downtime here..." value="<?= (old('monitoring_downtime') ? old('monitoring_downtime') : $data['monitoring_downtime']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("monitoring_downtime"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Comment</h6>
                    </div>
                    <div class="col-md-6">
                        <textarea style="border-radius: 15px;" name="monitoring_comment" id="monitoring_comment" class="form-control <?= ($validation->hasError('monitoring_comment') ? 'is-invalid' : '') ?>" cols="30" rows="10"><?= (old('monitoring_comment') ? old('monitoring_comment') : $data['monitoring_comment']) ?></textarea>
                        <div class="invalid-feedback">
                            <?= $validation->getError("monitoring_comment"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="update" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Update Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection(); ?>