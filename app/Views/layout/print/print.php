<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>PT Pacific Furniture - Print Template</title>
    <!-- Custom CSS -->
</head>

<body>
    <div class="container" style="float: left; width: 100%; height: 100%"> 

    <?php foreach($data as $d) : ?>
	<?php if($typeInput == "range") : ?>
        <div class="card" style="width: 320px; height: auto; border: 1px solid red; margin-bottom: 10px; float: left; margin-left: 10px">
            <table>
                <tr>
                    <td style="font-size: 15px; padding-left: 70px; padding-top: 10px">INVENTORY COUNT TAG</td>
                </tr>
                <tr>
                    <td style="padding-left: 105px; font-size: 12px;">Site : Stock Room 1</td>
                </tr>
                <tr>
                    <td style="padding-left: 10px; font-size: 12px; padding-top: 20px">Date :</td>
                    <td style="font-size: 12px; padding-right: 10px; padding-left: -60px; padding-top: 20px">Tag No :</td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px; padding-top: 10px">Item No : <?= $d['product_id'] ?></td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px">Description :  <?= $d['description'] ?></td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px; padding-top: 10px">Location : </td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px">Stock on Hand : </td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px">JO Number : </td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px">Model Code : </td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px; padding-top: 5px;">Based Unit of Measurement :</td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 82px; padding-top: 5px;">Physical Count :</td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px; padding-top: 5px;">Actual Unit of Measurement :</td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px; padding-top: 15px;">Remarks :</td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px; padding-top: 30px; padding-bottom: 50px">Counted By :</td>
                    <td style="font-size: 12px; padding-left: -140px; padding-top: 30px; padding-bottom: 50px">Checked By :</td>
                    <td style="font-size: 12px; padding-right: 10px; padding-left: -20px; padding-top: 30px; padding-bottom: 50px">Audited By :</td>
                </tr>
            </table>
        </div>
    <?php elseif($typeInput == "multiple") : ?>
        <div class="card" style="width: 300px; height: 400px; border: 1px solid red; margin-bottom: 10px; float: left; margin-left: 10px">
            <table style="width: 300px; height: 400px;">
                <tr>
                    <td style="font-size: 15px; padding-left: 70px; padding-top: 10px">INVENTORY COUNT TAG</td>
                </tr>
                <tr>
                    <td style="padding-left: 105px; font-size: 12px;">Site : Stock Room 1</td>
                </tr>
                <tr>
                    <td style="padding-left: 10px; font-size: 12px; padding-top: 20px">Date :</td>
                    <td style="font-size: 12px; padding-right: 10px; padding-left: -60px; padding-top: 20px">Tag No :</td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px; padding-top: 10px">Item No :  <?= $d[0]['product_id'] ?></td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px">Description :  <?= $d[0]['description'] ?>guyguygihihibugbbgikjbikhihniohkihno</td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px; padding-top: 10px">Location : </td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px">Stock on Hand : </td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px">JO Number : </td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px">Model Code : </td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px; padding-top: 5px;">Based Unit of Measurement :</td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 82px; padding-top: 5px;">Physical Count :</td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px; padding-top: 5px;">Actual Unit of Measurement :</td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px; padding-top: 15px;">Remarks :</td>
                </tr>
                <tr>
                    <td style="font-size: 12px; padding-left: 10px; padding-top: 30px; padding-bottom: 50px">Counted By :</td>
                    <td style="font-size: 12px; padding-left: -145px; padding-top: 30px; padding-bottom: 50px">Checked By :</td>
                    <td style="font-size: 12px; padding-right: 10px; padding-left: -25px; padding-top: 30px; padding-bottom: 50px">Audited By :</td>
                </tr>
            </table>
        </div>
    <?php else : ?>
        <?= "Sorry i dont understand.." ?>
    <?php endif; ?>
    <?php endforeach; ?>
       
    </div>
</body>

</html>

