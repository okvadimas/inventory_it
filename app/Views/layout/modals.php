<!-- MODALS INSERT ASSET -->
<div class="modal fade" id="modalInsertAssets" tabindex="-1" aria-labelledby="modalInsertAssets" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header  text-center justify-content-center">
                <h5 class="modal-title" id="exampleModalLabel">Asset Input Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('/Asset/insertData'); ?>" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                    <input type="hidden" class="form-control" name="asset_type" id="asset_type" placeholder="Input Asset Type Here...">

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <input style="border-radius: 15px" type="text" disabled class="form-control" name="preview" id="preview" value="Insert data for : <?= $lastUser['user_asset_id'] ?> - <?= $lastUser['user_fullname'] ?> - <?= $lastUser['dept'] ?> Department">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="asset_type">Asset Type</label>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('asset_type') ? 'is-invalid' : '') ?>" name="asset_type" id="asset_type" placeholder="Input Asset Type Here..." autocomplete="off">
                                    <option value="">Choose...</option>
                                    <?php foreach($type as $t) : ?>
                                        <option value="<?= $t['id'] ?>" <?= (old("asset_type") == $t['id'] ? 'selected' : '') ?>><?= $t["id"] ?> - <?= $t["type"]; ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("asset_type"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="information">Information</label>
                            <input style="border-radius: 15px" required type="text" class="form-control <?= ($validation->hasError('information') ? 'is-invalid' : '') ?>" name="information" id="information" placeholder="Input Information Here..." autocomplete="off" value="<?= old('information') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("information"); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="processor">Processor</label>
                            <input style="border-radius: 15px" required type="text" class="form-control <?= ($validation->hasError('processor') ? 'is-invalid' : '') ?>" name="processor" id="processor" placeholder="Intel i5 - 5200u 3.9Ghz" autocomplete="off" value="<?= old('processor') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("processor"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="motherboard">Motherboard</label>
                            <input style="border-radius: 15px" required type="text" class="form-control <?= ($validation->hasError('motherboard') ? 'is-invalid' : '') ?>" name="motherboard" id="motherboard" placeholder="AsRock B350 LGA 1151" autocomplete="off" value="<?= old('motherboard') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("motherboard"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="storage">Storage</label>
                            <input style="border-radius: 15px" required type="text" class="form-control <?= ($validation->hasError('storage') ? 'is-invalid' : '') ?>" name="storage" id="storage" placeholder="Seagate HDD 1TB 5000 Rpm" autocomplete="off" value="<?= old('storage') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("storage"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="memory">Memory</label>
                            <input style="border-radius: 15px" required type="text" class="form-control <?= ($validation->hasError('memory') ? 'is-invalid' : '') ?>" name="memory" id="memory" placeholder="Corsair Sodimm 4 Gb DDR3 1333Mhz" autocomplete="off" value="<?= old('memory') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("memory"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="graphics">Graphics</label>
                            <input style="border-radius: 15px" required type="text" class="form-control <?= ($validation->hasError('graphics') ? 'is-invalid' : '') ?>" name="graphics" id="graphics" placeholder="Asus GTX 1650 4Gb " autocomplete="off" value="<?= old('graphics') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("graphics"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="monitor">Monitor</label>
                            <input style="border-radius: 15px" required type="text" class="form-control <?= ($validation->hasError('monitor') ? 'is-invalid' : '') ?>" name="monitor" id="monitor" placeholder="Samsung Flat 20 Inc FHD" autocomplete="off" value="<?= old('monitor') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("monitor"); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="jumlah">Jumlah</label>
                            <input style="border-radius: 15px" required type="text" class="form-control <?= ($validation->hasError('jumlah') ? 'is-invalid' : '') ?>" name="jumlah" id="jumlah" placeholder="Jumlah Here..." autocomplete="off" value="<?= old('jumlah') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("jumlah"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="status">Operating System</label>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('operatingSystem') ? 'is-invalid' : '') ?>" name="operatingSystem" id="operatingSystem">
                                    <option value="">Choose...</option>
                                    <?php foreach($operatingSystem as $os) : ?>
                                        <option value="<?= $os['id']; ?>" <?= (old('operatingSystem') == $os['id'] ? 'selected' : '') ?>><?= $os['id'] . ' - ' . $os['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("operatingSystem"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="status">Serial Number</label>
                            <input style="border-radius: 15px" required type="text" class="form-control <?= ($validation->hasError('serialNumber') ? 'is-invalid' : '') ?>" name="serialNumber" id="serialNumber" placeholder="Insert serial number..." value="<?= old('serialNumber') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("serialNumber"); ?>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="text-right justify-content-right">
                        <button type="submit" name="insert" class="btn btn-success text-white">Insert Data</button>
                        <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal" aria-label="Close">
                            Cancel
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Insert Assets -->

<!-- MODALS INSERT EMPLOYEE -->
<div class="modal fade" id="modalInsertEmployee" tabindex="-1" aria-labelledby="modalInsertEmployee" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Employee Input Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('/Employee/insertData') ?>" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                    <input type="hidden" class="form-control" name="fullname" id="fullname" placeholder="Input Fullname Here..." value="<?= old("employee_id"); ?>">
 
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <input style="border-radius: 15px" type="text" disabled class="form-control" name="preview" id="preview" value="Insert data for : <?= $lastUser['user_asset_id'] ?> - <?= $lastUser['user_fullname'] ?> - <?= $lastUser['dept'] ?> Department">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="position">Position</label>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px;" class="form-control selectNoArrow <?= ($validation->hasError('position') ? 'is-invalid' : '') ?>" name="position" id="position" autocomplete="off" required>
                                    <option value="" disabled selected>Choose...</option>
                                    <?php foreach($position as $p) : ?>
                                        <option value="<?= $p['id'] ?>" <?= (old('position') == $p['id'] ? 'selected' : '') ?>><?= $p['id'] ?> - <?= $p['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("position"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="user">Gender</label><br>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px;" class="form-control selectNoArrow <?= ($validation->hasError('gender') ? 'is-invalid' : '') ?>" name="gender" id="gender" autocomplete="off" required>
                                    <option value="" disabled selected>Choose...</option>
                                    <?php foreach($gender as $g) : ?>
                                        <option value="<?= $g['id'] ?>" <?= (old('gender') == $g['id'] ? 'selected' : '') ?>><?= $g['id'] ?> - <?= $g['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("gender"); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="condition">Condition</label>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px;" class="form-control selectNoArrow <?= ($validation->hasError('condition') ? 'is-invalid' : '') ?>" name="condition" id="condition" autocomplete="off" required>
                                    <option value="" disabled selected>Choose...</option>
                                    <?php foreach($condition as $c) : ?>
                                        <option value="<?= $c['id'] ?>" <?= (old("condition") == $c['id'] ? 'selected' : '') ?>><?= $c['id'] ?> - <?= $c['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("condition"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-9">
                            <label for="asset_type">Address</label>
                            <input style="border-radius: 15px;" type="text" class="form-control <?= ($validation->hasError('address') ? 'is-invalid' : '') ?>" name="address" id="address" placeholder="Input Address Here..." value="<?= old("address"); ?>" autocomplete="off" required>
                            <div class="invalid-feedback">
                                <?= $validation->getError("address"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="text-right justify-content-right mt-4">
                        <button type="submit" name="tambah" class="btn btn-success text-white">Insert Data</button>
                        <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Insert Employee -->

<!-- MODALS INSERT ACCESS -->
<div class="modal fade" id="modalInsertAccess" tabindex="-1" aria-labelledby="modalInsertAccess" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert Access Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('/access/insertData') ?>" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                    <input type="hidden" class="form-control" name="user_access" id="user_access" placeholder="Input Here...">

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <input style="border-radius: 15px" type="text" disabled class="form-control" name="preview" id="preview" value="Insert data for : <?= $lastUser['user_asset_id'] ?> - <?= $lastUser['user_fullname'] ?> - <?= $lastUser['dept'] ?> Department">
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="domain_access">Domain Access</label>
                            <input style="border-radius: 15px;" required type="text" class="form-control <?= ($validation->hasError('domain_access') ? 'is-invalid' : '') ?>" name="domain_access" id="domain_access" placeholder="Input Domain Access Here..." value="<?= old('domain_access') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("domain_access"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="domain_pass">Domain Password</label>
                            <input style="border-radius: 15px;" required type="text" class="form-control <?= ($validation->hasError('domain_pass') ? 'is-invalid' : '') ?>" name="domain_pass" id="domain_pass" placeholder="Input Domain Pass Here..." value="<?= old('domain_pass') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("domain_pass"); ?>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="use_access">Use Access</label><br>
                            <select class="js-example-basic-multiple <?= ($validation->hasError('use_access') ? 'is-invalid' : '') ?>" name="use_access[]" id="use_access" multiple="multiple" required>
                                <!-- <option value="" disabled selected>Choose...</option> -->
                                    <?php foreach($use as $u) : ?>
                                        <option value="<?= $u['id'] ?>" <?= (old('use_access') == $u['id'] ? 'selected' : '') ?>><?= $u['id'] ?> - <?= $u['name'] ?></option>
                                    <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError("use_access"); ?>
                            </div>
                            <!-- <div class="selectWrapper">
                                <select style="border-radius: 15px;" class="form-control selectNoArrow <?= ($validation->hasError('use_access') ? 'is-invalid' : '') ?>" name="use_access" id="use_access" required>
                                    <option value="" disabled selected>Choose...</option>
                                    <?php foreach($use as $u) : ?>
                                        <option value="<?= $u['id'] ?>" <?= (old('use_access') == $u['id'] ? 'selected' : '') ?>><?= $u['id'] ?> - <?= $u['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("use_access"); ?>
                                </div>
                            </div> -->
                        </div>
                        <div class="form-group col-md-3">
                            <label for="deskera_access">Deskera</label><br>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px;" class="form-control selectNoArrow <?= ($validation->hasError('deskera_access') ? 'is-invalid' : '') ?>" name="deskera_access" id="deskera_access" required>
                                    <option value="" disabled selected>Choose...</option>
                                    <?php foreach($deskera as $d) : ?>
                                        <option value="<?= $d['id'] ?>" <?= (old('deskera_access') == $d['id'] ? 'selected' : '') ?>><?= $d['id'] ?> - <?= $d['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("deskera_access"); ?>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-group col-md-3">
                            <label for="status">Status</label>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px;" class="form-control selectNoArrow <?= ($validation->hasError('status') ? 'is-invalid' : '') ?>" name="status" id="status" required>
                                    <option value="" disabled selected>Choose...</option>
                                    <?php foreach($status as $s) : ?>
                                        <option value="<?= $s['id'] ?>" <?= (old('status') == $s['id'] ? 'selected' : '') ?>><?= $s['id'] ?> - <?= $s['name1'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("status"); ?>
                                </div>
                            </div>                        
                        </div> -->
                        <div class="form-group col-md-6">
                            <label for="anydesk">Anydesk</label>
                            <input style="border-radius: 15px;" required type="text" class="form-control <?= ($validation->hasError('anydesk') ? 'is-invalid' : '') ?>" name="anydesk" id="anydesk" placeholder="Input anydesk id here..." value="<?= old('anydesk') ?>">                        
                            <div class="invalid-feedback">
                                <?= $validation->getError("anydesk"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="text-right justify-content-right mt-4">
                        <button type="submit" name="tambah" class="btn btn-success text-white">Insert Data</button>
                        <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Insert Access -->

<!-- MODALS INSERT EMAIL -->
<div class="modal fade" id="modalInsertEmail" tabindex="-1" aria-labelledby="modalInsertEmail" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert Email Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('/email/insertData') ?>" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                    <input type="hidden" class="form-control" name="email_address" id="email_address" placeholder="Input Asset Type Here...">
                    
                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <input style="border-radius: 15px;" style="border-radius: 15px" type="text" disabled class="form-control" name="preview" id="preview" value="Insert data for : <?= $lastUser['user_fullname'] ?> - <?= $lastUser['user_email'] ?> - <?= $lastUser['dept'] ?> Department">
                        </div>
                    </div>
                    
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="email_user">Email User</label>
                            <input style="border-radius: 15px;" required type="text" class="form-control <?= ($validation->hasError('email_user') ? 'is-invalid' : '') ?>" name="email_user" id="email_user" placeholder="dimasokva" value="<?= old('email_user') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("email_user"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                        <label for="email_use">Using</label>
                        <div class="selectWrapper">
                            <select style="border-radius: 15px;" required class="form-control selectNoArrow <?= ($validation->hasError('email_use') ? 'is-invalid' : '') ?>" name="email_use" id="email_use" placeholder="Using Here...">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($emailUse as $e) : ?>
                                    <option value="<?= $e['id'] ?>" <?= (old('email_use') == $e['id'] ? 'selected' : '') ?>><?= $e['id'] ?> - <?= $e['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError("email_use"); ?>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="email_pass">Email Password</label>
                            <input style="border-radius: 15px;" required type="text" class="form-control <?= ($validation->hasError('email_pass') ? 'is-invalid' : '') ?>" name="email_pass" id="email_pass" placeholder="Password Here..." value="<?= old('email_pass') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("email_pass"); ?>
                            </div>
                        </div>
                    </div>
                        <!-- <div class="form-group col-md-6">
                            <label for="status">Status</label>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px;" required style="border-radius: 15px;" class="form-control selectNoArrow <?= ($validation->hasError('email_status') ? 'is-invalid' : '') ?>" name="email_status" id="email_status">
                                    <option value="" disabled selected>Choose...</option>
                                    <?php foreach($status as $s) : ?>
                                    <option value="<?= $s['id'] ?>" <?= (old('email_status') == $s['id'] ? 'selected' : '') ?>><?= $s['id'] ?> - <?= $s['name1'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("email_status"); ?>
                                </div>
                            </div>                        
                        </div> -->
                    </div>
                    <div class="text-right justify-content-right mt-4">
                        <button type="submit" name="insert" class="btn btn-success text-white">Insert Data</button>
                        <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Insert Email -->

<!-- MODALS INSERT DAILY -->
<div class="modal fade" id="modalInsertDaily" tabindex="-1" aria-labelledby="modalInsertDaily" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert Daily Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('/daily/insertData') ?>" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                    <input type="hidden" class="form-control" name="date_preventive" id="date_preventive" placeholder="Input Fullname Here...">

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="date_daily">Date</label>
                            <input style="border-radius: 15px;" type="date" class="form-control <?= ($validation->hasError('date_daily') ? 'is-invalid' : '') ?>" name="date_daily" id="date_daily" required value="<?= old('date_daily') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("date_daily"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-8">
                            <label for="user">User</label>
                            <input style="border-radius: 15px;" type="text" class="form-control" disabled value="Insert data for : <?= "PTPF-" . $it['dept_slug'] . "-" . $it['user_asset_id']  ?> - <?= $it['user_fullname'] ?>">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="task_daily">Task</label><br>
                        <pre><textarea style="border-radius: 15px;" required name="task_daily" id="task_daily" class="form-control <?= ($validation->hasError('task_daily') ? 'is-invalid' : '') ?>" rows="5" style="width: 100%;"><?= old('task_daily') ?></textarea></pre>
                        <div class="invalid-feedback">
                            <?= $validation->getError("task_daily"); ?>
                        </div>
                    </div>   
                    
                    <div class="text-right justify-content-right mt-4">
                        <button type="submit" name="insert" class="btn btn-success text-white">Insert Data</button>
                        <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Insert Daily -->

<!-- MODALS INSERT PREVENTIVE -->
<div class="modal fade" id="modalInsertPreventive" tabindex="-1" aria-labelledby="modalInsertPreventive" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert Preventive Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('/preventive/insertData') ?>" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                    <input style="border-radius: 15px;" type="hidden" class="form-control" name="date_preventive" id="date_preventive" placeholder="Input Fullname Here...">

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="name_preventive">User ID</label>
                            <input style="border-radius: 15px;" required type="text" class="form-control <?= ($validation->hasError('name_preventive') ? 'is-invalid' : '') ?>" name="name_preventive" id="name_preventive" placeholder="Input Name Here...">
                            <div class="invalid-feedback">
                                <?= $validation->getError("name_preventive"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="sequence_preventive">Sequence</label>
                            <input style="border-radius: 15px;" required type="text" placeholder="Input sequence here..." class="form-control <?= ($validation->hasError('sequence_preventive') ? 'is-invalid' : '') ?>" name="sequence_preventive" id="sequence_preventive">
                            <div class="invalid-feedback">
                                <?= $validation->getError("sequence_preventive"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="date_preventive">Date</label>
                            <input style="border-radius: 15px;" required type="date" class="form-control <?= ($validation->hasError('date_preventive') ? 'is-invalid' : '') ?>" name="date_preventive" id="date_preventive">
                            <div class="invalid-feedback">
                                <?= $validation->getError("date_preventive"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="antivirus_preventive">Antivirus</label>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px;" required class="form-control selectNoArrow <?= ($validation->hasError('antivirus_preventive') ? 'is-invalid' : '') ?>" name="antivirus_preventive" id="antivirus_preventive" placeholder="Use ?">
                                    <option value="">Choose...</option>
                                    <?php foreach($preventiveStatus as $ps) : ?>
                                        <option value="<?= $ps['id'] ?>"><?= $ps['id'] ?> - <?= $ps['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("antivirus_preventive"); ?>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="hardware_preventive">Hardware</label>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px;" required class="form-control selectNoArrow <?= ($validation->hasError('hardware_preventive') ? 'is-invalid' : '') ?>" name="hardware_preventive" id="hardware_preventive" placeholder="Use ?">
                                    <option value="">Choose...</option>
                                    <?php foreach($preventiveStatus as $ps) : ?>
                                        <option value="<?= $ps['id'] ?>"><?= $ps['id'] ?> - <?= $ps['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("hardware_preventive"); ?>
                                </div>
                            </div>
                        </div><div class="form-group col-md-4">
                            <label for="connection_preventive">Connection</label>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px;" required class="form-control selectNoArrow <?= ($validation->hasError('connection_preventive') ? 'is-invalid' : '') ?>" name="connection_preventive" id="connection_preventive" placeholder="Use ?">
                                    <option value="">Choose...</option>
                                    <?php foreach($preventiveStatus as $ps) : ?>
                                        <option value="<?= $ps['id'] ?>"><?= $ps['id'] ?> - <?= $ps['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("connection_preventive"); ?>
                                </div>
                            </div>
                    </div>
                    <div class="row">
                        </div><div class="form-group col-md-4">
                            <label for="security_preventive">Security</label>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px;" required class="form-control selectNoArrow <?= ($validation->hasError('security_preventive') ? 'is-invalid' : '') ?>" name="security_preventive" id="security_preventive" placeholder="Use ?">
                                    <option value="">Choose...</option>
                                    <?php foreach($preventiveStatus as $ps) : ?>
                                        <option value="<?= $ps['id'] ?>"><?= $ps['id'] ?> - <?= $ps['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("security_preventive"); ?>
                                </div>
                            </div>
                        </div><div class="form-group col-md-4">
                            <label for="backup_preventive">Backup</label>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px;" required class="form-control selectNoArrow <?= ($validation->hasError('backup_preventive') ? 'is-invalid' : '') ?>" name="backup_preventive" id="backup_preventive" placeholder="Use ?">
                                    <option value="">Choose...</option>
                                    <?php foreach($preventiveStatus as $ps) : ?>
                                        <option value="<?= $ps['id'] ?>"><?= $ps['id'] ?> - <?= $ps['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("backup_preventive"); ?>
                                </div>
                            </div>
                        </div><div class="form-group col-md-4">
                            <label for="clean_preventive">Clean Up</label>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px;" required class="form-control selectNoArrow <?= ($validation->hasError('clean_preventive') ? 'is-invalid' : '') ?>" name="clean_preventive" id="clean_preventive" placeholder="Use ?">
                                    <option value="">Choose...</option>
                                    <?php foreach($preventiveStatus as $ps) : ?>
                                        <option value="<?= $ps['id'] ?>"><?= $ps['id'] ?> - <?= $ps['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("clean_preventive"); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="comment_preventive">Comment</label><br>
                        <pre><textarea style="border-radius: 15px;" name="comment_preventive" id="comment_preventive" rows="5" style="width: 100%;" class="form-control"></textarea></pre>
                    </div>   
                    
                    <div class="text-right justify-content-right mt-4">
                        <button type="submit" name="insert" class="btn btn-success text-white">Insert Data</button>
                        <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Insert Preventive -->

<!-- MODALS INSERT RESET PASSWORD -->
<div class="modal fade" id="modalInsertResetPassword" tabindex="-1" aria-labelledby="modalInsertResetPassword" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert Reset Password Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('/resetPassword/insertData') ?>" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                    <input style="border-radius: 15px;" type="hidden" class="form-control" name="date_preventive" id="date_preventive" placeholder="Input Fullname Here...">

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="reset_date">Date</label>
                            <input style="border-radius: 15px;" required type="date" class="form-control <?= ($validation->hasError('reset_date') ? 'is-invalid' : '') ?>" name="reset_date" id="reset_date">
                            <div class="invalid-feedback">
                                <?= $validation->getError("reset_date"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="reset_user">User ID</label>
                            <input style="border-radius: 15px;" required type="text" class="form-control <?= ($validation->hasError('reset_user') ? 'is-invalid' : '') ?>" name="reset_user" id="reset_user" placeholder="Input Name Here...">
                            <div class="invalid-feedback">
                                <?= $validation->getError("reset_user"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="reset_seq">Sequence</label>
                            <input style="border-radius: 15px;" required type="number" placeholder="Input sequence here..." class="form-control <?= ($validation->hasError('reset_seq') ? 'is-invalid' : '') ?>" name="reset_seq" id="reset_seq">
                            <div class="invalid-feedback">
                                <?= $validation->getError("reset_seq"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label for="reset_domain">Domain</label>
                            <input style="border-radius: 15px;" required type="text" placeholder="Input sequence here..." class="form-control <?= ($validation->hasError('reset_domain') ? 'is-invalid' : '') ?>" name="reset_domain" id="reset_domain">
                            <div class="invalid-feedback">
                                <?= $validation->getError("reset_domain"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="reset_old">Old Password</label>
                            <input style="border-radius: 15px;" required type="text" placeholder="Input Old Password here..." class="form-control <?= ($validation->hasError('reset_old') ? 'is-invalid' : '') ?>" name="reset_old" id="reset_old">
                            <div class="invalid-feedback">
                                <?= $validation->getError("reset_old"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="reset_new">New Password</label>
                            <input style="border-radius: 15px;" required type="text" placeholder="Input New Password here..." class="form-control <?= ($validation->hasError('reset_new') ? 'is-invalid' : '') ?>" name="reset_new" id="reset_new">
                            <div class="invalid-feedback">
                                <?= $validation->getError("reset_new"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="reset_comment">Comment</label><br>
                        <pre><textarea style="border-radius: 15px;" name="reset_comment" id="reset_comment" rows="5" style="width: 100%;" class="form-control"></textarea></pre>
                    </div>   
                    
                    <div class="text-right justify-content-right mt-4">
                        <button type="submit" name="insert" class="btn btn-success text-white">Insert Data</button>
                        <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Insert Reset Password -->

<!-- MODALS INSERT BACKUP -->
<div class="modal fade" id="modalInsertBackup" tabindex="-1" aria-labelledby="modalInsertBackup" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert Backup Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('/backup/insertData') ?>" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                    <input style="border-radius: 15px;" type="hidden" class="form-control" placeholder="Input Fullname Here...">

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="backup_date">Date</label>
                            <input style="border-radius: 15px;" required type="date" required class="form-control <?= ($validation->hasError('backup_date') ? 'is-invalid' : '') ?>" name="backup_date" id="backup_date" placeholder="Date here...">
                            <div class="invalid-feedback">
                                <?= $validation->getError("backup_date"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="backup_start">Storage Awal</label>
                            <input style="border-radius: 15px;" required type="text" class="form-control <?= ($validation->hasError('backup_start') ? 'is-invalid' : '') ?>" name="backup_start" id="backup_start" placeholder="3.7 TB">
                            <div class="invalid-feedback">
                                <?= $validation->getError("backup_start"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="backup_end">Storage Akhir</label>
                            <input style="border-radius: 15px;" required type="text" class="form-control <?= ($validation->hasError('backup_end') ? 'is-invalid' : '') ?>" name="backup_end" id="backup_end" placeholder="4.0 TB">
                            <div class="invalid-feedback">
                                <?= $validation->getError("backup_end"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-8">
                            <label for="backup_comment">Comment</label><br>
                            <pre><textarea style="border-radius: 15px;" name="backup_comment" id="backup_comment" rows="5" style="width: 100%;" class="form-control"></textarea></pre>
                        </div> 
                        <div class="form-group col-md-4">
                            <div class="row">
                                <div class="form-group col-12">
                                    <label for="backup_month">Month/Sequence</label>
                                    <input style="border-radius: 15px;" required type="number" class="form-control <?= ($validation->hasError('backup_month') ? 'is-invalid' : '') ?>" name="backup_month" id="backup_month" placeholder="1">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError("backup_month"); ?>
                                    </div>
                                </div>
                                <div class="form-group col-12">
                                    <label for="backup_type">Type</label>
                                    <input style="border-radius: 15px;" required type="number" class="form-control <?= ($validation->hasError('backup_type') ? 'is-invalid' : '') ?>" name="backup_type" id="backup_type" placeholder="1">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError("backup_type"); ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-right justify-content-right mt-4">
                        <button type="submit" name="insert" class="btn btn-success text-white">Insert Data</button>
                        <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Insert Backup -->

<!-- MODALS INSERT MONITORING -->
<div class="modal fade" id="modalInsertMonitoring" tabindex="-1" aria-labelledby="modalInsertMonitoring" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert Monitoring Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('/monitoringSpeed/insertData') ?>" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                    <input style="border-radius: 15px;" type="hidden" class="form-control" placeholder="Input Fullname Here...">

                    <div class="row">
                        <div class="form-group col-md-3">
                            <label for="backup_date">Year</label>
                            <input style="border-radius: 15px;" required type="number" required class="form-control <?= ($validation->hasError('monitoring_year') ? 'is-invalid' : '') ?>" name="monitoring_year" id="monitoring_year" placeholder="Year here...">
                            <div class="invalid-feedback">
                                <?= $validation->getError("backup_date"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="backup_date">Month</label>
                            <input style="border-radius: 15px;" required type="number" required class="form-control <?= ($validation->hasError('monitoring_month') ? 'is-invalid' : '') ?>" name="monitoring_month" id="monitoring_month" placeholder="Month here...">
                            <div class="invalid-feedback">
                                <?= $validation->getError("backup_date"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="backup_date">Average</label>
                            <input style="border-radius: 15px;" required type="number" step="0.01" required class="form-control <?= ($validation->hasError('monitoring_average') ? 'is-invalid' : '') ?>" name="monitoring_average" id="monitoring_average" placeholder="Average here...">
                            <div class="invalid-feedback">
                                <?= $validation->getError("backup_date"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label for="backup_date">Downtime</label>
                            <input style="border-radius: 15px;" required type="number" required class="form-control <?= ($validation->hasError('monitoring_downtime') ? 'is-invalid' : '') ?>" name="monitoring_downtime" id="monitoring_downtime" placeholder="Downtime here...">
                            <div class="invalid-feedback">
                                <?= $validation->getError("backup_date"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-12">
                            <label for="backup_comment">Comment</label><br>
                            <pre><textarea style="border-radius: 15px;" name="monitoring_comment" id="monitoring_comment" rows="5" style="width: 100%;" class="form-control"><?= ($validation->hasError('monitoring_comment') ? 'is-invalid' : '') ?></textarea></pre>
                        </div> 
                    </div>
                    <div class="text-right justify-content-right mt-4">
                        <button type="submit" name="insert" class="btn btn-success text-white">Insert Data</button>
                        <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Insert Monitoring -->

<!-- MODALS INSERT INTERNET -->
<div class="modal fade" id="modalInsertInternet" tabindex="-1" aria-labelledby="modalInsertInternet" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert Internet Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('/Internet/insertData') ?>" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                    <input style="border-radius: 15px;" type="hidden" class="form-control" placeholder="Input Fullname Here...">

                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="backup_date">User ID</label>
                            <input style="border-radius: 15px;" required type="number" required class="form-control <?= ($validation->hasError('userIdInternet') ? 'is-invalid' : '') ?>" name="userIdInternet" id="userIdInternet" placeholder="User ID here...">
                            <div class="invalid-feedback">
                                <?= $validation->getError("userIdInternet"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="backup_date">Profile</label>
                            <input style="border-radius: 15px;" required type="text" required class="form-control <?= ($validation->hasError('profileInternet') ? 'is-invalid' : '') ?>" name="profileInternet" id="profileInternet" placeholder="Profile here...">
                            <div class="invalid-feedback">
                                <?= $validation->getError("profileInternet"); ?>
                            </div>
                        </div>         
                    </div>
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="backup_date">Username</label>
                            <input style="border-radius: 15px;" required type="text" required class="form-control <?= ($validation->hasError('usernameInternet') ? 'is-invalid' : '') ?>" name="usernameInternet" id="usernameInternet" placeholder="Username here...">
                            <div class="invalid-feedback">
                                <?= $validation->getError("usernameInternet"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="backup_date">Password</label>
                            <input style="border-radius: 15px;" required type="text" required class="form-control <?= ($validation->hasError('passwordInternet') ? 'is-invalid' : '') ?>" name="passwordInternet" id="passwordInternet" placeholder="Password here...">
                            <div class="invalid-feedback">
                                <?= $validation->getError("passwordInternet"); ?>
                            </div>
                        </div>         
                    </div>
                    <div class="text-right justify-content-right mt-4">
                        <button type="submit" name="insert" class="btn btn-success text-white">Insert Data</button>
                        <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Insert Internet -->

<!-- MODALS INSERT PROJECT -->
<div class="modal fade" id="modalInsertProject" tabindex="-1" aria-labelledby="modalInsertProject" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert Project Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('/project/insertData'); ?>" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                    <input style="border-radius: 15px;" type="hidden" class="form-control" name="date_preventive" id="date_preventive" placeholder="Input Fullname Here..." value="<?= old("date_preventive"); ?>">

                    <div class="form-group">
                        <label for="project">Project</label>
                        <input style="border-radius: 15px;" required type="text" class="form-control <?= ($validation->hasError('project_name') ? 'is-invalid' : '') ?>" name="project_name" id="project_name">
                        <div class="invalid-feedback">
                            <?= $validation->getError("project_name"); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="planning_date_project">Planning Date</label>
                            <input style="border-radius: 15px;" required type="date" class="form-control <?= ($validation->hasError('planning_date_project') ? 'is-invalid' : '') ?>" name="planning_date_project" id="planning_date_project">
                            <div class="invalid-feedback">
                                <?= $validation->getError("planning_date_project"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="actual_date_project">Actual Date</label>
                            <input style="border-radius: 15px;" required type="date" class="form-control <?= ($validation->hasError('actual_date_project') ? 'is-invalid' : '') ?>" name="actual_date_project" id="actual_date_project">
                            <div class="invalid-feedback">
                                <?= $validation->getError("actual_date_project"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                        <label for="status_project">Status</label>
                            <div class="selectWrapper">
                                <select style="border-radius: 15px;" required class="form-control selectNoArrow <?= ($validation->hasError('project_status') ? 'is-invalid' : '') ?>" name="project_status" id="project_status" placeholder="Use ?">
                                    <option value="">Choose...</option>
                                    <?php foreach($projectStatus as $ps) : ?>
                                        <option value="<?= $ps['id'] ?>"><?= $ps['id'] ?> - <?= $ps['name'] ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <div class="invalid-feedback">
                                    <?= $validation->getError("project_status"); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label for="comment_project">Comment</label><br>
                        <pre><textarea style="border-radius: 15px;" name="project_comment" id="project_comment" class="form-control <?= ($validation->hasError('project_comment') ? 'is-invalid' : '') ?>" rows="5" style="width: 100%;" class="rounded"></textarea></pre>
                        <div class="invalid-feedback">
                            <?= $validation->getError("project_comment"); ?>
                        </div>
                    </div>   
                    
                    <div class="text-right justify-content-right mt-4">
                        <button type="submit" name="insert" class="btn btn-success text-white">Insert Data</button>
                        <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Insert Project -->

<!-- MODALS INSERT OVERVIEW -->
<div class="modal fade" tabindex="-1" aria-labelledby="modalInsertOverview" aria-hidden="true" id="modalInsertOverview">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert Overview Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('/Dashboard/insertData') ?>" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                    <input type="hidden" class="form-control" name="asset_name" id="asset_name" placeholder="Input Asset Type Here..." value="<?= old("asset_name"); ?>">
 
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="asset_id">Asset Identity</label>
                            <input style="border-radius: 15px;" type="text" class="form-control" name="asset_id" id="asset_id" placeholder="PTPF-IT-2021" value="">
                        </div>  
                        <div class="form-group col-md-6">
                            <label for="asset_name">Asset Name</label>
                            <input style="border-radius: 15px;" type="text" class="form-control" name="asset_name" id="asset_name" placeholder="Computer i7" autocomplete="off">
                        </div>
                    </div>                                               
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="borrowed">Borrowed</label>
                            <input style="border-radius: 15px;" type="number" class="form-control" name="borrowed" id="borrowed" placeholder="Input Item Borrowed Here..." autocomplete="off">
                        </div>
                        <div class="form-group col-md-6">
                            <label for="available">Available</label>
                            <input style="border-radius: 15px;" type="number" class="form-control" name="available" id="available" placeholder="Input Item Available Here..." autocomplete="off">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="information">Information</label>
                        <pre><textarea style="border-radius: 15px;" name="information" class="form-control" id="information" rows="5" style="width: 100%;"></textarea></pre>
                    </div>
                    <br>
                    <div class="text-right justify-content-right">
                        <button type="submit" name="insert" class="btn btn-success">Insert Data</button>
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Insert Overview -->

<!-- MODALS Reset Password -->
<div class="modal fade" id="modalResetPass" tabindex="-1" aria-labelledby="modalResetPass" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Reset Password Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form method="post" action="<?= base_url('/Profile/resetPassword'); ?>">
                    <div class="row d-flex justify-content-center">
                        <div class="col-md-10 pt-2">
                            <input type="hidden" name="currentUser">
                            <div class="form-group">
                                <label for="currentPass">Current Password</label>
                                <input style="border-radius: 15px;" required type="password" class="form-control <?= ($validation->hasError('currentPass') ? 'is-invalid' : ''); ?>" name="currentPass" id="currentPass" placeholder="Input Current Password Here..." value="<?= (old('currentPass') ? old('currentPass') : '') ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError("currentPass"); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="newPass">New Password</label>
                                <input style="border-radius: 15px;" required type="password" class="form-control <?= ($validation->hasError('newPass') ? 'is-invalid' : ''); ?>" name="newPass" id="newPass" placeholder="Input New Password Here..." value="<?= (old('newPass') ? old('newPass') : '') ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError("newPass"); ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="confirmPass">Confirm Password</label>
                                <input style="border-radius: 15px;" required type="password" class="form-control <?= ($validation->hasError('confirmPass') ? 'is-invalid' : ''); ?>" name="confirmPass" id="confirmPass" placeholder="Confirm Password..." value="<?= (old('confirmPass') ? old('confirmPass') : '') ?>">
                                <div class="invalid-feedback">
                                    <?= $validation->getError("confirmPass"); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="d-flex justify-content-center my-4">
                        <button type="submit" name="reset" class="btn btn-success text-white">Reset Password</button>
                        <button type="button" class="btn btn-danger text-white" style="margin-left: 10px;" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Reset Password -->

<!-- MODALS INSERT USER MANAGEMENT -->
<div class="modal fade" tabindex="-1" aria-labelledby="modalInsertUserManagement" aria-hidden="true" id="modalInsertUserManagement">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Insert User Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="<?= base_url('/userManagement/insertData') ?>" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                    <input type="hidden" class="form-control" name="asset_name" id="asset_name" placeholder="Input Asset Type Here...">
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="username">Username</label>
                            <input style="border-radius: 15px;" required type="text" class="form-control <?= ($validation->hasError('username') ? 'is-invalid' : '') ?>" name="username" id="username" placeholder="dimasokva" autocomplete="off" value="<?= (old('username') ? old('username') : '') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("username"); ?>
                            </div>  
                        </div>
                        <div class="form-group col-md-6">
                            <label for="password">Password</label>
                            <input style="border-radius: 15px;" required type="password" class="form-control <?= ($validation->hasError('password') ? 'is-invalid' : '') ?>" name="password" id="password" placeholder="Input Password Here..." autocomplete="off" value="<?= (old('password') ? old('password') : '') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("password"); ?>
                            </div>
                        </div>
                    </div>                                               
                    <div class="row">
                        <div class="form-group col-md-6">
                            <label for="fullname">Fullname</label>
                            <input style="border-radius: 15px;" required type="text" class="form-control <?= ($validation->hasError('fullname') ? 'is-invalid' : '') ?>" name="fullname" id="fullname" placeholder="Dimas Okva" autocomplete="off" value="<?= (old('fullname') ? old('fullname') : '') ?>">
                            <div class="invalid-feedback">
                                <?= $validation->getError("fullname"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="identity">Identity</label>
                            <input style="border-radius: 15px;" required type="number" class="form-control <?= ($validation->hasError('identity') ? 'is-invalid' : '') ?>" name="identity" id="identity" placeholder="2175" autocomplete="off" value="<?= (old('identity') ? old('identity') : '') ?>">
                            <div class="invalid-feedback"> 
                                <?= $validation->getError("identity"); ?>
                            </div>
                        </div>
                    </div>                                               
                    <div class="row">
                        <div class="form-group col-md-4">
                            <label for="department">Department</label>
                            <select style="border-radius: 15px;" required class="form-control selectNoArrow <?= ($validation->hasError('department') ? 'is-invalid' : '') ?>" name="department" id="department" placeholder="Input Dept..." value="<?= (old('department') ? old('department') : '') ?>">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($dept as $d) : ?>
                                    <option value="<?= $d['id'] ?>"><?= $d['id'] ?> - <?= $d['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError("department"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="level">Level</label>
                            <select style="border-radius: 15px;" required class="form-control selectNoArrow <?= ($validation->hasError('level') ? 'is-invalid' : '') ?>" name="level" id="level">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($level as $l) : ?>
                                    <option value='<?= $l['id'] ?>'><?= $l['id'] ?> - <?= $l['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError("level"); ?>
                            </div>
                        </div>
                        <div class="form-group col-md-4">
                            <label for="status">Status</label>
                            <select style="border-radius: 15px;" required class="form-control selectNoArrow <?= ($validation->hasError('status') ? 'is-invalid' : '') ?>" name="status" id="status">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($status as $s) : ?>
                                    <option value='<?= $s['id'] ?>'><?= $s['id'] ?> - <?= $s['name1'] ?> - <?= $s['name2'] ?></option>
                                <?php endforeach; ?>
                            </select>
                            <div class="invalid-feedback">
                                <?= $validation->getError("status"); ?>
                            </div>
                        </div>
                    </div>
                    <div class="text-right justify-content-right mt-2">
                        <button type="submit" name="update" class="btn btn-success text-white">Insert Data</button>
                        <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Insert User Management -->

<!-- MODALS EDIT POST MESSAGE -->
<div class="modal fade" tabindex="-1" aria-labelledby="modalEditPostMessage" aria-hidden="true" id="modalEditPostMessage">
    <div class="modal-dialog modal-lg modal-dialog-centered">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Post Form</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <!-- saya buat hidden satu karena semua yg div pertama sealu tidak memunculkan error ketika ada error (is-invalid tidak bekerja) dan tidak tau kenapa -->
                    <input type="hidden" class="form-control" name="asset_name" id="asset_name" placeholder="Input Asset Type Here...">
                    <div class="form-row">
                        <div class="form-group col-md-7">
                            <label for="by">Post By</label>
                            <input type="text" required name="by" id="by" class="form-control" style="border-radius: 15px;" placeholder="Muhammad Muktar - HRD Dept" value="">
                        </div>
                        <div class="form-group col-md-5">
                            <label for="mytime">Create at</label>
                            <input type="text" disabled id="mytime" class="form-control" style="border-radius: 15px;" placeholder="10-10-2021" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="title">Post Title</label>
                        <input type="text" required name="title" id="title" class="form-control" style="border-radius: 15px;" placeholder="Holiday Announcements">
                    </div>
                    <div class="form-group">
                        <label for="message">Post Message</label>
                        <textarea name="message" required id="message" class="form-control" cols="30" rows="10"></textarea>
                    </div>
                    <div class="text-right justify-content-right mt-2">
                        <button type="submit" name="update" class="btn btn-success">Insert Data</button>
                        <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Edit Post Message -->

<!-- MODALS DELETE ALERT -->
<div class="modal fade" tabindex="-1" aria-labelledby="modalDelete" aria-hidden="true" id="modalDelete">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="border-radius: 15px;">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Confirmation</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="" method="post">
                    <input type="hidden" name="deleteId" class="deleteId" id="deleteId">
                    <p class="text-center">Are you sure ?</p>
                    <div class="text-center mt-4">
                        <button type="submit" name="delete" class="btn btn-success text-white">Confirm</button>
                        <button type="button" class="btn btn-danger text-white" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Delete Alert -->

<!-- MODALS SPEC -->
<div class="modal fade" id="modalSpec" tabindex="-1" aria-labelledby="modalSpec" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered justify-content-center">
        <div class="modal-content" style="width: 400px; height: 600px">
            <div class="modal-body text-center p-0 m-0">
                <div id="carouselExampleCaptions" class="carousel slide" data-bs-ride="carousel">
                    <ol class="carousel-indicators">
                        <li type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></li>
                        <li type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="1" aria-label="Slide 2"></li>                    
                        <li type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="2" aria-label="Slide 3"></li>
                        <li type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="3" aria-label="Slide 4"></li>
                        <li type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="4" aria-label="Slide 5"></li>
                        <li type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="5" aria-label="Slide 6"></li>
                    </ol>
                    <div class="carousel-inner">
                        <div class="carousel-item active">
                            <img src="/images/spec/processor.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Processor</h5>
                                <p class="cpu"></p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/spec/motherboard.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Motherboard</h5>
                                <p class="mb"></p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/spec/hdd.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Hard Disk</h5>
                                <p class="hdd"></p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/spec/ram.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Memory</h5>
                                <p class="ram"></p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/spec/vga.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Graphics</h5>
                                <p class="vga"></p>
                            </div>
                        </div>
                        <div class="carousel-item">
                            <img src="/images/spec/monitor.jpg" class="d-block w-100" alt="...">
                            <div class="carousel-caption d-none d-md-block">
                                <h5>Monitor</h5>
                                <p class="monitor"></p>
                            </div>
                        </div>
                    </div>
                    <a class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </a>
                    <a class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </a>
                    <!-- <a class="carousel-control-prev" href="#carouselExampleCaptions" data-bs-target="#carouselExampleCaptions" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleCaptions" data-bs-target="#carouselExampleCaptions" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a> -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Spec -->

<!-- MODALS DASHBOARD PREVENTIVE -->
<div class="modal fade" id="modalDashboardPreventive" tabindex="-1" aria-labelledby="modalDashboardPreventive" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content" style="border-radius: 10px;">
            <div class="modal-header justify-content-center">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fa fa-lock fa-fw"></i> Preventive Overview Details</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                <div class="col-3">
                        <?php $bulan = ['February', 'April', 'June', 'August', 'October', 'December'] ?>
                        <div class="row">
                            <?php foreach($bulan as $b) : ?>
                            <div class="col-12 text-center"><p class="fw-bold" style="padding: 5px 8px; background-color: #55CE63; border-radius: 3px; color: #fff"><?= $b; ?></p></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-9">
                        <?php foreach($overviewPreventive as $op) : ?>
                        <div class="row">
                            <div class="col-4 d-md-block d-none"><p style="padding: 5px 8px;">Sequence <?= $op['preventive_seq'] ?></p></div>
                            <div class="col-4 d-block d-md-none"><p style="padding: 5px 8px;">Seq <?= $op['preventive_seq'] ?></p></div>
                            <div class="col-6 d-md-block d-none"><p style="padding: 5px 8px;"><?= date('d F Y', strtotime($op['date'])); ?></p></div>
                            <div class="col-6 d-block d-md-none"><p style="padding: 5px 8px;"><?= date('d M Y', strtotime($op['date'])); ?></p></div>
                            <div class="col-2 p-0 m-0"><i class="fas fa-check fa-fw text-success" style="padding: 5px 8px;"></i></div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Dashboard Preventive -->

<!-- MODALS DASHBOARD RESET PASSWORD -->
<div class="modal fade" id="modalDashboardResetPassword" tabindex="-1" aria-labelledby="modalDashboardResetPassword" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content" style="border-radius: 10px;">
            <div class="modal-header justify-content-center">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-sync-alt fa-fw"></i> Reset Password Overview Details</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                <div class="col-3">
                        <?php $bulan = ['March', 'June', 'September', 'December'] ?>
                        <div class="row">
                            <?php foreach($bulan as $b) : ?>
                            <div class="col-12 text-center"><p class="fw-bold" style="padding: 5px 8px; background-color: #55CE63; border-radius: 3px; color: #fff"><?= $b; ?></p></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-9">
                        <?php foreach($overviewReset as $or) : ?>
                        <div class="row">
                            <div class="col-4 d-md-block d-none"><p style="padding: 5px 8px;">Sequence <?= $or['reset_seq'] ?></p></div>
                            <div class="col-4 d-block d-md-none"><p style="padding: 5px 8px;">Seq <?= $or['reset_seq'] ?></p></div>
                            <div class="col-6 d-md-block d-none"><p style="padding: 5px 8px;"><?= date('d F Y', strtotime($or['date'])); ?></p></div>
                            <div class="col-6 d-block d-md-none"><p style="padding: 5px 8px;"><?= date('d M Y', strtotime($or['date'])); ?></p></div>
                            <div class="col-2 p-0 m-0"><i class="fas fa-check fa-fw text-success" style="padding: 5px 8px;"></i></div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Dashboard Reset Password -->

<!-- MODALS DASHBOARD BACKUP -->
<div class="modal fade" id="modalDashboardBackup" tabindex="-1" aria-labelledby="modalDashboardBackup" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content" style="border-radius: 10px;">
            <div class="modal-header justify-content-center">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-hdd fa-fw"></i> Backup Overview Details</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                <div class="col-3">
                        <?php $bulan = ['January', 'Febuary', 'March', 'April', 'May', 'Juny', 'July', 'August', 'September', 'October', 'November', 'December'] ?>
                        <div class="row">
                            <?php foreach($bulan as $b) : ?>
                            <div class="col-12 text-center"><p class="fw-bold" style="padding: 5px 8px; background-color: #55CE63; border-radius: 3px; color: #fff"><?= $b; ?></p></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-9">
                        <?php foreach($overviewBackup as $ob) : ?>
                        <div class="row">
                            <div class="col-5 d-md-block d-none"><p style="padding: 5px 8px;">Sequence <?= $ob['backup_month'] ?></p></div>
                            <div class="col-5 d-block d-md-none"><p style="padding: 5px 8px;">Seq <?= $ob['backup_month'] ?></p></div>
                            <div class="col-5"><p style="padding: 5px 8px;"><?= date('d M Y', strtotime($ob['date'])); ?></p></div>
                            <div class="col-2 p-0 m-0"><i class="fas fa-check fa-fw text-success" style="padding: 5px 8px;"></i></div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Dashboard Backup -->

<!-- MODALS DASHBOARD MONITORING -->
<div class="modal fade" id="modalDashboardMonitoring" tabindex="-1" aria-labelledby="modalDashboardMonitoring" aria-hidden="true">
    <div class="modal-dialog modal-md modal-dialog-centered">
        <div class="modal-content" style="border-radius: 10px;">
            <div class="modal-header justify-content-center">
                <h5 class="modal-title" id="exampleModalLabel"><i class="fas fa-tachometer-alt fa-fw"></i> Monitoring Overview Details</h5>
            </div>
            <div class="modal-body">
                <div class="row">
                <div class="col-3">
                        <?php $bulan = ['January', 'Febuary', 'March', 'April', 'May', 'Juny', 'July', 'August', 'September', 'October', 'November', 'December'] ?>
                        <div class="row">
                            <?php foreach($bulan as $b) : ?>
                            <div class="col-12 text-center"><p class="fw-bold" style="padding: 5px 8px; background-color: #55CE63; border-radius: 3px; color: #fff"><?= $b; ?></p></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-9">
                        <?php foreach($overviewMonitoring as $om) : ?>
                        <div class="row">
                            <div class="col-5 d-md-block d-none"><p style="padding: 5px 8px;"><?= date("F", mktime(0, 0, 0, $om['monitoring_month'], 10)); ?> <?= $om['year'] ?></p></div>
                            <div class="col-5 d-block d-md-none"><p style="padding: 5px 8px;"><?= date("M", mktime(0, 0, 0, $om['monitoring_month'], 10)); ?> <?= $om['year'] ?></p></div>
                            <div class="col-5 d-md-block d-none"><p style="padding: 5px 8px;">Avg <?= $om['monitoring_average'] ?> Mbps</p></div>
                            <div class="col-5 d-block d-md-none"><p style="padding: 5px 8px;">Avg <?= $om['monitoring_average'] ?></p></div>
                            <div class="col-2 p-0 m-0"><i class="fas fa-check fa-fw text-success" style="padding: 5px 8px;"></i></div>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- End Modals Dashboard Monitoring -->

<!-- modals import data -->
<div class="modal fade" id="modalImport" tabindex="-1" aria-labelledby="modalImport" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="POST" enctype="multipart/form-data" id="formImportData">
                <div class="modal-body">
                    <div class="form-group">
                        <label>File upload</label>
                        <input type="file" required name="importData" class="file-upload-default form-control" accept=".xlsx,.xls">
                    </div>
                </div>
                <div class="px-3 pb-3">
                    <button type="submit" class="btn btn-success text-white">Upload</button>
                    <a class="text-decoration-none" id="downloadTemplateImportData"><button type="button" class="btn btn-warning">Download Template</button></a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals export  -->
<div class="modal fade" id="modalExport" tabindex="-1" aria-labelledby="modalExport" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="POST" id="formExportData">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group exportData1">
                            <label for="" id='exportData1'></label>
                            <input type="" id='exportData1Input' name="exportData1" class="file-upload-default form-control">
                        </div>
                        <div class="form-group exportData2">
                            <label for="" id='exportData2'></label>
                            <input type="" id='exportData2Input' name="exportData2" class="file-upload-default form-control">
                        </div>
                        <div class="form-group exportData3">
                            <label for="" id='exportData3'></label>
                            <input type="" id='exportData3Input' name="exportData3" class="file-upload-default form-control">
                        </div>
                    </div>
                    <pre class="note_export" style="margin-bottom: -15px">
*jika kosong semua maka export all data
*jika sequence salah satu di isi maka sequence lain juga harus di isi
*tahun opsional
                    </pre>
                </div>
                <div class="px-4 pb-3">
                    <div class="row">
                        <button type="submit" class="btn btn-success text-white">Export</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals import data -->
<div class="modal fade" id="modalImport" tabindex="-1" aria-labelledby="modalImport" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel"></h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="POST" enctype="multipart/form-data" id="formImportData">
                <div class="modal-body">
                    <div class="form-group">
                        <label>File upload</label>
                        <input type="file" required name="importData" class="file-upload-default form-control" accept=".xlsx,.xls">
                    </div>
                </div>
                <div class="px-3 pb-3">
                    <button type="submit" class="btn btn-success text-white">Upload</button>
                    <a class="text-decoration-none" id="downloadTemplateImportData"><button type="button" class="btn btn-warning">Download Template</button></a>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals -->

<!-- modals export ticket  -->
<div class="modal fade" id="modalExportTicket" tabindex="-1" aria-labelledby="modalExportTicket" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Export Ticket</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form method="POST" action="<?= base_url('/Ticketing/exportData') ?>">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group exportData1">
                            <label for="fromDate" id='fromDate'></label>
                            <input type="date" id='fromDate' name="fromDate" class="file-upload-default form-control">
                        </div>
                        <div class="form-group exportData2">
                            <label for="toDate" id='toDate'></label>
                            <input type="date" id='toDate' name="toDate" class="file-upload-default form-control">
                        </div>
                    </div>
                </div>
                <div class="px-4 pb-3">
                    <div class="row">
                        <button type="submit" class="btn btn-success text-white">Export</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- end modals ticket -->