<script>
    
    // chart
    let ctx = document.getElementById('myChart');
    let data = [];
    let label = [];

    <?php foreach($test as $t) : ?>
        label.push(<?= $t->report_month ?>);
        data.push(<?= $t->report_total ?>);
    <?php endforeach; ?>

    $("#testToggle").click(function(){
        $(".contentSend").toggle('fast', 'swing');
    });

    // tabel dataTable ticket
    $(document).ready(function() {  
        $(".contentSend").hide();

        let table = $('#tabel_ticket').DataTable({
            "processing": true,
            "serverSide": true,
            'scrollX': true,
            "order": [],
            "ajax": {
                "url": "<?php echo base_url('/ticketing/ajaxList') ?>",
                "type": "POST",
                'data': function(data){
                data.fromDate = $('#fromDateTicket').val();
                data.toDate = $('#toDateTicket').val();
                }
            },
            "columnDefs": [{
                "targets": [],
                "orderable": false,
            }, {
                "targets" : [4],
                "width" : 500,
            }, {
                "targets" : [3,6],
                "width" : 100,
            }, {
                "targets" : 2,
                "width" : 90,
            }],
        });

        $('#toDateTicket').change(function(){
            table.draw();
        });
    });

    // let allData = {
    //     labels: label,
    //     datasets: [{
    //         label: 'Monthly User Login',
    //         data: data,
    //         backgroundColor: [
    //             'rgba(255, 99, 132, 0.2)',
    //             'rgba(54, 162, 235, 0.2)',
    //             'rgba(255, 206, 86, 0.2)',
    //             'rgba(75, 192, 192, 0.2)',
    //             'rgba(153, 102, 255, 0.2)',
    //             'rgba(255, 159, 64, 0.2)'
    //         ],
    //         borderColor: [
    //             'rgba(255, 99, 132, 1)',
    //             'rgba(54, 162, 235, 1)',
    //             'rgba(255, 206, 86, 1)',
    //             'rgba(75, 192, 192, 1)',
    //             'rgba(153, 102, 255, 1)',
    //             'rgba(255, 159, 64, 1)'
    //         ],
    //         borderWidth: 1
    //     }]
    // }
    // let myChart = new Chart(ctx, {
    //     type: 'line',
    //     data: allData,
    // });

    // select2 dasboard opsi
    $(document).ready(function() {
        $('.opsi1').select2();
        $('.opsi2').select2();
    });

    // $('#sendITMessage').click(function() {
    //     let msg = $('#ticketMessage').val();
    //     console.log(msg);
    //     alert(msg);
    //     $('#sendToWA').attr('href', 'https://api.whatsapp.com/send?phone=625875502569&text=test' + msg);
    // })

    function opsi() {
        if($('.opsi1').val() == 2) {
            $('.opsi2').removeAttr('disabled');
            $('.opsi2').attr('required', true)
            $('.opsi3').removeAttr('disabled');
            $('.opsi3').attr('required', true)
        } else {
            $('.opsi2').attr('disabled', 'disabled');
            $('.opsi2').removeAttr('required')
            $('.opsi3').attr('disabled', 'disabled');
            $('.opsi3').removeAttr('required')
        }
    }

    function triggerSelect2() {
        $('.js-example-basic-multiple').select2({
            width: '100%',
        });
    };

    $('#importDataResetPassword').click(function() {
        let title = 'Import Data Reset Password';
        let action = 'ResetPassword/importData';
        let template = 'ResetPassword/templateImportData';

        $('.modal-title').text(title);
        $('#formImportData').attr('action', action);
        $('#downloadTemplateImportData').attr('href', template);
    })

    $('#exportDataResetPassword').click(function() {
        let title = 'Export Data Reset Password';
        let action = 'ResetPassword/exportData';
        let data1 = 'From Sequence';
        let data1Type = 'number';
        let data1Col = 'col-4';
        let data2 = 'To Sequence';
        let data2Type = 'number';
        let data2Col = 'col-4';
        let data3 = 'From Year';
        let data3Type = 'number';
        let data3Col = 'col-4';

        $('#modalExport .modal-title').text(title);
        $('#exportData1').text(data1);
        $('#exportData1Input').attr('type', data1Type);
        $('.exportData1').addClass(data1Col);
        $('#exportData2').text(data2);
        $('#exportData2Input').attr('type', data2Type);
        $('.exportData2').addClass(data2Col);
        $('#exportData3').text(data3);
        $('#exportData3Input').attr('type', data3Type);
        $('.exportData3').addClass(data3Col);
        $('#formExportData').attr('action', action);
    })

    $('#importDataPreventive').click(function() {
        let title = 'Import Data Preventive';
        let action = 'Preventive/importData';
        let template = 'Preventive/templateImportData';

        $('.modal-title').text(title);
        $('#formImportData').attr('action', action);
        $('#downloadTemplateImportData').attr('href', template);
    })

    $('#exportDataPreventive').click(function() {
        let title = 'Export Data Preventive';
        let action = 'Preventive/exportData';
        let data1 = 'From Sequence';
        let data1Type = 'number';
        let data1Col = 'col-4';
        let data2 = 'To Sequence';
        let data2Type = 'number';
        let data2Col = 'col-4';
        let data3 = 'From Year';
        let data3Type = 'number';
        let data3Col = 'col-4';

        $('#modalExport .modal-title').text(title);
        $('#exportData1').text(data1);
        $('#exportData1Input').attr('type', data1Type);
        $('.exportData1').addClass(data1Col);
        $('#exportData2').text(data2);
        $('#exportData2Input').attr('type', data2Type);
        $('.exportData2').addClass(data2Col);
        $('#exportData3').text(data3);
        $('#exportData3Input').attr('type', data3Type);
        $('.exportData3').addClass(data3Col);
        $('#formExportData').attr('action', action);
    })

    $('#importDataBackup').click(function() {
        let title = 'Import Data Backup';
        let action = 'Backup/importData';
        let template = 'Backup/templateImportData';

        $('.modal-title').text(title);
        $('#formImportData').attr('action', action);
        $('#downloadTemplateImportData').attr('href', template);
    })

    $('#exportDataBackup').click(function() {
        let title = 'Export Data Backup';
        let action = 'Backup/exportData';
        let data1 = '';
        let data1Type = 'hidden';
        let data1Col = 'd-none';
        let data2 = 'To Sequence';
        let data2Type = 'hidden';
        let data2Col = 'd-none';
        let data3 = 'From Year';
        let data3Type = 'number';
        let data3Col = 'col-12';

        $('#modalExport .modal-title').text(title);
        $('#exportData1').text(data1);
        $('#exportData1Input').attr('type', data1Type);
        $('.exportData1').addClass(data1Col);
        $('#exportData2').text(data2);
        $('#exportData2Input').attr('type', data2Type);
        $('.exportData2').addClass(data2Col);
        $('#exportData3').text(data3);
        $('#exportData3Input').attr('type', data3Type);
        $('.exportData3').addClass(data3Col);
        $('.note_export').addClass('d-none');
        $('#formExportData').attr('action', action);
    })

    $('#importDataMonitoring').click(function() {
        let title = 'Import Data Monitoring';
        let action = 'MonitoringSpeed/importData';
        let template = 'MonitoringSpeed/templateImportData';

        $('.modal-title').text(title);
        $('#formImportData').attr('action', action);
        $('#downloadTemplateImportData').attr('href', template);
    })

    $('#exportDataMonitoring').click(function() {
        let title = 'Export Data Monitoring Speed';
        let action = 'MonitoringSpeed/exportData';
        let data1 = '';
        let data1Type = 'hidden';
        let data1Col = 'd-none';
        let data2 = 'To Sequence';
        let data2Type = 'hidden';
        let data2Col = 'd-none';
        let data3 = 'From Year';
        let data3Type = 'number';
        let data3Col = 'col-12';

        $('#modalExport .modal-title').text(title);
        $('#exportData1').text(data1);
        $('#exportData1Input').attr('type', data1Type);
        $('.exportData1').addClass(data1Col);
        $('#exportData2').text(data2);
        $('#exportData2Input').attr('type', data2Type);
        $('.exportData2').addClass(data2Col);
        $('#exportData3').text(data3);
        $('#exportData3Input').attr('type', data3Type);
        $('.exportData3').addClass(data3Col);
        $('.note_export').addClass('d-none');
        $('#formExportData').attr('action', action);
    })

    $('#importDataAsset').click(function() {
        let title = 'Import Data Asset';
        let action = 'Asset/importData';
        let template = 'Asset/templateImportData';

        $('.modal-title').text(title);
        $('#formImportData').attr('action', action);
        $('#downloadTemplateImportData').attr('href', template);
    })

    $('#importDataEmployee').click(function() {
        let title = 'Import Data Employee';
        let action = 'Employee/importData';
        let template = 'Employee/templateImportData';

        $('.modal-title').text(title);
        $('#formImportData').attr('action', action);
        $('#downloadTemplateImportData').attr('href', template);
    })

    $('#importDataEmail').click(function() {
        let title = 'Import Data Email';
        let action = 'Email/importData';
        let template = 'Email/templateImportData';

        $('.modal-title').text(title);
        $('#formImportData').attr('action', action);
        $('#downloadTemplateImportData').attr('href', template);
    })

    $('#importDataAccess').click(function() {
        let title = 'Import Data Access';
        let action = 'Access/importData';
        let template = 'Access/templateImportData';

        $('.modal-title').text(title);
        $('#formImportData').attr('action', action);
        $('#downloadTemplateImportData').attr('href', template);
    })

    $('#importDataInternet').click(function() {
        let title = 'Import Data Internet';
        let action = 'Internet/importData';
        let template = 'Internet/templateImportData';

        $('.modal-title').text(title);
        $('#formImportData').attr('action', action);
        $('#downloadTemplateImportData').attr('href', template);
    })

    $(document).ready(function() {

        var table = $('#example').DataTable({
            lengthChange: true,
            // buttons: ['copy', 'excel', 'pdf']
        });

        table.buttons().container()
        .appendTo( '#example_wrapper .col-md-6:eq(0)' );

        $('#example tfoot th').each(function() {
            var title = $(this).text();
            $(this).html('<input class="form-control" type="text" placeholder="Search ' + '" />');
        });

        table.columns().every(function() {
            var that = this;
            $('input', this.footer() ).on('keyup change', function() {
                if(that.search() !== this.value) {
                    that.search(this.value).draw();
                }
            });
    });


    // inspeksi tabel qc
    $(document).ready(function() {
        $('#inspeksi').DataTable({
            "dom": '<"top"i>rt<"bottom"flp><"clear">'
        } );
    });

        // Modal Delete Overview
        $('#example tbody').on('click', '.deleteOverview', function() {

        var overviewId = $(this).data('delete');
        var link = '/Dashboard/deleteData';
        $('.deleteId').val(overviewId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Asset
        $('#example tbody').on('click', '.deleteAsset', function() {

        var assetId = $(this).data('delete');
        var link = '/Asset/deleteData';
        $('.deleteId').val(assetId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Employee
        $('#example tbody').on('click', '.deleteEmp', function() {

        var empId = $(this).data('delete');
        var link = '/Employee/deleteData';
        $('.deleteId').val(empId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Access
        $('#example tbody').on('click','.deleteAccess', function() {

        var accessId = $(this).data('delete');
        var link = '/Access/deleteData';
        $('.deleteId').val(accessId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Email
        $('#example tbody').on('click', '.deleteEmail', function() {

        var emailId = $(this).data('delete');
        var link = '/Email/deleteData';
        $('.deleteId').val(emailId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Daily
        $('#example tbody').on('click', '.deleteDaily', function() {

        var dailyId = $(this).data('delete');
        var link = '/Daily/deleteData';
        $('.deleteId').val(dailyId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Project
        $('#example tbody').on('click', '.deleteProject', function() {

        var projectId = $(this).data('delete');
        var link = '/Project/deleteData';
        $('.deleteId').val(projectId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Preventive
        $('#example tbody').on('click', '.deletePreventive', function() {

        var preventiveId = $(this).data('delete');
        var link = '/Preventive/deleteData';
        $('.deleteId').val(preventiveId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Reset Password
        $('#example tbody').on('click', '.deleteResetPassword', function() {

        var resetPasswordId = $(this).data('delete');
        var link = '/ResetPassword/deleteData';
        $('.deleteId').val(resetPasswordId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Backup
        $('#example tbody').on('click', '.deleteBackup', function() {

        var backupId = $(this).data('delete');
        var link = '/backup/deleteData';
        $('.deleteId').val(backupId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Monitoring
        $('#example tbody').on('click', '.deleteMonitoring', function() {

        var monitoringId = $(this).data('delete');
        var link = '/monitoringSpeed/deleteData';
        $('.deleteId').val(monitoringId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete User Management
        $('#example tbody').on('click', '.deleteUserManagement', function() {

        var userId = $(this).data('delete');
        var link = '/userManagement/deleteData';
        $('.deleteId').val(userId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Menu Management
        $('#example tbody').on('click', '.deleteMenuManagement', function() {

        var menuId = $(this).data('delete');
        var link = '/menuManagement/deleteData';
        $('.deleteId').val(menuId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Asset Type Management
        $('#example tbody').on('click','.deleteAssetType', function() {

        var assetTypeId = $(this).data('delete');
        var link = '/m_assetType/deleteData';
        $('.deleteId').val(assetTypeId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete User Level Management
        $('#example tbody').on('click', '.deleteUserLevel', function() {

        var userLevelId = $(this).data('delete');
        var link = '/m_userLevel/deleteData';
        $('.deleteId').val(userLevelId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Dept Management
        $('#example tbody').on('click', '.deleteDept', function() {

        var deptId = $(this).data('delete');
        var link = '/m_dept/deleteData';
        $('.deleteId').val(deptId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Access Deskera Management
        $('#example tbody').on('click', '.deleteDeskera', function() {

        var deskeraId = $(this).data('delete');
        var link = '/m_otherManagement/deleteDataDeskera';
        $('.deleteId').val(deskeraId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Access Use Management
        $('#example tbody').on('click', '.deleteAccessUse', function() {

        var accessUseId = $(this).data('delete');
        var link = '/m_otherManagement/deleteDataAccessUse';
        $('.deleteId').val(accessUseId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Condition Management
        $('#example tbody').on('click', '.deleteCondition', function() {

        var conditionId = $(this).data('delete');
        var link = '/m_otherManagement/deleteDataCondition';
        $('.deleteId').val(conditionId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Email Use Management
        $('#example tbody').on('click', '.deleteEmailUse', function() {

        var emailUseId = $(this).data('delete');
        var link = '/m_otherManagement/deleteDataEmailUse';
        $('.deleteId').val(emailUseId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Operating System Management
        $('#example tbody').on('click', '.deleteOperatingSystem', function() {

        var operatingSystemId = $(this).data('delete');
        var link = '/m_otherManagement/deleteDataOperatingSystem';
        $('.deleteId').val(operatingSystemId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Gender Management
        $('#example tbody').on('click', '.deleteGender', function() {

        var genderId = $(this).data('delete');
        var link = '/m_otherManagement/deleteDataGender';
        $('.deleteId').val(genderId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Position Management
        $('#example tbody').on('click', '.deletePosition', function() {

        var positionId = $(this).data('delete');
        var link = '/m_otherManagement/deleteDataPosition';
        $('.deleteId').val(positionId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Status Management
        $('#example tbody').on('click', '.deleteStatus', function() {

        var statusId = $(this).data('delete');
        var link = '/m_otherManagement/deleteDataStatus';
        $('.deleteId').val(statusId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Project Status Management
        $('#example tbody').on('click', '.deleteProjectStatus', function() {

        var projectStatusId = $(this).data('delete');
        var link = '/m_otherManagement/deleteDataProjectStatus';
        $('.deleteId').val(projectStatusId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Post Message Management
        $('#example tbody').on('click', '.deletePostMessage', function() {

        var postMessageId = $(this).data('delete');
        var link = '/PostMessage/deletePostMessage';
        $('.deleteId').val(postMessageId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete User Access Management
        $('#example tbody').on('click', '.deleteUserAccess', function() {

        var userAccessId = $(this).data('delete');
        var link = '/m_userAccessManagement/deleteDataUserAccess';
        $('.deleteId').val(userAccessId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Support System
        $('#example tbody').on('click', '.deleteSupportSystem', function() {

        var supportSystemId = $(this).data('delete');
        var link = '/SupportSystem/deleteData';
        $('.deleteId').val(supportSystemId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Log Status
        $('#example tbody').on('click', '.deleteLog', function() {

        var LogId = $(this).data('delete');
        var link = '/Log/deleteData';
        $('.deleteId').val(LogId);
        $('#modalDelete form').attr('action', link);

        });

        // Modal Delete Internet
        $('#example tbody .deleteInet').click(function() {
        var inetId = $(this).data('delete');
        var link = '/Internet/deleteData';
        $('.deleteId').val(inetId);
        $('#modalDelete form').attr('action', link);

        });


        // Modal Info Specifications
        $('a#spec').on('click', function() {

        var cpu = $(this).data('cpu');
        var mb = $(this).data('mb');
        var ram = $(this).data('ram');
        var hdd = $(this).data('hdd');
        var vga = $(this).data('vga');
        var monitor = $(this).data('monitor');

        $('#modalSpec .modal-body .cpu').text(cpu);
        $('#modalSpec .modal-body .mb').text(mb);
        $('#modalSpec .modal-body .ram').text(ram);
        $('#modalSpec .modal-body .hdd').text(hdd);
        $('#modalSpec .modal-body .vga').text(vga);
        $('#modalSpec .modal-body .monitor').text(monitor);

        });

    } );


    AOS.init();

    function DisplayTime(){
        if (!document.all && !document.getElementById)
        return
        timeElement=document.getElementById? document.getElementById("mytime"): document.all.tick2
        var CurrentDate=new Date()
        var x = CurrentDate
        var hours=CurrentDate.getHours()
        var minutes=CurrentDate.getMinutes()
        var seconds=CurrentDate.getSeconds()
        var DayNight="PM"
        // if (hours<12) DayNight="AM";
        if (hours>12) hours=hours;
        if (hours==0) hours=12;
        if (minutes<=9) minutes="0"+minutes;
        if (seconds<=9) seconds="0"+seconds;
        var currentTime=hours+":"+minutes+":"+seconds;
        timeElement.value=x;
        setTimeout("DisplayTime()",1000)
    }
    
    window.onload=DisplayTime

// mencoba Minimalis
    // if($('.deleteAsset').on('click')) {
    //     $('.deleteAsset').on('click', function() {

    //         var assetId = $(this).data('delete');
    //         var link = '/Asset/deleteData';
    //         window.alert(link);
    //         // $('.deleteId').val(assetId);
    //         // $('#modalDelete form').attr('action', link);

    //     });
    // } else if ($('.deleteEmp').on('click')) {
    //     $('.deleteEmp').on('click', function() {

    //         var empId = $(this).data('delete');
    //         var link = '/Employee/deleteData';
    //         window.alert(link);
    //         // $('.deleteId').val(empId);
    //         // $('#modalDelete form').attr('action', link);

    //     });
    // } else {
    //     window.alert('Salah Semua! ')
    // }

    function modalEditPostMessage() {
        window.alert('test1')
        var myModal = document.getElementById('modalEditPostMessage')
        window.alert('test2')

        myModal.addEventListener('shown.bs.modal')
        window.alert('test3')
    }

    // new GMaps({
    //     div: '#map',
    //     lat: -6.963999303747894,
    //     lng: 110.33315892784559
    // });


              
</script>