<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Menu Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Menu</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/MenuManagement/insertData') ?>">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Add new menu ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Name</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="name" id="name" class="form-control <?= ($validation->hasError('name') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="Dimas Okva" value="<?= old('name') ?>" required>
                        <div class="invalid-feedback">
                            <?= $validation->getError("name"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Icon</h6>
                    </div>
                    <div class="col-md-6"> 
                        <input type="text" name="icon" id="icon" class="form-control <?= ($validation->hasError('icon') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="fas fa-menu fa-fw" value="<?= old('icon') ?>" required>
                        <div class="invalid-feedback">
                            <?= $validation->getError("icon"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Url</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="url" id="url" class="form-control <?= ($validation->hasError('url') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="/dashboard" value="<?= old('url') ?>" required>
                        <div class="invalid-feedback">
                            <?= $validation->getError("url"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Order</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="sort" id="sort" class="form-control <?= ($validation->hasError('sort') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="17" <?= old('sort') ?> required>
                        <div class="invalid-feedback">
                            <?= $validation->getError("sort"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 mt-4 mb-5">
                <div class="col-md-3" style="float: right;">
                    <button type="muted" name="insert" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Insert Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>

        <div class="col-sm-12">
            <div class="card" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Icon</th>
                                <th scope="col">Url</th>
                                <th scope="col">Sort</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($menu as $m) : ?>
                            <tr class="active">
                                <td class="align-middle" style="width: 10px;"><p class="ml-3"><?= $no++; ?></p></td>
                                <td class="align-middle"><?= $m['menu_name'] ?></td>
                                <td class="align-middle"><?= $m['menu_icon'] ?></td>
                                <td class="align-middle"><?= $m['menu_url'] ?></td>
                                <td class="align-middle"><?= $m['sort'] ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url("/MenuManagement/editData" . '/' . $m['menu_id']) ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteMenuManagement" id="deleteMenuManagement" data-delete="<?= $m['menu_id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Icon</th>
                                <th scope="col">Url</th>
                                <th scope="col">Sort</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>