<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Warehouse</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Warehouse</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <!-- <a class="btn btn-success <?= (session()->get('level') != 1 ? 'disabled' : '') ?> d-md-inline-block text-white fa fa-plus-square mb-1 mb-sm-0" data-bs-toggle="modal" data-bs-target="#modalInsertDaily"> Add</a> -->
                <a href="<?= base_url('/Warehouse/htmltopdf') ?>"
                    class="btn btn-danger d-md-inline-block text-white">Sign Out</a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <section id="filter">
        <div class="row">
            <div class="form-group col-md-2">
                <form action="<?= base_url('/Warehouse/print') ?>" method="post">
                    <label>Choose by range</label>
                    <input type="number" required id="range1_filter" class="form-control" name="range1_filter" placeholder="start">
                    <input type="number" required id="range2_filter" class="form-control my-2" name="range2_filter" placeholder="end">
                    <button type="submit" name="range" id="range" class="btn btn-success">Search</button>                
                </form>
            </div>
            <div class="form-group col-md-5">
                <form action="<?= base_url('/Warehouse/print') ?>" method="post">
                    <label>Choose with multiple values</label>
                    <input type="text" required id="multiple" class="form-control mb-2" name="multiple" placeholder="12,13,14,15 - No Space and Separate by Coma">
                    <button type="submit" name="multipleSubmit" id="multiple" class="btn btn-success">Search</button>                
                </form>
            </div>
        </div>
    </section>

    <!-- <section id="search">
        <div class="row d-flex justify-content-center p-0 m-0">
        <div class="col-md-12">
        
            <form action="<?= base_url('Search')?>" method="GET">
                <div class="form-group d-flex justify-content-center">
                    <input style="width: 100%; padding-left: 20px;" type="text" class="form-control" id="searchqc" placeholder="Looking for something ? " autocomplete="false">
                </div>                    
            </form>
        </div>
        </div>
    </section> -->

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card" data-aos-duration="500" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4">
                    <table id="inspeksi" class="table table-hover" style="width:100%" data-aos-duration="500" data-aos="fade-down">
                        <thead>
                            <tr data-aos="fade-down">
                                <th scope="col">No</th>
                                <th scope="col">Product ID</th>
                                <th scope="col">Product Name</th>
                                <th scope="col">Description</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($warehouse as $i) : ?>
                            <tr class="active" data-aos="fade-down">
                                <td class="align-middle"><?= $no++; ?></td>
                                <td class="align-middle"><?= $i['product_id'] ?></td>
                                <td class="align-middle"><?= $i['product_name'] ?></td>
                                <td class="align-middle"><?= $i['description'] ?></td>
                                <td class="align-middle">
                                    <a href="" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteDaily" data-delete="" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>