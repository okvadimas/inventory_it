<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Post Message</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Post</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/auth/signOut'); ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/PostMessage/postMessage'); ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Add new post ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Title</h6>
                    </div>
                    <div class="col-md-7">
                        <input type="text" required name="title" id="title" class="form-control <?= ($validation->hasError('title') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Holiday Announcements" value="<?= old('title') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("title"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Post By</h6>
                    </div>
                    <div class="col-md-7">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px;" required class="form-control selectNoArrow" name="by" id="by">
                                <option value="" disabled selected>Choose user here...</option>
                                <?php foreach($hrd as $h) : ?>
                                    <option value="<?= $h['user_id']; ?>" <?= (old('by') == $h['user_id'] ? 'selected' : '') ?>><?= $h['user_fullname'] ?> - <?= $h['dept'] ?> Dept</option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("by"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Created at</h6>
                    </div>
                    <div class="col-md-7">
                        <input type="text" disabled id="mytime" class="form-control" style="border-radius: 15px;" placeholder="10-10-2021" value="">
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Message</h6>
                    </div>
                    <div class="col-md-7">
                        <textarea name="message" required id="message" class="form-control <?= ($validation->hasError('message') ? 'is-invalid' : '') ?>" cols="30" rows="10"><?= old('message') ?></textarea>
                        <div class="invalid-feedback">
                            <?= $validation->getError("message"); ?> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="insertPostMessage" class="btn btn-success text-white <?= (session()->get('level') != 1 or 2 ? 'disabled' : '') ?>">Insert Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>

        <div class="col-sm-12">
            <div class="card" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Post By</th>
                                <th scope="col">Title</th>
                                <th scope="col">Message</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($allPost as $ap) : ?>
                            <tr class="active">
                                <td class="align-middle" style="width: 10px;"><?= $no++; ?></td>
                                <td class="align-middle" style="width: 150px;">
                                    <h6><?= $ap["user_fullname"] ?> - <?= $ap['dept'] ?></h6><small class="text-muted"><?= $ap["post_create"] ?></small>
                                </td>
                                <td class="align-middle" style="width: 100px;"><?= $ap["post_title"] ?></td>
                                <td class="align-middle"><?= $ap["post_message"] ?></td>
                                <td class="align-middle">

                                    <!-- awalnya buat gantiin ternary yg gk work tp sekarang udah ngga kepake cuma buat cadangan aja -->
                                    <?php if(session()->get('level') == '1') { $css = 'text-warning'; } elseif (session()->get('level') == '2') { $css = 'text-warning'; } else { $css = 'text-muted disabled'; } ?>

                                    <a href="/PostMessage/editPostMessage/<?= $ap['post_id'] ?>" class="fa fa-edit <?= (session()->get('level') == 1 ? 'text-warning' : (session()->get('level') == 2 ? 'text-danger' : 'text-muted disabled')) ?>" style="<?= (session()->get('level') == 1 ? '' : (session()->get('level') == 2 ? '' : 'pointer-events: none')) ?>"
                                    ></a>
                                    <a href="" class="fa fa-trash <?= (session()->get('level') != 1 or 2 ? 'text-muted disabled' : 'text-danger') ?> deletePostMessage" data-delete="<?= $ap['post_id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 or 2 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Post By</th>
                                <th scope="col">Title</th>
                                <th scope="col">Message</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>