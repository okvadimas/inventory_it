<?php $this->extend("layout/edit"); ?>
<?php $this->section('edit'); ?>


<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Los Status</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Log Status</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/Log/doEditData' . '/' . $data['id']) ?>">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Update existing log status ?</h4>
                </div>
                <div class="row">
                <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Asset</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px;" required class="form-control selectNoArrow <?= ($validation->hasError('log_asset') ? 'is-invalid' : '') ?>" name="log_asset" id="log_asset" autocomplete="off">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($type as $t) : ?>
                                    <option value="<?= $t['asset_id'] ?>" <?= ($data['log_asset'] == $t['asset_id'] ? 'selected' : '') ?>><?= $t['id'] ?> - <?= $t['asset_id'] ?> - <?= $t['type'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("log_asset"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Name</h6>
                    </div>
                    <div class="col-md-6"> 
                        <input type="text" name="log_name" id="log_name" class="form-control <?= ($validation->hasError('log_name') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="Edit name here..." value="<?= (old('log_name') ? old('log_name') : $data['log_name']) ?>" required>
                        <div class="invalid-feedback">
                            <?= $validation->getError("log_name"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Dept</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px;" required class="form-control selectNoArrow <?= ($validation->hasError('log_dept') ? 'is-invalid' : '') ?>" name="log_dept" id="log_dept" autocomplete="off">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($dept as $d) : ?>
                                    <option value="<?= $d['id'] ?>" <?= ($data['log_dept'] == $d['id'] ? 'selected' : '') ?>><?= $d['id'] ?> - <?= $d['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("log_dept"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Status</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px;" required class="form-control selectNoArrow <?= ($validation->hasError('log_status') ? 'is-invalid' : '') ?>" name="log_status" id="log_status" autocomplete="off">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($statusLog as $s) : ?>
                                    <option value="<?= $s['id'] ?>" <?= ($data['log_status'] == $s['id'] ? 'selected' : '') ?>><?= $s['id'] ?> - <?= $s['name1'] ?> - <?= $s['name2'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("log_status"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Stock</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="number" name="log_stock" id="log_stock" class="form-control <?= ($validation->hasError('log_stock') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="Insert ready stock here..." value="<?= (old('log_stock') ? old('log_stock') : $data['log_stock']) ?>" required>
                        <div class="invalid-feedback">
                            <?= $validation->getError("log_stock"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Desc</h6>
                    </div>
                    <div class="col-md-6">
                        <textarea style="border-radius: 15px;" name="log_desc" id="log_desc" rows="5" style="width: 100%;" class="form-control"><?= (old('log_desc') ? old('log_desc') : $data['log_desc']) ?></textarea>
                    </div>
                </div> 
            </div>
            <div class="row pl-2 mt-4 mb-5">
                <div class="col-md-3" style="float: right;">
                    <button type="muted" name="update" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Update Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection(); ?>