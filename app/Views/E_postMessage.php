<?php $this->extend("layout/edit"); ?>
<?php $this->section('edit'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Post Message</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Post</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/auth/signOut'); ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->

<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/PostMessage/doEditPostMessage') . '/' . $data['post_id']; ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Add new post ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Title</h6>
                    </div>
                    <div class="col-md-7">
                        <input type="text" required name="title" id="title" class="form-control <?= ($validation->hasError('title') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Holiday Announcements" value="<?= (old('title') ? old('title') : $data['post_title']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("title"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Post By</h6>
                    </div>
                    <div class="col-md-7">
                        <input type="text" disabled name="by" id="by" class="form-control <?= ($validation->hasError('by') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Muhammad Muktar - HRD Dept" value="<?= (old('by') ? old('by') : $data['name']) ?>"> 
                        <div class="invalid-feedback">
                            <?= $validation->getError("by"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Created at</h6>
                    </div>
                    <div class="col-md-7">
                        <input type="text" disabled id="mytime" class="form-control" style="border-radius: 15px;" placeholder="10-10-2021" value="">
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Message</h6>
                    </div>
                    <div class="col-md-7">
                        <textarea name="message" required id="message" style="border-radius: 15px;" class="form-control <?= ($validation->hasError('message') ? 'is-invalid' : '') ?>" cols="30" rows="10"><?= (old('message') ? old('message') : $data['post_message']) ?></textarea>
                        <div class="invalid-feedback">
                            <?= $validation->getError("message"); ?> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="updatePostMessage" class="btn btn-success text-white <?= (session()->get('level') != 1 or 2 ? 'disabled' : '') ?>">Update Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection(); ?>