<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Profile</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="#">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Profile</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <button class="btn btn-primary text-white" style="margin-left: 10px;" data-bs-toggle="modal" data-bs-target="#modalResetPass">Change
                Password</button>
                <a href="<?= base_url('/auth/signOut'); ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- ============================================================== -->

    <?php if (session()->getFlashData("error_message")) : ?>
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Start Page Content -->
    <!-- ============================================================== -->
    <!-- Row -->
    <div class="row">
        <!-- Column -->
        <div class="col-lg-4 col-xlg-3 col-md-5">
            <div class="card" data-aos-duration="300" data-aos="fade-down">
                <div class="card-body profile-card">
                    <center class="mt-4"> <img src="/images/users/<?= $user['user_picture'] ?>"
                            class="rounded-circle" width="150" />
                        <h5 class="card-title mt-3"><?= $user['user_username']; ?></h4>
                        <h6 class="card-subtitle"><?= $user['status_name1']; ?> - <?= $user['status_name2']; ?></h6>
                    </center>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="card" data-aos-duration="350" data-aos="fade-down">
                        <div class="card-body profile-card text-center">
                            <h5 class="mt-2"><?= $user['user_level']; ?></h5>
                            <h6 class="card-subtitle">Level</h6>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card" data-aos-duration="400" data-aos="fade-down">
                        <div class="card-body profile-card text-center">
                            <h5 class="mt-2">PTPF-<?= $deptName['slug'] ?>-<?= $user['user_asset_id']; ?></h5>
                            <h6 class="card-subtitle">Identity</h6>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Column -->
        <!-- Column -->
        <div class="col-lg-8 col-xlg-9 col-md-7">
            <div class="card">
                <div class="card-body">
                    <form class="form-horizontal form-material mx-2" method="post" action="<?= base_url('/Profile/updateProfile/' . $user['user_id']) ?>" enctype="multipart/form-data">
                        <div class="form-group" data-aos-duration="300" data-aos="fade-down">
                            <label class="col-md-12 mb-0">Full Name</label>
                            <div class="col-md-12">
                                <input type="text" placeholder="Johnathan Doe" name="name"
                                    class="form-control ps-0 <?= ($validation->hasError('name') ? 'is-invalid' : '') ?>" value="<?= (old('name') ? old("name") : $user['user_fullname']) ?>"> 
                                    <div class="invalid-feedback">
                                        <?= $validation->getError("name"); ?>
                                    </div>
                            </div>
                        </div>
                        <div class="form-group" data-aos-duration="300" data-aos="fade-down">
                            <label class="col-md-12 mb-0">Identity</label>
                            <div class="col-md-12">
                                <input type="text" placeholder="Johnathan Doe" name="identity"
                                    class="form-control ps-0 form-control-line <?= ($validation->hasError('identity') ? 'is-invalid' : '') ?>" value="<?= (old('identity') ? old('identity') : $user['user_asset_id']) ?>"> 
                                    <div class="invalid-feedback">
                                        <?= $validation->getError("identity"); ?>
                                    </div>
                            </div>
                        </div>
                        <div class="form-group" data-aos-duration="320" data-aos="fade-down">
                            <label for="example-email" class="col-md-12">Email</label>
                            <div class="col-md-12">
                                <input type="email" placeholder="johnathan@admin.com"
                                    class="form-control ps-0 form-control-line <?= ($validation->hasError('email') ? 'is-invalid' : '') ?>" name="email"
                                    id="example-email" value="<?= (old('user_email') ? old('user_email') : $user['user_email']) ?>">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError("email"); ?>
                                    </div>
                            </div>
                        </div>
                        <div class="form-group" data-aos-duration="360" data-aos="fade-down">
                            <label class="col-md-12 mb-0">Phone No</label>
                            <div class="col-md-12">
                                <input type="text" placeholder="123 456 7890" name="phone"
                                    class="form-control ps-0 form-control-line <?= ($validation->hasError('phone') ? 'is-invalid' : '') ?>" value="<?= (old('user_phone') ? old('user_phone') : $user['user_phone']) ?>">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError("phone"); ?>
                                    </div>
                            </div>
                        </div>
                        <div class="form-group" data-aos-duration="380" data-aos="fade-down">
                            <label class="col-sm-12">Department</label>
                            <div class="col-sm-12 border-bottom">
                                <select class="form-select shadow-none border-0 ps-0 form-control-line" name="dept">
                                <?php foreach($dept as $d) : ?>
                                    <option value="<?= $d['id']; ?>" <?= ($user["dept_name"] == $d['name'] ? 'selected' : ''); ?>><?= $d["name"]; ?></option>
                                <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" data-aos-duration="400" data-aos="fade-down">
                            <label class="col-md-12 mb-0">Birth Date</label>
                            <div class="col-md-12">
                                <input type="date" placeholder="123 456 7890" name="birth"
                                    class="form-control ps-0 form-control-line <?= ($validation->hasError('birth') ? 'is-invalid' : '') ?>" value="<?= (old('user_birth') ? old('user_birth') : $user['user_birth']) ?>">
                                    <div class="invalid-feedback">
                                        <?= $validation->getError("birth"); ?>
                                    </div>
                            </div>
                        </div>
                        <div class="form-group" data-aos-duration="400" data-aos="fade-down">
                            <label class="col-md-12 mb-0">Profile Picture</label>
                            <div class="col-md-12 mt-2">
                                <input type="file" name="picture" class="form-control <?= ($validation->hasError('picture') ? 'is-invalid' : '') ?>" id="picture" value="dawdawd">
                                <div class="invalid-feedback"> 
                                        <?= $validation->getError("picture"); ?>
                                    </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12 d-flex">
                                <button class="btn btn-success text-white">Update
                                    Profile</button>
                                <!-- <button class="btn btn-warning text-white" style="margin-left: 10px;" data-bs-toggle="modal" data-bs-target="#modalResetPass">Change
                                    Password</button> -->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Column -->
    </div>
    <!-- Row -->
    <!-- ============================================================== -->
    <!-- End PAge Content -->
    <!-- ============================================================== -->
    <!-- ============================================================== -->
    <!-- Right sidebar -->
    <!-- ============================================================== -->
    <!-- .right-sidebar -->
    <!-- ============================================================== -->
    <!-- End Right sidebar -->
    <!-- ============================================================== -->
</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>