<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Access Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Access</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <!-- <a class="btn btn-success <?= (session()->get('level') != 1 ? 'disabled' : '') ?> d-md-inline-block text-white fa fa-plus-square mb-1 mb-sm-0" data-bs-toggle="modal" data-bs-target="#modalInsertAccess"> Add</a> -->
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-md-inline-block text-white">Sign Out</a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card" data-aos-duration="500" data-aos="fade-down">
                <div class="card-body">
                    <div class="d-sm-flex align-items-center justify-content-between border-bottom pb-2">
                        <div>
                            <h4>Access List</h4>
                        </div>
                        <div>
                            <div class="btn-wrapper">
                                <?php if(session()->get('level') <= 2) : ?>
                                    <a class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>" data-bs-toggle="modal" data-bs-target="#modalInsertAccess" onclick="triggerSelect2()"><i class="icon-plus"></i> Add</a>
                                    <a href="#" class="btn btn-info text-white" id="importDataAccess" data-bs-toggle="modal" data-bs-target="#modalImport"><i class="icon-upload"></i> Import</a>
                                <?php endif; ?>
                                <a href="<?= site_url('Access/exportData') ?>" class="btn btn-warning text-white me-0"><i class="icon-download"></i> Export</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-4">
                    <table id="example" class="table table-hover tableEditAccess" style="width:100%" data-aos-duration="600" data-aos="fade-down">
                        <thead>
                            <tr data-aos="fade-down">
                                <th scope="col" class="text-center">Looks</th>                                
                                <th scope="col">User ID</th>
                                <th scope="col">Username</th>
                                <th scope="col" style="display: none;">Asset User ID</th>
                                <th scope="col">Anydesk</th>
                                <th scope="col">Domain</th>
                                <th scope="col">Deskera</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($access as $a) : ?>
                            <tr class="active" data-aos="fade-down">
                                <td class="align-middle text-center"><span class="round"><img src="images/users/<?= $a['user_picture'] ?>"
                                            alt="user" width="50"></span></td>
                                <td class="align-middle">
                                    <h6><?= $a['user_asset_id']; ?></h6>
                                </td>
                                <td class="align-middle" style="width: 150px;">
                                    <h6><?= $a['user_fullname'] ?></h6><small class="text-muted"><?= $a['dept'] ?> - Department</small>
                                </td>
                                <td class="align-middle" style="display: none;">PTPF-<?= $deptName['slug'] ?>-<?= $a['user_asset_id']; ?></td>
                                <?php
                                    // $access = explode(",",$a['access_use']);
                                    // $result = '';
                                    // if(count($access) > 1) {
                                    //     foreach($access as $au) {
                                    //         if($au == 1) {
                                    //             $result .= 'Computer,';
                                    //         } elseif($au == 2) {
                                    //             $result .= 'AIO,';
                                    //         } elseif($au == 3) {
                                    //             $result .= 'Laptop,';
                                    //         }
                                    //     }
                                    //     $result = substr($result, 0, -1);
                                    // } else {
                                    //     if($access[0] == 1) {
                                    //         $result .= 'Computer';
                                    //     } elseif($access[0] == 2) {
                                    //         $result .= 'AIO';
                                    //     } elseif($access[0] == 3) {
                                    //         $result .= 'Laptop';
                                    //     }
                                    // }
                                ?>
                                <td class="align-middle">
                                    <h6 class="text-info"><?= $a['access_anydesk']; ?></h6>
                                </td>
                                <td class="align-middle">
                                    <h6><?= $a['access_domain']; ?></h6><small class="text-muted"><?= (session()->get('level') == 1 ? $a['access_pass'] : 'pass' ) ?></small>
                                </td>
                                <td class="align-middle"><?= $a['deskera']; ?></td>
                                <td class="align-middle fw-bold <?= ($a['status'] == 'Active' ? 'text-success' : 'text-danger') ?>"><?= $a['status']; ?></td>
                                <td class="align-middle" style="width: 70px;">
                                    <a href="<?= base_url("/Access/editData" . '/' . $a['access_id']) ?>" data-access_use="<?= $a['access_use'] ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none;' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteAccess" data-delete="<?= $a['access_id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none;' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">My Looks</th>
                                <th scope="col">User Id</th>
                                <th scope="col">Username</th>
                                <th scope="col" style="display: none;">Asset User ID</th>
                                <th scope="col">Anydesk</th>
                                <th scope="col">Domain</th>
                                <th scope="col">Deskera</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>