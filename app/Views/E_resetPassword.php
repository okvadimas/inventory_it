<?php $this->extend("layout/edit"); ?>
<?php $this->section('edit'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Reset Password Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Reset Password</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/ResetPassword/doEditData' . '/' . $data['reset_id']) ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Edit existing data ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Date *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="date" required name="reset_date" id="reset_date" class="form-control <?= ($validation->hasError('reset_date') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Date here..." value="<?= (old('reset_date') ? old('reset_date') : $data['reset_date']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("reset_date"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">User *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="reset_user" id="reset_user" class="form-control <?= ($validation->hasError('reset_user') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Username here..." value="<?= (old('reset_user') ? old('reset_user') : $data['reset_user']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("reset_user"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Domain *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="reset_domain" id="reset_domain" class="form-control <?= ($validation->hasError('reset_domain') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Domain here..." value="<?= (old('reset_domain') ? old('reset_domain') : $data['reset_domain']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("reset_domain"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Sequence *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="number" required name="reset_seq" id="reset_seq" class="form-control <?= ($validation->hasError('reset_seq') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Domain here..." value="<?= (old('reset_seq') ? old('reset_seq') : $data['reset_seq']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("reset_seq"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Old Password *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="reset_old" id="reset_old" class="form-control <?= ($validation->hasError('reset_old') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Old Password here..." value="<?= (old('reset_old') ? old('reset_old') : $data['reset_old']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("reset_old"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">New Password *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="reset_new" id="reset_new" class="form-control <?= ($validation->hasError('reset_new') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="New Password here..." value="<?= (old('reset_new') ? old('reset_new') : $data['reset_new']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("reset_new"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Comment</h6>
                    </div>
                    <div class="col-md-6">
                        <textarea style="border-radius: 15px;" name="reset_comment" id="reset_comment" class="form-control <?= ($validation->hasError('reset_comment') ? 'is-invalid' : '') ?>" cols="30" rows="10"><?= (old('reset_comment') ? old('reset_comment') : $data['reset_comment']) ?></textarea>
                        <div class="invalid-feedback">
                            <?= $validation->getError("reset_comment"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="update" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Update Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection(); ?>