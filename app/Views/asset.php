<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Asset Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Asset</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <!-- <a class="btn btn-success <?= (session()->get('level') != 1 ? 'disabled' : '') ?> fas fa-plus-square d-md-inline-block text-white mb-1 mb-sm-0" data-bs-toggle="modal" data-bs-target="#modalInsertAssets"> Add</a> -->
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-md-inline-block text-white">Sign Out</a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card" data-aos-duration="500" data-aos="fade-down">
                <div class="card-body">
                    <div class="d-sm-flex align-items-center justify-content-between border-bottom pb-2">
                        <div>
                            <h4>Asset List</h4>
                        </div>
                        <div>
                            <div class="btn-wrapper">
                                <?php if(session()->get('level') <= 2) : ?>
                                <a href="#" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>" data-bs-toggle="modal" data-bs-target="#modalInsertAssets"><i class="icon-plus"></i> Add</a>
                                <a href="#" class="btn btn-info text-white" id="importDataAsset" data-bs-toggle="modal" data-bs-target="#modalImport"><i class="icon-upload"></i> Import</a>
                                <?php endif; ?>
                                <a href="<?= site_url('Asset/exportData') ?>" class="btn btn-warning text-white me-0"><i class="icon-download"></i> Export</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%" data-aos-duration="600" data-aos="fade-down">
                        <thead>
                            <tr data-aos="fade-down">
                                <th scope="col" class="text-center">Looks</th>
                                <th scope="col">Asset ID</th>
                                <th scope="col" style="display: none;">Asset User ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Spesifications</th>
                                <th scope="col" style="display: none;">Processor</th>
                                <th scope="col" style="display: none;">Motherboard</th>
                                <th scope="col" style="display: none;">Storage</th>
                                <th scope="col" style="display: none;">Memory</th>
                                <th scope="col" style="display: none;">Graphics</th>
                                <th scope="col" style="display: none;">Monitor</th>
                                <th scope="col">OS</th>
                                <th scope="col">Status</th>
                                <th scope="col">Information</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($asset as $a) : ?>
                            <tr class="active" data-aos="fade-down">
                                <td class="align-middle text-center"><span class="round"><img src="images/users/<?= $a['user_picture'] ?>"
                                            alt="user" width="50"></span></td>
                                <td class="align-middle"><?= $a['asset_type_id']; ?></td>
                                <td class="align-middle" style="display: none;">PTPF-<?= $deptName['slug'] ?>-<?= $a['user_asset_id']; ?></td>
                                <td class="align-middle">
                                    <h6><?= $a['user_fullname']; ?></h6><small class="text-muted"><?= $a['dept']; ?> Department</small>
                                </td>
                                <td class="align-middle"><strong><p class="text-success"><a id="spec"
                                data-bs-toggle="modal" data-bs-target="#modalSpec" data-cpu="<?= $a['asset_cpu'] ?>" data-mb="<?= $a['asset_mb'] ?>" data-hdd="<?= $a['asset_hdd'] ?>" data-ram="<?= $a['asset_ram'] ?>" data-vga="<?= $a['asset_vga'] ?>" data-monitor="<?= $a['asset_monitor'] ?>"
                                style="cursor: pointer;;" class="text-success text-decoration-none">Click Me</a></p></strong></td>
                                <td class="align-middle" style="display: none;">Processor <?= $a['asset_cpu'] ?></td>
                                <td class="align-middle" style="display: none;">Motherboard <?= $a['asset_mb'] ?></td>
                                <td class="align-middle" style="display: none;">Storage <?= $a['asset_hdd'] ?></td>
                                <td class="align-middle" style="display: none;">Memory <?= $a['asset_ram'] ?></td>
                                <td class="align-middle" style="display: none;">Graphics <?= $a['asset_vga'] ?></td>
                                <td class="align-middle" style="display: none;">Monitor <?= $a['asset_monitor'] ?></td>
                                <td class="align-middle">
                                    <h6><?= $a['os']; ?></h6><small class="text-muted"><?= (session()->get('level') == 1 ? $a['asset_serial'] : 'serial number' ) ?></small>
                                </td>                                
                                <td class="align-middle"><?= ($a['status'] == 'Active' ? '<i class="far fa-check-circle fa-2x text-success"></i>' : '<i class="far fa-times-circle fa-2x text-danger"></i>') ?></td>
                                <td class="align-middle"><?= $a['asset_info']; ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url("/Asset/editData" . '/' . $a['asset_id']) ?>" class="fa fa-edit text-warning"></a>
                                    <a href="" data-delete="<?= $a['asset_id']; ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" class="fa fa-trash  <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteAsset" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">My Looks</th>
                                <th scope="col">Asset ID</th>
                                <th scope="col" style="display: none;">Asset User ID</th>
                                <th scope="col">Name</th>
                                <th scope="col">Spesifications</th>
                                <th scope="col" style="display: none;">Processor</th>
                                <th scope="col" style="display: none;">Motherboard</th>
                                <th scope="col" style="display: none;">Storage</th>
                                <th scope="col" style="display: none;">Memory</th>
                                <th scope="col" style="display: none;">Graphics</th>
                                <th scope="col" style="display: none;">Monitor</th>
                                <th scope="col">OS</th>
                                <th scope="col">Status</th>
                                <th scope="col">Information</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>