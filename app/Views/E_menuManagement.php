<?php $this->extend("layout/edit"); ?>
<?php $this->section('edit'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Menu Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Menu</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/MenuManagement/doEditData' . '/' . $data['menu_id']) ?>">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Update existing menu ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Name</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="menu_name" id="menu_name" class="form-control <?= ($validation->hasError('menu_name') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="Dimas Okva" value="<?= (old("menu_name") ? old("menu_name") : $data["menu_name"]) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("menu_name"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Icon</h6>
                    </div>
                    <div class="col-md-6"> 
                        <input type="text" required name="menu_icon" id="menu_icon" class="form-control <?= ($validation->hasError('menu_icon') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="fas fa-menu fa-fw" value="<?= (old("menu_icon") ? old("menu_icon") : $data['menu_icon']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("menu_icon"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Url</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="menu_url" id="menu_url" class="form-control <?= ($validation->hasError('menu_url') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="/dashboard" value="<?= (old("menu_url") ? old("menu_url") : $data['menu_url']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("menu_url"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Order</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="sort" id="sort" class="form-control <?= ($validation->hasError('sort') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="17" value="<?= (old("sort") ? old("sort") : $data['sort']) ?>" required>
                        <div class="invalid-feedback">
                            <?= $validation->getError("sort"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 mt-4 mb-5">
                <div class="col-md-3" style="float: right;">
                    <button type="muted" name="insert" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Insert Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection(); ?>