<!DOCTYPE html>
<html lang="en">

<head>
    <title><?= $title; ?></title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--===============================================================================================-->
    <link rel="icon" type="image/png" href="/login/images/icons/favicon.ico" />
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login/vendor/bootstrap/css/bootstrap.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login/vendor/animate/animate.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login/vendor/css-hamburgers/hamburgers.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login/vendor/animsition/css/animsition.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login/vendor/select2/select2.min.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login/vendor/daterangepicker/daterangepicker.css">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="/login/css/util.css">
    <link rel="stylesheet" type="text/css" href="/login/css/main.css">
    <!--===============================================================================================-->
</head>

<body>

    <div class="limiter">
        <div class="container-login100">
            <div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
                <!-- <form class="login100-form validate-form flex-sb flex-w" method="post" action="<?= base_url('auth/doSignUp'); ?>"> -->
                <form class="login100-form validate-form flex-sb flex-w" method="post" action="<?= base_url('/auth'); ?>">
                    <span class="login100-form-title p-b-32">
                        Account Sign Up
                    </span>

                    <span class="txt1 p-b-11">
                        Username
                    </span>
                    <div class="wrap-input100 validate-input m-b-36 " data-validate="Username is required" required>
                        <input class="input100" type="text" name="userReg" placeholder="dimas">
                        <span class="focus-input100"></span>
                    </div>
                    
                    <span class="txt1 p-b-11">
                        Identity
                    </span>
                    <div class="wrap-input100 validate-input m-b-36 " required>
                        <input class="input100" type="text" name="identity" placeholder="2175">
                        <span class="focus-input100"></span>
                    </div>

                    <span class="txt1 p-b-11">
                        Email Address
                    </span>
                    <div class="wrap-input100 validate-input m-b-36 " data-validate="Email is required" required>
                        <input class="input100" type="email" name="emailReg" placeholder="dosolichin@ptpacificfurniture.id">
                        <span class="focus-input100"></span>
                    </div>

                    <span class="txt1 p-b-11">
                        Department
                    </span>
                    <div class="wrap-input100 validate-input m-b-36 " >
                        <div class="selectWrapper">
                            <select class="form-control selectNoArrow input100" name="dept" id="dept" required>
                                <option disabled selected>Choose...</option>
                                <?php foreach($dept as $d) : ?>
                                    <option value="<?= $d['id'] ?>"><?= $d['id'] ?> - <?= $d['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div> 
                        <span class="focus-input100"></span>
                    </div>

                    <span class="txt1 p-b-11">
                        Level
                    </span>
                    <div class="wrap-input100 validate-input m-b-36 " >
                        <div class="selectWrapper">
                            <select class="form-control selectNoArrow input100" name="level" id="level" required>
                                <option disabled selected>Choose...</option>
                                <?php foreach($level as $l) : ?>
                                    <option value="<?= $l['id'] ?>"><?= $l['id'] ?> - <?= $l['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div> 
                        <span class="focus-input100"></span>
                    </div>

                    <span class="txt1 p-b-11">
                        Password
                    </span>
                    <div class="wrap-input100 validate-input m-b-12" data-validate="Password is required" required>
                        <span class="btn-show-pass">
                            <i class="fa fa-eye"></i>
                        </span>
                        <input class="input100" type="password" name="pass1Reg" placeholder="password">
                        <span class="focus-input100"></span>
                    </div>
                    
                    <span class="txt1 p-b-11">
                        Confirm Password
                    </span>
                    <div class="wrap-input100 validate-input m-b-12" data-validate="Password is required" required>
                        <span class="btn-show-pass">
                            <i class="fa fa-eye"></i>
                        </span>
                        <input class="input100" type="password" name="pass2Reg" placeholder="confirm password">
                        <span class="focus-input100"></span>
                    </div>

                    <!-- <div class="flex-sb-m w-full p-b-48">
                        <div class="contact100-form-checkbox">
                            <input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
                            <label class="label-checkbox100" for="ckb1">
                                Remember me
                            </label>
                        </div>

                        <div>
                            <a href="#" class="txt3">
                                Forgot Password?
                            </a>
                        </div>
                    </div> -->

                    <?php if (session()->getFlashData("error_message")) : ?>
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <?= session()->getFlashData("error_message") ?>
                        </div>
                    <?php endif; ?>

                    <div class="container-login100-form-btn pt-2">
                        <button class="login100-form-btn" disabled>
                            Sign Up
                        </button>
                    </div>

                    <div class="container-login100-form-btn pt-2">
                    <p class="pr-1">Already have an account ?</p><a href="<?= base_url('/auth') ?>">Sign In</a>
                    </div>

                </form>
            </div>
        </div>
    </div>


    <div id="dropDownSelect1"></div>

    <!--===============================================================================================-->
    <script src="/login/vendor/jquery/jquery-3.2.1.min.js"></script>
    <!--===============================================================================================-->
    <script src="/login/vendor/animsition/js/animsition.min.js"></script>
    <!--===============================================================================================-->
    <script src="/login/vendor/bootstrap/js/popper.js"></script>
    <script src="/login/vendor/bootstrap/js/bootstrap.min.js"></script>
    <!--===============================================================================================-->
    <script src="/login/vendor/select2/select2.min.js"></script>
    <!--===============================================================================================-->
    <script src="/login/vendor/daterangepicker/moment.min.js"></script>
    <script src="/login/vendor/daterangepicker/daterangepicker.js"></script>
    <!--===============================================================================================-->
    <script src="/login/vendor/countdowntime/countdowntime.js"></script>
    <!--===============================================================================================-->
    <script src="/login/js/main.js"></script>

</body>

</html>