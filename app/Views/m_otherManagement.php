<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Other Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Other</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/m_othermanagement/insertDataDeskera') ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Add new Deskera ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Identity</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="number" name="identityDeskera" required id="identityDeskera" class="form-control <?= ($validation->hasError('identityDeskera') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="1"  value="<?= old('identityDeskera') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("identityDeskera"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Name</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="nameDeskera" required id="nameDeskera" class="form-control <?= ($validation->hasError('nameDeskera') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="Yes" value="<?= old('nameDeskera') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("nameDeskera"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="insertDeskera" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Insert Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>

        <div class="col-sm-12">
            <div class="card" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($deskera as $d) : ?>
                            <tr class="active">
                                <td class="align-middle" style="width: 10px;"><?= $no++; ?></td>
                                <td class="align-middle"><?= $d['name'] ?></td>
                                <td class="align-middle"><?= $d['id'] ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url("/M_OtherManagement/editDataDeskera" . '/' . $d['id']) ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteDeskera" data-delete="<?= $d['id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

    <hr class="style-two">
    <hr class="style-eight">
    <hr class="style-two">

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row mt-5">
        <form action="<?= base_url('/m_OtherManagement/insertDataAccessUse') ?>">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Add new access ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Identity</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="identityAccessUse" required id="identityAccessUse" class="form-control <?= ($validation->hasError('identityAccessUse') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="1" value="<?= old('identityAccessUse') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("identityAccessUse"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Name</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="nameAccessUse" required id="nameAccessUse" class="form-control <?= ($validation->hasError('nameAccessUse') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="Computer" value="<?= old('nameAccessUse') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("nameAccessUse"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="insertAccesUse" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Insert Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>

        <div class="col-sm-12">
            <div class="card" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($use as $u) : ?>
                            <tr class="active">
                                <td class="align-middle" style="width: 10px;"><?= $no++; ?></td>
                                <td class="align-middle"><?= $u['name'] ?></td>
                                <td class="align-middle"><?= $u['id'] ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url("/M_OtherManagement/editAccessUse" . '/' . $u['id']) ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteAccessUse" data-delete="<?= $u['id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

    <hr class="style-two">
    <hr class="style-eight">
    <hr class="style-two">

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row mt-5">
        <form action="<?= base_url('/m_otherManagement/insertDataCondition') ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Add new condition ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Identity</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="identityCondition" required id="identityCondition" class="form-control <?= ($validation->hasError('identityCondition') ? 'is-invalid' : ''); ?>" style="border-radius: 15px;" placeholder="1" value="<?= old('identityCondition') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("identityCondition"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Name</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="nameCondition" required id="nameCondition" class="form-control <?= ($validation->hasError('nameCondition') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Married" value="<?= old('nameCondition') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("nameCondition"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="insertCondition" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Insert Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>

        <div class="col-sm-12">
            <div class="card" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($condition as $c) : ?>
                            <tr class="active">
                                <td class="align-middle" style="width: 10px;"><?= $no++; ?></td>
                                <td class="align-middle"><?= $c['name']; ?></td>
                                <td class="align-middle"><?= $c['id']; ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url("/M_OtherManagement/editCondition" . '/' . $c['id']) ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteCondition" data-delete="<?= $c['id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

    <hr class="style-two">
    <hr class="style-eight">
    <hr class="style-two">

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row mt-5">
        <form action="<?= base_url('/m_otherManagement/insertDataEmailUse') ?>">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Add new email use ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Identity</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="identityEmailUse" required id="identityEmailUse" class="form-control <?= ($validation->hasError('identityEmailUse') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="1" value="<?= old('identityEmailUse') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("identityEmailUse"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Name</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="nameEmailUse" required id="nameEmailUse" class="form-control <?= ($validation->hasError('nameEmailUse') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Outlook" value="<?= old('nameEmailUse') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("nameEmailUse"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="insertEmailUse" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Insert Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>

        <div class="col-sm-12">
            <div class="card" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($emailUse as $eu) : ?>
                            <tr class="active">
                                <td class="align-middle" style="width: 10px;"><?= $no++; ?></td>
                                <td class="align-middle"><?= $eu['name'] ?></td>
                                <td class="align-middle"><?= $eu['id'] ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url("/M_OtherManagement/editEmailUse" . '/' . $eu['id']) ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteEmailUse" data-delete="<?= $eu['id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

    <hr class="style-two">
    <hr class="style-eight">
    <hr class="style-two">

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row mt-5">
        <form action="<?= base_url('/m_otherManagement/insertDataOperatingSystem') ?>">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Add new operating system ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Identity</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="identityOperatingSystem" required id="identityOperatingSystem" class="form-control <?= ($validation->hasError('identityOperatingSystem') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="1" value="<?= old('identityOperatingSystem') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("identityOperatingSystem"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Name</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="nameOperatingSystem" required id="nameOperatingSystem" class="form-control <?= ($validation->hasError('nameOperatingSystem') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Windows 7 32" value="<?= old('nameOperatingSystem') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("nameOperatingSystem"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="insertEmailUse" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Insert Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>

        <div class="col-sm-12">
            <div class="card" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($operatingSystem as $os) : ?>
                            <tr class="active">
                                <td class="align-middle" style="width: 10px;"><?= $no++; ?></td>
                                <td class="align-middle"><?= $os['name'] ?></td>
                                <td class="align-middle"><?= $os['id'] ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url("/M_OtherManagement/editOperatingSystem" . '/' . $os['id']) ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteOperatingSystem" data-delete="<?= $os['id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

    <hr class="style-two">
    <hr class="style-eight">
    <hr class="style-two">

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row mt-5">
        <form action="<?= base_url('/m_otherManagement/insertDataGender') ?>">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Add new gender ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Identity</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="identityGender" id="identityGender" class="form-control <?= ($validation->hasError('identityGender') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="1" value="<?= old('identityGender') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("identityGender"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Name</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="nameGender" id="nameGender" class="form-control <?= ($validation->hasError('nameGender') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Female" value="<?= old('nameGender') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("nameGender"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="insertGender" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Insert Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>

        <div class="col-sm-12">
            <div class="card" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no= 1; ?>
                        <?php foreach($gender as $g) : ?>
                            <tr class="active">
                                <td class="align-middle" style="width: 10px;"><?= $no++; ?></td>
                                <td class="align-middle"><?= $g['name'] ?></td>
                                <td class="align-middle"><?= $g['id'] ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url("/M_OtherManagement/editDataGender" . '/' . $g['id']) ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteGender" data-delete="<?= $g['id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Name</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

    <hr class="style-two">
    <hr class="style-eight">
    <hr class="style-two">

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row mt-5">
        <form action="<?= base_url('/m_otherManagement/insertDataPosition') ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Add new position ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Identity</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="identityPosition" id="identityPosition" class="form-control <?= ($validation->hasError('identityPosition') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="3" value="<?= old('identityPosition') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("identityPosition"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Position</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="namePosition" id="namePosition" class="form-control <?= ($validation->hasError('namePosition') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Staff" value="<?= old('namePosition') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("namePosition"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="insertPosition" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Insert Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>

        <div class="col-sm-12">
            <div class="card" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Position</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($position as $p) : ?>
                            <tr class="active">
                                <td class="align-middle" style="width: 10px;"><?= $no++; ?></td>
                                <td class="align-middle"><?= $p['name'] ?></td>
                                <td class="align-middle"><?= $p['id'] ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url("/M_OtherManagement/editDataPosition" . '/' . $p['id']) ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" <?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deletePosition" data-delete="<?= $p['id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Position</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

    <hr class="style-two">
    <hr class="style-eight">
    <hr class="style-two">

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row mt-5">
        <form action="<?= base_url('/m_otherManagement/insertDataStatus') ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Add new status ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Identity</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="identityStatus" required id="identityStatus" class="form-control <?= ($validation->hasError('identityStatus') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="1" value="<?= old('identityStatus') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("identityStatus"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Status 1</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="name1Status" required id="name1Status" class="form-control <?= ($validation->hasError('name1Status') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Active" value="<?= old('name1Status') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("name1Status"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Status 2</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="name2Status" required id="name2Status" class="form-control <?= ($validation->hasError('name2Status') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Contract" value="<?= old('name2Status') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("name2Status"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="insertStatus" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Insert Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>

        <div class="col-sm-12">
            <div class="card" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Status 1</th>
                                <th scope="col">Status 2</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($status as $s) : ?>
                            <tr class="active">
                                <td class="align-middle" style="width: 10px;"><?= $no++; ?></td>
                                <td class="align-middle"><?= $s['name1'] ?></td>
                                <td class="align-middle"><?= $s['name2'] ?></td>
                                <td class="align-middle"><?= $s['id'] ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url("/M_OtherManagement/editStatus" . '/' . $s["id"]) ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteStatus" data-delete="<?= $s['id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Status 1</th>
                                <th scope="col">Status 2</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

    <hr class="style-two">
    <hr class="style-eight">
    <hr class="style-two">

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row mt-5">
        <form action="<?= base_url('/m_otherManagement/insertDataProjectStatus') ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Add new project status ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Identity</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="identityProjectStatus" required id="identityProjectStatus" class="form-control <?= ($validation->hasError('identityProjectStatus') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="1" value="<?= old('identityProjectStatus') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("identityProjectStatus"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-1 d-flex align-items-center">
                        <h6 class="font-weight-bold">Name</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="nameProjectStatus" required id="nameProjectStatus" class="form-control <?= ($validation->hasError('nameProjectStatus') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Open" value="<?= old('nameProjectStatus') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("nameProjectStatus"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="update" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Insert Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>

        <div class="col-sm-12">
            <div class="card" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Status</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($projectStatus as $ps) : ?>
                            <tr class="active">
                                <td class="align-middle" style="width: 10px;"><?= $no++; ?></td>
                                <td class="align-middle"><?= $ps['name'] ?></td>
                                <td class="align-middle"><?= $ps['id'] ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url("/M_OtherManagement/editProjectStatus" . '/' . $ps['id']) ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteProjectStatus" data-delete="<?= $ps['id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" <?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Status</th>
                                <th scope="col">Identity</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->


</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>