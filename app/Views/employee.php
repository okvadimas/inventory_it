<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Employee Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Employee</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <!-- <a class="btn btn-success <?= (session()->get('level') != 1 ? 'disabled' : '') ?> d-md-inline-block text-white fa fa-plus-square mb-1 mb-sm-0" data-bs-toggle="modal" data-bs-target="#modalInsertEmployee"> Add</a> -->
                <a href="<?= base_url('/auth/signOut'); ?>"
                    class="btn btn-danger d-md-inline-block text-white">Sign Out</a>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <div class="col-sm-12">
            <div class="card" data-aos-duration="500" data-aos="fade-down">
                <div class="card-body">
                    <div class="d-sm-flex align-items-center justify-content-between border-bottom pb-2">
                        <div>
                            <h4>Employee List</h4>
                        </div>
                        <div>
                            <div class="btn-wrapper">
                                <?php if(session()->get('level') <= 2) : ?>
                                <a href="#" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>" data-bs-toggle="modal" data-bs-target="#modalInsertEmployee"><i class="icon-plus"></i> Add</a>
                                <a href="#" class="btn btn-info text-white" id="importDataEmployee" data-bs-toggle="modal" data-bs-target="#modalImport"><i class="icon-upload"></i> Import</a>
                                <?php endif; ?>
                                <a href="<?= site_url('Employee/exportData') ?>" class="btn btn-warning text-white me-0"><i class="icon-download"></i> Export</a>
                            </div>
                        </div>
                    </div>
                    <div class="table-responsive mt-4">
                    <table id="example" class="table table-hover" style="width:100%" data-aos-duration="600" data-aos="fade-down">
                        <thead>
                            <tr data-aos="fade-down">
                                <th scope="col" class="text-center">Looks</th>
                                <th scope="col">Name</th>
                                <th scope="col" style="display: none;">Asset User ID</th>
                                <th scope="col">Birth</th>
                                <th scope="col">Phone</th>
                                <th scope="col">Gender</th>
                                <th scope="col">Address</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($emp as $e) : ?>
                            <tr class="active" data-aos="fade-down">
                                <td class="align-middle text-center"><span class="round"><img src="images/users/<?= $e['user_picture'] ?>"
                                            alt="user" width="50"></span></td>
                                <td class="align-middle" style="width: 150px;">
                                    <h6><?= $e['user_fullname']; ?></h6><small class="text-muted"><?= $e['position']; ?> - <?= $e['dept']; ?></small>
                                </td>
                                <td class="align-middle" style="display: none;">PTPF-<?= $deptName['slug'] ?>-<?= $e['user_asset_id']; ?></td>
                                <td class="align-middle" style="width: 100px;"><?= $e['user_birth']; ?></td>
                                <td class="align-middle"><?= $e['user_phone']; ?></td>
                                <td class="align-middle" style="width: 150px;"><?= $e['gender']; ?> - <?= $e['kondisi']; ?></td>
                                <td class="align-middle" style="width: 100px;"><?= $e['emp_address']; ?></td>
                                <td class="align-middle fw-bold <?= ($e['status1'] == 'Active' ? 'text-success' : 'text-danger') ?> font-weight-bold pl-4"><?= $e['status1']; ?><br><?= $e['status2']; ?></td>
                                <td class="align-middle" style="width: 70px;">
                                    <a href="<?= base_url("/Employee/editData" . '/' . $e['emp_id']) ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteEmp" data-delete="<?= $e['emp_id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none;' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">My Looks</th>
                                <th scope="col">Name</th>
                                <th scope="col" style="display: none;">Asset User ID</th>
                                <th scope="col">Birth Date</th>
                                <th scope="col">Nomer HP</th>
                                <th scope="col">Gender</th>
                                <th scope="col">Address</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>