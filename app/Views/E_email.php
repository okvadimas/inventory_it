<?php $this->extend("layout/edit"); ?>
<?php $this->section('edit'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Email Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Email</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/Email/doEditData' . '/' . $data['email_id']) ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Edit existing email ?</h4>
                </div>
                <div class="row my-2">
                    <div class="col-md-8">
                        <input type="text" disabled class="form-control text-center" style="border-radius: 15px;" value="Update data for : <?= $data['user_asset_id'] ?> - <?= $data['user_fullname'] ?> - <?= $data['dept'] ?> Department">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Email User *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="email_user" id="email_user" class="form-control <?= ($validation->hasError('email_user') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Seagate HDD 1TB 5000 Rpm" value="<?= (old('email_user') ? old('email_user') : $data['email_user']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("email_user"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Email Password *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="email_pass" id="email_pass" class="form-control <?= ($validation->hasError('email_pass') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Intel i5 - 5200u 3.9Ghz" value="<?= (old('email_pass') ? old('email_pass') : $data['email_pass']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("email_pass"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Using *</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('email_use') ? 'is-invalid' : '') ?>" name="email_use" id="email_use" placeholder="Status Here...">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($using as $u) : ?>
                                    <option value="<?= $u['id']; ?>" <?= ($data['email_use'] == $u['id'] ? 'selected' : '') ?>><?= $u['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("email_use"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="update" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Update Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection(); ?>