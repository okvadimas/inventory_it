<?php $this->extend("layout/edit"); ?>
<?php $this->section('edit'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Preventive Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Preventive</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/UserManagement/doEditData' . '/' . $data['user_id']) ?>" method="post" enctype="multipart/form-data">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Edit existing access ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Username *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="user_username" id="user_username" class="form-control <?= ($validation->hasError('user_username') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Update username" value="<?= (old('user_username') ? old('user_username') : $data['user_username']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("user_username"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Password *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="user_pass" id="user_pass" class="form-control <?= ($validation->hasError('user_pass') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Update password" value="<?= (old('user_pass') ? old('user_pass') : $data['user_pass']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("user_pass"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Fullname</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="user_fullname" id="user_fullname" class="form-control <?= ($validation->hasError('user_fullname') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Update fullname" value="<?= (old('user_fullname') ? old('user_fullname') : $data['user_fullname']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("user_fullname"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Email</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="user_email" id="user_email" class="form-control <?= ($validation->hasError('user_email') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Update email" value="<?= (old('user_email') ? old('user_email') : $data['user_email']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("user_email"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Birth Date</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="date" name="user_birth" id="user_birth" class="form-control <?= ($validation->hasError('user_birth') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Update birth date" value="<?= (old('user_birth') ? old('user_birth') : $data['user_birth']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("user_birth"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Phone</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" name="user_phone" id="user_phone" class="form-control <?= ($validation->hasError('user_phone') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Update phone number" value="<?= (old('user_phone') ? old('user_phone') : $data['user_phone']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("user_phone"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Picture</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="file" name="user_picture" id="user_picture" class="form-control <?= ($validation->hasError('user_picture') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Update picture" value="<?= (old('user_picture') ? old('user_picture') : $data['user_picture']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("user_picture"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Identity *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="user_asset_id" id="user_asset_id" class="form-control <?= ($validation->hasError('user_asset_id') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Update asset id" value="<?= (old('user_asset_id') ? old('user_asset_id') : $data['user_asset_id']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("user_asset_id"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Department</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" class="form-control selectNoArrow <?= ($validation->hasError('user_dept') ? 'is-invalid' : '') ?>" name="user_dept" id="user_dept">
                                <option disabled selected>Choose...</option>
                                <?php foreach($dept as $d) : ?>
                                    <option value="<?= $d['id']; ?>" <?= ($data['user_dept'] == $d['id'] ? 'selected' : '') ?>><?= $d['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("user_dept"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Level *</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('user_level') ? 'is-invalid' : '') ?>" name="user_level" id="user_level">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($level as $l) : ?>
                                    <option value="<?= $l['id']; ?>" <?= ($data['user_level'] == $l['id'] ? 'selected' : '') ?>><?= $l['id'] ?> - <?= $l['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("user_level"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Status *</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('user_status') ? 'is-invalid' : '') ?>" name="user_status" id="user_status">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($status as $s) : ?>
                                    <option value="<?= $s['id']; ?>" <?= ($data['user_status'] == $s['id'] ? 'selected' : '') ?>><?= $s['name1'] ?> - <?= $s['name2'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("user_status"); ?>
                        </div>
                    </div>
                </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="update" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Update Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection(); ?>       