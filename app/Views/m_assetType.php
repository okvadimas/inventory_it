<?php $this->extend("layout/template"); ?>
<?php $this->section('content'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Asset Type Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Asset Type</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/M_AssetType/insertData') ?>">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Add new menu ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Asset Identity</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="identity" id="identity" class="form-control <?= ($validation->hasError('identity') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="PTPF-IT-2021" value="<?= old('identity') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("identity"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Type</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="type" id="type" class="form-control <?= ($validation->hasError('type') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Computer i7" value="<?= old('type') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("type"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Borrowed</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="number" required name="borrowed" id="borrowed" class="form-control <?= ($validation->hasError('borrowed') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="0" value="<?= old('borrowed') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("borrowed"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Available</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="number" required name="available" id="available" class="form-control <?= ($validation->hasError('available') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="0" value="<?= old('available') ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("available"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Information</h6>
                    </div>
                    <div class="col-md-6">
                        <textarea style="border-radius: 15px;" name="information" id="information" class="form-control <?= ($validation->hasError('information') ? 'is-invalid' : '') ?>" cols="30" rows="10"><?= old('information') ?></textarea>
                        <div class="invalid-feedback">
                            <?= $validation->getError("information"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="update" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Insert Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>

        <div class="col-sm-12">
            <div class="card" data-aos="fade-down">
                <div class="card-body">
                    <div class="table-responsive mt-4"> 
                    <table id="example" class="table table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Asset Identity</th>
                                <th scope="col">Type</th>
                                <th scope="col">Borrowed</th>
                                <th scope="col">Available</th>
                                <th scope="col">Information</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php $no = 1; ?>
                        <?php foreach($type as $t) : ?>
                            <tr class="active">
                                <td class="align-middle" style="width: 10px;"><p class="ml-3"><?= $no++; ?></p></td>
                                <td class="align-middle"><?= $t['asset_id'] ?></td>
                                <td class="align-middle"><?= $t['type'] ?></td>
                                <td class="align-middle"><?= $t['borrowed'] ?></td>
                                <td class="align-middle"><?= $t['available'] ?></td>
                                <td class="align-middle"><?= $t['information'] ?></td>
                                <td class="align-middle">
                                    <a href="<?= base_url('/M_AssetType/editAssetType') . '/' . $t['id'] ?>" class="fa fa-edit <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-warning') ?>" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                    <a class="fa fa-trash <?= (session()->get('level') != 1 ? 'text-muted disabled' : 'text-danger') ?> deleteAssetType" data-delete="<?= $t['id'] ?>" data-bs-toggle="modal" data-bs-target="#modalDelete" style="<?= (session()->get('level') != 1 ? 'pointer-events: none' : '') ?>"></a>
                                </td>
                            </tr>
                        <?php endforeach; ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th scope="col">No</th>
                                <th scope="col">Asset Identity</th>
                                <th scope="col">Type</th>
                                <th scope="col">Borrowed</th>
                                <th scope="col">Available</th>
                                <th scope="col">Information</th>
                                <th scope="col">Action</th>
                            </tr>
                        </tfoot>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection('content'); ?>