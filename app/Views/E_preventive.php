<?php $this->extend("layout/edit"); ?>
<?php $this->section('edit'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Preventive Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Preventive</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/Preventive/doEditData' . '/' . $data['preventive_id']) ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Edit existing access ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">User ID *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="preventive_user" id="preventive_user" class="form-control <?= ($validation->hasError('preventive_user') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Seagate HDD 1TB 5000 Rpm" value="<?= (old('preventive_user') ? old('preventive_user') : $data['user_id']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("preventive_user"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Date *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="preventive_date" id="preventive_date" class="form-control <?= ($validation->hasError('preventive_date') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Intel i5 - 5200u 3.9Ghz" value="<?= (old('preventive_date') ? old('preventive_date') : $data['preventive_date']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("preventive_date"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Sequence *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="preventive_seq" id="preventive_seq" class="form-control <?= ($validation->hasError('preventive_seq') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="Asus GTX 1650 4Gb" value="<?= (old('preventive_seq') ? old('preventive_seq') : $data['preventive_seq']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("preventive_seq"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Antivirus *</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('preventive_antivirus') ? 'is-invalid' : '') ?>" name="preventive_antivirus" id="preventive_antivirus" placeholder="Status Here...">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($preventiveStatus as $ps) : ?>
                                    <option value="<?= $ps['id']; ?>" <?= ($data['preventive_antivirus'] == $ps['id'] ? 'selected' : '') ?>><?= $ps['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("preventive_antivirus"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Hardware *</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('preventive_hardware') ? 'is-invalid' : '') ?>" name="preventive_hardware" id="preventive_hardware" placeholder="Status Here...">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($preventiveStatus as $ps) : ?>
                                    <option value="<?= $ps['id']; ?>" <?= ($data['preventive_hardware'] == $ps['id'] ? 'selected' : '') ?>><?= $ps['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("preventive_hardware"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Connection *</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('preventive_conn') ? 'is-invalid' : '') ?>" name="preventive_conn" id="preventive_conn" placeholder="Status Here...">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($preventiveStatus as $ps) : ?>
                                    <option value="<?= $ps['id']; ?>" <?= ($data['preventive_conn'] == $ps['id'] ? 'selected' : '') ?>><?= $ps['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("preventive_conn"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Security *</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('preventive_sec') ? 'is-invalid' : '') ?>" name="preventive_sec" id="preventive_sec" placeholder="Status Here...">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($preventiveStatus as $ps) : ?>
                                    <option value="<?= $ps['id']; ?>" <?= ($data['preventive_sec'] == $ps['id'] ? 'selected' : '') ?>><?= $ps['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("preventive_sec"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Backup *</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('preventive_backup') ? 'is-invalid' : '') ?>" name="preventive_backup" id="preventive_backup" placeholder="Status Here...">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($preventiveStatus as $ps) : ?>
                                    <option value="<?= $ps['id']; ?>" <?= ($data['preventive_backup'] == $ps['id'] ? 'selected' : '') ?>><?= $ps['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("preventive_backup"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Clean Up *</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('preventive_clean') ? 'is-invalid' : '') ?>" name="preventive_clean" id="preventive_clean" placeholder="Status Here...">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($preventiveStatus as $ps) : ?>
                                    <option value="<?= $ps['id']; ?>" <?= ($data['preventive_clean'] == $ps['id'] ? 'selected' : '') ?>><?= $ps['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("preventive_clean"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Comment</h6>
                    </div>
                    <div class="col-md-6">
                        <textarea style="border-radius: 15px;" name="preventive_comment" id="preventive_comment" class="form-control <?= ($validation->hasError('preventive_comment') ? 'is-invalid' : '') ?>" cols="30" rows="10"><?= (old('preventive_comment') ? old('preventive_comment') : $data['preventive_comment']) ?></textarea>
                        <div class="invalid-feedback">
                            <?= $validation->getError("preventive_comment"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="update" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Update Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection(); ?>