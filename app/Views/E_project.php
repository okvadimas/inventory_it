<?php $this->extend("layout/edit"); ?>
<?php $this->section('edit'); ?>

<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="page-breadcrumb" data-aos-duration="500" data-aos="fade-down">
    <div class="row align-items-center">
        <div class="col-md-6 col-8 align-self-center">
            <h3 class="page-title mb-0 p-0">Project Management</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="<?= base_url('/'); ?>">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Project</li>
                    </ol>
                </nav>
            </div>
        </div>
        <div class="col-md-6 col-4 align-self-center">
            <div class="text-end upgrade-btn">
                <a href="<?= base_url('/Auth/signOut') ?>" class="btn btn-danger d-none d-md-inline-block text-white">Sign Out</a>
            </div>

        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <?php if (session()->getFlashData("error_message")) : ?>    
        <div class="alert alert-danger alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("error_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php elseif (session()->getFlashData("success_message")) : ?>
        <div class="alert alert-success alert-dismissible fade show text-center" role="alert">
            <strong><?= session()->getFlashData("success_message") ?></strong>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
            </button>
        </div>
    <?php endif; ?>

    <!-- Table -->
    <!-- ============================================================== -->
    <div class="row">
        <form action="<?= base_url('/Project/doEditData' . '/' . $data['project_id']) ?>" method="post">
            <div class="col-sm-12 pl-2" style="color: #54667a;">
                <div class="row align-items-center">
                    <h4 class="mb-4 font-weight-bold">Edit existing project ?</h4>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Project *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="project_name" id="project_name" class="form-control <?= ($validation->hasError('project_name') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="2021-04-15" value="<?= (old('project_name') ? old('project_name') : $data['project_name']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("project_name"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Planning Date *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="project_planning" id="project_planning" class="form-control <?= ($validation->hasError('project_planning') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="2021-04-15" value="<?= (old('project_planning') ? old('project_planning') : $data['project_planning']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("project_planning"); ?>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Actual Date *</h6>
                    </div>
                    <div class="col-md-6">
                        <input type="text" required name="project_actual" id="project_actual" class="form-control <?= ($validation->hasError('project_actual') ? 'is-invalid' : '') ?>" style="border-radius: 15px;" placeholder="2021-04-15" value="<?= (old('project_actual') ? old('project_actual') : $data['project_actual']) ?>">
                        <div class="invalid-feedback">
                            <?= $validation->getError("project_actual"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Status *</h6>
                    </div>
                    <div class="col-md-6">
                        <div class="selectWrapper">
                            <select style="border-radius: 15px" required class="form-control selectNoArrow <?= ($validation->hasError('project_status') ? 'is-invalid' : '') ?>" name="project_status" id="project_status" placeholder="Status Here...">
                                <option value="" disabled selected>Choose...</option>
                                <?php foreach($projectStatus as $ps) : ?>
                                    <option value="<?= $ps['id']; ?>" <?= ($data['project_status'] == $ps['id'] ? 'selected' : '') ?>><?= $ps['name'] ?></option>
                                <?php endforeach; ?>
                            </select>
                        </div>
                        <div class="invalid-feedback">
                            <?= $validation->getError("project_status"); ?>
                        </div>
                    </div>
                </div>
                <div class="row my-2">
                    <div class="col-md-2 d-flex align-items-center">
                        <h6 class="font-weight-bold">Comment</h6>
                    </div>
                    <div class="col-md-6">
                        <textarea style="border-radius: 15px;" name="project_comment" id="project_comment" class="form-control <?= ($validation->hasError('project_comment') ? 'is-invalid' : '') ?>" cols="30" rows="10"><?= (old('project_comment') ? old('project_comment') : $data['project_comment']) ?></textarea>
                        <div class="invalid-feedback">
                            <?= $validation->getError("project_comment"); ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row pl-2 my-5">
                <div class="col-md-3" style="float: right;">
                    <button type="submit" name="update" class="btn btn-success text-white <?= (session()->get('level') != 1 ? 'disabled' : '') ?>">Update Data</button>                
                    <button type="reset" class="btn btn-danger text-white">Reset</button>                
                </div>
            </div>
        </form>
    </div>
    <!-- ============================================================== -->
    <!-- Table -->

</div>
<!-- ============================================================== -->
<!-- End Container fluid  -->

<?php $this->endSection(); ?>