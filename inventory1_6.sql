-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 21, 2022 at 09:23 AM
-- Server version: 10.4.21-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `inventory1.6`
--

-- --------------------------------------------------------

--
-- Table structure for table `access`
--

CREATE TABLE `access` (
  `access_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `access_use` varchar(50) NOT NULL,
  `access_domain` varchar(100) NOT NULL,
  `access_pass` varchar(255) NOT NULL,
  `access_deskera` int(11) NOT NULL,
  `access_anydesk` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `access`
--

INSERT INTO `access` (`access_id`, `user_id`, `access_use`, `access_domain`, `access_pass`, `access_deskera`, `access_anydesk`) VALUES
(1, 2423, '2,4', 'costingstaff1', 'vcky@ptpf2022', 2, '928 115 848'),
(2, 82, '1,3,4', 'maccounting', 'khr@ptpf2021', 1, '805 113 163'),
(3, 1959, '1,4', 'csgstaff', 'nurul@ptpf2022', 1, '627 361 017'),
(4, 1307, '1,4', 'pakristanto', 'g@ptpf2022', 2, '789 844 731'),
(5, 2110, '1', 'sbgunawan', 'nt@ptpf2022', 1, '232 203 070'),
(6, 2648, '1,4', 'finance04', 'gd@ptpf2022', 1, '805 113 163'),
(7, 2417, '1,4', 'csgstaff1', '5uli5@exim', 1, '415 196 835'),
(8, 2351, '1,4', 'ppicstaff4', 'ndy@ptpf2022', 2, '402373020'),
(9, 1272, '1,4', 'ppicstaff2', 'ppt@ptpf2022', 1, '920 208 669'),
(10, 2607, '1,4', 'hrdstaff2', 'rf@ptpf2022', 2, '109 048 253'),
(11, 2618, '1,4', 'headcosting', 'tr1@ptpf2022', 2, '591 119 996'),
(12, 566, '3,4', 'dkrisna', 'dh@ptpf2022', 1, '735 160 119'),
(13, 533, '1,4', 'mlatif', 'ltf@ptpf2022', 1, '464 621 605'),
(14, 2145, '1,4', 'purchasing3', 'yud@ptpf2022', 1, '988 280 646'),
(15, 952, '1,4', 'pprabowo', 'ptt@ptpf2022', 1, '417 945 874'),
(16, 962, '1', 'pdestaff1', 'ntk@ptpf2022', 2, '943 771 266'),
(17, 2371, '1,4', 'spvwarehouse', 'ltf@ptpf2022', 1, '803 475 635'),
(18, 84, '3,4', 'alistiani', 'nn@ptpf2022', 1, '685 651 912'),
(19, 994, '3,4', 'msumaryono', 'mryn@ptpf2022', 1, '817 996 584'),
(20, 440, '3,4', 'dkurnianto', 'dw@ptpf2022', 2, '404 444 186'),
(21, 2508, '1,4', 'purchasingstaff1', 'dt@ptpf2022', 1, '661 453 358'),
(22, 123, '3,4', 'nraharto', 'nwng@ptpf2022', 1, '455 114 842'),
(23, 2090, '1,4', 'aacounting', 'dnn@ptpf2022', 1, '252 484 755'),
(24, 2407, '3,4', 'spvqc', 'd4ni@qc', 1, '343 077 177'),
(25, 2310, '2,4', 'melisa', 'ml@ptpf2022', 1, '734 345 063'),
(26, 111, '3,4', 'tarwanto', 'trwnt@ptpf2022', 1, '848 210 825'),
(27, 2382, '1,4', 'ppicstaff1', '4bi@ptpf2021', 1, '867 043 968'),
(28, 2245, '1,4', 'akholidin', 'hmd@ptpf2022', 1, '861 423 405'),
(29, 115, '1,4', 'sutanta', 'utnt@ptpf2022', 1, '666 056 127'),
(30, 1173, '1,4', 'accounting1', 'yu@ptpf2022', 1, '567 194 729'),
(31, 198, '1,4', 'pdestaff3', 'tur@ptpf2022', 2, '626 207 474'),
(32, 1421, '2', 'whstaff1', 'nm@ptpf2022', 1, '579 809 147'),
(33, 2347, '1,4', 'hrdstaff2', 'mukt4r@hr', 2, '109 048 253'),
(34, 1164, '3,4', 'WORKGROUP', '-', 1, '873 306 137'),
(35, 319, '1,4', 'purchasing02', 'vnny@ptpf2022', 1, '117 375 684'),
(36, 2091, '1,4', 'adminqc', 'ulf@ptpf2022', 1, '338 593 632'),
(37, 2622, '1,4', 'costingstaff2', 'dt@ptpf2022', 2, '700 957 159'),
(38, 2307, '1,4', 'hrdstaff1', 'dv@ptpf2022', 1, '581 315 938'),
(39, 1810, '1,4', 'accounting3', 'nurul@ptpf2022', 1, '947 682 567'),
(40, 2411, '1', 'pdestaff5', 'rfn@ptpf2022', 2, '852902016'),
(41, 104, '1,4', 'pdindriyasari', 'dbb@ptpf2022', 1, '205 155 845'),
(42, 2175, '1,4', '-', '-', 1, '235 123 972'),
(43, 1903, '1,4', 'pdestaff4', 'ul@ptpf2022', 2, '693 995 276'),
(44, 1211, '1,4', 'mudhalifah', 'f@ptpf2022', 1, '970 146 516'),
(45, 1186, '2,4', 'rkurniasari', 'rth@ptpf2022', 1, '323 546 201'),
(46, 900, '3,4', 'trochmani', 'tr1@ptpf2022', 2, '-'),
(47, 2023, '1', 'ppicstaff3', 'yhrul@ptpf2022', 1, '244 499 254'),
(48, 2275, '1', 'adminpde', 'luqmn@ptpf2022', 1, '865 500 509'),
(49, 273, '1,4', 'nuryadi', 'nuryd@ptpf2022', 2, '772 195 297'),
(50, 9999, '1', 'PC Server Fingerspot', '12345', 2, '213 302 044'),
(51, 2532, '1,4', 'csgstaff1', 'frz@ptpf2022', 1, '415 196 835'),
(52, 1255, '1,4', 'afinishing', 'lv@ptpf2022', 1, '416222088'),
(53, 78, 'Staff 1', 'bsetyawan ', 'btywn@ptpf2022', 0, 'Asetyawan'),
(54, 129, 'Staff 1', 'cromandy ', 'crmndy@ptpf2022', 0, 'aris '),
(55, 2568, 'Staff 1', 'effendi ', 'ffnd@ptpf2022', 0, 'nursidi '),
(56, 191, 'Staff 1', 'eko ', 'k@ptpf2022', 0, 'sgunawan '),
(57, 922, 'Staff 1', 'hartono ', 'hrtn@ptpf2022', 0, 'sbarjo ');

-- --------------------------------------------------------

--
-- Table structure for table `asset`
--

CREATE TABLE `asset` (
  `asset_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `asset_info` text NOT NULL,
  `asset_total` varchar(10) NOT NULL,
  `asset_cpu` varchar(100) NOT NULL,
  `asset_mb` varchar(100) NOT NULL,
  `asset_hdd` varchar(100) NOT NULL,
  `asset_ram` varchar(100) NOT NULL,
  `asset_vga` varchar(100) NOT NULL,
  `asset_monitor` varchar(100) NOT NULL,
  `asset_os` int(11) NOT NULL,
  `asset_serial` varchar(50) NOT NULL,
  `asset_type` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `asset`
--

INSERT INTO `asset` (`asset_id`, `user_id`, `asset_info`, `asset_total`, `asset_cpu`, `asset_mb`, `asset_hdd`, `asset_ram`, `asset_vga`, `asset_monitor`, `asset_os`, `asset_serial`, `asset_type`) VALUES
(1, 2310, 'AIO', '1', ' Intel Celeron - 1017U', ' -', ' 500 GB SSD', ' 4 GB', ' Integrated ', ' Lenovo 19 Inc HD', 1, 'D2BH9-C86R6-XQBH8-FR7V9-DDV93', 4),
(2, 2351, 'Dell Optiplex 7010', '1', ' Intel i7 - 3770', ' -', ' 1 TB Hardisk', ' 4 GB', ' Intergrated ', ' Dell 19 Inc', 1, '89XHR-TBR74-DC7P7-7M3WK-CTM2C', 3),
(3, 2110, 'Dell Optiplex 7010', '1', ' Intel i7 - 3770', ' -', ' 1 TB Hardisk', ' 4 Gb', ' Integrated ', ' Dell 18.5 Inc HD', 1, 'XJFKP-2FD44-Q377M-YR8D8-BY769', 3),
(4, 2382, 'Rakitan Infinity', '1', ' Intel i3 - 10100', ' H310M Asus', ' SSD 250GB & 1 TB HDD', ' 8 GB DDR4', ' Integrated  (Intel UHD 630)', ' Samsung 22 Inc', 5, '6TPJN-KGWW4-CGBJQ-488WC-XTPKF', 2),
(5, 198, 'Rakitan Cooler Master', '1', ' Intel i7 - 5960X', ' -', ' 120 GB SSD & 1 TB HDD', ' 16 Gb', ' GTX 1650 Asus', ' Samsung 20 Inc', 5, '9MNGK-MQY4W-JR7VT-7M64V-MBH24', 3),
(6, 1810, 'Good', '1', ' Intel i5 - 8400', ' -', ' -', ' 8 Gb', ' Integrated ', ' LG 19 Inc', 1, 'MBT6M-YJHY2-RCPFK-T3RPR-F24YW', 1),
(7, 1186, 'Lenovo AIO - Good', '1', ' Intel Pentium G645', ' -', ' SSD Samsung 500 GB Evo', ' 6 GB', ' Integrated ', ' Lenovo 20 Inc', 5, 'W3VXY-4XK7D-8BV8M-P93H4-CT9YG', 4),
(8, 2145, 'Rakitan Power Logic', '1', ' Intel i3 - 3240', ' -', ' 1 TB Hardisk', ' 4 Gb', ' Integrated ', ' Dell 19 Inc HD', 2, '......-4MMJJ9-X8QJ4', 2),
(9, 2023, 'Rakitan Infinity', '1', ' Intel i3 - 10100', ' Asus H310M', ' 1 TB HDD & 250 GB SSD', ' 8 GB DDR4', ' Intel UHD 630', ' Dell 19 Inc HD', 5, 'JXH7N-FQ9TV-RFK4H-KBH8H-94R9F', 2),
(10, 2371, 'Dell Optiplex 7010  183502-704325-727956-436586-006043-442226-979106-178280 (Phone Activation)', '1', ' Intel i7 - 3770', ' -', ' 1 TB Hardisk', ' 4 GB', ' Integrated ', ' Dell 18.5 Inc', 1, '777FR-7C94Q-C96FW-FBK6Y-TFMVJ', 3),
(11, 962, 'Rakitan Armagedon', '1', ' Intel i7 - 10700f', ' -', ' 500 GB SSD', ' 16 Gb', ' ASUS GTX 1660 Super 6 Gb', ' Samsung Flat 24 Inc FHD', 5, 'PYJ4H-8PNXR-HRGF6-9RXPQ-XHJXF', 3),
(12, 2532, 'Rakitan Infinity', '1', ' Intel i3 - 10100f', ' LGA 1200', ' SSD 128 GB & 1 TB Hardisk', ' 8 GB', ' AMD Radeon R5 200', ' Dell 19 Inc', 5, '6R8WR-FN8R7-MMCKR-36CPX-H6CPX', 2),
(13, 900, 'HP 1000', '1', ' Intel i3', ' -', ' 500 GB SSD', ' 6 GB', ' -', ' 14 Inc', 5, 'D39JR-TYFFB-PW97T-DGYF9-246BC', 7),
(14, 82, 'Good', '1', ' Intel i5 - 8400', ' -', ' -', ' 8 Gb', ' Integrated ', ' Samsung 19 Inc HD', 5, '-', 1),
(15, 1903, 'Good', '1', ' Intel i7 - 11700KF', ' -', ' 500 GB SSD', ' 16 Gb', ' Asus GTX 1660 6 GB', ' Samsung 24 Inc', 5, 'QB49K-DVN33-8DJBM-P6QFB-F9CKF', 3),
(16, 566, 'Acer Aspire 5', '1', ' i5 - 1135G7', '-', ' 1 TB HDD, 240 GB SSD', ' 8 GB', ' Integrated Irix XE, Nvidia MX350', ' Acer 14 Inc FHD Display &amp; Samsung 24 Inc FHD', 5, 'Windows 10 Home Digital License (Online)', 8),
(17, 1272, 'Dell Optiplex 7010', '1', ' Intel i7 - 3770', ' -', ' 1 TB Hardisk', ' 4 Gb', ' Intergrated ', ' Dell 19 Inc HD', 1, '86H6M-P9XMP-9YBY7-6YJHJ-MV3K8', 3),
(18, 2648, 'Good', '1', ' Intel i5 - 8400', ' -', ' -', ' 8 Gb', ' Integrated ', ' Samsung 19 Inc HD', 5, '-', 1),
(19, 1307, 'Rakitan Infinity', '1', ' Intel i7 - 7700', ' -', '500GB HDD, 240GB SSD', ' 8 Gb', ' Asus GTX 1050 TI 2 Gb', ' Samsung 24 Inc FHD', 5, 'D4HYN-JRMVK-3QB6D-Y2MY7-RGDGR', 3),
(20, 123, 'Asus ROG STRIX GL553V', '1', ' Intel i7 - 7700HQ', ' MSI H410M Pro VH', ' SSD 120 GB & 1 TB HDD', ' 16 GB', ' GTX 1050', ' Asus ROG 15 Inc FHD', 5, 'GV7TR-NM3QJ-MYRYT-VWTDG-4M49F', 9),
(21, 2407, 'Normal', '1', ' Intel i7 - 3520', ' -', ' -', ' 4 Gb', ' Integrated ', ' Lenovo 14 Inv', 1, '-', 9),
(22, 2347, 'Good', '1', ' Intel i7 - 3770', ' -', ' -', ' 4 Gb', ' Integrated ', ' Phillips 19.5 Inc', 5, 'X9WQP-7K6CR-64MKP-QYPF2-GJFRY', 3),
(23, 2411, 'Rakitan Venom RX', '1', ' Intel i5 - 10400', ' MSI H510M A Pro', ' 250 GB SSD & 1 TB hardisk', ' 8 GB', ' GTX 960', ' Acer 14 Inc FHD Display', 6, '-', 1),
(24, 2618, 'Rakitan Venom RX', '1', ' Intel i3 - 10105', ' -', ' 128 GB SSD &amp; 1 TB Hardisk', ' 8 GB', ' Intel UHD 630', ' Samsung 24 Inc FHD', 6, 'NQGCP-GR4XV-PCHT2-2CD89-3RR9F', 2),
(25, 2423, 'HP AIO Pavilion 20', '1', ' Intel i3 - 3240 3,4 Ghz', ' -', ' SSD 256 GB', ' 8 GB', ' Integrated ', ' AIO HP 20 Inc', 5, 'DYTT6-6Y77F-F2J3P-XXT2G-8WGCF', 4),
(26, 1959, 'Dell Optiplex 7010', '1', ' Intel i7 - 3770', ' -', ' 1 TB Hardisk', ' 6 GB', ' Integrated ', ' LG 19 Inc', 2, 'BQ673-V47TP-HFG2F-VPJT9-JPT40', 3),
(27, 104, 'Good', '1', ' Intel i7 - 3770', ' -', ' -', ' 4 Gb', ' Integrated ', ' Dell 20 Inc HD', 1, 'YDY89-K6CFT-26F78-9H2XW-WT6CG', 3),
(28, 115, 'Soon', '1', ' i5 - 4460', ' -', ' 500 GB HDD', ' 4 GB', ' HD 4600', ' LG 16 Inc HD', 1, '73VMP-YYYD3-T8CB9-QTJWZ-89FVY', 1),
(29, 2417, 'Good', '1', ' Intel i3 - 10300f', ' Asus LGA 1200', ' SSD VGEN 128 Gb, HDD 1 Tb', ' 8 Gb', ' AMD Radeon Dedicated ', ' Samsung Flat 24 Inc', 5, '-', 2),
(30, 2508, 'Rakitan Infinity', '1', ' Intel i3 - 10100', ' Asus H310M', ' 1 TB HDD & 250 GB SSD', ' 8 GB DDR4', ' Intel UHD 630 LGA 1200', ' Dell 19 Inc', 5, 'FHD4F-RNKXY-3W4C3-QTF9T-J2QGR', 2),
(31, 2175, 'Normal', '1', ' Intel i7 - 3770 3.40GHz', ' Dell', ' Seagate HDD 500Gb', ' 6Gb DDR3', ' NVIDIA GeForce GT 730 2Gb', ' Dell 19 Inc HD', 5, 'BHKTD-TKX22-8G9GF-X3DW3-VRVVP', 3),
(32, 2245, 'Rakitan Votre', '1', ' Intel i5 - 3330', ' -', ' 1 TB Hardisk', ' 4 Gb', ' Integrated ', ' Dell 19 Inc', 1, 'J263R-9MWQC-C967X-XT63M-6VPP9', 1),
(33, 1164, 'Very Good', '1', ' Intel i5 - 7200 - Dell Inspiron 5567', ' -', ' HDD 1 TB', ' 8 Gb', ' Integrated ', ' Dell 15.6 Inc HD', 5, '-', 8),
(34, 111, 'Good', '1', ' Intel i7 - 7500U (Lenovo)', ' -', ' 1 TB Hardisk', ' 8 Gb', ' Nvidia 940MX', ' Lenovo 14 Inc FHD', 5, 'Windows 10 Home Digital License (Online)', 9),
(35, 2607, 'Dell Optiplex 7010', '1', ' Intel i7 - 3770', ' -', ' 1 TB Hardisk', ' 4 GB', ' Intergrated ', ' Phillips 19.5 Inc', 5, 'X9WQP-7K6CR-64MKP-QYPF2-GJFRY', 3),
(36, 440, 'Asus A450L', '1', ' Intel i7 - 4500U (Asus A450L)', ' -', ' 500 GB Hardisk', ' 4 Gb', ' Integrated ', ' Asus 14 Inc HD', 2, 'FBRMC-3WWWR-GMV93-Y6JJC-Y6846', 9),
(37, 2090, 'Good', '1', ' Intel i5 - 8500', ' -', ' -', ' 8 Gb', ' Integrated ', ' Samsung 19 Inc', 5, '-', 1),
(38, 952, 'Dell Optiplex 7010', '1', ' Intel i7 - 3770', ' -', ' 1 TB Hardisk', ' 6 Gb', ' Integrated ', ' LG 19 Inc HD', 2, 'TMJXW-XB388-4JWBQ-WCTTY-WMQXM', 3),
(39, 273, 'Rakitan Dazumba', '1', ' Intel i7  - 3370', ' -', ' 1 TB Hardisk', ' 8 GB', ' Integrated ', ' Dell 19 Inc', 5, 'W79JJ-TRB3D-Q69GK-JMWR3-B9C4V', 3),
(40, 1173, 'Good', '1', ' Intel i5 - 8400', ' -', ' -', ' 8 Gb', ' Integrated ', ' LG 20 Inc FHD', 5, 'NRTG8-3PP99-JWBD6-HV4RV-CYT6R', 1),
(41, 319, 'Dell Optiplex 7010', '1', ' Intel i7 - 3770', ' -', ' 1 TB Hardisk', ' 4 Gb', ' Integrated ', ' Acer FHD 24 Inc', 1, 'D4TT3-MWG69-MYPJX-DP46G-D8BCG', 3),
(42, 994, 'Dell Inpiron 7000 Series', '1', ' i7 - 7500U', ' -', ' 1 TB HDD, 120 GB SSD', ' 8 GB', ' Nvidia 940MX', ' Dell 14 Inc FHD Display', 5, 'Windows 10 Home Digital License (Online)', 9),
(43, 533, 'Rakitan Votre', '1', ' Intel i5 - 4460', ' -', ' 1 TB Hardisk', ' 4 Gb', ' Integrated ', ' Dell 19 Inc HD', 1, 'X9M8V-R9V7M-V9C8C-RXHYW-H7K66', 1),
(44, 1211, 'Dell Optiplex 7010', '1', ' Intel i7 - 3770', ' -', ' 1 TB Hardisk', ' 4 Gb', ' Integrated ', ' Dell 19 Inc', 5, 'TGGGT-HX6H4-3HRR6-H9TW9-RPHBB', 3),
(45, 2307, 'Rakitan', '1', ' Intel i5 - 4460', ' -', ' 1 TB Hardisk', ' 4 Gb', ' Integrated ', ' Dell 18.5 Inc HD', 5, 'RHQXG-D33PQ-K74KT-VB98F-F8MR7', 1),
(46, 2622, 'Rakitan Dazumba', '1', ' Intel i7 - 4790', ' -', ' 1 TB Hardisk', ' 12 GB', ' Intel HD 4600', ' LG 20 Inc', 5, 'FHD4F-RNKXY-3W4C3-QTF9T-J2QGR', 3),
(47, 84, 'Lenovo V14 - IIL', '1', ' Intel i3 - 1005G1 1.2 Ghz', ' -', ' 500 Gb NVme', ' 8 Gb', ' Integrated ', ' 14 Inc', 5, 'Windows 10 Home Digital License (Online)', 7),
(48, 2091, 'Dell Optiplex 7010', '1', ' Intel i7 - 3770', ' -', ' 1 TB Hardisk', ' 4 Gb', ' Integrated ', ' Dell 19 Inc', 1, 'D3HW3-3XGCQ-K6D4G-PQQKG-QWBG7', 3),
(49, 1421, 'HP AIO 20', '1', ' Intel Pentium - G465', ' -', ' 500 GB SSD', ' 6 Gb', ' Integrated ', ' Lenovo 20 Inc', 5, '-', 4),
(50, 9999, 'Good', '1', ' Intel i7 - 6700', ' -', ' 1 TB Hardisk', ' 4 GB', ' Nvidia GT 720', ' Samsung 20 Inc', 3, '-', 3),
(51, 2275, 'Good', '1', ' Intel i3 - 10300f', ' Asus LGA 1200', ' SSD VGEN 128 Gb, HDD 1 Tb', ' 8 Gb', ' AMD Radeon Dedicated ', ' Samsung Flat 24 Inc', 5, 'CR3M3-6KN7Y-HFQ6F-PV9F9-2YT6R', 2),
(52, 1255, 'Good', '1', 'Intel i7 - 3770', '-', '500 GB HDD', '4 GB', 'Integrated Graphics', '19 Inc Dell', 1, 'V2VVP-DBMV2-CWJYF-4GPFF-JG9QB', 3);

-- --------------------------------------------------------

--
-- Table structure for table `backup`
--

CREATE TABLE `backup` (
  `backup_id` int(11) NOT NULL,
  `backup_date` date NOT NULL,
  `backup_month` int(11) NOT NULL,
  `backup_start` varchar(20) NOT NULL,
  `backup_end` varchar(20) NOT NULL,
  `backup_type` int(11) NOT NULL,
  `backup_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `backup`
--

INSERT INTO `backup` (`backup_id`, `backup_date`, `backup_month`, `backup_start`, `backup_end`, `backup_type`, `backup_comment`) VALUES
(1, '2021-01-12', 1, '3.5 TB', '4.8 TB', 1, 'Data januari + data mam ciel'),
(2, '2021-02-19', 2, '3.6 TB', '3.79 TB', 2, 'Data sampai 11 feb 2021'),
(3, '2021-03-20', 3, '-', '-', 0, 'Synology Rusak'),
(4, '2021-04-20', 4, '-', '-', 0, 'Synology Rusak'),
(5, '2021-05-20', 5, '-', '-', 0, 'Synology Rusak'),
(6, '2021-06-20', 6, '-', '-', 0, 'Synology Rusak'),
(7, '2021-07-26', 7, '4.8 TB', '6.7 TB', 1, 'Data sampai 24 juli 2021'),
(8, '2021-08-20', 8, '-', '-', 0, 'Server perlu upgrade'),
(9, '2021-09-20', 9, '-', '-', 0, 'Server perlu upgrade'),
(10, '2021-10-20', 10, '-', '-', 0, 'Server perlu upgrade'),
(11, '2021-11-20', 11, '-', '-', 0, 'Server perlu upgrade'),
(12, '2021-12-20', 12, '-', '-', 0, 'Server perlu upgrade'),
(15, '2022-01-21', 1, '7 TB', '5.8 TB', 1, 'Pengurangan duplikat data'),
(16, '2022-02-18', 2, '7 TB', '5.9 TB', 2, 'Good'),
(17, '2022-03-21', 3, '5.8 TB', '6 TB', 1, 'Good');

-- --------------------------------------------------------

--
-- Table structure for table `daily`
--

CREATE TABLE `daily` (
  `daily_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `daily_date` date NOT NULL,
  `daily_task` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE `email` (
  `email_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `email_pass` varchar(100) NOT NULL,
  `email_use` int(11) NOT NULL,
  `email_user` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`email_id`, `user_id`, `email_pass`, `email_use`, `email_user`) VALUES
(1, 2175, 'fy(vSdN=OU?$', 1, 'dosolichin'),
(2, 2245, 'R2m^*o0hq.f-', 2, 'akholidin'),
(3, 198, 'kVwW)~6g)%z~', 2, 'turwansari'),
(4, 2351, 'Yt1tb&C%B}WQ', 2, 'asprayogo'),
(5, 1810, '2T_Ph+{y-z^p', 1, 'nurul'),
(6, 900, '?4l3;j+r_Hii', 2, 'trochmani'),
(7, 2347, 'm=VRrlQ8H;^*', 2, 'memuktar'),
(8, 566, 'hbC5e=Y79aaF', 1, 'dkrisnawati'),
(9, 1173, 'meP2n#TQxbIu', 1, 'adinasari'),
(10, 319, 'TrAh{$LGXuH$', 2, 'ivaviani'),
(11, 123, 'q)Dp;qi3c}1o', 1, 'nraharto'),
(12, 104, '*)b*QI7xXSS+', 1, 'pdinriyasari'),
(13, 111, ']ITB%T@-Tbe0', 1, 'tarwanto'),
(14, 962, 'sC!,O^%~!)@$', 2, 'antok'),
(15, 2110, 'RuexkjYv$q9X', 2, 'npurwaningsih'),
(16, 1307, 'sjcuN*];v&{c', 2, 'pakristanto'),
(17, 2532, 'e!f}8vxC{(]V', 2, 'frapriliani'),
(18, 2091, ' / qcontrol2020@gmail.com7e0-&KY)FNMF / ulfaqcontrol@ptpf2020', 3, 'ulfaizah'),
(19, 2275, 'Pb@AmdvkXa%,', 2, 'lshodiqin'),
(20, 2508, '-{O*cD)c]Kwn', 2, 'Aditya Nugroho'),
(21, 2648, '@Ylaxa#]Hcam', 1, 'gkwidyarti'),
(22, 2607, 'i+TD.Whn6&3#', 2, 'hrd@ptpacificfurniture.id'),
(23, 2423, '[y%+%v6AnOQ;', 2, 'vspratama'),
(24, 1272, 'RR9b)2P-hZ#1', 2, 'pindriyanti'),
(25, 82, '}oTDe~PMx)7f', 1, 'khoiri'),
(26, 2407, 'FHaXLbIiJ)OB', 2, 'daardata'),
(27, 2023, 'M{kZOP^Fq!P8', 2, 'sfaudjan'),
(28, 952, 'XM.KNv+O3mAR', 2, 'pprabowo'),
(29, 994, '~ASGI(!vc26M', 2, 'msumaryono'),
(30, 115, '$Ol6M)jIn0Zg', 2, 'sutanta'),
(31, 1959, 'vlWQsD8@EH!]', 3, 'nhidayah'),
(32, 84, '[+$5?r*Zh5M,', 2, 'alistiani'),
(33, 2411, 'kcLddA~Lqark', 2, 'isoryansyah'),
(34, 1211, 'R5z_VCF8bR[~', 2, 'mudhalifah'),
(35, 2310, '{Hq&KHSMh-Bd', 2, 'melisa'),
(36, 2417, '!xHVOX3goY&r', 2, 'saminingsih'),
(37, 2622, '2;b6UR!s=G7g', 2, 'fakristiawan'),
(38, 2090, 'Efb]cH0E[1lP', 1, 'dlestari'),
(39, 2307, 'Y0GblOt7V#M% / ptpf@2021(smtp2go.com)', 2, 'dcputri'),
(40, 2382, 'wkE$QOc^AK+p', 2, 'araheksa'),
(41, 1421, '8VYw?[[V4_W;', 2, 'fatkhunniam'),
(42, 1903, '8tpkfLH(egZm', 2, 'sulistyawan'),
(43, 273, 'f3mmWn6#E0yh', 2, 'nuryadi'),
(44, 9999, '-', 1, 'PC Server Fingerspot'),
(45, 2371, 'G?Je]F2MGT!*', 2, 'muhammad.latif'),
(46, 1164, 'N0)y57+XW{}t', 1, 'sanam'),
(47, 440, 'H5zOU7n)trGQ', 1, 'dwi'),
(48, 2145, '1]cr!~1L4pE[', 3, 'ydarwanto'),
(49, 1186, '9Cu.%jRH{-yW', 1, 'rkurniasari'),
(50, 2618, 'a_67lQ]%f!zp', 2, 'tsupriatna'),
(51, 533, '6vDSKYWF{n-*', 2, 'mlatif'),
(52, 1255, '0aNRan#L_by3', 2, 'nurkholifah');

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `emp_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `emp_position` int(11) NOT NULL,
  `emp_gender` int(11) NOT NULL,
  `emp_condition` int(11) NOT NULL,
  `emp_address` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`emp_id`, `user_id`, `emp_position`, `emp_gender`, `emp_condition`, `emp_address`) VALUES
(1, 1173, 4, 2, 2, 'Jl. Tugu Lapangan RT 04/1 Semarang'),
(2, 1307, 3, 1, 1, 'Semarang'),
(3, 2091, 4, 2, 1, 'Mangkang Wetan Krajan RT 01 RW 01, Mangkang Wetan, Tugu'),
(4, 1164, 3, 1, 2, 'Perum Bukit Jatisari Lestari A22/5 RT.02/11 Mijen Semarang'),
(5, 1810, 4, 2, 2, 'Semarang'),
(6, 9999, 7, 3, 3, 'PT Pacific Furniture'),
(7, 900, 7, 1, 2, 'Semarang'),
(8, 2371, 3, 1, 2, 'Kendal'),
(9, 2307, 4, 2, 1, 'Jl. Sinar Gemah Timur 964'),
(10, 2382, 4, 1, 1, 'Kendal'),
(11, 123, 2, 1, 2, 'Dk. Talun Kacanf RT.04/III Kandri Gunung Pati Semarang'),
(12, 2351, 4, 1, 1, 'Patebon Kendal'),
(13, 2347, 4, 1, 2, 'Rejosari Semarang Timur'),
(14, 2310, 4, 2, 2, 'Jl. Tosari Blangsong, Kendal'),
(15, 440, 3, 1, 2, 'Jl.Brotojoyo Barat III / No.12 Semarang'),
(16, 952, 4, 1, 2, 'Semarang'),
(17, 533, 2, 1, 2, 'Jl. Rorojonggrang RT.4 RW.6, Manyaran'),
(18, 1421, 4, 1, 2, 'Semarang'),
(19, 1186, 4, 2, 2, 'Mijen RT.03 RW II Semarang'),
(20, 1959, 4, 2, 1, 'Semarang'),
(21, 198, 4, 1, 2, 'Jl. Tanjung Sari RT.7 RW.5'),
(22, 2275, 4, 1, 2, 'Semarang'),
(23, 104, 4, 2, 2, 'Jl. Padi IV B No.172 Semarang'),
(24, 2245, 4, 1, 2, 'Semarang'),
(25, 273, 4, 1, 2, 'Jl. Siwarak RW.2, Gunung Pati'),
(26, 84, 2, 2, 2, 'Jl. Avonia V/18 Graha Padma'),
(27, 2423, 4, 1, 1, 'Karangroto RT.2 RW.9'),
(28, 2110, 4, 2, 1, 'Semarang'),
(29, 2508, 4, 1, 2, 'Jl. Damar Timur Dalam 3/380 Banyumanik'),
(30, 2622, 4, 1, 1, 'Cinde Utara 24'),
(31, 2175, 4, 1, 1, 'Semarang'),
(32, 2090, 4, 2, 1, 'Margasari, Tegal'),
(33, 2407, 3, 1, 2, 'Krakatau 2, II A, Semarang'),
(34, 111, 2, 1, 2, 'Semarang'),
(35, 1903, 4, 1, 2, 'Semarang'),
(36, 962, 4, 1, 2, 'Jl. Kaliwiru II 509A Semarang'),
(37, 2648, 4, 2, 1, 'Semarang'),
(38, 2607, 4, 1, 2, 'Candi Prambanan VI'),
(39, 994, 3, 1, 2, 'Gubug'),
(40, 2145, 4, 1, 2, 'Randugarut RT.3/1'),
(41, 2411, 4, 1, 1, 'Nongkosawit 0101, Gunung Pati'),
(42, 2417, 4, 2, 2, 'Abu Bakar IV No.4 RT.15/RW.12'),
(43, 319, 4, 2, 2, 'Jl. Soekarno Hatta No.119A Semarang'),
(44, 1211, 4, 2, 2, 'Semarang'),
(45, 115, 2, 1, 2, 'Jl. Sriwibowo Rt. 03/06 Semarang'),
(46, 2618, 3, 1, 1, 'Jl. Jagalan 1/398'),
(47, 2532, 4, 2, 2, 'RT 03 RW 01 Sukolilan Kendal'),
(48, 1272, 4, 2, 2, 'Jl. Ringin Telu RT.9'),
(49, 2023, 4, 1, 2, 'Kaliwungu, Kendal'),
(50, 566, 3, 2, 2, 'Genuksari Semarang'),
(51, 82, 2, 1, 2, 'Kedungsari RT 01/08 Semarang'),
(52, 1255, 4, 2, 2, 'Semarang');

-- --------------------------------------------------------

--
-- Table structure for table `google`
--

CREATE TABLE `google` (
  `google_id` varchar(50) NOT NULL,
  `google_username` varchar(50) DEFAULT NULL,
  `google_family_name` varchar(50) DEFAULT NULL,
  `google_email` varchar(50) DEFAULT NULL,
  `google_picture` varchar(255) DEFAULT NULL,
  `google_gender` varchar(50) DEFAULT NULL,
  `google_verified_email` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `google`
--

INSERT INTO `google` (`google_id`, `google_username`, `google_family_name`, `google_email`, `google_picture`, `google_gender`, `google_verified_email`) VALUES
('102618266679497534707', 'Beta', '', 'okvadimas@gmail.com', 'https://lh3.googleusercontent.com/a-/AOh14GjhqD4bCZ6XZaW-1HzDLYwCXOzIYqnE5ynLnTewyQ=s96-c', '', 1),
('110722011165340215640', 'Dimas Okva', 'Okva', 'dimasokva@gmail.com', 'https://lh3.googleusercontent.com/a-/AOh14GiUYprkpQIyW83EgXPo7IcaJyf-b8OV9n02Vqr03Q=s96-c', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `internet`
--

CREATE TABLE `internet` (
  `inet_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `inet_user` varchar(255) NOT NULL,
  `inet_password` varchar(255) NOT NULL,
  `inet_profile` varchar(255) NOT NULL,
  `inet_fullname` varchar(255) DEFAULT NULL,
  `inet_position` int(11) DEFAULT NULL,
  `inet_dept` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `internet`
--

INSERT INTO `internet` (`inet_id`, `user_id`, `inet_user`, `inet_password`, `inet_profile`, `inet_fullname`, `inet_position`, `inet_dept`) VALUES
(1, 2090, 'dlestari', 'dnn@ptpf2022', 'Administrator', NULL, NULL, NULL),
(2, 104, 'debbie', 'dbb@ptpf2022', 'Administrator', NULL, NULL, NULL),
(3, 2648, 'gadis', 'gd@ptpf2022', 'Administrator', NULL, NULL, NULL),
(4, 1173, 'adinasari', 'yu@ptpf2022', 'Administrator', NULL, NULL, NULL),
(5, 1810, 'nurul', 'nurul@ptpf2022', 'Administrator', NULL, NULL, NULL),
(6, 566, 'dkrisna', 'dh@ptpf2022', 'SPV 2', NULL, NULL, NULL),
(7, 1959, 'nhidayah', 'nurul@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(8, 2532, 'firza', 'frz@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(9, 2607, 'arief', 'rf@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(10, 2307, 'devi', 'dv@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(11, 84, 'alistiani', 'nn@ptpf2022', 'Administrator', NULL, NULL, NULL),
(12, 1211, 'mudhalifah', 'f@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(13, 2023, 'syahrul', 'yhrul@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(14, 533, 'latif', 'ltf@ptpf2022', 'SPV 2', NULL, NULL, NULL),
(15, 2310, 'melisa', 'ml@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(16, 1272, 'pipit', 'ppt@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(17, 111, 'tarwanto', 'trwnt@ptpf2022', 'Manager', NULL, NULL, NULL),
(18, 2351, 'andy', 'ndy@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(19, 994, 'maryono', 'mryn@ptpf2022', 'SPV 2', NULL, NULL, NULL),
(20, 319, 'vanny', 'vnny@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(21, 2245, 'kholidin', 'hmd@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(22, 2145, 'yudo', 'yud@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(23, 2508, 'aditya', 'dt@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(24, 2091, 'ulfa', 'ulf@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(25, 2622, 'aditia', 'dt@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(26, 2423, 'vicky', 'vcky@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(27, 2618, 'tri', 'tr1@ptpf2022', 'SPV 2', NULL, NULL, NULL),
(28, 273, 'nuryadi', 'nuryd@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(29, 440, 'dwi', 'dw@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(30, 952, 'putut', 'ptt@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(31, 2275, 'luqman ', 'luqmn@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(32, 2411, 'irfan ', 'rfn@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(33, 123, 'nawang', 'nwng@ptpf2022', 'Administrator', NULL, NULL, NULL),
(34, 1307, 'aga ', 'g@ptpf2022', 'SPV 2', NULL, NULL, NULL),
(35, 198, 'turwansari', 'tur@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(36, 962, 'wagiyanto', 'ntk@ptpf2022', 'Staff 1', NULL, NULL, NULL),
(37, 1421, 'niam', 'nm@ptpf2022', 'Staff 2', NULL, NULL, NULL),
(38, 2371, 'latifwh', 'ltf@ptpf2022', 'SPV 2', NULL, NULL, NULL),
(39, 115, 'sutanta', 'utnt@ptpf2022', 'Manager', NULL, NULL, NULL),
(40, 78, 'bsetyawan ', 'btywn@ptpf2022', 'Staff 1', NULL, NULL, NULL),
(41, 129, 'cromandy ', 'crmndy@ptpf2022', 'Staff 1', NULL, NULL, NULL),
(42, 2568, 'effendi ', 'ffnd@ptpf2022', 'Staff 1', NULL, NULL, NULL),
(43, 191, 'eko ', 'k@ptpf2022', 'Staff 1', NULL, NULL, NULL),
(44, 922, 'hartono ', 'hrtn@ptpf2022', 'Staff 1', NULL, NULL, NULL),
(45, 792, 'nazachah ', 'nzchh@ptpf2022', 'Staff 1', NULL, NULL, NULL),
(46, 1092, 'nur ', 'nur@ptpf2022', 'Staff 1', NULL, NULL, NULL),
(47, 80, 'ponco ', 'pnc@ptpf2022', 'Staff 1', NULL, NULL, NULL),
(48, 2424, 'siti ', 't@ptpf2022', 'Staff 1', NULL, NULL, NULL),
(49, 1749, 'sujono ', 'ujn@ptpf2022', 'Staff 1', NULL, NULL, NULL),
(50, 1570, 'surono ', 'urn@ptpf2022', 'Staff 1', NULL, NULL, NULL),
(51, 2467, 'taslik ', 'tlk@ptpf2022', 'Staff 1', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id` int(11) NOT NULL,
  `log_asset` varchar(100) NOT NULL,
  `log_name` varchar(100) NOT NULL,
  `log_dept` int(11) NOT NULL,
  `log_status` int(11) NOT NULL,
  `log_stock` int(11) NOT NULL,
  `log_desc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `menu_name` varchar(100) NOT NULL,
  `menu_icon` varchar(255) NOT NULL,
  `menu_url` varchar(255) NOT NULL,
  `sort` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_name`, `menu_icon`, `menu_url`, `sort`) VALUES
(1, 'Dashboard', 'far fa-clock fa-fw', '/dashboard', 1),
(2, 'Profile', 'fa fa-user fa-fw', '/profile', 2),
(3, 'Asset', 'fas fa-table fa-fw', '/asset', 3),
(4, 'Employee', 'fas fa-users fa-fw', '/employee', 4),
(5, 'Access', 'fas fa-user-circle fa-fw', '/access', 5),
(6, 'Email ', 'fas fa-envelope fa-fw', '/email', 6),
(8, 'Project', 'fa fa-briefcase fa-fw', '/project', 8),
(9, 'Preventive', 'fa fa-lock fa-fw', '/preventive', 9),
(10, 'User Management', 'fas fa-key fa-fw', '/userManagement', 10),
(11, 'Menu Management', 'fas fa-bars fa-fw', '/menuManagement', 11),
(12, 'M Asset Type', 'fa fa-desktop fa-fw', '/m_assetType', 12),
(13, 'M User Level', 'fa fa-filter fa-fw', '/m_userLevel', 13),
(14, 'M Department', 'fa fa-sitemap fa-fw', '/m_dept', 14),
(16, 'Other Management', 'fa fa-ellipsis-h fa-fw', '/m_otherManagement', 15),
(17, 'M User Access ', 'fa fa-id-card fa-fw', '/m_userAccessManagement', 16),
(18, 'Post Message', 'fa fa-paper-plane fa-fw', '/postMessage', 17),
(19, 'Support System', 'fas fa-handshake', '/supportSystem', 18),
(25, 'Inspection', 'fas fa-info fa-fw', '/inspection', 19),
(27, 'Reset Password', 'fas fa-sync-alt fa-fw ', '/resetPassword', 9),
(28, 'Backup Data', 'fas fa-hdd fa-fw', '/backup', 9),
(29, 'Log Status', 'fas fa-history fa-fw', '/log', 20),
(30, 'Monitoring', 'fas fa-tachometer-alt fa-fw', '/monitoringSpeed', 9),
(31, 'Internet', 'fas fa-wifi fa-fw', '/internet', 7),
(32, 'Ticketing', 'fas fa-ticket-alt fa-fw', '/ticketing', 2);

-- --------------------------------------------------------

--
-- Table structure for table `monitoring_speed`
--

CREATE TABLE `monitoring_speed` (
  `monitoring_id` int(11) NOT NULL,
  `monitoring_year` int(11) NOT NULL,
  `monitoring_month` int(11) NOT NULL,
  `monitoring_average` float NOT NULL,
  `monitoring_downtime` int(11) NOT NULL,
  `monitoring_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `monitoring_speed`
--

INSERT INTO `monitoring_speed` (`monitoring_id`, `monitoring_year`, `monitoring_month`, `monitoring_average`, `monitoring_downtime`, `monitoring_comment`) VALUES
(1, 2022, 1, 51.65, 0, 'Good'),
(2, 2022, 2, 50.44, 0, 'Good'),
(3, 2022, 3, 52.76, 0, 'Good');

-- --------------------------------------------------------

--
-- Table structure for table `m_access_deskera`
--

CREATE TABLE `m_access_deskera` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_access_deskera`
--

INSERT INTO `m_access_deskera` (`id`, `name`) VALUES
(1, 'Yes'),
(2, 'No');

-- --------------------------------------------------------

--
-- Table structure for table `m_access_use`
--

CREATE TABLE `m_access_use` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_access_use`
--

INSERT INTO `m_access_use` (`id`, `name`) VALUES
(1, 'Computer'),
(2, 'AIO'),
(3, 'Laptop'),
(4, 'Mobile ');

-- --------------------------------------------------------

--
-- Table structure for table `m_asset_type`
--

CREATE TABLE `m_asset_type` (
  `id` int(11) NOT NULL,
  `type` varchar(100) NOT NULL,
  `asset_id` varchar(100) DEFAULT NULL,
  `available` int(11) DEFAULT NULL,
  `borrowed` int(11) DEFAULT NULL,
  `information` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_asset_type`
--

INSERT INTO `m_asset_type` (`id`, `type`, `asset_id`, `available`, `borrowed`, `information`) VALUES
(1, 'Computer Mid-End Intel i5', 'PTPF-IT-0001', 0, 0, 'Komputer Spesifikasi i5'),
(2, 'Computer Low-End Intel i3 - Lower', 'PTPF-IT-0002', 0, 0, 'Komputer Spesifikasi i3 dan Kebawah'),
(3, 'Computer High-End Intel i7 - Higher', 'PTPF-IT-0003', 1, 0, 'Komputer Spesifikasi i7 dan Keatas'),
(4, 'AIO Low-End Intel i3 - Lower', 'PTPF-IT-0004', 1, 0, 'All In One Komputer Spesifikasi i3 dan Kebawah'),
(5, 'AIO Mid-End Intel i5', 'PTPF-IT-0005', 0, 0, 'All In One Komputer Spesifikasi i5 '),
(6, 'AIO High-End Intel i7 - Higher', 'PTPF-IT-0006', 0, 0, 'All In One Komputer Spesifikasi i7 dan Keatas'),
(7, 'Laptop Low-End Intel i3 - Lower', 'PTPF-IT-0007', 0, 1, 'Laptop Spesifikasi i3 dan Kebawah <br>\r\n1. Laptop HP 1000 dipinjam Bu Debbie<br>'),
(8, 'Laptop Mid-End Intel i5', 'PTPF-IT-0008', 0, 1, 'Laptop Spesifikasi i5 <br>\r\n1. Dipakai Veneer Cutter Machine <br>'),
(9, 'Laptop High-End Intel i7 - Higher', 'PTPF-IT-0009', 0, 0, 'Laptop Spesifikasi i7 dan Keatas'),
(10, 'Smartphone Android', 'PTPF-IT-0010', 0, 0, 'Smartphone Android'),
(11, 'Tablet Android', 'PTPF-IT-0011', 0, 0, 'Tablet Android'),
(21, 'Keyboard', 'PTPF-IT-0012', 10, 0, 'Keyboard '),
(22, 'Mouse', 'PTPF-IT-0013', 4, 0, 'Mouse'),
(23, 'Monitor', 'PTPF-IT-0014', 2, 0, 'Monitor'),
(24, 'Printer ', 'PTPF-IT-0015', 0, 0, 'Printer'),
(25, 'UPS', 'PTPF-IT-0016', 2, 0, 'UPS');

-- --------------------------------------------------------

--
-- Table structure for table `m_condition`
--

CREATE TABLE `m_condition` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_condition`
--

INSERT INTO `m_condition` (`id`, `name`) VALUES
(1, 'Single'),
(2, 'Married'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `m_dept`
--

CREATE TABLE `m_dept` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `slug` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_dept`
--

INSERT INTO `m_dept` (`id`, `name`, `slug`) VALUES
(1, 'IT ', 'IT '),
(2, 'Accounting', 'ACC'),
(3, 'Finance', 'FNC'),
(4, 'Exim', 'EX'),
(5, 'HRD', 'HRD'),
(6, 'Purchasing', 'PRC'),
(7, 'PPIC', 'PPIC'),
(8, 'Quality Control', 'QC'),
(9, 'Costing', 'CST'),
(10, 'PDE', 'PDE'),
(11, 'Finishing', 'FNS'),
(12, 'Warehouse', 'WH'),
(13, 'Maintenance', 'MTC'),
(14, 'Boiler', 'BL'),
(15, 'Security', 'SEC'),
(16, 'Production', 'PROC');

-- --------------------------------------------------------

--
-- Table structure for table `m_email_use`
--

CREATE TABLE `m_email_use` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_email_use`
--

INSERT INTO `m_email_use` (`id`, `name`) VALUES
(1, 'Outlook'),
(2, 'Windows Live Mail'),
(3, 'Thunderbird'),
(4, 'Gmail'),
(5, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `m_gender`
--

CREATE TABLE `m_gender` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_gender`
--

INSERT INTO `m_gender` (`id`, `name`) VALUES
(1, 'Male'),
(2, 'Female'),
(3, 'Other');

-- --------------------------------------------------------

--
-- Table structure for table `m_operating_system`
--

CREATE TABLE `m_operating_system` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_operating_system`
--

INSERT INTO `m_operating_system` (`id`, `name`) VALUES
(1, 'Windows 7 32'),
(2, 'Windows 7 64'),
(3, 'Windows 8 32'),
(4, 'Windows 10 32'),
(5, 'Windows 10 64'),
(6, 'Windows 11');

-- --------------------------------------------------------

--
-- Table structure for table `m_position`
--

CREATE TABLE `m_position` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_position`
--

INSERT INTO `m_position` (`id`, `name`) VALUES
(1, 'Director'),
(2, 'Manager'),
(3, 'Supervisor'),
(4, 'Staff'),
(5, 'Training'),
(6, 'Outsourcing'),
(7, 'Other'),
(8, 'Leadman Production');

-- --------------------------------------------------------

--
-- Table structure for table `m_preventive_status`
--

CREATE TABLE `m_preventive_status` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_preventive_status`
--

INSERT INTO `m_preventive_status` (`id`, `name`) VALUES
(1, 'Finished'),
(2, 'Unfinished');

-- --------------------------------------------------------

--
-- Table structure for table `m_project_status`
--

CREATE TABLE `m_project_status` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_project_status`
--

INSERT INTO `m_project_status` (`id`, `name`) VALUES
(1, 'Open'),
(2, 'Close');

-- --------------------------------------------------------

--
-- Table structure for table `m_status`
--

CREATE TABLE `m_status` (
  `id` int(11) NOT NULL,
  `name1` varchar(100) NOT NULL,
  `name2` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_status`
--

INSERT INTO `m_status` (`id`, `name1`, `name2`) VALUES
(1, 'Active', 'Tetap'),
(2, 'Active', 'Kontrak'),
(3, 'Deactive', 'Sementara'),
(4, 'Deactive', 'Keluar'),
(5, 'In', 'Masuk'),
(6, 'Out ', 'Keluar'),
(7, 'Done', 'Approved'),
(8, 'Pending', 'Pending'),
(9, '', 'Reject');

-- --------------------------------------------------------

--
-- Table structure for table `m_user_level`
--

CREATE TABLE `m_user_level` (
  `id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `m_user_level`
--

INSERT INTO `m_user_level` (`id`, `name`) VALUES
(1, 'Administrator'),
(2, 'HRD'),
(3, 'User');

-- --------------------------------------------------------

--
-- Table structure for table `post`
--

CREATE TABLE `post` (
  `post_id` int(11) NOT NULL,
  `post_title` text NOT NULL,
  `post_message` text NOT NULL,
  `post_create` varchar(100) NOT NULL,
  `post_by` varchar(100) NOT NULL,
  `user_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `preventive`
--

CREATE TABLE `preventive` (
  `preventive_id` int(11) NOT NULL,
  `preventive_date` date NOT NULL,
  `user_id` varchar(100) NOT NULL,
  `preventive_seq` int(11) NOT NULL,
  `preventive_antivirus` int(11) NOT NULL,
  `preventive_hardware` int(11) NOT NULL,
  `preventive_conn` int(11) NOT NULL,
  `preventive_sec` int(11) NOT NULL,
  `preventive_backup` int(11) NOT NULL,
  `preventive_clean` int(11) NOT NULL,
  `preventive_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `preventive`
--

INSERT INTO `preventive` (`preventive_id`, `preventive_date`, `user_id`, `preventive_seq`, `preventive_antivirus`, `preventive_hardware`, `preventive_conn`, `preventive_sec`, `preventive_backup`, `preventive_clean`, `preventive_comment`) VALUES
(1, '2022-03-05', '2175', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(2, '2022-03-05', '1164', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(3, '2022-03-07', '566', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(4, '2022-03-05', '1959', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(5, '2022-03-05', '2532', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(6, '2022-03-05', '319', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(7, '2022-03-05', '2245', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(8, '2022-03-05', '2145', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(9, '2022-03-07', '994', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(10, '2022-03-05', '2508', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(11, '2022-03-05', '1173', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(12, '2022-03-05', '104', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(13, '2022-03-05', '2648', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(14, '2022-03-05', '1810', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(15, '2022-03-05', '2090', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(16, '2022-03-07', '84', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(17, '2022-03-05', '2607', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(18, '2022-03-05', '1211', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(19, '2022-03-05', '2307', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(20, '2022-03-05', '2023', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(21, '2022-03-05', '2310', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(22, '2022-03-05', '1272', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(23, '2022-03-05', '533', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(24, '2022-03-07', '111', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(25, '2022-03-05', '2382', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(26, '2022-03-05', '1903', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(27, '2022-03-05', '1307', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(28, '2022-03-05', '198', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(29, '2022-03-05', '962', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(30, '2022-03-05', '952', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(31, '2022-03-07', '123', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(32, '2022-03-05', '2411', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(33, '2022-03-05', '2275', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(34, '2022-03-05', '1421', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(35, '2022-03-05', '2110', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(36, '2022-03-05', '2371', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(37, '2022-03-05', '115', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(38, '2022-03-05', '1186', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(39, '2022-03-07', '440', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(40, '2022-03-05', '2622', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(41, '2022-03-05', '2618', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(42, '2022-03-05', '2423', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(43, '2022-03-05', '273', 1, 1, 1, 1, 1, 1, 1, 'All Good'),
(44, '2022-03-05', '1255', 1, 1, 1, 1, 1, 1, 1, 'All Good');

-- --------------------------------------------------------

--
-- Table structure for table `project`
--

CREATE TABLE `project` (
  `project_id` int(11) NOT NULL,
  `project_name` varchar(255) NOT NULL,
  `project_planning` date NOT NULL,
  `project_actual` date NOT NULL,
  `project_status` int(11) NOT NULL,
  `project_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `report`
--

CREATE TABLE `report` (
  `report_id` int(11) NOT NULL,
  `report_date` int(11) NOT NULL,
  `report_month` int(11) NOT NULL,
  `report_year` int(11) NOT NULL,
  `report_total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `report`
--

INSERT INTO `report` (`report_id`, `report_date`, `report_month`, `report_year`, `report_total`) VALUES
(1, 15, 4, 2022, 3),
(2, 18, 4, 2022, 2),
(3, 19, 4, 2022, 3),
(4, 20, 4, 2022, 1),
(5, 21, 4, 2022, 7);

-- --------------------------------------------------------

--
-- Table structure for table `reset_password`
--

CREATE TABLE `reset_password` (
  `reset_id` int(11) NOT NULL,
  `reset_date` date NOT NULL,
  `reset_user` varchar(50) NOT NULL,
  `reset_domain` varchar(20) NOT NULL,
  `reset_old` varchar(50) NOT NULL,
  `reset_new` varchar(50) NOT NULL,
  `reset_seq` int(11) NOT NULL,
  `reset_comment` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `reset_password`
--

INSERT INTO `reset_password` (`reset_id`, `reset_date`, `reset_user`, `reset_domain`, `reset_old`, `reset_new`, `reset_seq`, `reset_comment`) VALUES
(1, '2022-03-08', '2090', 'aacounting', 'd1nni@ptpf2021', 'dnn@ptpf2022', 1, 'Good'),
(2, '2022-03-08', '104', 'pdindriyasari', 'd3bbi3@ptpf2021', 'dbb@ptpf2022', 1, 'Good'),
(3, '2022-03-08', '2648', 'finance04', 'gd@ptpf2021', 'gd@ptpf2022', 1, 'Good'),
(4, '2022-03-08', '1173', 'accounting1', '4yu@ptpf2021', 'yu@ptpf2022', 1, 'Good'),
(5, '2022-03-08', '1810', 'accounting3', 'nurul@ptpf2021', 'nurul@ptpf2022', 1, 'Good'),
(6, '2022-03-08', '566', 'dkrisna', 'd1ah@ptpf2021', 'dh@ptpf2022', 1, 'Good'),
(7, '2022-03-08', '1959', 'csgstaff', 'nurul@ptpf2021', 'nurul@ptpf2022', 1, 'Good'),
(8, '2022-03-08', '2532', 'csgstaff1', 'sul1s@ptpf2021', 'frz@ptpf2022', 1, 'Good'),
(9, '2022-03-09', '2607', 'hrdstaff2', 'mukt4r@ptpf2021', 'rf@ptpf2022', 1, 'Good'),
(10, '2022-03-09', '2307', 'hrdstaff1', 'd3vi@ptpf2021', 'dv@ptpf2022', 1, 'Good'),
(11, '2022-03-09', '84', 'alistiani', '4nna@ptpf2021', 'nn@ptpf2022', 1, 'Good'),
(12, '2022-03-09', '1211', 'mudhalifah', '1fa@ptpf2021', 'f@ptpf2022', 1, 'Good'),
(13, '2022-03-09', '2023', 'ppicstaff3', 'sy4hrul@ptpf2021', 'yhrul@ptpf2022', 1, 'Good'),
(14, '2022-03-09', '533', 'mlatif', 'l4tif@ptpf2021', 'ltf@ptpf2022', 1, 'Good'),
(15, '2022-03-09', '2310', 'melisa', 'm3lis4@ptpf2021', 'ml@ptpf2022', 1, 'Good'),
(16, '2022-03-09', '1272', 'ppicstaff2', 'p1pit@ptpf2021', 'ppt@ptpf2022', 1, 'Good'),
(17, '2022-03-09', '111', 'tarwanto', 't4rw4nto@ptpf2021', 'trwnt@ptpf2022', 1, 'Good'),
(18, '2022-03-09', '2351', 'ppicstaff4', '4ndy@ptpf2021', 'ndy@ptpf2022', 1, 'Good'),
(19, '2022-03-10', '994', 'msumaryono', 'm4ryon0@ptpf2021', 'mryn@ptpf2022', 1, 'Good'),
(20, '2022-03-10', '319', 'purchasing02', 'v4nny@ptpf2021', 'vnny@ptpf2022', 1, 'Good'),
(21, '2022-03-10', '2245', 'akholidin', '4hmad@ptpf2021', 'hmd@ptpf2022', 1, 'Good'),
(22, '2022-03-10', '2145', 'purchasing3', 'yud0@ptpf2021', 'yud@ptpf2022', 1, 'Good'),
(23, '2022-03-10', '2508', 'purchasingstaff1', '4dity4@ptpf2021', 'dt@ptpf2022', 1, 'Good'),
(24, '2022-03-10', '2091', 'adminqc', 'ulf4@ptpf2021', 'ulf@ptpf2022', 1, 'Good'),
(25, '2022-03-10', '2622', 'costingstaff2', 'ptpf@2021', 'dt@ptpf2022', 1, 'Good'),
(26, '2022-03-10', '2423', 'costingstaff1', 'v1cky@ptpf2021', 'vcky@ptpf2022', 1, 'Good'),
(27, '2022-03-10', '2618', 'headcosting', 'ptpf@2022', 'tr1@ptpf2022', 1, 'Good'),
(28, '2022-03-10', '273', 'nuryadi', 'nury4di@ptpf2021', 'nuryd@ptpf2022', 1, 'Good'),
(29, '2022-03-10', '440', 'dkurnianto', 'dw1@ptpf2021', 'dw@ptpf2022', 1, 'Good'),
(30, '2022-03-11', '952', 'pprabowo', 'putut@ptpf2021', 'ptt@ptpf2022', 1, 'Good'),
(31, '2022-03-11', '2275', 'adminpde', 'luqm4n@ptpf2021', 'luqmn@ptpf2022', 1, 'Good'),
(32, '2022-03-11', '2411', 'pdestaff5', '1rfan@ptpf2021', 'rfn@ptpf2022', 1, 'Good'),
(33, '2022-03-11', '123', 'nraharto', 'n4wang@ptpf2021', 'nwng@ptpf2022', 1, 'Good'),
(34, '2022-03-11', '1307', 'pakristanto', '4ga@ptpf2021', 'g@ptpf2022', 1, 'Good'),
(35, '2022-03-11', '198', 'pdestaff3', 'turw4nwsar1@ptpf2021', 'tur@ptpf2022', 1, 'Good'),
(36, '2022-03-11', '1903', 'pdestaff4', 'sul1s@ptpf2021', 'ul@ptpf2022', 1, 'Good'),
(37, '2022-03-11', '962', 'pdestaff1', '4ntok@ptpf2021', 'ntk@ptpf2022', 1, 'Good'),
(38, '2022-03-11', '2110', 'sbgunawan', 'n1ta@ptpf2021', 'nt@ptpf2022', 1, 'Good'),
(39, '2022-03-11', '1421', 'whstaff1', 'n1am@ptpf2021', 'nm@ptpf2022', 1, 'Good'),
(40, '2022-03-11', '2371', 'spvwarehouse', 'l4tif@ptpf2021', 'ltf@ptpf2022', 1, 'Good'),
(41, '2022-03-11', '1186', 'rkurniasari', 'r4tih@ptpf2021', 'rth@ptpf2022', 1, 'Good'),
(42, '2022-03-11', '115', 'sutanta', 'sut4nta@ptpf2021', 'utnt@ptpf2022', 1, 'Good'),
(43, '2022-03-11', '1255', 'afinishing', 'oliv3@ptpf2021', 'lv@ptpf2022', 1, 'Good');

-- --------------------------------------------------------

--
-- Table structure for table `support`
--

CREATE TABLE `support` (
  `support_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `support_text` varchar(255) NOT NULL,
  `support_date` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `ticket`
--

CREATE TABLE `ticket` (
  `ticket_id` int(11) NOT NULL,
  `ticket_number` varchar(50) NOT NULL,
  `ticket_message` text NOT NULL,
  `ticket_type` int(11) DEFAULT NULL,
  `ticket_level` int(11) DEFAULT NULL,
  `ticket_due` date DEFAULT NULL,
  `ticket_status` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ticket`
--

INSERT INTO `ticket` (`ticket_id`, `ticket_number`, `ticket_message`, `ticket_type`, `ticket_level`, `ticket_due`, `ticket_status`, `created_by`, `created_at`, `updated_by`, `updated_at`) VALUES
(1, 'IT-202200001', 'Upgrade OS Windows 7 ke Windows 10 Vicky', 1, 0, '2022-04-21', 7, 2175, '2022-04-18 09:00:00', 2175, '2022-04-18 12:00:00'),
(2, 'IT-202200002', 'Upgrade Windows 7 ke Windows 10 Nuryadi', 1, 0, '2022-04-21', 7, 2175, '2022-04-18 10:00:00', 2175, '2022-04-20 12:00:00'),
(3, 'IT-202200003', 'Membetulkan Kabel Jaringan Fotokopi PDE IR3045', 1, 0, '2022-04-21', 7, 2175, '2022-04-18 11:00:00', 2175, '2022-04-18 11:30:00'),
(4, 'IT-202200004', 'Komputer Mas Tri PDE lemot ', 1, 0, '2022-04-21', 7, 2175, '2022-04-19 15:00:00', 2175, '2022-04-19 14:20:00'),
(5, 'IT-202200005', 'Upgrade Windows 7 ke Windows 10 Pak Putut', 1, 0, '2022-04-21', 7, 2175, '2022-04-18 14:00:00', 2175, '2022-04-18 16:00:00'),
(6, 'IT-202200006', 'Membetulkan Jaringan Mas Arief HRD', 1, 0, '2022-04-21', 7, 2175, '2022-04-18 11:20:00', 2175, '2022-04-18 11:30:00'),
(7, 'IT-202200007', 'Membetulkan Jaringan PDE karena putus pembangunan ', 1, 0, '2022-04-21', 7, 2175, '2022-04-19 10:30:00', 2175, '2022-04-19 14:00:00'),
(8, 'IT-202200008', 'Update System IT', 1, 0, '2022-04-21', 8, 2175, '2022-04-18 08:00:00', 2175, NULL),
(9, 'IT-202200009', 'Membetulkan jaringan qc inspeksi karena kabel access point putus pembangunan', 1, 0, '2022-04-21', 7, 2175, '2022-04-20 09:00:00', 2175, '2022-04-20 11:00:00'),
(10, 'IT-202200010', 'Solidwork mas tri costing lemot', 1, 0, '2022-04-21', 7, 2175, '2022-04-20 10:30:00', 2175, '2022-04-20 10:40:00'),
(11, 'IT-202200011', 'Fix printer PDE semua karena server printer di pak john ganti domain', 1, 0, '2022-04-21', 7, 2175, '2022-04-18 13:00:00', 2175, '2022-04-18 16:00:00'),
(12, 'IT-202200012', 'Install Skectup Vicky n Adit', 1, 0, '2022-04-21', 7, 2175, '2022-04-19 10:00:00', 2175, '2022-04-19 11:00:00'),
(13, 'IT-202200013', 'Install Vray Vicky n Adit', 1, 0, '2022-04-21', 7, 2175, '2022-04-19 10:00:00', 2175, '2022-04-19 11:00:00'),
(14, 'IT-202200014', 'Install AntiVirus Symantec Andy', 1, 0, '2022-04-21', 7, 2175, '2022-04-20 09:00:00', 2175, '2022-04-20 09:00:00'),
(15, 'IT-202200015', 'Install Antivirus Symantec Ulfa, Melisa', 1, 0, '2022-04-21', 7, 2175, '2022-04-20 09:30:00', 2175, '2022-04-20 10:20:00'),
(16, 'IT-202200016', 'Mba Diah Laptop Lemot dan Panas', 1, 0, '2022-04-21', 7, 2175, '2022-04-20 11:26:07', 2175, '2022-04-20 11:32:44'),
(17, 'IT-202200017', 'Wifi inspeksi trouble', 1, 0, '2022-04-21', 7, 2175, '2022-04-20 13:04:20', 2175, '2022-04-20 13:08:49'),
(18, 'IT-202200018', 'Install antivirus baru mba pipit', 1, 0, '2022-04-21', 7, 2175, '2022-04-20 15:18:56', 2175, '2022-04-20 16:05:57'),
(19, 'IT-202200019', 'Printer accouting macet', 1, 0, '2022-04-21', 7, 2175, '2022-04-20 15:43:14', 2175, '2022-04-20 15:49:03'),
(20, 'IT-202200020', 'Bu Debbie ngga bisa ngeprint data dari aplikasi pph 4 ayat 2', 1, 0, '2022-04-22', 8, 2175, '2022-04-21 10:54:37', NULL, NULL),
(21, 'IT-202200021', 'Mas tri solid lemot lagi, konsultasi ke solidgrub ', 1, 0, '2022-04-22', 7, 2175, '2022-04-21 11:37:02', 2175, '2022-04-21 12:56:29'),
(22, 'IT-202200022', 'Mba Melisa ngga bisa nge print', 1, 0, '2022-04-22', 7, 2175, '2022-04-21 11:44:04', 2175, '2022-04-21 12:56:24');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `user_username` varchar(100) NOT NULL,
  `user_pass` varchar(255) NOT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_fullname` varchar(100) DEFAULT NULL,
  `user_birth` date DEFAULT NULL,
  `user_dept` int(11) DEFAULT NULL,
  `user_phone` varchar(20) DEFAULT NULL,
  `user_level` int(11) NOT NULL,
  `user_create` datetime NOT NULL,
  `user_picture` varchar(255) NOT NULL,
  `user_status` int(11) DEFAULT NULL,
  `user_asset_id` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `user_username`, `user_pass`, `user_email`, `user_fullname`, `user_birth`, `user_dept`, `user_phone`, `user_level`, `user_create`, `user_picture`, `user_status`, `user_asset_id`) VALUES
(27, 'dimas', '$2y$10$j6rP89WyHf46Z0QHUnmg9O0swDhydOyI3xDohctJVs/d1wdvRFc76', 'dosolichin@ptpacificfurniture.id', 'Dimas Okva', '2001-10-18', 1, '085 875 502 569', 1, '2021-06-21 22:26:31', '1624939095_78dd187a3f1cd44d8a61.jpg', 2, '2175'),
(28, 'araheksa', '$2y$10$JffpP1ICgALeIg27Q//w/OkZfhNmOfLOwGxG77lnLJaVvvswIViMG', 'araheksa@ptpacificfurniture.id', 'Abimanyu Raheksa', '1994-05-09', 7, '0882 2878 8682', 3, '2021-06-22 22:16:08', 'default.jpg', 6, '2382'),
(29, 'daardata', '$2y$10$JwkSl2dGABk0/9qoUtKLy..3I9S7mB1xd.NuWM1JiL2hPPXxFiNle', 'daardata@ptpacificfurniture.id', 'Dani Ari Ardata', '1978-06-03', 8, '088 2005 3070 60', 3, '2021-06-23 02:46:56', 'default.jpg', 4, '2407'),
(30, 'muktar', '$2y$10$m2zS4F6a2B7m7sB/hcIMQeCmF6HD7Vx6to/7R1lO9llGJxR9B7L4y', 'memuktar@ptpacificfurniture.id', 'Muhamad Edi Muktar', '1997-01-24', 5, '0895 4133 24500', 2, '2021-06-24 04:25:35', 'default.jpg', 6, '2347'),
(31, 'nita', '$2y$10$Bzgfzd1UcsdkwLBu9P.jGu3B6/amcG.Fo7HcyRoFs.ghj5IlAxoLW', 'npurwaningsih@ptpacificfurniture.id', 'Nita Purwaningsih', '1984-10-10', 12, '0838 3663 0034', 3, '2021-06-24 21:21:12', 'default.jpg', 2, '2110'),
(32, 'devi', '$2y$10$qQzZfNG9WVk3QyaNlVlple97X/IJSd4WFUqXtFBmRntViCrAw5k8S', 'dcputri@ptpacificfurniture.id', 'Devi Christiana Putri', '1995-01-01', 5, '0895 3650 53783', 2, '2021-06-24 21:27:46', 'default.jpg', 2, '2307'),
(33, 'latifwh', '$2y$10$MWpEL9Qrg6yl8U5p6U8EfuWPyaWaAsH0QX4tW9Wkxc2mvs9iO1uGa', 'muhammad.latif@ptpacificfurniture.id', 'Muhammad Latif', '1985-12-10', 12, '081 899 7879', 3, '2021-06-24 21:34:08', 'default.jpg', 2, '2371'),
(34, 'debbie', '$2y$10$/kmCx8RVabnLV1xyBbgFyeAVxK73ZYIlU8FRc39Zklx6AhqR94IwK', 'pdindriyasari@ptpacificfurniture.id', 'Putu Debbie Indriyasari', '1978-08-26', 2, '0812 2593 2817', 3, '2021-06-27 22:48:32', 'default.jpg', 1, '104'),
(35, 'ulfa', '$2y$10$RXk.YpJUkHVjh2f3ouP4s.ZMZLTt9t5/pgOMQ3Ykp.R3YVPku8.zq', 'ulfaizah@ptpacificfurniture.id / qcontrol2020@gmail.com', 'Ulfaizah', '1994-02-09', 8, '0823 2567 5568', 3, '2021-06-27 22:53:52', 'default.jpg', 2, '2091'),
(36, 'pipit', '$2y$10$dS3YXPQ.gn3a.YkLh53dUOayL35cQmm7mdDGZXon39simImj.P2v.', 'pindriyanti@ptpacificfurniture.id', 'Pipit Indriyanti', '1994-05-17', 7, '0898 9099 299', 3, '2021-06-27 22:57:58', 'default.jpg', 2, '1272'),
(37, 'ratih', '$2y$10$Y4K0.KizRIdR/9xKUnGNq.k21OisQ/FlTqdn13XD6Sn55goM6j852', 'rkurniasari@ptpacificfurniture.id', 'Ratih Kurniasari', '1991-07-22', 13, '0896 6820 0238', 3, '2021-06-27 23:01:43', 'default.jpg', 2, '1186'),
(38, 'adinasari', '$2y$10$WMFFqicK1Wv833wLJpweVeudF8hZokN4/OzvPL4iCv7QfmzkwquK.', 'adinasari@ptpacificfurniture.id', 'Ayu Dinasari', '1992-04-16', 3, '0858 2624 2584', 3, '2021-06-27 23:06:49', 'default.jpg', 1, '1173'),
(39, 'anam', '$2y$10$AGUOn4vYNJhkaHn1ntdbduns7vHWEG.1K3HyZ4TIvMNzr2LC3WFN6', 'sanam@ptpacificfurniture.id', 'M. Syariful Anam', '1992-09-07', 1, '0878 9811 8877', 1, '2021-06-27 23:11:17', 'default.jpg', 1, '1164'),
(40, 'maryono', '$2y$10$YNegOF7gf90Tpiu77OxTpeF6KN7tUMHD4QZzwZc/uPKe8RMGxV6Ku', 'msumaryono@ptpacificfurniture.id', 'M. Sumaryono', '1981-08-23', 6, '0857 4068 3134', 3, '2021-06-27 23:17:24', 'default.jpg', 1, '994'),
(41, 'wagiyanto', '$2y$10$/plF9UTD6pIvBYEC7OxjveLk1qvfLmlH0PKc8p82KrRLqj1zQklyi', 'antok@ptpacificfurniture.id', 'Wagiyanto', '1979-01-29', 10, '-', 3, '2021-06-27 23:21:10', 'default.jpg', 1, '962'),
(42, 'putut', '$2y$10$X7DoZodRcjlH.TWAjsrbvOP78xQC6ZXuU9VR2.9OogY0IVNAU.Ygi', 'pprabowo@ptpacificfurniture.id', 'Putut Prabowo', '1980-03-27', 10, '0815 6595 835', 3, '2021-06-27 23:26:52', 'default.jpg', 1, '952'),
(43, 'nuryadi', '$2y$10$sNRnvxUR9Vt1n6eIg0KdjORWVu5S100dR7/zf2fRoztRoeoBU4dru', 'nuryadi@ptpacificfurniture.id', 'Nuryadi', '1980-05-16', 9, '0852 2608 3096', 3, '2021-06-27 23:33:35', '1650356210_805ead238bb20eada7cc.jpg', 2, '273'),
(44, 'diah', '$2y$10$AItIjMCo3IEM/iscKkZsuuwcLpwz6/O86H9kImCHnAcEeD32gjey.', 'dkrisnawati@ptpacificfurniture.id', 'Diah Krisnawati', '1992-06-24', 4, '0821 3471 8146', 3, '2021-06-27 23:37:25', 'default.jpg', 1, '566'),
(45, 'latif', '$2y$10$x1aPfNKccLY31uwfJrpmFOL8b.jnGqwS035U5J9f9yJLyrotjl/0q', 'mlatif@ptpacificfurniture.id', 'Mochamad Latif', '1971-11-22', 7, '0817 4505 31', 3, '2021-06-27 23:40:27', 'default.jpg', 1, '533'),
(46, 'dwi', '$2y$10$5x3cX7aKF8lC0AdqZ2iNtumo3d7k8sBWaZoiBL..oTTW5AMAUjYre', 'dwi@ptpacificfurniture.id', 'Dwi Kurnianto', '1976-09-11', 9, '0813 2511 3869', 3, '2021-06-27 23:43:56', 'default.jpg', 1, '440'),
(47, 'vanny', '$2y$10$B0a0Jc6Ma0eRegVCT1NShuGpWLhpzzF7YLW6XPyOz.xTs10/VMW72', 'ivaviany@ptpacificfurniture.id', 'Ivanny Vina Aviany', '1994-01-23', 6, '0877 0010 8808', 3, '2021-06-27 23:47:15', 'default.jpg', 1, '319'),
(48, 'turwansari', '$2y$10$E3kkfuY49nUPxbzGNqLULuAYw8y4uhMuMplaHbSDIwxi8XP2m.ufi', 'turwansari@ptpacificfurniture.id', 'Turwansari', '1981-12-12', 10, '0838 3815 2739', 3, '2021-06-27 23:52:36', 'default.jpg', 1, '198'),
(49, 'nawang', '$2y$10$eEcwmy1KaK2MzIdNEogFIeqFRE8MminXIku56G2O1Yu9vtjCfGF0i', 'nraharto@ptpacificfurniture.id', 'Nawang Raharto', '1977-12-11', 10, '0857 1408 3301', 3, '2021-06-27 23:55:38', 'default.jpg', 1, '123'),
(50, 'sutanta', '$2y$10$jH9EhZR/gb2auMh3GqU8zeWj4eyjXI067Mmu4iKPaqs33nWHMMLya', 'sutanta@ptpacificfurniture.id', 'Sutanta', '1978-12-25', 13, '0819 1467 0274', 3, '2021-06-28 00:54:48', 'default.jpg', 1, '115'),
(51, 'tarwanto', '$2y$10$AsiblCtuy/fVBfS98fwPC.ltP/U8OceblPfYfmNLkFjPeC/Z9oc/K', 'tarwanto@ptpacificfurniture.id', 'Tarwanto', '1980-09-10', 7, '0858 0364 6376', 3, '2021-06-28 01:00:12', 'default.jpg', 1, '111'),
(52, 'khoiri', '$2y$10$gxlBDd1NaFSaOmFX.5UVPu87xT3mHP7JsO6hmFmcl7jnyoLsK3uIq', 'khoiri@ptpacificfurniture.id', 'Khoiri', '1978-06-22', 2, '0856 4155 2080', 3, '2021-06-28 01:03:59', 'default.jpg', 4, '82'),
(53, 'yudo', '$2y$10$0k.nLbrEQ6Y0okKWN6EkjO7c78jeZNif35kMELnS1qnjFTNfHdcAO', 'ydarwanto@ptpacificfurniture.id', 'Yudo Darwanto', '1987-03-18', 6, '0897 9828 031', 3, '2021-06-28 01:08:44', 'default.jpg', 2, '2145'),
(54, 'melisa', '$2y$10$v0vpLAJM7RgAje8GbNu5zeaF43fqeYlZ2eXVDBwpCUBLSi5M.lxeq', 'melisa@ptpacificfurniture.id', 'Melisa', '1996-05-25', 7, '0838 4255 5220', 3, '2021-06-28 01:12:10', 'default.jpg', 2, '2310'),
(55, 'syahrul', '$2y$10$KQrhONBTXlonVWcyRVQXN.Obic2fEHm7niXoJORfhJ8/CNb5FvH1e', 'sfaudjan@ptpacificfurniture.id', 'Syahrul Faudjan', '1987-09-18', 7, '0822 2897 5769', 3, '2021-06-28 04:07:43', 'default.jpg', 2, '2023'),
(59, 'ifa', '$2y$10$0wf3RQCSF3ke7hbomxxuWuq22EsTqA40kKXMgrpDSAuXsI6TRguny', 'mudhalifah@ptpacificfurniture.id', 'Mudhalifah', '1992-06-12', 5, '0813 9095 9082', 2, '2021-07-21 04:47:09', 'default.jpg', 1, '1211'),
(60, 'anna', '$2y$10$9s8t4QKau6V3Cw6SbCBVXeLUym5lCTpt/bZvIGnTgVsMn.cC8TxOi', 'alistiani@ptpacificfurniture.id', 'Anna Listiani', '1986-08-05', 5, '0813 2532 5286', 2, '2021-07-21 08:07:41', 'default.jpg', 1, '84'),
(61, 'kholidin', '$2y$10$csHnfYhYo6UeAvHgJLuBBeryl7gemCZw404.UaErRBISDyFrKQACO', 'akholidin@ptpacificfurniture.id', 'Ahmad Kholidin', '1994-05-15', 6, '0857 2662 9898', 3, '2021-07-21 08:14:49', 'default.jpg', 2, '2245'),
(62, 'luqman', '$2y$10$wdNFBEqD1UnIpNs3TIu6UeVjZ/voRYe4hyntNE8d/sf0VrUwVMzj2', 'lshodiqin@ptpacificfurniture.id', 'Luqman Shodiqin', '1995-07-20', 10, '0812 2888 6409', 3, '2021-07-21 08:28:06', 'default.jpg', 1, '2275'),
(63, 'sulistyawan', '$2y$10$r03SzPN08lJuIP6UC3Dz7uWPwiK8lHIVbmZCjkmrlL7B75g9gTCDu', 'sulistyawan@ptpacificfurniture.id', 'Sulistyawan', '0000-00-00', 10, '0812 2940 5345', 3, '2021-07-21 08:40:01', 'default.jpg', 4, '1903'),
(64, 'aga', '$2y$10$ecao1JjDZonnLjgHDw0kaehoFh.kyBKD3SImS8hKB5E8Eqg8A3//u', 'pakristanto@ptpacificfurniture.id', 'Pionius Aga Kristanto', '1997-03-11', 10, '0822 2709 5595', 3, '2021-07-21 08:45:36', 'default.jpg', 1, '1307'),
(66, 'niam', '$2y$10$6KlkibkopD/4phAarYCPn.jGCNBm8enOeHRkI8X9hbkz0VtbRLOhO', 'fatkhunniam@ptpacificfurniture.id', 'Fatkhunniam', '1996-05-19', 12, '-', 3, '2021-07-21 08:56:34', 'default.jpg', 2, '1421'),
(67, 'dinni', '$2y$10$cE1v39AHixAFdNQsmmAuge9/A5p1SHfB3pBI/V9eYgPU2YOqAokRK', 'dlestari@ptpacificfurniture.id', 'Dinni Lestari ', '1996-11-06', 2, '0813 2865 3687', 3, '2021-07-21 09:28:06', 'default.jpg', 2, '2090'),
(68, 'nurul', '$2y$10$zu1.apzLpmV5H1mjozX03OWrOF/DsydnUj3nwY.RjC9aKozWm1gOO', 'nurul@ptpacificfurniture.id', 'Nurul Maghrifah', '1982-09-07', 2, '0813 5860 2185', 3, '2021-07-21 09:52:54', 'default.jpg', 1, '1810'),
(69, 'nhidayah', '$2y$10$m.Y9ajqPvnALTdlAVHSae.Z0Im/O6pQYu01yL9PwPQufH7iA.KdRC', 'nhidayah@ptpacificfurniture.id', 'Nurul Hidayah', '1995-04-02', 4, '0877 0041 1447', 3, '2021-07-21 09:56:55', 'default.jpg', 2, '1959'),
(70, 'vicky', '$2y$10$S5cgc3w8Bi7L4ANeVfrOoe3HK3pFu9mhRQI5AJGjdkvIYNSbM0cPu', 'vspratama@ptpacificfurniture.id', 'Vicky Sukma Pratama', '2000-02-16', 9, '0896 6203 4496', 3, '2021-08-29 21:30:02', 'default.jpg', 2, '2423'),
(71, 'irfan', '$2y$10$J5UP4VZ5P6ff6vPbfQ7F4ehpp9usx76PHLLqoS8xh4JDk4v9eydKW', 'isoryansyah@ptpacificfurniture.id', 'Irfan Soryansyah', '1997-04-04', 10, '0838 3858 5821', 3, '2021-08-29 21:39:22', 'default.jpg', 2, '2411'),
(72, 'sulis', '$2y$10$mWQeC4mfpnpf47OW5hCnpeKOIjKZEeEQ5DONcrUUKS6JevcoFPi8y', 'saminingsih@ptpacificfurniture.id', 'Sulistianah Aminingsih', '1986-12-31', 4, '0813 2630 7088', 3, '2021-09-01 21:22:25', 'default.jpg', 4, '2417'),
(73, 'aditya', '$2y$10$V3rvruo7uxoh7Pn27LFWaOc/lY1lwNb/icRqlSfqrZ22Yb55ZgG6.', 'anugroho@ptpacificfurniture.id', 'Aditya Nugroho', '1993-07-19', 6, '0888 0646 0591', 3, '2021-10-06 21:11:56', 'default.jpg', 2, '2508'),
(74, 'rochmani', '$2y$10$tiXhpZGo1mbJdtmUmEhqZe1PGycpOWT/Lea826eTtVC4kecmFKRPy', 'trochmani@ptpacificfurniture.id', 'Tri Rochmani', '0000-00-00', 8, '-', 3, '2022-02-14 19:02:22', 'default.jpg', 1, '900'),
(75, 'fingerspot', '$2y$10$EANrBnfLNBlTVmsxgHaCleFxwM/EZEf0r6EtGFDMrylp95rpyRG26', NULL, 'PC Server Fingerspot', NULL, 1, NULL, 1, '2022-03-14 23:53:24', 'default.jpg', 1, '9999'),
(76, 'gadis', '$2y$10$aaxDHv3plFM46q34iWjc0OwJiaS8zRilR1pXLRv2IHLIToCkH9DBi', 'gkwidyarti@ptpacificfurniture.id', 'Gadis Karina Widyarti', '1995-04-27', 2, '0812 2607 8209', 3, '2022-03-15 20:42:30', 'default.jpg', 2, '2648'),
(77, 'firza', '$2y$10$t4AUloczlZdc1492GE3sruw8D3x5qZFsDlbnAXqozieI1wGRKQHMW', 'frapriliani@ptpacificfurniture.id', 'Firza Rizki Apriliani', '1999-04-27', 4, '0896 8558 6063', 3, '2022-03-15 21:12:22', 'default.jpg', 2, '2532'),
(78, 'arief', '$2y$10$.D2DW3XG/yJP5ejr6g75tOZ4eBZDYkz2CeW4DmcSn7lqjQp/QgNZu', 'hrd@ptpacificfurniture.id', 'Arief Prastya Wicaksono', '1990-04-06', 5, '0857 4231 1807', 2, '2022-03-15 21:25:55', 'default.jpg', 2, '2607'),
(79, 'andy', '$2y$10$fOB1GgGZKlEZItBngIXwM.3Sh2RMzcad8C7V.j745/imvQ0e7ltri', 'asprayogo@ptpacificfurniture.id', 'Andy Setiyanto Prayogo', '2000-12-03', 7, '0896 8121 1077', 3, '2022-03-15 21:58:57', 'default.jpg', 2, '2351'),
(80, 'tri', '$2y$10$B08xZVa3vHctoBGZZuqGoO3iG0Q31TgSIRFTIEjSh6q/hmXzIltm6', 'tsupriatna@ptpacificfurniture.id', 'Tri Supriatna', '1991-07-24', 9, '0857 2696 6839', 3, '2022-03-15 22:31:29', 'default.jpg', 2, '2618'),
(81, 'aditia', '$2y$10$3lCREcXjpg1559OvtklaHuWf2WJYIaiS2As9I2jDCfXxUgTLmE9me', 'fakristiawan@ptpacificfurniture.id', 'Fransiskus Aditia Kristiawan', '1996-10-12', 9, '0812 5985 0695', 3, '2022-03-15 22:39:25', 'default.jpg', 2, '2622'),
(82, 'olive', '$2y$10$neVObhUNvL8vxMtqZOOqF.ssINfStKeseDFdXNIKceUY0H.6VY1ly', 'nurkholifah@ptpacificfurniture.id', 'Nurcholifah', '0000-00-00', 11, '-', 3, '2022-03-15 23:32:53', 'default.jpg', 1, '1255'),
(83, 'bsetyawan', '$2y$10$nDknQw1xVAbNEAlXLAKibeAZdd7nokeYb.NAG4bdoVplVhjyxCT8i', NULL, 'Budi Setyawan', NULL, 16, NULL, 3, '2022-04-21 02:00:06', 'default.jpg', 1, '78'),
(84, 'cromandy', '$2y$10$58hXyraH7Ulkx.1MjJzzS.7zAh3cp9Zain9Bmh6e7dkh85wwuY/E2', NULL, 'Culis Romandy', NULL, 16, NULL, 3, '2022-04-21 02:00:38', 'default.jpg', 1, '129'),
(85, 'effendi', '$2y$10$Ega9bhWqTTVhxSA9AY7WH.eibQ7NwSI6M/EIepiZODRCMAw/5tGXG', NULL, 'Efendi Darmawan', NULL, 16, NULL, 3, '2022-04-21 02:01:05', 'default.jpg', 2, '2568'),
(86, 'eko', '$2y$10$5/f5CgxNeR7FRqkGb0Gxguk15m8m3Kbft7T.mp75OprC/wGeaL8GS', NULL, 'Eko Wahyudi', NULL, 16, NULL, 3, '2022-04-21 02:01:29', 'default.jpg', 1, '191'),
(87, 'hartono', '$2y$10$iLPv6JEYzPRSB0IkygfUDOHlmc28RuyJPY2di34f/4Br8WUaGlY4i', NULL, 'Hartono', NULL, 16, NULL, 3, '2022-04-21 02:02:08', 'default.jpg', 1, '922'),
(88, 'nazachah', '$2y$10$qSZGEmemLEojFIHMkhokKuxqeQ359oxL8XIvjjIuWRkiDJocXryk6', NULL, 'Moh Nur Azachah', NULL, 16, NULL, 3, '2022-04-21 02:02:34', 'default.jpg', 2, '792'),
(89, 'nur', '$2y$10$rHTTFv5aLcj2XqowxaDnxey6zk2lQ.dr4whMCM0VRIt8m7GE2LShm', NULL, 'Nur Solikhin', NULL, 16, NULL, 3, '2022-04-21 02:02:56', 'default.jpg', 1, '1092'),
(90, 'ponco', '$2y$10$w84HIFrBbN8ZUrkDadpGDuN9mu46TwkfWNahLYnrUuEpijOs6CA96', NULL, 'Ponco Ari Prasetya', NULL, 16, NULL, 3, '2022-04-21 02:03:21', 'default.jpg', 1, '80'),
(91, 'siti', '$2y$10$BHKIqHYvWV/z.XJ/tpIz6eQMnvCHb.q8mkqhzgTS73Y083rhTmi4K', NULL, 'Siti Salamah', NULL, 16, NULL, 3, '2022-04-21 02:03:44', 'default.jpg', 2, '2424'),
(92, 'sujono', '$2y$10$T4llSt7L8m99A7zRBJ946unEI.R3FhWiBnwSCUN5NYgnFGH.IrsH6', NULL, 'Sujono', NULL, 8, NULL, 3, '2022-04-21 02:04:10', 'default.jpg', 2, '1749'),
(93, 'surono', '$2y$10$GL/cHEmZg8hahocXvMfi4OnYtQWkwlnx75o7VbaYwggavz1RJgX1S', NULL, 'Surono', NULL, 16, NULL, 3, '2022-04-21 02:04:32', 'default.jpg', 1, '1570'),
(94, 'taslik', '$2y$10$6CMhHfLHJ8yvI1Ckw8NmTO2BKqYGUzGp5vYks9HQsPVk7sEAxLqdi', NULL, 'Muhammad Taslik', NULL, 8, NULL, 3, '2022-04-21 02:04:56', 'default.jpg', 2, '2467');

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `uam_id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`uam_id`, `level`, `menu_id`) VALUES
(1, 2, 1),
(2, 2, 2),
(3, 2, 3),
(4, 2, 4),
(5, 2, 5),
(6, 2, 6),
(7, 2, 18),
(8, 3, 1),
(9, 3, 2),
(10, 3, 3),
(11, 3, 4),
(12, 3, 5),
(13, 3, 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `access`
--
ALTER TABLE `access`
  ADD PRIMARY KEY (`access_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `asset`
--
ALTER TABLE `asset`
  ADD PRIMARY KEY (`asset_id`);

--
-- Indexes for table `backup`
--
ALTER TABLE `backup`
  ADD PRIMARY KEY (`backup_id`);

--
-- Indexes for table `daily`
--
ALTER TABLE `daily`
  ADD PRIMARY KEY (`daily_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
  ADD PRIMARY KEY (`email_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`emp_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `google`
--
ALTER TABLE `google`
  ADD PRIMARY KEY (`google_id`);

--
-- Indexes for table `internet`
--
ALTER TABLE `internet`
  ADD PRIMARY KEY (`inet_id`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `monitoring_speed`
--
ALTER TABLE `monitoring_speed`
  ADD PRIMARY KEY (`monitoring_id`);

--
-- Indexes for table `m_access_deskera`
--
ALTER TABLE `m_access_deskera`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_access_use`
--
ALTER TABLE `m_access_use`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_asset_type`
--
ALTER TABLE `m_asset_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_condition`
--
ALTER TABLE `m_condition`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_dept`
--
ALTER TABLE `m_dept`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_email_use`
--
ALTER TABLE `m_email_use`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_gender`
--
ALTER TABLE `m_gender`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_operating_system`
--
ALTER TABLE `m_operating_system`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_position`
--
ALTER TABLE `m_position`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_preventive_status`
--
ALTER TABLE `m_preventive_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_project_status`
--
ALTER TABLE `m_project_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_status`
--
ALTER TABLE `m_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `m_user_level`
--
ALTER TABLE `m_user_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `preventive`
--
ALTER TABLE `preventive`
  ADD PRIMARY KEY (`preventive_id`);

--
-- Indexes for table `project`
--
ALTER TABLE `project`
  ADD PRIMARY KEY (`project_id`);

--
-- Indexes for table `report`
--
ALTER TABLE `report`
  ADD PRIMARY KEY (`report_id`);

--
-- Indexes for table `reset_password`
--
ALTER TABLE `reset_password`
  ADD PRIMARY KEY (`reset_id`);

--
-- Indexes for table `support`
--
ALTER TABLE `support`
  ADD PRIMARY KEY (`support_id`);

--
-- Indexes for table `ticket`
--
ALTER TABLE `ticket`
  ADD PRIMARY KEY (`ticket_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`uam_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `access`
--
ALTER TABLE `access`
  MODIFY `access_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=58;

--
-- AUTO_INCREMENT for table `asset`
--
ALTER TABLE `asset`
  MODIFY `asset_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `backup`
--
ALTER TABLE `backup`
  MODIFY `backup_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `daily`
--
ALTER TABLE `daily`
  MODIFY `daily_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
  MODIFY `email_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `emp_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `internet`
--
ALTER TABLE `internet`
  MODIFY `inet_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;

--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `monitoring_speed`
--
ALTER TABLE `monitoring_speed`
  MODIFY `monitoring_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `m_access_deskera`
--
ALTER TABLE `m_access_deskera`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_access_use`
--
ALTER TABLE `m_access_use`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `m_asset_type`
--
ALTER TABLE `m_asset_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `m_condition`
--
ALTER TABLE `m_condition`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_dept`
--
ALTER TABLE `m_dept`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=103;

--
-- AUTO_INCREMENT for table `m_email_use`
--
ALTER TABLE `m_email_use`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `m_gender`
--
ALTER TABLE `m_gender`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_operating_system`
--
ALTER TABLE `m_operating_system`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `m_position`
--
ALTER TABLE `m_position`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `m_preventive_status`
--
ALTER TABLE `m_preventive_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `m_project_status`
--
ALTER TABLE `m_project_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `m_status`
--
ALTER TABLE `m_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `m_user_level`
--
ALTER TABLE `m_user_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `post`
--
ALTER TABLE `post`
  MODIFY `post_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `preventive`
--
ALTER TABLE `preventive`
  MODIFY `preventive_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `project`
--
ALTER TABLE `project`
  MODIFY `project_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `report`
--
ALTER TABLE `report`
  MODIFY `report_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `reset_password`
--
ALTER TABLE `reset_password`
  MODIFY `reset_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `support`
--
ALTER TABLE `support`
  MODIFY `support_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `ticket`
--
ALTER TABLE `ticket`
  MODIFY `ticket_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=95;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `uam_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `daily`
--
ALTER TABLE `daily`
  ADD CONSTRAINT `daily_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
